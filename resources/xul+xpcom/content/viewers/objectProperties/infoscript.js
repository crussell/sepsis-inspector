/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Supplementary script for the objectProperties viewer.
 *
 * This file contains function definitions that are used by the Inspector
 * framework to determine whether the viewer is capable of inspecting a given
 * object and how to refer to the viewer by name in the Inspector UI.
 *
 * The viewer registry has some support for "statically" declaring the types
 * of objects the corresponding viewer will accept, but our filter criteria
 * ("I'll inspect anything") cannot be expressed using the registry's
 * declarative primitives.
 *
 * @see viewers/objectProperties/info.json
 * @see registration/theViewerRegistry
 *      For more about info scripts and how they're used.  Look for
 *      occurrences of "scriptURI".
 */

//////////////////////////////////////////////////////////////////////////////

/**
 * @see registration/viewerInfo
 * @see registration/magicFilter.jsm
 */
exports.filter = function filter()
{
  // The objectDetails viewer supports inspecting all possible objects (minus
  // synthetics).
  return true;
}

/**
 * @see registration/viewerInfo
 * @see registration/magicLocalizer.jsm
 */
exports.getLocalizedName = function getLocalizedName(aLocale)
{
  // XXX Hardcoded to the en-US localization (i.e., the only localization) for
  // now.
  return "Object Properties and Methods";
}
