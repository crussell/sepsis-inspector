/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Viewer to list the properties of any object (whether JS or native).
 *
 * TODO
 * - Show more than just the prototype chain in the inheritance visualization.
 *   Do interfaces, too. I guess the way this should work is,  if you toggle
 *   an interface's properties' visibility, all property names that match
 *   names from the interface should be hidden (unless they match on some
 *   other non-hidden interface?).
 *
 * - Special case proto-hiding so that if everything is toggled off and the
 *   root gets autocollapsed, toggling anything back on will automatically
 *   toggle the root back open.
 *
 * @see registration/theViewerRegistry
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

const kXULNS =
  "http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul";

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { FilteredPropertiesView } = require("./filteredPropertiesView");

var { ObjectDetailsSupplier } = require("synthetic/objectDetailsSupplier");

var { TheFakeClipboardHack } = require("hacks/theFakeClipboardHack");
var { FixedFontTooltipHack } = require("hacks/fixedFontTooltipHack");

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aPanel
 *        The inspectorpanel XUL element that the viewer is being loaded into.
 */
function ObjectPropertiesViewer(aPanel)
{
  this.mPanel = aPanel;

  // So we can use `updateCommand`.
  aPanel.commandMediator.decorate(this);

  // The inspector framework will set this up, but we're inside the viewer
  // constructor right now; the framework is waiting for us to return the
  // instance it will use.  However, `ObjectDetailsSupplier` needs this to be
  // initialized by the time we call its `create` method.
  aPanel.viewer = this;

  this.supplier = ObjectDetailsSupplier.create(aPanel);

  let $ = document.getElementById.bind(document);
  this.stringBundle = $("objectPropertiesStringBundle");
  this.mProtoBox = $("bxProtos");
  this.mTree = $("olObjectProperties");
  this.mTreeBody = $("olbObjectProperties");

  this.mFixedFontTooltipHack =
    new FixedFontTooltipHack(document.documentElement, this.mTreeBody);

  this.mTree.addEventListener("select", this, false);
  this.mTree.controllers.appendController(this);
}

{
  let proto = ObjectPropertiesViewer.prototype;
  proto.mFixedFontTooltipHack = null;
  proto.mPanel = null;
  proto.mPropertiesView = null;
  proto.mProtoBox = null;
  proto.mTree = null;

  proto.stringBundle = null;
  proto.subject = null;
  proto.supplier = null;
  proto.target = null;

  proto.onPrototypeToggle = function OPV_OnPrototypeToggle(aEvent)
  {
    this.mPropertiesView.togglePropertyVisibility(aEvent.target.heritageItem);
  };

  proto.updateContextMenuItems = function OPV_UpdateContextMenuItems()
  {
    this.updateCommand("cmd_copyQualifiedName");
    this.updateCommand("cmd_copyValue");
  };

  ////////////////////////////////////////////////////////////////////////////
  //// viewer interface

  /**
   * Called by the inspector framework when this panel's inspected object
   * changes.  (E.g., if a DOM node is selected in the parent panel, and the
   * user selects a different one.)
   *
   * @param aSubject
   *        The new object to inspect.  ("Object" here is used in a loose
   *        sense--it may very well be a primitive.)
   */
  proto.inspectObject = function OPV_InspectObject(aSubject)
  {
    // We want the new target's row to occupy the same position on-screen
    // that the old target's row did, if the old subject and the new subject
    // have similar structure and if the old target was visible.
    let scrollOffset = this._findScrollOffset(this.mPropertiesView);
    let targetNameChain = this._buildNameChain(this.getTargetSynthetic());

    this.subject = aSubject;
    this._updateProtoBox(this.subject);

    this.mPropertiesView =
      new FilteredPropertiesView(this.subject, this.supplier);
    this.mPropertiesView.stringBundle = this.stringBundle;

    if (this.mPropertiesView.isContainer(0) &&
        !this.mPropertiesView.isContainerEmpty(0)) {
      this.mPropertiesView.toggleOpenState(0);
    }

    this.mTree.view = this.mPropertiesView;
    this._targetSpec(this.mPropertiesView, targetNameChain, scrollOffset);

    return this.subject;
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface required by ObjectDetailsSupplier

  /**
   * @param aSID [optional]
   *        The SID of the type of synthetic requested.  The only supported
   *        synthetics are `ObjectDetails` sythetics, so it must match the
   *        `ObjectDetails` SID, if specified.  If not specified, defaults to
   *        the `ObjectDetails` SID, anyway.
   * @return
   *        A synthetic representing the object corresponding to
   *        the viewer's current target, or null.
   * @see this.mPanel.changeTarget
   */
  proto.getTargetSynthetic = function OPV_GetTargetSynthetic(aSID)
  {
    if (aSID && aSID != this.supplier.ObjectDetails.sid) return null;
    if (!this.mPropertiesView) return null;

    let currentIndex = this.mPropertiesView.selection.currentIndex;
    return (currentIndex >= 0) ?
      this.mPropertiesView.getSynthetic(currentIndex) :
      null;
  };
  
  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIDOMEventListener

  proto.handleEvent = function OPV_HandleEvent(aEvent)
  {
    switch (aEvent.type) {
      case "select":
        this._onSelect();
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIController

  proto.supportsCommand = function OPV_SupportsCommand(aCommandName)
  {
    switch (aCommandName) {
      case "cmd_copy":
      case "cmd_copyQualifiedName":
      case "cmd_copyValue":
        return true;
      break;
    }

    return false;
  };

  proto.isCommandEnabled = function OPV_IsCommandEnabled(aCommandName)
  {
    switch (aCommandName) {
      case "cmd_copy":
      case "cmd_copyQualifiedName":
        return this.mPropertiesView &&
               this.mPropertiesView.selection.currentIndex > 0;
      break;
      case "cmd_copyValue":
        return this.mPropertiesView &&
               this._getValueString(this.getTargetSynthetic()) !== null;
      break;
    }

    return false;
  };

  proto.doCommand = function OPV_DoCommand(aCommandName)
  {
    switch (aCommandName) {
      case "cmd_copy":
        if (this.isCommandEnabled("cmd_copyValue")) {
          this.doCommand("cmd_copyValue");
        }
        else {
          this.doCommand("cmd_copyQualifiedName");
        }
      break;
      case "cmd_copyQualifiedName":
        this._copyPropertyName();
      break;
      case "cmd_copyValue":
        this._copyPropertyValue();
      break;
      default:
        throw new Error("wat.  Command: " + aCommandName);
      break;
    }
  };

  proto.onEvent = function OPV_OnEvent(aCommandName)
  {
    // no-op
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods
  
  proto._onSelect = function OPV_OnSelect()
  {
    let idx = this.mPropertiesView.selection.currentIndex;
    if (idx >= 0) {
      this.mPanel.changeTarget(this.mPropertiesView.getRowObject(idx));
    }

    this.updateCommand("cmd_copy");
    this.updateCommand("cmd_copyQualifiedName");
    this.updateCommand("cmd_copyValue");
  };

  /**
   * Get the qualified name of the given synthetic, relative to the viewer
   * subject, in the form of an array of strings in reverse order.
   *
   * For example, if the view looks like this:
   *
   *   (subject)
   *   +-foo
   *     +-bar
   *       +-baz   <-- viewer target
   *
   * ... and we want the name chain for the viewer target, then we'll get a
   * result that looks like this:
   *
   *   ["baz", "bar", "foo"]
   *
   * @param aSynthetic
   *        The `ObjectDetails` synthetic for the current target.
   * @return
   *        An array of strings effectively giving the qualified name of the
   *        given synthetic, in reverse order, or null if `aSynthetic` is null
   *        or undefined.
   */
  proto._buildNameChain = function OPV_BuildNameChain(aSynthetic)
  {
    if (!aSynthetic) {
      return null;
    }

    let result = [];
    let details = aSynthetic;
    while (details && details.name) {
      // assert((details.name == null) == !details.baseDetails);
      result.push(details.name);

      // `details.name` should be null if `details` is the details
      // synthetic for the viewer subject.
      details = details.baseDetails;
    }

    return result;
  };

  /**
   * @param aView
   *        A `BasicPropertiesView` instance.
   * @return
   *        The offset of the view's current index, such that it refers to the
   *        current index's position relative to only the other rows that are
   *        currently visible.  This will be null if the view's current index
   *        is not among the visible rows, or if `aView` itself is null.
   */
  proto._findScrollOffset = function OPV_FindScrollOffset(aView)
  {
    let offset = null;
    if (aView) {
      let currentIndex = aView.selection.currentIndex;
      offset = currentIndex - aView.treeBoxObject.getFirstVisibleRow();
      if (aView.treeBoxObject.getLastVisibleRow() < currentIndex ||
          offset < 0) {
        offset = 0;
      }
    }

    return offset;
  };

  /**
   * Show in the given view the property matching the given name spec.  The
   * view will be scrolled such that the row appears at the given offest.  If
   * no property matching the spec can be found, the first row will be
   * selected.
   *
   * @param aPropertiesView
   *        A `BasicPropertiesView` instance where the target can be found.
   *        Must already be associated with some tree box object.
   * @param aNameChain
   *        An array of strings, giving the name spec of the object that
   *        should be used as the target.
   * @param aScrollOffset
   *        An index describing the vertical position within the visible rows
   *        that the object matching the `aNameChain` spec should appear.
   * @see _buildNameChain
   * @see _findScrollOffset
   */
  proto._targetSpec =
    function OPV_TargetSpec(aPropertiesView, aNameChain, aScrollOffset)
  {
    let useOffset = aScrollOffset;
    let selectRow = this._targetSearch(aPropertiesView, aNameChain);
    if (selectRow < 0) {
      useOffset = null;
      selectRow = 0;
    }

    // NB: We don't check `!useOffset`, because 0 is falsy.
    if (useOffset != null) {
      aPropertiesView.treeBoxObject.scrollToRow(selectRow - useOffset);
    }

    aPropertiesView.selection.select(selectRow);
  };

  /**
   * Expands the rows of the given view, looking for one matching the spec
   * described by the given name chain.  If not found, the rows that were
   * expanded will be toggled back to the closed state.
   *
   * NB: This is not a general search algorithm.  It presupposes that all rows
   * except for the first are completely collapsed.  This is not guaranteed to
   * give the right answer if called when the view already has other rows
   * toggled open.
   *
   * @param aPropertiesView
   *        The `BasicPropertiesView` instance that `aNameChain` applies to.
   * @param aNameChain
   *        An array of strings naming the properties whose rows should be
   *        toggled open.  NB: These should be "in reverse".
   * @return
   *        The index of the row that we resolved, or -1 if not found.
   * @see _buildNameChain
   * @see inspectObject
   */
  proto._targetSearch = function OPV_TargetSearch(aPropertiesView, aNameChain)
  {
    if (!aNameChain) {
      return -1;
    }

    // We'll try to work backwards through `aNameChain`, expanding the rows
    // in the new view that match the names from `aNameChain`.
    //
    // We'll want to keep track of any rows we open, in case we aren't able to
    // find everything in `aNameChain` so that we can close them back up.
    let toUnwind = [];
    let foundRow = 0;
    resolvingNameChain:
    while (aNameChain.length) {
      // NB: It's possible the `toggleOpenState` call in the last iteration
      // failed to do anything (because `foundRow` had no children).  If
      // that's the case, we'll want to treat it the same as if we weren't
      // able to find the row and never attempted to open it.
      if (foundRow < 0 || !aPropertiesView.isContainerOpen(foundRow)) {
        while (toUnwind.length) {
          let closeRow = toUnwind.pop();
          if (aPropertiesView.isContainerOpen(closeRow)) {
            aPropertiesView.toggleOpenState(closeRow);
          }
        }

        break;
      }

      // Look for this in the current child group (more on that below):
      let waldo = aNameChain.pop();

      // Make sure we only look through `foundRow`'s children; i.e., between
      // `foundRow` and its nearest sibling that follows (exclusive).
      let nextSiblingIndex =
        aPropertiesView.getNextSiblingIndex(foundRow, foundRow, true);
      for (let i = foundRow + 1; i < nextSiblingIndex; ++i) {
        if (aPropertiesView.getSynthetic(i).name == waldo) {
          // Got it.  Are we opening this row to look for more, or was that
          // all and we were able to fully resolve `aNameChain`?
          if (aNameChain.length) {
            aPropertiesView.toggleOpenState(i);
            toUnwind.push(i);
          }
          else {
            // That was all of it.
            return i;
          }

          continue resolvingNameChain;
        }
      }

      // We couldn't find it.  Prepare to unwind.
      foundRow = -1;
    }

    return -1;
  };

  /**
   * @param aHeritageItem
   *        A mirror for some object that exists in the subject's prototype
   *        chain, or a mirror around the viewer subject itself.
   */
  proto._updateProtoBox = function OPV_UpdateProtoBox(aHeritageItem)
  {
    {
      let node = this.mProtoBox.firstChild;
      while (node) {
        this.mProtoBox.removeChild(node);
        node = this.mProtoBox.firstChild;
      }
    }

    let currentItem = aHeritageItem;
    while (currentItem) {
      this._addProtoButton(currentItem);
      currentItem = currentItem.prototype;
    }
  };

  /**
   * @param aHeritageItem
   *        A mirror for some object that exists in the subject's prototype
   *        chain, or a mirror around the viewer subject itself.
   */
  proto._addProtoButton = function OPV_AddProtoButton(aHeritageItem)
  {
    let unknown = this.stringBundle.getString("unknownPrototypeName.label");
    let name = (aHeritageItem.sameAs(this.subject)) ?
      this.stringBundle.getString("subjectName.label") :
      this.supplier.ObjectDetails.getProtoName(aHeritageItem, unknown);

    // `getProtoName` will return, e.g., "Constructor.prototype" in some
    // cases, but we just want the "Constructor" part of it here.
    // XXX Need a note for localizers to warn against using something that
    // ends in ".prototype" for unknownPrototypeName.label.
    if (!aHeritageItem.sameAs(this.subject)) {
      name = name.replace(/\.prototype$/, "");
    }

    let button = document.createElementNS(kXULNS, "button");
    this.mProtoBox.insertBefore(button, this.mProtoBox.firstChild);
    button.setAttribute("type", "checkbox");
    button.setAttribute("autocheck", "true");
    button.setAttribute("checked", "true");
    button.label = name;
    button.heritageItem = aHeritageItem;
  };

  proto._copyPropertyName = function OPV_CopyPropertyName()
  {
    let synthetic = this.getTargetSynthetic();
    if (!synthetic) {
      return;
    }

    let name = "";
    let nameChain = this._buildNameChain(synthetic);
    for (let i = 0, n = nameChain.length; i < n; ++i) {
      // XXX We need a `_divineSubjectName` method or something.  For now, we
      // act as if they have a reference to the viewer subject somehow, and
      // just want a quick way to get at the viewer target from there.  See
      // issue #93.
      let part = nameChain.pop();
      if (/^[a-zA-Z_$][a-zA-Z0-9_$]*$/.test(part)) {
        name += "." + part;
      }
      else {
        // XXX We just punt on non-ASCII code points and treat them like
        // special chars that require quoting.  ¯\_(ツ)_/¯
        name += "[" + JSON.stringify(part) + "]";
      }
    }

    TheFakeClipboardHack.copyContents(name);
  };

  proto._copyPropertyValue = function OPV_CopyPropertyValue()
  {
    let valueString = this._getValueString(this.getTargetSynthetic());
    if (valueString !== null) {
      TheFakeClipboardHack.copyContents(valueString);
    }
  };

  proto._getValueString = function OPV_GetValueString(aSynthetic)
  {
    let result = null;
    if (aSynthetic && aSynthetic.exception === null) {
      switch (aSynthetic.value.typeof) {
        default:
          // "function" and "object", or any host types.  We deal with the
          // default case first because `typeof(null)` is "object", and we want
          // to be able to copy that.
          if (!aSynthetic.value.sameAs(null)) break;
        // Fall through for null.
        case "number":
        case "undefined":
        case "boolean":
          result = String(
            this.mPanel.synthBloc.strip(aSynthetic.value)
          );
        break;
        case "string":
          result = JSON.stringify(
            this.mPanel.synthBloc.strip(aSynthetic.value)
          );
        break;
      }
    }

    return result;
  };
}
