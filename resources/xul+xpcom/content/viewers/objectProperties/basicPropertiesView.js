/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * A tree view recursively listing an object's properties and their values.
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.BasicPropertiesView = BasicPropertiesView;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { CommonTreeView } = require("utils/commonTreeView");
var { Enumerator } = require("utils/enumerator");
var { PropertyRowData } = require("./propertyRowData");

//////////////////////////////////////////////////////////////////////////////

/**
 * NB: Consumer must set `stringBundle` before connecting the view to the
 * tree.
 *
 * @param aRoot
 *        A mirror around the "root" object whose properties should be listed.
 * @param aSupplier
 *        The `ObjectDetailsSupplier`
 */
function BasicPropertiesView(aRoot, aSupplier)
{
  CommonTreeView.call(this);

  this.mSupplier = aSupplier;

  let rootRowData = new PropertyRowData(this.mSupplier.createDetails(aRoot));
  this.rowStore = [ rootRowData ];
  this.rowCount = 1;

  this.addManagedRowData(this.rowStore);
}

{
  let proto = Object.create(CommonTreeView.prototype);
  BasicPropertiesView.prototype = proto;
  proto.constructor = BasicPropertiesView;

  proto.rowStore = null;
  proto.stringBundle = null;

  proto.mSupplier = null;

  /**
   * @param aBase
   *        A mirror for the object whose properties are being compared.
   * @param x
   *        A string naming some property of the base object.
   * @param y
   *        A string naming some property of the base object.
   * @return
   *        An integer indicating the preferred relative ordering of the two
   *        properties, with those that look like constants appearing first.
   * @see Array.prototype.sort
   */
  BasicPropertiesView.keySortComparator =
    function BPV_KeySortComparator(aBase, x, y)
  {
    let xMayBeConstant = x == x.toUpperCase() && isNaN(x);
    let yMayBeConstant = y == y.toUpperCase() && isNaN(y);

    let xProp, yProp;

    if (xMayBeConstant) {
      xProp = aBase.lookUp(x);
      if (!xProp || xProp.isAccessor) xMayBeConstant = false;
    }

    if (yMayBeConstant) {
      yProp = aBase.lookUp(y);
      if (!yProp || yProp.isAccessor) yMayBeConstant = false;
    }

    if (xMayBeConstant) {
      if (yMayBeConstant) {
        // Both look like they could be constants.  Sort by numeric value, or
        // lexicographically ordered property names as a fallback.
        let numericComparison = BasicPropertiesView.numericSortComparator(
          // NB: This works due to a quirk of the jsmirrors implementation--
          // mirrors around number primitives (XXX and strings that look like
          // numbers) will get coerced into that value, while other mirrors
          // will get coerced into NaN.  XXX We may not want to rely on this.
          Number(xProp.value), Number(yProp.value)
        );

        return numericComparison || x.localeCompare(y);
      }

      // `x` is constant, `y` is not
      return -1;
    }

    if (yMayBeConstant) {
      // `y` is constant, `x` is not
      return 1;
    }

    // Neither are constants.  Sort by numeric property names, or
    // lexicographically ordered property names as a fallback.
    return BasicPropertiesView.numericSortComparator(Number(x), Number(y)) ||
           x.localeCompare(y);
  };

  BasicPropertiesView.numericSortComparator =
    function BPV_NumericSortComparator(a, b)
  {
    if (isNaN(a)) {
      return isNaN(b) ? 0 : 1;
    }
    if (isNaN(b)) {
      return -1;
    }
    return a - b;
  };

  /**
   * Fulfill the interface required of `CommonTreeView` subclasses.  Called
   * internally when a container row has been toggled open to signal that the
   * given row's child row data needs to be inserted.
   *
   * @param aRow
   *        The index of the row that was toggled open.
   * @return
   *        The number of inserted child rows.
   */
  proto.expandRow = function BPV_ExpandRow(aRow)
  {
    let rowData = this.rowStore[aRow];
    let propertyNames = rowData.details.value.propertyNames;
    let kids = this._buildChildRowData(propertyNames, rowData);

    CommonTreeView.arrayCopy(kids, this.rowStore, aRow + 1);

    return kids.length;
  };

  /**
   * Fulfill the interface required of `CommonTreeView` subclasses.  Called
   * internally when a container row has been toggled open or closed.  We have
   * to override this, because the default implementation provided by
   * `CommonTreeView` expects row's open state data to be stored in
   * `mOpenStates`, but we're also overriding `isContainerOpen` and friends,
   * so we're choosing not to use `mOpenStates`.
   *
   * @param aRow
   *        The index of the row that has been toggled open or closed.
   * @param aState
   *        A boolean encoding whether the row is now open (true) or closed
   *        (false).
   * @see CommonTreeView
   */
  proto.setRowOpenState = function BPV_SetRowOpenState(aRow, aState)
  {
    this.rowStore[aRow].isOpen = aState;
  };

  /**
   * @param aRow
   *        The zero-based index of the row whose value should be returned.
   * @return
   *        A mirror around the value that the row represents.
   */
  proto.getRowObject = function BPV_GetRowObject(aRow)
  {
    return this.getSynthetic(aRow).value;
  };

  /**
   * @param aRow
   *        The zero-based index of the row whose synthetic should be
   *        returned.
   * @return
   *        The object details synthetic for the given row.
   */
  proto.getSynthetic = function BPV_GetSynthetic(aRow)
  {
    return this.rowStore[aRow].details;
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsITreeView

  proto.getCellText = function BPV_GetCellText(aRow, aColumn)
  {
    let rowData = this.rowStore[aRow];
    switch (aColumn.id) {
      case "colName":
        return (rowData.details.name == null) ?
          this.stringBundle.getString("subjectName.label") :
          rowData.details.name;
      break;
      case "colValue":
        return rowData.text;
      break;
    }

    return "";
  };

  proto.getLevel = function BPV_GetLevel(aRow)
  {
    return this.rowStore[aRow].level;
  };

  proto.isContainer = function BPV_IsContainer(aRow)
  {
    let mirror = this.rowStore[aRow].details.value;
    return (mirror.typeof == "object" && !mirror.sameAs(null)) ||
           mirror.typeof == "function";
  };

  proto.isContainerEmpty = function BPV_IsContainerEmpty(aRow)
  {
    return this.rowStore[aRow].details.value.hasOwn();
  };

  proto.isContainerOpen = function BPV_IsContainerOpen(aRow)
  {
    return this.rowStore[aRow].isOpen;
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  /**
   * @param aPropertyNames
   *        An array of strings naming the properties of the object that the
   *        row data represents.
   * @param aParentRowData
   *        A `PropertyRowData` object for the base object containing the
   *        properties for which we're building child row data.
   */
  proto._buildChildRowData =
    function BPV_BuildChildRowData(aPropertyNames, aParentRowData)
  {
    let propertyNames = aPropertyNames.slice(0).sort(
      BasicPropertiesView.keySortComparator.bind(
        null, aParentRowData.details.value
      )
    );

    let kids = [];
    for (let i = 0, n = propertyNames.length; i < n; ++i) {
      let details = this.mSupplier.createPropertyDetails(
        propertyNames[i], aParentRowData.details
      );

      let rowData = new PropertyRowData(details, aParentRowData);

      kids.push(rowData);
    }

    return kids;
  };
}
