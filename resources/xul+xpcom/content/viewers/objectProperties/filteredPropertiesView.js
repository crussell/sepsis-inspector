/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * A tree view recursively listing an object's properties and their values.
 *
 * The view supports selectively hiding properties that are "ultimately
 * contributed" by a given object on the prototype chain.  For example, you
 * may create a view for some object, but don't want `__defineGetter__` or
 * other methods inherited from `Object.prototype` to appear.  Passing
 * `Object.prototype` to the view's `togglePropertyVisibility` will take care
 * of that.
 *
 * Here's how this is implemented:
 *
 * We have a `BasicPropertiesView` implementation that works for showing all
 * properties and values.  What we'll do in `FilteredPropertiesView` is
 * create an off-screen `BasicPropertiesView` instance that we'll keep as our
 * full, "backing" view.  Whenever a row is toggled open in the filtered view,
 * we'll first do the same in the backing view.  We'll then iterate through
 * the backing view's newly-inserted row data, copying over into our row data
 * store only the data for the rows that we don't want filtered out.  If at
 * any point we need to unhide some rows, we'll iterate through the entire
 * backing view's row data and copy into our row store any rows that were
 * previously hidden but should now be shown.
 *
 * One complication is that, since at any given time the backing view may have
 * some row data for rows that don't correspond to any visible row in our own
 * row data store, we'll have to devise some way to map our row "coordinate
 * space" to the backing view's "coordinate space".  Since the hidden rows can
 * occur in any number and at any location, there is no simple, constant-time
 * transformation we can apply to a index into our row data to derive the
 * corresponding index into the backing view's data.  (See below;
 * `_getBackingIndex` is our implementation of this routine).
 *
 * One other thing is we need to make sure that if all of some parent's child
 * rows get hidden, then the parent itself needs to get collapsed in both the
 * backing view and our own row store.  This problem and our approach for
 * solving it is discussed in greater detail in `_hideProperties` below.
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.FilteredPropertiesView = FilteredPropertiesView;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { BasicPropertiesView } = require("./basicPropertiesView");
var { CommonTreeView } = require("utils/commonTreeView");
var { InitializedEnumerator } = require("utils/initializedEnumerator");
var { TransformListNode } = require("./transformListNode");

//////////////////////////////////////////////////////////////////////////////

function FilteredPropertiesView(aRoot, aSupplier)
{
  CommonTreeView.call(this);

  this.mSupplier = aSupplier;

  this.mHiddenOwners = [];

  this.mBackingView = new BasicPropertiesView(aRoot, aSupplier);

  // assert(this.mBackingView.rowStore.length == 1);
  this.rowStore = [ this.mBackingView.rowStore[0] ];
  this.rowCount = 1;

  this.addManagedRowData(this.rowStore);
}

{
  let proto = Object.create(CommonTreeView.prototype);
  FilteredPropertiesView.prototype = proto;
  proto.constructor = FilteredPropertiesView;

  proto.mBackingView = null;
  proto.mHiddenOwners = null;
  proto.mSupplier = null;

  proto.rowStore = null;

  // Required since we're implemented in terms of `BasicPropertiesView` not
  // through inheritance but through self-decoration and composition.
  proto.__defineGetter__("stringBundle", function FPV_StringBundle_Get()
  {
    return this.mBackingView.stringBundle;
  });

  proto.__defineSetter__("stringBundle", function FPV_StringBundle_Set(aVal)
  {
    this.mBackingView.stringBundle = aVal;
  });

  proto.__defineGetter__("treeBoxObject", function FPV_TreeBoxObject_Get()
  {
    // `this.mTreeBoxObject` is set up by the `setTree` implementation we
    // inherit from `CommonTreeView`.  This is a "public" accessor for it.
    return this.mTreeBoxObject;
  });

  proto.expandRow = function FPV_ExpandRow(aRow)
  {
    let backingIndex = this._getBackingIndex(aRow);
    // assert(!this.isContainerOpen(aRow) &&
    //        !this.mBackingView.isContainerOpen(aRow));
    let backingChangeCount = this.mBackingView.expandRow(backingIndex);

    // Not strictly necessary, but we do this anyway just so things will look
    // sane if you start actually poking around the backing view...
    this.mBackingView.setRowOpenState(backingIndex, true);
    this.mBackingView.rowCount += backingChangeCount;

    let kids = [];
    for (let i = 0; i < backingChangeCount; ++i) {
      let rowData = this.mBackingView.rowStore[backingIndex + 1 + i];
      let property = rowData.details.property;
      rowData.hidden = this._isHiddenOwner(property && property.owner);
      if (!rowData.hidden) {
        kids.push(rowData);
      }
    }

    CommonTreeView.arrayCopy(kids, this.rowStore, aRow + 1);

    return kids.length;
  };

  proto.collapseRow = function FPV_CollapseRow(aRow)
  {
    let backingIndex = this._getBackingIndex(aRow);
    // assert(this.isContainerOpen(aRow) &&
    //        this.mBackingView.isContainerOpen(aRow));
    let backingChangeCount = this.mBackingView.collapseRow(backingIndex);

    this.mBackingView.setRowOpenState(backingIndex, false);
    this.mBackingView.rowCount -= backingChangeCount;

    return CommonTreeView.prototype.collapseRow.call(this, aRow);
  };

  /**
   * Fulfill the interface required of `CommonTreeView` subclasses.  This
   * method will be called when a row is toggled open or closed. 
   * `CommonTreeView` provides a default implementation, but it only works if
   * we're storing the row open states in a managed array at `this.
   * mOpenStates`, which we aren't.  We're implementing our own
   * `isContainerOpen` method, instead, which means we have to implement this
   * in order for the default `toggleOpenState` implementation to work.
   *
   * @param aRow
   *        The index of the row that has been toggled open or closed.
   * @param aState
   *        A boolean encoding whether the row is now open (true) or closed
   *        (false).
   * @see CommonTreeView
   */
  proto.setRowOpenState = function FPV_SetRowOpenState(aRow, aState)
  {
    this.rowStore[aRow].isOpen = aState;
  };

  proto.togglePropertyVisibility =
    function FPV_TogglePropertyVisibility(aHeritageItem)
  {
    if (this._isHiddenOwner(aHeritageItem)) {
      this._showProperties(aHeritageItem);
    }
    else {
      this._hideProperties(aHeritageItem);
    }
  };

  proto.getRowObject = BasicPropertiesView.prototype.getRowObject;
  proto.getSynthetic = BasicPropertiesView.prototype.getSynthetic;

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsITreeView

  proto.getCellText = BasicPropertiesView.prototype.getCellText;
  proto.getLevel = BasicPropertiesView.prototype.getLevel;
  proto.isContainer = BasicPropertiesView.prototype.isContainer;
  proto.isContainerOpen = BasicPropertiesView.prototype.isContainerOpen;

  proto.isContainerEmpty = function FPV_IsContainerEmpty(aRow)
  {
    // Need to do a custom override; can't even delegate to backing view.
    let mirror = this.rowStore[aRow].details.value;
    while (mirror) {
      if (!this._isHiddenOwner(mirror) && mirror.hasOwn()) return false;
      mirror = mirror.prototype;
    }

    return true;
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  /**
   * NB: This is O(n) wrt the number of rows in the backing view.
   *
   * @param aRow
   *        A row index in the local coordinate space.
   * @return
   *        The given row index translated into the backing view's coordinate
   *        space.
   */
  proto._getBackingIndex = function FPV_GetBackingIndex(aRow)
  {
    let localIndex = 0;
    for (let i = 0, n = this.mBackingView.rowCount; i < n; ++i) {
      if (!this.mBackingView.rowStore[i].hidden) {
        if (localIndex == aRow) {
          return i;
        }

        ++localIndex;
      }
    }

    return -1;
  };

  proto._isHiddenOwner = function FPV_IsHiddenOwner(aHeritageItem)
  {
    if (aHeritageItem === null) return false;
    return this.mHiddenOwners.some((item) => aHeritageItem.sameAs(item));
  };

  proto._hideProperties = function FPV_HideProperties(aHeritageItem)
  {
    this.mHiddenOwners.push(aHeritageItem);

    // We need to figure out which of the currently visible rows are going to
    // disappear.  There's one special condition we need to deal with: if a
    // row is currently open and all of its children are about to disappear,
    // we want to make sure it gets toggled closed.  The bulk of the
    // complexity here comes from having to deal with that condition,
    // otherwise, things would be fairly straightforward--we'd just remove the
    // row data for the newly hidden rows and call it a day.  Instead, we
    // devise this scheme to handle things:
    //
    // The general idea is that we're going to iterate through the visible
    // rows and find *which* rows will need these things done (i.e., a row
    // hide or row collapse or both), and then we'll iterate through them
    // again, *actually performing* the collapsing and/or hiding.  Note that
    // if we store any row indexes on the first pass, when we do any
    // collapsing and hiding, the indexes will become invalid for all rows
    // that appear after the collapsed or hidden row.  For this reason, on the
    // second pass, we'll iterate backwards.  Observe that this neatly
    // sidesteps the invalidation problem: the only indexes that will become
    // invalid are the ones which are greater than the collapsed index, but we
    // will have already processed those; all indexes we will have yet to
    // process will remain valid, because they will all be indexes for rows
    // that proceed the collapsed row.
    //
    // Further observe that we won't be able to determine if a row will need
    // to be closed until we've looked at all its child rows.  Somehow, upon
    // processing the last in a sibling group, we'll need to recognize at that
    // point whether its parent either needs to be collapsed or doesn't.  We
    // can access any given `PropertyRowData` instance's `parent`, but we
    // won't actually be storing the metadata we generate in this method on
    // the row data object itself; this is ephemeral information and not even
    // relevant outside this method, we're going to keep the generated
    // metadata in a transform list (see below).
    //
    // Before we explain the implemented solution for this, note also that we
    // can't just keep a single reference to the metadata for the parent of
    // the current sibling group.  Consider the following example:
    //
    //   A
    //   B
    //    X
    //     P
    //    Y
    //    Z
    //   C
    //
    // Upon finishing our checks on P and moving to Y, we'll need to be able
    // to reference the metadata for X's parent (B).  This won't be possible
    // if we use a single reference, because our reference to the metadata for
    // B will have been overwritten by a reference to the metadata for X when
    // we descended and began our checks on P.  This calls for a stack.
    //
    // So here's how we're actually going to do this:
    //
    // We'll visit the row data in order as described, building up the stack
    // and popping as appropriate.  As this is happening, we'll be building up
    // a transform list--from which we can later guide ourselves when
    // performing transformations--enumerating which transformations need to
    // happen--where a transformation can be either a row hide or a collapse
    // (or both).  After the first pass, it is on the transform list that we
    // will do the second pass, in reverse order as described above.
    let transforms = [];
    let visitStack = [];

    // Again, for now we are only determining *which* transformations will
    // need to be done (partly by way of `_popAndProcess`).
    for (let i = 0, n = this.rowCount; i < n; ++i) {
      let rowData = this.rowStore[i];
      this._popAndProcess(visitStack, transforms, rowData.level - 1);

      // Let this transform node remember both the row data's index into our
      // `rowStore` and its own position in the transform list.  This works
      // because we will only ever add or remove elements from the end of
      // `transforms`.  Otherwise, inserting or removing from the middle would
      // mean these indexes would go stale and need an explicit update step.
      let node = new TransformListNode(rowData, i, transforms.length);
      let property = rowData.details.property;
      node.hide = this._isHiddenOwner(property && property.owner);

      visitStack.push(node);
      transforms.push(node);
    }

    // Process whatever remains on the stack.
    this._popAndProcess(visitStack, transforms, -1);

    // Now actually do the deed(s).
    this._processHideTransforms(transforms);
  };

  /**
   * Pop from the top of the given stack until the top node is one of the
   * given level.
   *
   * @param aStack
   *        A stack of `TransformListNode` instances.
   * @param aTransforms
   *        The corresponding transform list.  Processing nodes from the stack
   *        may modify the transform list, including dropping entries from it.
   * @param aLevel
   *        0-based number giving the depth of the row as defined by the
   *        semantics of `nsITreeView`.  This method will continue popping and
   *        processing nodes until it encounters one at this level.  Use -1 to
   *        process the entire stack.
   * @see _hideProperties
   */
  proto._popAndProcess =
    function FPV_PopAndProcess(aStack, aTransforms, aLevel)
  {
    while (aStack.length && this._peek(aStack).rowData.level > aLevel) {
      let poppedTop = aStack.pop();

      // Observe that:
      // - Everything after `poppedTop` in the transform list must correspond
      //   to one of `poppedTop.rowData`'s descendants.
      // - If the row for `poppedTop` is going to get collapsed, there's no
      //   need to process the transforms for its descendants, so...
      // - We can drop `poppedTop`'s descendants from the transform list if
      //   the row associated with `poppedTop` is going to get collapsed.
      if (!poppedTop.hasVisibleChildren) {
        aTransforms.splice(
          poppedTop.transformListIndex + 1, Number.POSITIVE_INFINITY
        );
      }

      if (!poppedTop.hide && poppedTop.rowData.parent) {
        let parentIndex = this._peek(aStack).transformListIndex;
        aTransforms[parentIndex].hasVisibleChildren = true;
      }
    }
  };

  proto._peek = function FPV_Peek(aStack)
  {
    return (aStack.length) ?
      aStack[aStack.length - 1]:
      null;
  };

  /**
   * @param aTransforms
   *        An array of `TransformListNode` instances.
   *
   *        NB: The relationship with transform nodes is one-to-one wrt the
   *        affected rows, not one-to-one wrt to the transformations.  That
   *        is, if some row will need to be collapsed and hidden, there will
   *        be a single node for that, and not one in the list corresponding
   *        to the collapse transform and a separate for the hiding.
   * @see _hideProperties
   */
  proto._processHideTransforms =
    function FPV_ProcessHideTransforms(aTransforms)
  {
    this.mTreeBoxObject && this.mTreeBoxObject.beginUpdateBatch();

    for (let i = aTransforms.length - 1; i >= 0; --i) {
      let transform = aTransforms[i];
      let deleteFrom, changeCount = 0;
      if ((transform.hide || !transform.hasVisibleChildren) &&
          this.isContainerOpen(transform.localIndex)) {

        deleteFrom = transform.localIndex + 1;
        changeCount = this.collapseRow(transform.localIndex);
        this.setRowOpenState(transform.localIndex, false);
      }

      if (transform.hide) {
        deleteFrom = transform.localIndex;
        ++changeCount

        this.rowStore.splice(deleteFrom, 1);
        transform.rowData.hidden = true;
      }

      if (changeCount) {
        this.rowCount -= changeCount;
        if (this.mTreeBoxObject) {
          this.mTreeBoxObject.rowCountChanged(deleteFrom, -changeCount);
        }
      }
    }

    this.mTreeBoxObject && this.mTreeBoxObject.endUpdateBatch();
  };

  proto._showProperties = function FPV_ShowProperties(aHeritageItem)
  {
    let idx = this.mHiddenOwners.reduce((result, item, idx) => (
      (result < 0 && aHeritageItem.sameAs(item)) ? idx : result
    ), -1);
    this.mHiddenOwners.splice(idx, 1);

    // We'll employ another strategy using a list of transforms, similar to
    // `_hideProperties` above, only this time the transforms will be for
    // rows that need to be unhidden.
    //
    // We'll go through the backing view, checking if the row needs to be
    // unhidden, while simultaneously traversing our local row data.  If it
    // needs to be unhidden, we'll take note of our current position in the
    // local row data, since that's where the unhidden row will need to go.
    //
    // We'll make a second pass afterward over the transform list, where we'll
    // actually be copying the row data over from the backing view into our
    // own records.
    let transforms = [];
    let localIndex = 0;
    for (let i = 0, n = this.mBackingView.rowCount; i < n; ++i) {
      let rowData = this.mBackingView.rowStore[i];
      if (rowData == this.rowStore[localIndex]) {
        ++localIndex;
        continue;
      }

      // So it's not currently being shown.  Is it one that was hidden before
      // and we now need start showing?
      if (rowData.details.property.owner.sameAs(aHeritageItem)) {
        // Yup.  Remember that it needs be inserted, and where.
        transforms.push(new TransformListNode(rowData, localIndex));
      }
      // Otherwise, it stays hidden.
    }

    this._processUnhideTransforms(transforms);
  };

  /**
   * @param aTransforms
   *        An array of TransformListNodes, in ascending order as they should
   *        appear in the new row data relative to one another.
   *
   *        NB: This time around, unlike with `_processHideTransforms` we make
   *        no use of the node's `hide` property.  It's meaningless here;
   *        everything in this list represents an unhide transform.
   * @see _showProperties
   */
  proto._processUnhideTransforms =
    function FPV_ProcessUnhideTransforms(aTransforms)
  {
    this.mTreeBoxObject && this.mTreeBoxObject.beginUpdateBatch();

    // We'll just create a whole new row data list that will be at the heart
    // of our view, and let that become our new `rowStore` when we're done.
    let newRowStore = [];
    let enumerator = new InitializedEnumerator(this.rowStore);
    let changeCount = 0;
    for (let i = 0, n = aTransforms.length; i < n; ++i) {
      let current = aTransforms[i];

      // Copy into `newRowStore` everything from `this.rowStore` that should
      // precede this unhidden row.
      this._copyRowData(enumerator, newRowStore, current.localIndex);

      // Now the unhidden row itself.
      newRowStore.push(current.rowData);
      current.rowData.hidden = false;
      ++changeCount;

      // We don't want to make a lot of `rowCountChanged` calls for rows that
      // are bunched up next to each other.  If there are, say, three adjacent
      // rows that get unhidden, we can accomplish what we want with a single
      // `rowCountChanged` call.  So we defer calling it until we know that we
      // aren't about to iterate over another row that should get inserted
      // right after the current one.
      let lookAhead = (i + 1 < n) ?
        aTransforms[i + 1] :
        null;

      if (!lookAhead || lookAhead.localIndex != current.localIndex) {
        this.rowCount += changeCount;
        this.mTreeBoxObject && this.mTreeBoxObject.rowCountChanged(
          current.localIndex, changeCount
        );
        changeCount = 0;
      }
    }

    // We've finished copying everything that should get unhidden and placed
    // in the now row store.  Now copy whatever remains from the old row
    // store, from `enumerator`'s current position onward.
    this._copyRowData(enumerator, newRowStore, -1);

    // Transplant the view's new heart.
    let oldRowStore = this.rowStore;
    this.updateManagedRowData(oldRowStore, newRowStore);
    this.rowStore = newRowStore;

    this.mTreeBoxObject && this.mTreeBoxObject.endUpdateBatch();
  };

  proto._copyRowData =
    function FPV_CopyRowData(aEnumerator, aDestination, aUntilIndex)
  {
    let stopAt = (0 <= aUntilIndex && aUntilIndex < this.rowStore.length) ?
      this.rowStore[aUntilIndex] :
      null;

    while (!aEnumerator.isDead && aEnumerator.getValue() != stopAt) {
      aDestination.push(aEnumerator.getValue());
      aEnumerator.move();
    }
  };
}
