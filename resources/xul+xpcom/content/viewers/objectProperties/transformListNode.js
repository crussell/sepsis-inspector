/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * When toggling the visibility of properties for certain objects from the
 * prototype chain in a `FilteredPropertiesView` instance, there are a number
 * of transformations that may need to be applied, such as row deletion or
 * insertion.  The view computes which transformations need to occur and
 * stores them in a local "transform list".  Such a list consists of
 * `TransformListNode` instances.  A node encapsulates the metadata for the
 * transforms.
 *
 * This is all much more thoroughly documented in filteredPropertiesView.js.
 *
 * @see FilteredPropertiesView.prototype._hideProperties
 * @see FilteredPropertiesView.prototype._showProperties
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.TransformListNode = TransformListNode;

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aRowData
 * @param aLocalIndex
 * @param aTransformListIndex [optional]
 */
function TransformListNode(aRowData, aLocalIndex, aTransformListIndex)
{
  this.rowData = aRowData;
  this.localIndex = aLocalIndex;

  if (aTransformListIndex !== undefined) {
    this.transformListIndex = aTransformListIndex;
  }

  this.hasVisibleChildren = false;
  this.hide = false;
}

{
  let proto = TransformListNode.prototype;
  proto.rowData = null;
  proto.localIndex = null;
  proto.hasVisibleChildren = null;
  proto.hide = null;
  proto.transformListIndex = null;

  /**
   * For debugging.
   */
  proto.toString = function TLN_ToString()
  {
    return (this.hide) ?
      "-" + this.rowData.details.name + "    @ " + this.localIndex:
      "+" + this.rowData.details.name + "    @ " + this.localIndex;
  };
}
