/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.PropertyRowData = PropertyRowData;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { jsValueToString } = require("utils/jsValueToString");

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aDetailsSynthetic
 * @param aParentRowData [optional]
 */
function PropertyRowData(aDetailsSynthetic, aParentRowData)
{
  this.details = aDetailsSynthetic;

  this.level = 0;
  this.isOpen = false;
  if (aParentRowData !== undefined) {
    this.parent = aParentRowData;
    this.level = aParentRowData.level + 1;
  }

  let mirror = this.details.exception || this.details.value;
  // NB: `getReflectee` return value is untrusted.
  this.text = jsValueToString(this.details.bloc.strip(mirror));
}

{
  let proto = PropertyRowData.prototype;
  proto.details = null;
  proto.isOpen = null;
  proto.level = null;
  proto.parent = null;
  proto.text = null;

  /**
   * For debugging.
   */
  proto.toString = function PRD_ToString()
  {
    return this.details.name + "   @ " + this.level;
  };
}
