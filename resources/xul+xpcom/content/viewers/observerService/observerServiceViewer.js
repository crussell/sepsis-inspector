/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Viewer to show the notifications sent through the XPCOM background service.
 *
 * @see registration/theViewerRegistry
 */

//////////////////////////////////////////////////////////////////////////////
//// Imported Symbols

var { NotificationsView } = require("./notificationsView");

var { ObserverService } = require("xpcom/observerService");

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aPanel
 *        The inspectorpanel XUL element that the viewer is being loaded into.
 */
function ObserverServiceViewer(aPanel)
{
  this.mPanel = aPanel;

  this.mTree = document.getElementById("olNotifications");
  this.mTree.addEventListener("select", this, false);

  this.mNotificationsView = new NotificationsView(ObserverService);

  this.mTree.view = this.mNotificationsView;
}

{
  let proto = ObserverServiceViewer.prototype;

  ////////////////////////////////////////////////////////////////////////////
  //// "interface" sepsisIViewer

  proto.mNotificationsView = null;
  proto.mPanel = null;
  proto.mTree = null;

  proto.target = null;

  proto.inspectApplication = function OSV_InspectApplication()
  {
    // No-op.
  };

  proto.destroy = function OSV_Destroy()
  {
    this.mNotificationsView.destroy();
  };

  ////////////////////////////////////////////////////////////////////////////

  proto.handleEvent = function OSV_HandleEvent(aEvent)
  {
    switch (aEvent.type) {
      case "select":
        this.onSelect();
      break;
    }
  };

  proto.onSelect = function OSV_OnSelect()
  {
    let notification = this.mNotificationsView.getRowObject(
      this.mNotificationsView.selection.currentIndex
    );

    this.mPanel.changeTarget(notification.subject);
  };
}
