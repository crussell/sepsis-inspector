/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.NotificationsView = NotificationsView;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { StubTreeView } = require("utils/stubTreeView");

//////////////////////////////////////////////////////////////////////////////

function NotificationsView(aObserverService)
{
  this.mNotifications = [];
  this.mObserverService = aObserverService;
  this.mObserverService.addObserver(this, "*", false);
}

{
  let proto = Object.create(StubTreeView.prototype);
  NotificationsView.prototype = proto;
  proto.constructor = NotificationsView;

  proto.mNotifications = null;
  proto.mObserverService = null;
  proto.mTreeBoxObject = null;

  proto.destroy = function NV_Destroy()
  {
    this.mObserverService.removeObserver(this, "*");
  };

  proto.getRowObject = function NV_GetRowObject(aRow)
  {
    return this.mNotifications[this.rowCount - aRow - 1];
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIObserver

  proto.observe = function NV_Observe(aSubject, aTopic, aData)
  {
    this.rowCount = this.mNotifications.push({
      subject: aSubject,
      topic: aTopic,
      data: aData
    });
    this.mTreeBoxObject.rowCountChanged(0, 1);
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsITreeView

  proto.getCellText = function NV_GetCellText(aRow, aColumn)
  {
    switch (aColumn.id) {
      case "colTopic":
        return this.getRowObject(aRow).topic;
      break;
      case "colData":
        return this.getRowObject(aRow).data;
      break;
    }

    return "";
  };

  proto.setTree = function NV_SetTree(aTreeBoxObject)
  {
    this.mTreeBoxObject = aTreeBoxObject;
  };
}
