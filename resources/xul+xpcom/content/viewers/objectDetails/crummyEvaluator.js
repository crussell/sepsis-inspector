/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Predicate DSL evaluator for asking questions about a property descriptor.
 *
 * Predicates can be built out of subpredicates separated by semicolons. 
 * Example input format:
 *
 *   "A B C; X Y"
 *
 * ... except that rather than A, B, etc., the tokens must match one of the
 * following to be considered valid:
 *
 *   isAccessor, !isAccessor, isWritable, !isWritable, isConfigurable,
 *   !isConfigurable, hasGetter, !hasGetter, hasSetter, or !hasSetter
 *
 * NB: The approach for both logical disjunction and conjunction is to use a
 * short-circuit evaluation strategy.
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.CrummyEvaluator = CrummyEvaluator;

//////////////////////////////////////////////////////////////////////////////

function CrummyEvaluator(aPropertyMirror)
{
  this.isConfigurable = aPropertyMirror.isConfigurable;
  this.isAccessor = aPropertyMirror.isAccessor;
  if (this.isAccessor) {
    this.hasGetter = !!aPropertyMirror.getter;
    this.hasSetter = !!aPropertyMirror.setter;
  }
  else {
    this.isWritable = aPropertyMirror.isWritable;
  }
}

{
  let proto = CrummyEvaluator.prototype;
  proto.isAccessor = null;
  proto.isWritable = null;
  proto.isConfigurable = null;
  proto.hasGetter = null;
  proto.hasSetter = null;

  proto.eval = function CE_Eval(aExpression)
  {
    return this._evalDisjunction(aExpression);
  },

  /**
   * @param aExpression
   *        A string of one or more substrings delimited by semicolons, where
   *        each substring is itself composed of valid tokens (see above)
   *        delimited by whitespace.
   * @return
   *        A boolean indicating the result of the evaluated expression.
   * @throws
   *        Error if the input string doesn't match described syntax,
   *        including invalid tokens.
   *
   *        Error if the input string checks for one of the has* or !has*
   *        tokens when isAccessor evaluates to false, or checks for
   *        isWritable or !isWritable when isAccessor evaluates to true.
   */
  proto._evalDisjunction = function CE_EvalDisjunction(aPredicate)
  {
    let conditions = aPredicate.split(";");
    for (let i = 0, n = conditions.length; i < n; ++i) {
      if (this._evalConjunction(conditions[i].trim())) {
        return true;
      }
    }

    return false;
  },

  proto._evalConjunction = function CE_EvalConjunction(aSubpredicate)
  {
    let components = aSubpredicate.split(" ");
    for (let i = 0, n = components.length; i < n; ++i) {
      let prop = components[i];
      let veritas = "";

      if (prop.charAt(0) == "!") {
        prop = prop.substring(1);
        veritas = "!";
      }

      switch (prop) {
        case "isConfigurable":
        case "isWritable":
        case "isAccessor":
        case "hasGetter":
        case "hasSetter":
          // Not allowed to check for (!)isWritable when isAccessor evaluates
          // to true, nor for (!)hasGetter or (!)hasSetter when isAccessor
          // would evaluate to false.
          if (this[prop] === null) {
            throw new Error("Improper token use: " + veritas + prop); // XXX
          }

          if (this[prop] && veritas || !this[prop] && !veritas) {
            return false;
          }
        break;
        default:
          throw new Error("Unrecognized token: " + veritas + prop); // XXX
        break;
      }
    }

    return true;
  };
};
