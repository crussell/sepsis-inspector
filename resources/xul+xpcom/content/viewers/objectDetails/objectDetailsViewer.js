/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Viewer to show the details of a property, method, or function.
 *
 * TODO:
 *  - Use the string bundle from our `ViewerInfo` instead?
 *  - When the value is a function, its parameters and the text source of its
 *    body should be hidden by default behind a clickable ellipsis.  Clicking
 *    either should show both (and add an inline button to rehide them).
 *  - Right now our filter only allows objectDetails synthetics to pass
 *    through.  We should also allow in, at the very least, any raw function,
 *    for which we'd just show the (automatically expanded) source and no
 *    heritage or property descriptor stuff (because there wouldn't be any).
 *    The way this would be implemented is as an adapter that when given such
 *    a permitted value, would output a details synthetic.
 *
 *    But this brings to light a problem with the current adapter
 *    implementation: what happens when a viewer can be used with multiple
 *    adapters?  (In this case the conflict would be between the null
 *    [passthrough] adapter and the whatever hypothetical adapter we would
 *    create in order to translate the permitted values into a details
 *    synthetic, but the problem is a bigger one that could stem from a
 *    conflict between any two or more adapters.)
 *
 *    I have a tentative design for an "adapter bar" sketched out that would
 *    solve this, but it's not implemented yet, so... --crussell
 *
 * @see registration/theViewerRegistry
 */

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { CrummyEvaluator } = require("./crummyEvaluator");
var { HelpTemplater } = require("./helpTemplater");

var { FunctionInfo } = require("utils/functionInfo");

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aPanel
 *        The inspectorpanel XUL element that the viewer is being loaded into.
 */
function ObjectDetailsViewer(aPanel)
{
  this.mPanel = aPanel;

  let $ = document.getElementById.bind(document);
  this.mNameBox = $("bxName");
  this.mValueLabel = $("bxValueLabel");
  this.mValueLink = $("bxValueValue");
  this.mValueSource = $("bxValueSource");
  this.mGetterSetterBox = $("bxGetterSetterInfo");
  this.mGetterLabel = $("bxGetterLabel");
  this.mGetterLink = $("bxGetterValue");
  this.mSetterLabel = $("bxSetterLabel");
  this.mSetterLink = $("bxSetterValue");
  this.mHeritageBox = $("bxHeritage");
  this.mHeritageList = $("bxHeritageList");
  this.mDescriptorAttributesBox = $("bxDescriptorAttributes");
  this.mDescriptorAttributesLabel = $("bxDescriptorAttributesLabel");
  this.mEnumerabilityLabel = $("bxEnumerabilityLabel");
  this.mConfigurabilityLabel = $("bxConfigurabilityLabel");
  this.mWritabilityLabel = $("bxWritabilityLabel");
  this.mHeritageLabel = $("bxHeritageLabel");

  // Faking a popover effect with a div that's confined to the viewer document
  // viewport makes for an unconvincing popup.  Even though this viewer is one
  // of the few written with HTML, we still want to be able to use some XUL,
  // both for that aforementioned popup, and so that we can make use of things
  // like stringbundles.  All of our XUL is sourced into the viewer by way of
  // this iframe.
  let xulDoc = $("bxPanelFrame").contentDocument;
  let $xulid = xulDoc.getElementById.bind(xulDoc);
  this.mStringBundle = $xulid("mainStringBundle");
  this.mHelpPopup = $xulid("ppHelpPopup");
  this.mHelpStringBundle = $xulid("helpStringBundle");

  this.mHelpTemplater =
    new HelpTemplater(this.mHelpStringBundle, this.mHelpPopup.firstChild);

  document.addEventListener("click", this, true);

  this._localizeUI();
}

{
  let proto = ObjectDetailsViewer.prototype;
  proto.mConfigurabilityLabel = null;
  proto.mDescriptorAttributesBox = null;
  proto.mDescriptorAttributesLabel = null;
  proto.mEnumerabilityLabel = null;
  proto.mGetterLabel = null;
  proto.mGetterLink = null;
  proto.mGetterSetterBox = null;
  proto.mHelpPopup = null;
  proto.mHelpStringBundle = null;
  proto.mHeritageBox = null;
  proto.mHeritageLabel = null;
  proto.mHeritageList = null;
  proto.mNameBox = null;
  proto.mPanel = null;
  proto.mSetterLabel = null;
  proto.mSetterLink = null;
  proto.mStringBundle = null;
  proto.mSubject = null;
  proto.mValueLabel = null;
  proto.mValueLink = null;
  proto.mValueStore = null;
  proto.mWritabilityLabel = null;

  ////////////////////////////////////////////////////////////////////////////
  //// viewer interface

  proto.target = null;

  proto.inspectObject = function ODV_InspectObject(aDetails)
  {
    delete this.functionInfo;

    // XXX What about `aDetails.exception`?
    this.mSubject = aDetails;

    this.mValueStore = [];

    if (this.mSubject.value.typeof == "function") {
      try {
        this.functionInfo = new FunctionInfo(this.mSubject.value);
      }
      catch (ex) { }
    }

    this._updateNameBox();
    this._updateValue();
    this._updateDescriptorAttributes();
    this._updateHeritage();

    this.mPanel.changeTarget(
      this.mPanel.synthBloc.strip(aDetails.value)
    );
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIDOMEventListener

  proto.handleEvent = function ODV_HandleEvent(aEvent)
  {
    switch (aEvent.type) {
      case "click":
        this._onClick(aEvent);
      break;
      case "focus":
        this._onFocusChange(aEvent);
      break;
      default:
        throw new Error("wat.  Event: " + aEvent.type);
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////

  proto._localizeUI = function ODV_LocalizeUI()
  {
    let separator = this.mStringBundle.getString("after.separator");
    let afterRuleText =
      "h2::after, dt::after { content: \"" + separator + "\"; }";

    let styleSheet = document.getElementById("ssViewerStyle").sheet;
    styleSheet.insertRule(afterRuleText, styleSheet.cssRules.length);

    let $ = this.mStringBundle.getString.bind(this.mStringBundle);
    this.mValueLabel.textContent = $("value.label");
    this.mGetterLabel.textContent = $("get.label");
    this.mSetterLabel.textContent = $("set.label");
    this.mDescriptorAttributesLabel.textContent =
      $("descriptorAttributes.label");
    this.mHeritageLabel.textContent = $("heritage.label");
  };

  proto._updateNameBox = function ODV_UpdateNameBox()
  {
    this.mNameBox.textContent = this.mSubject.name;
  };

  proto._updateValue = function ODV_UpdateValue()
  {
    if (this.functionInfo) {
      this.mNameBox.setAttribute("function", "true");
      this._setFunctionSource(this.functionInfo);
    }
    else {
      this.mNameBox.removeAttribute("function");
      this._clearValueContent();
      this._setValueLinkText(
        "[object " + this.mSubject.value.internalClass + "]"
      );
    }

    let property = this.mSubject.property;
    if (property) {
      this._updateAccessorInfo(
        property.getter, "hasGetter", "noGetter.label", this.mGetterLink
      );

      this._updateAccessorInfo(
        property.setter, "hasSetter", "noSetter.label", this.mSetterLink
      );
    }
  };

  proto._setFunctionSource =
    function ODV_SetFunctionSource(aFunctionInfo)
  {
    this._clearValueContent();

    if (!aFunctionInfo.isArrow) {
      this.mValueSource.appendChild(document.createTextNode("function "));
      // XXX What about unnamed functions..?
      this._setValueLinkText(aFunctionInfo.name);
    }

    // Note that with an arrow function, its original param list may or may
    // not be enclosed in parens.  (See examples D and E in functionInfo.jsm).
    // Regardless, we always display it with parens.
    let paramList = "(" + aFunctionInfo.params.join(", ") + ")";
    this.mValueSource.appendChild(document.createTextNode(paramList));

    if (aFunctionInfo.isArrow) {
      this.mValueSource.lastChild.textContent += " ";
      this._setValueLinkText("=>");
    }

    let body = (!aFunctionInfo.isNative) ?
      aFunctionInfo.bodyText :
      "{ [native code] }";

    // `aFunctionInfo.bodyText` can be null, so trying to do `indexOf` on it
    // could throw on it.  Fortunately `String(null).indexOf("\n")` will
    // result in the correct code path taken, so we'll go with that.
    let space = (String(aFunctionInfo.bodyText).indexOf("\n") < -1) ?
      "\n" :
      " ";

    this.mValueSource.appendChild(document.createTextNode(space + body));
  };

  proto._clearValueContent = function ODV_ClearValueContent()
  {
    while (this.mValueSource.hasChildNodes()) {
      this.mValueSource.removeChild(this.mValueSource.lastChild);
    }
  };

  proto._setValueLinkText = function ODV_SetValueLinkText(aText)
  {
    this.mValueLink.textContent = aText;
    this.mValueSource.appendChild(this.mValueLink);
  };

  proto._updateDescriptorAttributes =
    function ODV_UpdateDescriptorAttributes()
  {
    let property = this.mSubject.property;
    if (property) {
      this.mEnumerabilityLabel.textContent = (property.isEnumerable) ?
        this.mStringBundle.getString("isEnumerable.label") :
        this.mStringBundle.getString("nonEnumerable.label");
      this.mConfigurabilityLabel.textContent = (property.isConfigurable) ?
        this.mStringBundle.getString("isConfigurable.label") :
        this.mStringBundle.getString("nonConfigurable.label");

      // For properties with a getter or a setter (or both), "writable" is
      // meaningless, and the descriptor won't contain a `writable`.
      if (property.isAccessor) {
        this.mWritabilityLabel.textContent =
          this.mStringBundle.getString("accessorProperty.label");
      }
      else {
        this.mWritabilityLabel.textContent = (property.isWritable) ?
          this.mStringBundle.getString("isWritable.label") :
          this.mStringBundle.getString("nonWritable.label");
      }
      this.mWritabilityLabel.classList.toggle("accessor", property.isAccessor);

      this.mDescriptorAttributesBox.removeAttribute("hidden");
    }
    else {
      this.mDescriptorAttributesBox.setAttribute("hidden", "true");
    }
  };

  proto._updateHeritage = function ODV_UpdateHeritage()
  {
    while (this.mHeritageList.hasChildNodes()) {
      this.mHeritageList.removeChild(this.mHeritageList.lastChild);
    }

    let property = this.mSubject.property;
    let heritageItem = property && property.base;
    if (!heritageItem) {
      this.mHeritageBox.setAttribute("hidden", "true");
      return;
    }

    this.mHeritageBox.removeAttribute("hidden");
    while (heritageItem) {
      let li = document.createElement("li");
      let a = document.createElement("a");

      a.textContent = this._getHeritageItemName(heritageItem);

      a.setAttribute("href", "");
      a.setAttribute("vid", this.mValueStore.length);
      this.mValueStore.push(heritageItem);

      li.appendChild(a);

      if (heritageItem.hasOwn(property.name)) {
        li.classList.add("owner");
      }

      this.mHeritageList.appendChild(li);

      heritageItem = heritageItem.prototype;
    }
  };

  proto._getHeritageItemName = function ODV_GetHeritageItemName(aHeritageItem)
  {
    if (this.mSubject.baseDetails.value.sameAs(aHeritageItem)) {
      return (this.mSubject.baseDetails.name != null) ?
        this.mSubject.baseDetails.name :
        this.mStringBundle.getString("heritageBaseObject.label");
    }

    let name = this.mSubject.getProtoName(aHeritageItem);
    if (!name) {
      return this.mStringBundle.getString("heritageUnknownPrototype.label");
    }

    return name;
  };

  proto._onClick = function ODV_OnClick(aEvent)
  {
    // NB: We need to make sure the help popever isn't already showing,
    // because if if we're about to try and show it, `openPopup` won't
    // reposition it to be anchored to the correct button.  It makes no
    // difference when using the mouse, because the click will first
    // automatically hide the popup, but it's significant when tabbing between
    // buttons and activating them with the keyboard.
    //
    // We also want activating links with the keyboard to close the popup,
    // which is why we're doing this here, instead of right before the
    // `_showHelp` call.
    this.mHelpPopup.hidePopup();

    if (!aEvent.target.hasAttribute("href")) {
      if (aEvent.target.classList.contains("help")) {
        this._showHelp(aEvent.target);
      }
      else if (aEvent.target.classList.contains("welllinked")) {
        this._showHelp(
          document.getElementById(aEvent.target.getAttribute("otherhelp"))
        );
      }
      return;
    }

    let property = this.mSubject.property;
    switch (aEvent.target) {
      case this.mValueLink:
        this.mPanel.changeTarget(
          this.mPanel.synthBloc.strip(this.mSubject.value)
        );
      break;
      case this.mGetterLink:
        this.mPanel.changeTarget(
          this.mPanel.synthBloc.strip(property.getter)
        );
      break;
      case this.mSetterLink:
        this.mPanel.changeTarget(
          this.mPanel.synthBloc.strip(property.setter)
        );
      break;
      default:
        if (aEvent.target.hasAttribute("vid")) {
          let value = this.mValueStore[aEvent.target.getAttribute("vid")];
          this.mPanel.changeTarget(this.mPanel.synthBloc.strip(value));
        }
        else {
          throw new Error("wat.  Target: " + aEvent.target);
        }
      break;
    }

    aEvent.preventDefault();
  };

  /**
   * A snippet's "helpfor" attribute contains the IDs of the buttons whose
   * popup the snippet can appear in.
   *
   * The "showif" attribute, if present, can further control how the snippet
   * is conditionally hidden/shown.
   *
   * The "otherhelp" attribute on a link in the snippet, if present, should be
   * the ID of the button whose help should be opened when the link is
   * clicked.
   *
   * @param aButton
   *        The help button whose popup should be shown.
   * @see viewers/objectDetails/crummyEvaluator.js
   */
  proto._showHelp = function ODV_ShowHelp(aButton)
  {
    let first = true;
    let snippets = this.mHelpPopup.firstChild.querySelectorAll("[helpfor]");
    for (let i = 0, n = snippets.length; i < n; ++i) {
      if (snippets[i].nodeType != snippets[i].ELEMENT_NODE) {
        continue;
      }

      let helpfor = snippets[i].getAttribute("helpfor");
      if (helpfor.split(" ").indexOf(aButton.id) < 0 ||
          !this._isSnippetRelevant(snippets[i])) {
        snippets[i].style.display = "none";
      }
      else {
        snippets[i].style.display = "";

        // We want to be able to specify a margin between snippets in the
        // skin.  We can do this using adjacent sibling selectors, but since
        // the first snippet actually shown may be preceded by other snippets
        // that are not shown, it can end up with a margin above it.  So if
        // it's the first visible snippet, we add this class so we can
        // discrimate and not add a margin to it.
        if (first) {
          snippets[i].classList.add("firstsnippet");
          first = false;
        }
      }

      // Add a "welllinked" attribute to links only if the button isn't
      // hidden.
      let links = snippets[i].querySelectorAll("a[otherhelp]");
      for (let i = 0, n = links.length; i < n; ++i) {
        let otherButton =
          document.getElementById(links[i].getAttribute("otherhelp"));
        let otherRect = otherButton.getBoundingClientRect();
        links[i].classList.toggle(
          "welllinked", otherRect.width && otherRect.height
        );
      }
    }

    // We want to try not to obscure too much of the heritage list, so we use
    // special positioning for the popup when showing its help.
    let position = (aButton.id == "btHelpHeritage") ?
      "leftcenter bottomright" :
      "bottomcenter topright";
    this.mHelpPopup.openPopup(aButton, position);
  };

  proto._isSnippetRelevant = function ODV_IsSnippetRelevant(aSnippet)
  {
    let expression = aSnippet.getAttribute("showif");
    if (!expression) {
      return true;
    }

    let evaluator = new CrummyEvaluator(this.mSubject.property);
    return evaluator.eval(expression);
  };

  /**
   * @param aGetSetMirror
   *        A mirror of the property's getter or setter, or null.
   * @param aClass
   *        The name of the to update on the getter/setter container.  I.e.,
   *        "hasGetter" or "hasSetter".
   * @param aLabel
   *        The name of the string from the string bundle to be used to
   *        indicate there is no getter/setter.
   * @param aLink
   *        The element in the viewer document that represents the
   *        getter/setter's value.
   */
  proto._updateAccessorInfo =
    function ODV_UpdateAccessorInfo(aGetSetMirror, aClass, aLabel, aLink)
  {
    if (aGetSetMirror) {
      aLink.textContent = "[object " + aGetSetMirror.internalClass + "]";
      this.mGetterSetterBox.classList.add(aClass);
      aLink.setAttribute("href", "");
    }
    else {
      aLink.textContent = this.mStringBundle.getString(aLabel);
      this.mGetterSetterBox.classList.remove(aClass);
      aLink.removeAttribute("href");
    }
  };
}
