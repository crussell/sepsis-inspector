/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * For localized, parameterized rich output in Object Detail's help popup.
 *
 * The problem: we want the localized help popups to be able to contain rich
 * output, but there's no existing standard solution to this.  String bundles
 * can have parameters, but they're limited to substituting in simple strings.
 *
 * Suppose, for example, you want a popup to have the following content:
 *
 *   The getter returned the *value* shown.
 *
 * ... and suppose the "value" bit should be specially formatted (e.g., a
 * link).
 *
 * Our approach is to allow localizers to write out string properties files as
 * usual, but where the specially formatted pieces are also parameterized:
 *
 *   getterGotValue.label = The getter returned the %S shown.
 *   getterValue.label = value
 *
 * Then we can write out the template:
 *
 *   <div id="getterGotValue" class="template">
 *     <a href id="getterValue" class="localized parameter"></a>
 *   </div>
 *
 * Instantiating a `HelpTemplater` will look up the string properties and
 * fill in the template-class nodes.  The parameters will be passed to the
 * string in the order they occur in the DOM.  To fill a template that has
 * multiple parameters, where the parameter values should appear in an order
 * in the completed template different to the order in which they occur
 * naturally, localizers should explicitly specify the the parameter index in
 * the property as usual (e.g., %1$S, %2$S, et cetera).
 *
 * The fully transformed template with all substitutions from above will end
 * up looking something like this:
 *
 *   <div id="getterGotValue" class="template">
 *     The getter returned the
 *     <a href id="getterValue" class="localized parameter">value</a>
 *     shown.
 *   </div>
 *
 * (NB: The linebreaks are for readability; `HelpTemplater` will first
 * completely blow away the template element's child list and won't actually
 * insert extra linebreaks into the DOM.  To be clear, it'll look like:
 *
 *   <div [...]>The getter returned the <a [...]>value</a> shown.</div>
 *
 * ... with all elements' attributes intact.)
 *
 * The "localized" class controls whether the templater will do a lookup on
 * that element's ID in the stringbundle and replace its text content with
 * what it finds.  Leaving it out will keep the parameter element's subtree
 * intact.
 *
 * NB: `HelpTemplater` will not automatically recursively treat a parameter
 * element as a template element and fill it in.  If you need this, you must
 * give the parameter element the "template" class, too, or manually use the
 * `fill` method.
 *
 * NB: Every child element of the template will be treated as a parametric
 * element, regardless of whether or not it actually has the "parameter"
 * class.
 *
 * XXX This whole thing should have probably been done with XSLT or something.
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.HelpTemplater = HelpTemplater;

//////////////////////////////////////////////////////////////////////////////

/**
 * NB: There is a restriction on the string bundle key names--they must end in
 * ".label".
 *
 * @param aStringBundle
 *        The stringbundle XUL element with a src pointing to the property
 *        file that the strings should be pulled from.
 * @param aContainer [optional]
 *        A DOM element containing "template"-class child nodes, as described
 *        above.  NB: The container's template children should have an ID
 *        that, when the string ".label" is appended to the ID, it names one
 *        of the keys in the bundle.
 * @constructor
 */
function HelpTemplater(aStringBundle, aContainer)
{
  this.mStringBundle = aStringBundle;

  if (aContainer) {
    this.mContainer = aContainer;
    for (let i = 0, n = aContainer.children.length; i < n; ++i) {
      this.fill(
        aContainer.children[i], this._getStringName(aContainer.children[i])
      );
    }
  }
}

{
  let proto = HelpTemplater.prototype;
  proto.mContainer = null;
  proto.mStringBundle = null;

  proto.TEXT_PART = HelpTemplater.TEXT_PART = 0;
  proto.PARAM_PART = HelpTemplater.PARAM_PART = 1;

  /**
   * Fill in given template element, as described above.
   *
   * @param aTemplateNode
   *        A "template"-class DOM element with the exact number of child
   *        elements as required by the associated string property.  E.g., the
   *        "getterGotValue" div from the example above.
   * @param aName
   *        A string that names the associated property key from this
   *        `HelpTemplater` instance's string bundle.  E.g., "getterGotValue"
   *        in the example above.
   */
  proto.fill = function HT_Fill(aTemplateNode, aName)
  {
    let paramNodes = [];
    while (aTemplateNode.hasChildNodes()) {
      let kid = aTemplateNode.removeChild(aTemplateNode.firstChild);
      if (kid.nodeType == kid.ELEMENT_NODE) {
        paramNodes.push(kid);
      }
    }
    
    let outputNodes = this.getFormattedOutputNodes(aName, paramNodes);
    for (let i = 0, n = outputNodes.length; i < n; ++i) {
      aTemplateNode.appendChild(outputNodes[i]);
    }
  };

  /**
   * @param aName
   * @param aParamNodes
   *        An array of elements whose text content will each be filled in
   */
  proto.getFormattedOutputNodes =
    function HT_GetFormattedOutputNodes(aName, aParamNodes)
  {
    let nodes = [];
    let parts = this._getStringParts(aName, aParamNodes.length);
    for (let i = 0, n = parts.length; i < n; ++i) {
      switch (parts[i].type) {
        case HelpTemplater.TEXT_PART:
          nodes.push(
            this.mContainer.ownerDocument.createTextNode(parts[i].content)
          );
        break;
        case HelpTemplater.PARAM_PART:
          // NB: String properties' parameters are 1-based, so a parameter's
          // corresponding node in the `aParamNodes` array is off by one from
          // the index in the part's `content`.
          nodes.push(aParamNodes[parts[i].content - 1]);
          this._localizeParamNode(nodes[nodes.length - 1]);
        break;
        default:
          throw new Error("wat.  Part type: " + parts[i].type);
        break;
      }
    }

    return nodes;
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  proto._getStringName = function HT_GetStringName(aNode)
  {
    return (aNode.getAttribute("string") || aNode.id) + ".label";
  };

  /**
   * We want to be able to extract from a string property the parameter
   * positions, given a property like:
   *
   * Ex. 1.
   *   label = foo %S bar
   *
   * So we split this into parts, create an info object for each one--each
   * containing some metadata--and pass these parts back to the caller packed
   * into an array.
   *
   * In the the info object, we include the parameter index, for the benefit
   * of strings with explicitly ordered parameters, such as in the example:
   *
   * Ex. 2.
   *   label = fum %2$S omg hax %1$S nom
   *
   * For this example, the caller would get back an array:
   *
   *   [
   *     { type: HelpTemplater.TEXT_PART,  content: "fum " },
   *     { type: HelpTemplater.PARAM_PART, content: 2 },
   *     { type: HelpTemplater.TEXT_PART,  content: " omg hax " },
   *     { type: HelpTemplater.PARAM_PART, content: 1 },
   *     { type: HelpTemplater.TEXT_PART,  content: " nom" }
   *   ]
   *
   * For implicitly ordered parameters, the parameter index is inferred, so
   * param parts will always have a `content` property.  The array returned
   * for Example 1 above would be:
   *
   *   [
   *     { type: HelpTemplater.TEXT_PART,  content: "foo " },
   *     { type: HelpTemplater.PARAM_PART, content: 1 },
   *     { type: HelpTemplater.TEXT_PART,  content: " bar" }
   *   ]
   *
   * NB: String bundle parameters are not 0-based.
   *
   * @param aName
   *        The name of the string property from the string bundle's property
   *        file.
   * @param aNumberParams
   *        The number of parameters the string property will contain.
   * @return
   *        An array of objects, each with a `type` property with value either
   *        `HelpTemplater.TEXT_PART` or `HelpTemplater.PARAM_PART`.  Each
   *        object will also have a `content` property, which will be the text
   *        for text parts, or the parameter index (1-based) for param parts.
   */
  proto._getStringParts = function HT_GetStringParts(aName, aNumberParams)
  {
    // We're actually going to cheat and still use `getFormattedString` so
    // that we don't have to write a whole parser for the properties file.
    //
    // The idea is that for the parameters, we'll inject some markers, then
    // from the resulting "formatted" string, look for those markers, carve
    // them out, and split up the string.  (There's a risk here, which is that
    // the marker we choose may collide with some of the string's actual
    // contents, so we end up splitting it in the wrong place.  We'll have to
    // take care of that.  See below.)
    //
    // The marker will need to be able to encode the parameter index, in the
    // case of strings with explicitly ordered parameters.  What's a good
    // candidate for this?  Well, the format for parameters with explicit
    // indexes itself (e.g., "%1$S") should work, and it's ugly enough that
    // there's also a low risk that we'll actually need to end up taking the
    // code path to handle the collision edge case.  With this, we're
    // essentially using `getFormattedString` to parse and validate the
    // properties file (including comments, key/value pairs, most escaping, et
    // cetera that we don't want to have to handle ourselves), but getting
    // pretty close to a property's "raw", unsubstituted form.
    let markers = [];
    for (let i = 0; i < aNumberParams; ++i ) {
      markers.push("%" + (i + 1) + "$S");
    }

    let value;
    try {
      value = this.mStringBundle.getFormattedString(
        aName, markers, markers.length
      );
    }
    catch (ex) {
      throw new Error("Error getting string " + aName + ": " + ex);
    }

    let positions = [];
    for (let i = 0; i < aNumberParams; ++i) {
      let index = this._findMarker(value, markers[i]);
      // assert(!(index < 0));
      positions.push({ index: index, marker: markers[i] });
    }

    positions.sort(function (a, b) {
      return a.index - b.index;
    });

    let parts = [];
    let start = 0;
    for (let i = 0; i < aNumberParams; ++i) {
      let marker = positions[i].marker;
      let text = value.substring(start, positions[i].index);
      let paramIndex = Number(marker.substring(1, marker.indexOf("$")));

      parts.push({ type: HelpTemplater.TEXT_PART, content: text });
      parts.push({ type: HelpTemplater.PARAM_PART, content: paramIndex });

      start += text.length + marker.length;
    }
    
    {
      let endText = value.substring(start);
      parts.push({ type: HelpTemplater.TEXT_PART, content: endText });
    }

    return parts;
  };

  /**
   * @param aString
   *        The string in which to search for the marker.
   * @param aMarker
   *        Some string beginning with "%", which is expected to be in G
   * @return
   *        The index of the first occurrence of `aMarker` in `aString`,
   *        ignoring escaped occurrences.
   */
  proto._findMarker = function HT_FindMarker(aString, aMarker)
  {
    // There's the risk--outlined before--that there's an escaped occurrence
    // of this marker in the string and we run into that.  Avoiding this is
    // fairly straightforward.  Just backtrack from `index` to check how many
    // "%" characters precede it in the string.  One indicates it's escaped
    // and we should keep looking.  This goes for any other odd number,
    // right?, because if we just look back and find a "%", it's possible that
    // *that* one is escaped with another "%" preceding *it*.  Zero (or any
    // even number, for a similar reason) implies we've found it.
    //
    // XXX Actually, that shouldn't work, because if it were escaped,
    // `getFormattedString` will be giving it back to us in its unescaped
    // form, right? --crussell
    let searchFrom = 0;
    lookingForMarker:
    while (true) {
      let index = aString.indexOf(aMarker, searchFrom);
      if (index < 0) {
        return -1;
      }

      backtracking:
      for (let preceding = index - 1; preceding >= 0; --i) {
        if (aString[preceding] == "%") {
          continue backtracking;
        }

        // NB: We mentioned odd and even above, but note that our modulus will
        // be off-by-one, because `preceding` will be the index of the nearest
        // preceding character that _isn't_ "%".
        if ((index - preceding) % 2) {
          return index;
        }

        searchFrom = index + 1;
        continue lookingForMarker;
      }
    }

    return -1;
  };

  proto._localizeParamNode = function HT_LocalizeParamNode(aNode)
  {
    if (!aNode.classList.contains("localized")) {
      return;
    }

    if (aNode.classList.contains("template")) {
      this.fill(aNode, this._getStringName(aNode));
    }
    else {
      try {
        aNode.textContent = this.mStringBundle.getString(
          this._getStringName(aNode)
        );
      }
      catch (ex) {
        throw new Error(
          "Error getting string " + this._getStringName(aNode) + ": " + ex
        );
      }
    }
  };
}
