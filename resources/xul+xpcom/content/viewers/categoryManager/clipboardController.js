/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Command controller for the categoryManager viewer's copy-to-clipboard.
 *
 * NB: This assumes the tree is seltype="single".
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.ClipboardController = ClipboardController;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");

var { TheFakeClipboardHack } = require("hacks/theFakeClipboardHack");

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aViewer
 *        `CategoryManagerViewer` we are created for.
 * @param aTree
 *        The viewer's XUL tree element.
 * @param aView
 *        `CategoriesView` that will be shown in the tree.
 */
function ClipboardController(aViewer, aTree, aView)
{
  this.mViewer = aViewer;
  this.mTree = aTree;
  this.view = aView;

  aTree.controllers.appendController(this);
}

{
  let proto = ClipboardController.prototype;
  proto.mAttributesPasteHelper = null;
  proto.mTree = null;
  proto.mViewer = null;

  proto.QueryInterface = XPCOMUtils.generateQI([ "nsIController" ]);

  proto.destroy = function CC_Destroy()
  {
    this.mTree.controllers.removeController(this);
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIController

  proto.supportsCommand = function CC_SupportsCommand(aName)
  {
    switch (aName) {
      case "cmd_copy":
      case "cmd_copyName":
      case "cmd_copyValue":
        return true;
      break;
    }

    return false;
  };

  proto.isCommandEnabled = function CC_IsCommandEnabled(aName)
  {
    let rowData = this.mViewer.getTargetSynthetic();
    switch (aName) {
      case "cmd_copy":
      case "cmd_copyName":
        return !!rowData;
      break;
      case "cmd_copyValue":
        return rowData && "value" in rowData;
      break;
    }

    return false;
  };

  proto.doCommand = function CC_DoCommand(aName)
  {
    let rowData = this.mViewer.getTargetSynthetic();
    switch (aName) {
      case "cmd_copy":
        if (this.isCommandEnabled("cmd_copyValue")) {
          TheFakeClipboardHack.copyContents(
            "category " + rowData.parentRowData.name + " " + rowData.name +
            " " + rowData.value
          );
          break;
        }
      // Else, fall through.
      case "cmd_copyName":
        TheFakeClipboardHack.copyContents(rowData.name);
      break;
      case "cmd_copyValue":
        TheFakeClipboardHack.copyContents(rowData.value);
      break;
    }
  };

  proto.onEvent = function CC_OnEvent(aName)
  {
  };
}
