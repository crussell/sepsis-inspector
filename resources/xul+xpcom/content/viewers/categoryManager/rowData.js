/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.CategoryRowData = CategoryRowData
exports.EntryRowData = EntryRowData

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { CategoryManager } = require("xpcom/categoryManager");

//////////////////////////////////////////////////////////////////////////////

function RowData(aName)
{
  this.name = aName;
}

{
  let proto = RowData.prototype;
  proto.level = null;
  proto.name = null;
}

//////////////////////////////////////////////////////////////////////////////

function CategoryRowData(aName)
{
  RowData.call(this, aName);

  this.level = 0;
  this.isOpen = false;
}

{
  let proto = Object.create(RowData.prototype);
  CategoryRowData.prototype = proto;
  proto.constructor = CategoryRowData;

  proto.isOpen = null;
}

//////////////////////////////////////////////////////////////////////////////

function EntryRowData(aName, aCategoryRowData)
{
  RowData.call(this, aName);

  this.level = 1;
  this.parentRowData = aCategoryRowData;
  this.value = CategoryManager.getCategoryEntry(aCategoryRowData.name, aName);
}

{
  let proto = Object.create(RowData.prototype);
  EntryRowData.prototype = proto;
  proto.constructor = EntryRowData;

  proto.value = null;
  proto.parentRowData = null;
}
