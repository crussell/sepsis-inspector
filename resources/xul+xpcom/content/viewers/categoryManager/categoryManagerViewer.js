/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Viewer for the XPCOM Category Manager service.
 * 
 * TODO:
 *   - Allow deleting entries, entire categories from the UI.
 *   - Allow renaming entries from the UI.
 *   - Allow inserting new entries from the UI.
 *   - Lose `seltype="single"` and support multi-select.  NB: That includes
 *     discriminating between select events that are genuinely for selection
 *     and those for deselection.
 *
 * @see registration/theViewerRegistry
 */

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { CategoriesView } = require("./categoriesView");
var { ClipboardController } = require("./clipboardController");

var { CategoryManager } = require("xpcom/categoryManager");

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aPanel
 *        The inspectorpanel XUL element that the viewer is being loaded into.
 */
function CategoryViewer(aPanel)
{
  // This is where our `updateCommand` method comes from.
  aPanel.commandMediator.decorate(this);

  this.mTree = document.getElementById("olCategories");
  this.mCategoriesView = new CategoriesView(CategoryManager);
  this.mTree.view = this.mCategoriesView;

  this.mClipboardController = new ClipboardController(
    this, this.mTree, this.mCategoriesView
  );

  this.mTree.addEventListener("select", this, false);
}

{
  let proto = CategoryViewer.prototype;
  proto.mCategoriesView = null;
  proto.mTree = null;

  proto.updateContextMenuItems = function CVr_UpdateContextMenuItems(aPopup)
  {
    let kids = aPopup.childNodes;
    for (let i = 0, n = kids.length; i < n; ++i) {
      this.updateCommand(kids[i].command);
    }
  };

  /**
   * NB: We don't use a supplier.  These aren't actually registered
   * synthetics.  Oh well.
   *
   * @return
   *        A `CategoryRowData` or `EntryRowData` instance.
   */
  proto.getTargetSynthetic = function CVr_GetTargetSynthetic()
  {
    let currentIndex = this.mCategoriesView.selection.currentIndex;
    return (currentIndex >= 0) ?
      this.mCategoriesView.getSynthetic(currentIndex) :
      null;
  };

  ////////////////////////////////////////////////////////////////////////////
  //// "interface" sepsisIViewer

  proto.target = null;

  proto.inspectApplication = function CVr_InspectApplication()
  {
    // No-op.
  };

  proto.destroy = function CVr_Destroy()
  {
    this.mClipboardController.destroy();
    this.mCategoriesView.destroy();
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIDOMEventListener

  proto.handleEvent = function CVr_HandleEvent(aEvent)
  {
    switch (aEvent.type) {
      case "select":
        this._onSelect();
      break;
      default:
        throw new Error("wat.  Event: " + aEvent.type);
      break;
    };
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  proto._onSelect = function CVr_OnSelect()
  {
    this.updateCommand("cmd_copy");
    this.updateCommand("cmd_copyName");
    this.updateCommand("cmd_copyValue");
  };
}
