/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Two-level tree view for showing categories and their entries' values.
 *
 * Algorithmically, the implementation here is poor, given our use of linear
 * search and the unsophisticated approach to entry changes.  It's expected
 * not to be a big deal though, since, in contrast to something like a DOM
 * tree view,
 *
 *   a) even a fully exploded representation of the entire category manager
 *      tree is relatively small, and
 *   b) entry additions/removals/changes should be rare, unlike DOM mutations.
 *
 * If it's any consolation, originally I wrote 90% of a cleverer
 * implementation and threw it out for this simpler one after taking note of
 * the above.  I did it for you, weary traveller. --crussell
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.CategoriesView = CategoriesView;

const { nsISupportsCString } = Components.interfaces;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { CommonTreeView } = require("utils/commonTreeView");
var { CategoryRowData, EntryRowData } = require("./rowData");

var { ObserverService } = require("xpcom/observerService");

//////////////////////////////////////////////////////////////////////////////

function CategoriesView(aCategoryManager)
{
  CommonTreeView.call(this);

  this.mCategoryManager = aCategoryManager;

  // Build the initial row data: one row for every category.  We won't be
  // using `mLevels` and `mOpenStates`.  (See `CommonTreeView`.)
  this.mRowData = [];

  let e = this.mCategoryManager.enumerateCategories();
  while (e.hasMoreElements()) {
    let categoryName =
      String(e.getNext().QueryInterface(nsISupportsCString));
    let rowData = new CategoryRowData(categoryName);
    this.mRowData.push(rowData);
  }

  this.addManagedRowData(this.mRowData);

  this.rowCount = this.mRowData.length;

  ObserverService.addObserver(this, "xpcom-category-entry-added", false);
  ObserverService.addObserver(this, "xpcom-category-entry-removed", false);
  ObserverService.addObserver(this, "xpcom-category-cleared", false);
}

{
  let proto = Object.create(CommonTreeView.prototype);
  CategoriesView.prototype = proto;
  proto.constructor = CategoriesView;

  proto.mCategoryManager = null;
  proto.mRowData = null;

  proto.destroy = function CV_Destroy()
  {
    ObserverService.removeObserver(this, "xpcom-category-entry-added");
    ObserverService.removeObserver(this, "xpcom-category-entry-removed");
    ObserverService.removeObserver(this, "xpcom-category-cleared");
  };

  proto.expandRow = function CV_ExpandRow(aRow)
  {
    // assert(this.mRowData[aRow] instanceof CategoryRowData);
    let categoryRowData = this.mRowData[aRow];
    let e = this.mCategoryManager.enumerateCategory(categoryRowData.name);
    let entries = [];
    while (e.hasMoreElements()) {
      let name = String(e.getNext().QueryInterface(nsISupportsCString));
      entries.push(new EntryRowData(name, categoryRowData));
    }

    CommonTreeView.arrayCopy(entries, this.mRowData, aRow + 1);

    return entries.length;
  };

  /**
   * NB: We don't use the supplier supplier pattern, so these aren't actually
   * registered synthetics.  (And if we were using real synthetics, these
   * `RowData` instances wouldn't be them.)
   *
   * @param aIndex
   * @return
   *        A `RowData` instance.
   */
  proto.getSynthetic = function CV_GetSynthetic(aIndex)
  {
    return this.mRowData[aIndex];
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIObserver

  proto.observe = function CV_Observe(aSubject, aTopic, aData)
  {
    switch (aTopic) {
      case "xpcom-category-entry-added":
      case "xpcom-category-entry-removed":
      case "xpcom-category-cleared":
        this._onEntryChanged(aData);
      break;
      default:
        throw new Error("wat.  topic: " + aTopic);
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsITreeView

  proto.getCellText = function CV_GetCellText(aRow, aColumn)
  {
    let rowData = this.mRowData[aRow];
    switch (aColumn.id) {
      case "colName":
        return rowData.name;
      break;
      case "colValue":
        if (rowData instanceof EntryRowData) {
          return rowData.value;
        }
      break;
    }

    return "";
  };

  proto.isContainer = function CV_IsContainer(aRow)
  {
    return this._isCategoryRow(aRow);
  };

  proto.isContainerEmpty = function CV_IsContainerEmpty(aRow)
  {
    // There's no such thing as an empty category.  They're either there, with
    // one or more entries, or they don't exist.
    return false;
  };

  // Override `CommonTreeView`-implemented methods, since we're using
  // `mRowData` instead of `mLevels` and `mOpenStates`.

  proto.getLevel = function CV_GetLevel(aRow)
  {
    return this.mRowData[aRow].level;
  };

  proto.isContainerOpen = function CV_IsContainerOpen(aRow)
  {
    // assert(this.isContainer(aRow);
    return this.mRowData[aRow].isOpen;
  };

  proto.setRowOpenState = function CV_SetRowOpenState(aRow, aOpenState)
  {
    // assert(this.isContainer(aRow);
    this.mRowData[aRow].isOpen = aOpenState;
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  /**
   * The subject for the "xpcom-category-entry-removed" topic is so unreliable
   * that it's useless.  See bug 956857.  So the options were to write one
   * method for -entry-added, where we can trust the notification subject to
   * tell us the name of the entry, and one for -entry-removed, where we have
   * to figure out what was removed; or we can just have one method that's
   * general enough to handle both.  I chose the latter.  Bonus: it handles
   * "xpcom-category-cleared" for free. --crussell
   */
  proto._onEntryChanged =
    function CV_OnEntryChanged(aCategory)
  {
    // General plan:
    // If the category row is open, we'll collapse and re-expand it, then
    // compare the entries with what was there before.
    //
    // There are two case that the above doesn't take care of:
    //   - We're adding a new entry and no row for the given category exists
    //     yet.  We'll have to create one.
    //   - The change is the deletion of the category's last entry, or the
    //     deletion of the category itself. We'll need to remove its row and
    //     its child rows, if any.
    //
    // Whichever path we take, we'll have to finish up by notifying the tree
    // box object with `rowCountChanged`.  If multiple rows are affected by
    // the changes, they'll all be consecutive, so only one call is necessary.
    let changeIndex, changeCount;
    let categoryIndex = this._getCategoryRowIndex(aCategory);
    if (categoryIndex < 0) {
      // Exception #1.  We need to update the list.  Figure out where it goes
      // and put it in.
      let e = this.mCategoryManager.enumerateCategories();
      changeIndex = 0;
      while (e.hasMoreElements()) {
        let current = String(
          e.getNext().QueryInterface(nsISupportsCString).toString()
        );
        if (current == aCategory) {
          let rowData = new CategoryRowData(aCategory);
          this.mRowData.splice(changeIndex, 0, rowData);
          break;
        }

        ++changeIndex;
      }

      // assert(changeIndex < this.mRowData.length);
      changeCount = 1;
    }
    else {
      let e = this.mCategoryManager.enumerateCategory(aCategory);
      if (!e.hasMoreElements()) {
        // Exception #2.  Remove the entry row and the category row, too.
        changeIndex = categoryIndex;
        // `changeCount` is what gets passed to `rowCountChanged` below, and
        // this is a removal, so it needs to be negative.
        changeCount =
          categoryIndex -
          this.getNextSiblingIndex(categoryIndex, categoryIndex, true);
        this.mRowData.splice(changeIndex, -changeCount);
      }
      else if (this.mRowData[categoryIndex].isOpen) {
        let rangeEndIndex =
          this.getNextSiblingIndex(categoryIndex, categoryIndex, true);
        let beforeEntries =
          this.mRowData.slice(categoryIndex + 1, rangeEndIndex);

        changeCount = -this.collapseRow(categoryIndex) +
                      this.expandRow(categoryIndex);

        if (!changeCount) {
          // It was a replacement.  This method will actually be called twice
          // for replacements: once for -entry-removed and again for
          // -entry-added.  Given bug 956857, detecting and preventing this
          // complicates things a lot.  So we just let it happen.  Oh well.
          let column = this.mTreeBoxObject.columns.getNamedColumn("colValue");
          this.mTreeBoxObject.invalidateColumnRange(
            categoryIndex + 1,
            categoryIndex + 1 + beforeEntries.length,
            column
          );
          return;
        }

        // It was an addition to an already-open category or a removal from one
        // where other entries are still present.  Walk through the entries we
        // copied before and the ones that are there now to figure out which is
        // the affected entry.
        changeIndex = 0;
        while (changeIndex < beforeEntries.length) {
          if (beforeEntries[changeIndex].name !=
              this.mRowData[changeIndex + categoryIndex + 1].name) {
            break;
          }
          ++changeIndex;
        }
        changeIndex += categoryIndex + 1;
      }
      else {
        // It's some addition, replacement, or removal from some category
        // whose row isn't open, so we don't need to do anything.  (If it's a
        // removal, it's one that didn't result in the category being
        // completely cleared of entries, so we don't need to remove the
        // category row.)
        return;
      }
    }

    this.rowCount += changeCount;
    this.mTreeBoxObject.rowCountChanged(changeIndex, changeCount);
  };

  proto._getCategoryRowIndex = function CV_GetCategoryRowIndex(aCategory)
  {
    // Dumb linear search.
    for (let i = 0, n = this.rowCount; i < n; ++i) {
      if (this._isCategoryRow(i) && this.mRowData[i].name == aCategory) {
        return i;
      }
    }

    return -1;
  };

  proto._isCategoryRow = function CV_IsCategoryRow(aRow)
  {
    return this.mRowData[aRow] instanceof CategoryRowData;
  };
}
