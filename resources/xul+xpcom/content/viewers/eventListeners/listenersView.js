/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.ListenersView = ListenersView;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { ListenerRowData } = require("./listenerRowData");
var { StubTreeView } = require("utils/stubTreeView");

var { EventListenerService } = require("xpcom/eventListenerService");

//////////////////////////////////////////////////////////////////////////////

function ListenersView(aEventTarget)
{
  this.mRowData = [];
  let infos = EventListenerService.getListenerInfoFor(aEventTarget);
  for (let i = 0, n = infos.length; i < n; ++i) {
    this.mRowData.push(new ListenerRowData(infos[i]));
  }

  this.rowCount = this.mRowData.length;
}

{
  let proto = Object.create(StubTreeView.prototype);
  ListenersView.prototype = proto;
  proto.constructor = ListenersView;

  proto.mRowData = null;
  proto.mListenerService = null;
  proto.mTreeBoxObject = null;

  proto.getRowObject = function LV_GetRowObject(aRow)
  {
    let rowData = this.mRowData[aRow];
    return ("listenerObject" in rowData.info) ?
      rowData.info.listenerObject :
      null;
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsITreeView

  proto.getCellText = function LV_GetCellText(aRow, aColumn)
  {
    let rowData = this.mRowData[aRow];
    switch (aColumn.id) {
      case "colType":
        return rowData.info.type;
      break;
      case "colCapturing":
        return rowData.info.capturing;
      break;
      case "colAllowsUntrusted":
        return rowData.info.allowsUntrusted;
      break;
      case "colSystem":
        return rowData.info.inSystemEventGroup;
      break;
      case "colSource":
        return rowData.source;
      break;
    }

    return "";
  };

  proto.setTree = function LV_SetTree(aTreeBoxObject)
  {
    this.mTreeBoxObject = aTreeBoxObject;
  };
}
