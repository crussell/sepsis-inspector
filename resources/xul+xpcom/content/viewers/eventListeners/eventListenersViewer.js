/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Viewer to show the DOM event listeners attached to any event target.
 *
 * TODO
 * - nsIEventListenerInfo's `listenerObject` is null for XBL handlers.  We can
 *   scrape it from the XML definition, though, like DOMi (2)'s xblBinding
 *   viewer does.
 *
 * @see registration/theViewerRegistry
 */

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { ListenersView } = require("./listenersView");

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aPanel
 *        The inspectorpanel XUL element that the viewer is being loaded into.
 */
function EventListenersViewer(aPanel)
{
  this.mPanel = aPanel;

  this.mTree = document.getElementById("olListeners");

  this.mTree.addEventListener("select", this, false);
}

{
  let proto = EventListenersViewer.prototype;
  proto.mListenersView = null;
  proto.mPanel = null;
  proto.mSubject = null;
  proto.mTree = null;

  ////////////////////////////////////////////////////////////////////////////
  //// "interface" sepsisIViewer

  proto.target = null;

  /**
   * @param a_x_Listener
   *        NB: `a_x_Listener` is untrusted.
   */
  proto.inspectObject = function ELV_InspectObject(a_x_Listener)
  {
    this.mSubject = new XPCNativeWrapper(a_x_Listener);
    this.mListenersView = new ListenersView(this.mSubject);
    this.mTree.view = this.mListenersView;
    if (this.mListenersView.rowCount) {
      this.mListenersView.selection.select(0);
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIDOMEventListener

  proto.handleEvent = function ELV_HandleEvent(aEvent)
  {
    switch (aEvent.type) {
      case "select":
        this._onSelect();
      break;
      default:
        throw new Error("wat.  Event: " + aEvent.type);
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  proto._onSelect = function ELV_OnSelect()
  {
    let newTarget = this.mListenersView.getRowObject(
      this.mListenersView.selection.currentIndex
    );

    this.mPanel.changeTarget(newTarget);
  };
}
