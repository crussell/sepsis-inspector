/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.ListenerRowData = ListenerRowData;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { jsValueToString } = require("utils/jsValueToString");

//////////////////////////////////////////////////////////////////////////////

function ListenerRowData(aInfo)
{
  this.info = aInfo;

  if ("listenerObject" in aInfo) {
    if (typeof(aInfo.listenerObject) == "function") {
      this.source = jsValueToString(aInfo.listenerObject);
    }
    else {
      // XXX Not sure that this is really what we want to do.  Good enough
      // for now, though. --crussell
      try {
        this.source = "{\n\n/* ... */\n\n" +
                       jsValueToString(aInfo.listenerObject.handleEvent) +
                       "\n\n/* ... */\n\n}";
      }
      catch (ex) {
        this.source = "";
      }
    }
  }
  else {
    this.source = aInfo.toSource() || "";
  }
};

{
  let proto = ListenerRowData.prototype;
  proto.info = null;
  proto.source = null;
}
