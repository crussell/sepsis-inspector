/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.EventsView = EventsView;

const Cc = Components.classes;
const Ci = Components.interfaces;
const Cu = Components.utils;
const kELSContractID = "@mozilla.org/eventlistenerservice;1";

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { StubTreeView } = require("utils/stubTreeView");
var { EventRowData } = require("./eventRowData.js");
          
//////////////////////////////////////////////////////////////////////////////

function EventsView(aEventTarget)
{
  this.mEventTarget = aEventTarget;

  this.mEventListenerService =
    Cc[kELSContractID].getService(Ci.nsIEventListenerService);

  this.mRowData = [];

  this.mEventListenerService.addListenerForAllEvents(aEventTarget, this,
                                                     false, true, false);
};

{
  let proto = Object.create(StubTreeView.prototype);
  EventsView.prototype = proto;
  proto.constructor = EventsView;

  proto.mEventTarget = null;
  proto.mEventListenerService = null;
  proto.mRowData = null;
  proto.mTreeBoxObject = null;

  proto.destroy = function EV_Destroy()
  {
    this.mEventListenerService.removeListenerForAllEvents(this.mEventTarget,
                                                          this, false, false);
  };

  proto.getRowObject = function EV_GetRowObject(aRow)
  {
    return this.mRowData[aRow].event;
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsITreeView

  proto.setTree = function EV_SetTree(aTreeBoxObject)
  {
    this.mTreeBoxObject = aTreeBoxObject;
  };

  proto.getCellText = function EV_GetCellText(aRow, aColumn)
  {
    switch (aColumn.id) {
      case "colType":
        return this.mRowData[aRow].type;
      break;
      default:
        throw new Error("wat.  column: " + aColumn.id);
      break;
    }

    return "";
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIDOMEventListener

  proto.handleEvent = function EV_HandleEvent(aEvent)
  {
    this._addEvent(aEvent);
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  proto._addEvent = function EV_AddEvent(aEvent)
  {
    this.mRowData.push(new EventRowData(aEvent));
    ++this.rowCount;
    this.mTreeBoxObject.rowCountChanged(this.rowCount - 1, 1);
    this.mTreeBoxObject.ensureRowIsVisible(this.rowCount - 1);
  };
}
