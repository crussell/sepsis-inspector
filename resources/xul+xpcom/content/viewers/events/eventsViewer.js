/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Viewer to show the DOM events that arrive at event targets (e.g. elements).
 *
 * @see registration/theViewerRegistry
 */

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { EventsView } = require("./eventsView");

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aPanel
 *        The inspectorpanel XUL element that the viewer is being loaded into.
 */
function EventsViewer(aPanel)
{
  this.mPanel = aPanel;

  this.mTree = document.getElementById("olEvents");
  this.mTree.addEventListener("select", this, false);
}

{
  let proto = EventsViewer.prototype;
  proto.mPanel = null;
  proto.mSubject = null;

  ////////////////////////////////////////////////////////////////////////////
  //// "interface" sepsisIViewer

  proto.target = null;

  /**
   * @param a_x_Event
   *        NB: `a_x_Event` is untrusted.
   */
  proto.inspectObject = function EVr_InspectObject(a_x_Event)
  {
    if (this.mEventsView) {
      this.mEventsView.destroy();
    }

    this.mSubject = new XPCNativeWrapper(a_x_Event);
    this.mEventsView = new EventsView(this.mSubject);
    this.mTree.view = this.mEventsView;
    this.mSubject = this.mSubject;
    return this.mSubject;
  };

  proto.destroy = function EVr_Destroy()
  {
    this.mEventsView.destroy();
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIEventListener

  proto.handleEvent = function EVr_HandleEvent(aEvent)
  {
    switch (aEvent.type) {
      case "select":
        this._onSelect();
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  proto._onSelect = function EVr_OnSelect()
  {
    this.mPanel.changeTarget(
      this.mEventsView.getRowObject(this.mEventsView.selection.currentIndex)
    );
  };
}
