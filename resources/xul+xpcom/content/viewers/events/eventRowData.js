/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Objects created for each row in the events viewer's tree.
 *
 * By having the view manage these objects instead of just a list of the
 * events themselves, it acts as a simple caching mechanism.  Events can
 * expire, which is especially common after an unload.
 *
 * We may not be able to further inspect the events after expiration, but at
 * the very least, we want to be able to show the name of the event that
 * occurred.  This isn't possible if the view is relying on live event
 * objects.  So we cache the `type` and have the view defer to our own record,
 * rather than the event itself.
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.EventRowData = EventRowData;

//////////////////////////////////////////////////////////////////////////////

function EventRowData(aEvent)
{
  this.event = aEvent;
  this.type = aEvent.type;
}

{
  let proto = EventRowData.prototype;
  this.event = null;
  proto.type = null;
}
