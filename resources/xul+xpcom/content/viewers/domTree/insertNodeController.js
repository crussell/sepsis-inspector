/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Command controller for the domTree viewer's cmd_insertNode.
 *
 * @see InsertNodeFlow
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.InsertNodeController = InsertNodeController;;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");

var { InsertNodeFlow } = require("./insertNodeFlow");
var { InsertNodeTransaction } = require("transactions/insertNodeTransaction");
var { CommonTreeView } = require("utils/commonTreeView");
var { NotificationManager } = require("utils/notificationManager");
var { TreeEditWatcher } = require("utils/treeEditWatcher");

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aViewer
 *        `DOMTreeViewer` we are created for.
 * @param aTree
 *        The viewer's XUL tree element.
 */
function InsertNodeController(aViewer, aTree)
{
  this.mNotificationManager = new NotificationManager(this);

  this.mViewer = aViewer;
  this.mTree = aTree;

  this.mTreeEditWatcher = new TreeEditWatcher(aTree);

  aTree.controllers.appendController(this);
}

{
  let proto = InsertNodeController.prototype;
  proto.mNotificationManager = null;
  proto.mTree = null;
  proto.mTreeEditWatcher = null;
  proto.mViewer = null;

  proto.QueryInterface = XPCOMUtils.generateQI([ "nsIController" ]);

  proto.view = null;

  proto.destroy = function INC_Destroy()
  {
    this.mTreeEditWatcher.destroy();
    this.mTree.controllers.removeController(this);
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIController

  proto.supportsCommand = function INC_SupportsCommand(aName)
  {
    switch (aName) {
      case "cmd_insertNode":
      case "cmd_insertNodeBefore":
      case "cmd_insertNodeAfter":
      case "cmd_insertChildNode":
        return true;
      break;
    }

    return false;
  };

  proto.isCommandEnabled = function INC_IsCommandEnabled(aName)
  {
    switch (aName) {
      case "cmd_insertNode":
      case "cmd_insertNodeBefore":
      case "cmd_insertNodeAfter":
        {
          let node = CommonTreeView.getSelectedRowObject(this.view);
          return node && node.parentNode &&
                 node.parentNode.nodeType == node.ELEMENT_NODE;
        }
      break;
      case "cmd_insertChildNode":
        {
          let node = CommonTreeView.getSelectedRowObject(this.view);
          return node && node.nodeType == node.ELEMENT_NODE;
        }
      break;
    }

    return false;
  };

  proto.doCommand = function INC_DoCommand(aName)
  {
    let selectedNode = CommonTreeView.getSelectedRowObject(this.view);

    switch (aName) {
      case "cmd_insertNode":
      case "cmd_insertNodeBefore":
        this._enterFlow(selectedNode.parentNode, selectedNode);
      break;
      case "cmd_insertNodeAfter":
        this._enterFlow(selectedNode.parentNode, selectedNode.nextSibling);
      break;
      case "cmd_insertChildNode":
        this._enterFlow(selectedNode, selectedNode.firstChild);
      break;
    }
  };

  proto.onEvent = function INC_OnEvent(aName)
  {
  };

  ////////////////////////////////////////////////////////////////////////////
  //// Notifications

  proto.handleNotification = function INC_HandleNotification(aName, aData)
  {
    switch (aName) {
      case "flowCancelled":
        this._onFlowCancelled(aData.rowData);
      break;
      case "flowCompleting":
        this._onFlowCompleting(aData.rowData);
      break;
      default:
        throw new Error("wat.  Notification: " + aName);
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  proto._enterFlow = function INC_EnterFlow(aParentNode, aSiblingNode)
  {
    let flow = new InsertNodeFlow(
      this.mViewer, this.mTree, this.view, this.mTreeEditWatcher
    );

    flow.addObserver("flowCancelled", this.mNotificationManager);
    flow.addObserver("flowCompleting", this.mNotificationManager);

    this.view.insertNodeFlow = flow;
    flow.beginFlow(aParentNode, aSiblingNode);
  };

  proto._onFlowCancelled = function INC_OnFlowCancelled(aRowData)
  {
    this._forgetFlow(this.view.insertNodeFlow);
    this.view.onFlowCancelled(aRowData);
  };

  proto._onFlowCompleting = function INC_OnFlowCompleting(aRowData)
  {
    this._forgetFlow(this.view.insertNodeFlow);

    let attributes = Object.create(null);
    if (aRowData.id) {
      attributes.id = aRowData.id;
    }

    // Create the transaction.  We attach it to the dummy row so that the view
    // can properly handle the DOM mutation that will occur; it will need to
    // replace the dummy row instead of otherwise inserting a new row.
    aRowData.transaction = new InsertNodeTransaction(
      aRowData.parentObject, aRowData.parentObject.namespaceURI,
      aRowData.name, attributes, aRowData.siblingObject
    );
    this.mViewer.commandMediator.doTransaction(aRowData.transaction);
  };
  
  proto._forgetFlow = function INC_StopObserving(aFlow)
  {
    aFlow.removeObserver("flowCancelled", this.mNotificationManager);
    aFlow.removeObserver("flowCompleting", this.mNotificationManager);
    this.view.insertNodeFlow = null;
  };
}
