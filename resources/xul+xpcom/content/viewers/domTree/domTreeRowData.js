/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Encapsulates some fields used for a row in the domTree viewer.
 *
 * NB: `node` is allowed to be null.
 * @see DOMTreeDummyRowData
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.DOMTreeRowData = DOMTreeRowData;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { NodeTypeHelper } = require("utils/nodeTypeHelper");

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aNode
 * @param aDOMParentData
 *        NB: `aDOMParentData` is not necessarily the same as `aRowParentData`
 *        because of anonymous content.
 * @param aRowParentData [optional]
 *        NB: `aRowParentData` is not necessarily the same as `aDOMParentData`
 *        because of anonymous content.
 */
function DOMTreeRowData(aNode, aDOMParentData, aRowParentData)
{
  this.node = aNode;
  this.mDOMParentData = aDOMParentData;
  this.isOpen = false;
  this.level = (aRowParentData) ?
    aRowParentData.level + 1 :
    0;
  this.properties = this._computeNodeCellProperties();
}

{
  let proto = DOMTreeRowData.prototype;
  proto.mDOMParentData = null;

  proto.isOpen = null;
  proto.level = null;
  proto.node = null;
  proto.properties = null;

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  /**
   * Compute the properties for styling this node in the tree view.  Note that
   * the properties computed here correspond to those returned by
   * `getCellProperties` for the nodeName cell in this row; this row's cells
   * outside the nodeName column may be styled differently.
   */
  proto._computeNodeCellProperties = function RD_ComputeNodeCellProperties()
  {
    let properties = [];
    if (!this.node) {
      return properties;
    }

    properties.push(NodeTypeHelper.getName(this.node.nodeType));

    // Figure out if this node is anonymous or not.  If we already determined
    // that the node's DOM parent is anonymous, then it is, too.
    // XXX The checks we're doing now should mean we get no false positives
    // (for "is this node anonymous?"), but we can get false negatives in the
    // case of native anonymous nodes...
    if ((this.mDOMParentData &&
         this.mDOMParentData.properties.indexOf("anonymous") >= 0) ||
        (this.node.ownerDocument &&
         this.node.ownerDocument.getBindingParent(this.node))) {
      properties.push("anonymous");
    }

    return properties;
  };
}
