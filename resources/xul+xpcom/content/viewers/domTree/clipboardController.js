/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Command controller for the domTree viewer's interaction with the clipboard.
 *
 * TODO
 *   - When the owner document goes away, we probably want to convert any live
 *     nodes on the clipboard to serialized form (or store it a stable
 *     fragment)?
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.ClipboardController = ClipboardController;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");

var { PasteNodeTransaction } = require("transactions/pasteNodeTransaction");
var { AttributesPasteHelper } = require("utils/attributesPasteHelper");
var { CommonTreeView } = require("utils/commonTreeView");

var { TheFakeClipboardHack } = require("hacks/theFakeClipboardHack");

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aViewer
 *        `DOMTreeViewer` we are created for.
 * @param aTree
 *        The viewer's XUL tree element.
 * @param aAttributesPasteHelper
 *        `AttributesPasteHelper` instance.
 */
function ClipboardController(aViewer, aTree, aAttributesPasteHelper)
{
  this.mViewer = aViewer;
  this.mTree = aTree;
  this.mAttributesPasteHelper = aAttributesPasteHelper;

  aTree.controllers.appendController(this);
}

{
  let proto = ClipboardController.prototype;

  proto.QueryInterface = XPCOMUtils.generateQI([ "nsIController" ]);

  proto.mAttributesPasteHelper = null;
  proto.mTree = null;
  proto.mViewer = null;

  proto.view = null;

  proto.NODE_FLAVOR = ClipboardController.NODE_FLAVOR =
    "application/x-moz-node";

  proto.destroy = function CC_Destroy()
  {
    this.mTree.controllers.removeController(this);
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIController

  proto.supportsCommand = function CC_SupportsCommand(aName)
  {
    switch (aName) {
      case "cmd_copy":
      case "cmd_paste":
      case "cmd_pasteAttribute":
      case "cmd_pasteNodeAfter":
      case "cmd_pasteNodeBefore":
      case "cmd_pasteChildNode":
        return true;
      break;
    }

    return false;
  };

  proto.isCommandEnabled = function CC_IsCommandEnabled(aName)
  {
    let selectedNode = CommonTreeView.getSelectedRowObject(this.view)
    switch (aName) {
      case "cmd_copy":
        return !!selectedNode;
      break;
      case "cmd_paste":
        return this.isCommandEnabled("cmd_pasteAttribute") ||
               this.isCommandEnabled("cmd_pasteNodeAfter") ||
               this.isCommandEnabled("cmd_pasteNodeBefore") ||
               this.isCommandEnabled("cmd_pasteChildNode");
      break;
      case "cmd_pasteAttribute":
        return this._canPasteAttribute(selectedNode);
      break;
      case "cmd_pasteNodeAfter":
      case "cmd_pasteNodeBefore":
        return !!selectedNode && this._canPasteNode(selectedNode.parentNode);
      break;
      case "cmd_pasteChildNode":
        return this._canPasteNode(selectedNode);
      break;
    }

    return false;
  };

  proto.doCommand = function CC_DoCommand(aName)
  {
    let selectedNode = CommonTreeView.getSelectedRowObject(this.view);
    switch (aName) {
      case "cmd_copy":
        // NB: This is a live reference to the "copied" node (i.e., it hasn't
        // been copied at all).  When retrieving the clipboard contents, make
        // sure to use `cloneNode` as appropriate, e.g., for Paste.
        TheFakeClipboardHack.copyContents(selectedNode, this.NODE_FLAVOR);
      break;
      case "cmd_paste":
        if (this.isCommandEnabled("cmd_pasteAttribute")) {
          this.doCommand("cmd_pasteAttribute");
        }
        else if (this.isCommandEnabled("cmd_pasteNodeAfter")) {
          this.doCommand("cmd_pasteNodeAfter");
        }
      break;
      case "cmd_pasteAttribute":
        this._pasteAttribute(selectedNode);
      break;
      case "cmd_pasteNodeAfter":
        this._pasteNode(selectedNode.parentNode, selectedNode.nextSibling);
      break;
      case "cmd_pasteNodeBefore":
        this._pasteNode(selectedNode.parentNode, selectedNode);
      break;
      case "cmd_pasteChildNode":
        this._pasteNode(selectedNode, selectedNode.firstChild);
      break;
    }
  };

  proto.onEvent = function CC_OnEvent(aName)
  {
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  proto._canPasteAttribute = function CC_CanPasteAttribute(aNode)
  {
    // NB: We have to check the node against null here, because
    // `isClipboardPasteable` will happily accept null and then return true if
    // what's on the clipboard turns out to be parseable as an attribute.
    //
    // What we want instead is to return false if the given node is null.
    return aNode && this.mAttributesPasteHelper.isClipboardPasteable(aNode);
  };

  proto._pasteAttribute = function CC_PasteAttribute(aNode)
  {
    this.mAttributesPasteHelper.paste(aNode);
  };

  proto._canPasteNode = function CC_CanPasteNode(aParentNode)
  {
    if (TheFakeClipboardHack.hasFlavor(this.NODE_FLAVOR)) {
      return this._isValidChild(
        aParentNode, TheFakeClipboardHack.getContents(this.NODE_FLAVOR)
      );
    }

    return false;
  };

  proto._pasteNode = function CC_PasteNode(aParent, aNextSibling)
  {
    let clipboardNode = TheFakeClipboardHack.getContents(this.NODE_FLAVOR);
    // NB: We need to use `cloneNode` here, because `clipboardNode` is a
    // live reference to the literal node that was placed on the clipboard.
    let transaction = new PasteNodeTransaction(
      clipboardNode.cloneNode(true), aParent, aNextSibling
    );
    this.mViewer.commandMediator.doTransaction(transaction);
  };

 /**
  * Determines whether the passed parent/child combination is valid.
  *
  * @param aParent
  * @param aChild
  * @return
  *        True iff the given node can be pasted as a child of `aParent`.
  */
  proto._isValidChild = function CC_IsValidChild(aParent, aChild)
  {
    if (aParent == null) {
      return false;
    }

    // The only types that can ever have children.
    if (aParent.nodeType != aParent.ELEMENT_NODE &&
        aParent.nodeType != aParent.DOCUMENT_NODE &&
        aParent.nodeType != aParent.DOCUMENT_FRAGMENT_NODE) {
      return false;
    }

    // The only types that can't ever be children.
    if (aChild.nodeType == aChild.DOCUMENT_NODE ||
        aChild.nodeType == aChild.DOCUMENT_FRAGMENT_NODE ||
        aChild.nodeType == aChild.ATTRIBUTE_NODE) {
      return false;
    }

    // Doctypes can only be the children of documents.
    if (aChild.nodeType == aChild.DOCUMENT_TYPE_NODE &&
        aParent.nodeType != aParent.DOCUMENT_NODE) {
      return false;
    }

    // Only elements and fragments can have text, CDATA, and entities as
    // children.
    if (aParent.nodeType != aParent.ELEMENT_NODE &&
        aParent.nodeType != aParent.DOCUMENT_FRAGMENT_NODE &&
        (aChild.nodeType == aChild.TEXT_NODE ||
         aChild.nodeType == aChild.CDATA_NODE ||
         aChild.nodeType == aChild.ENTITY_NODE)) {
      return false;
    }

    // Documents can only have one document element or doctype.
    if (aParent.nodeType == aParent.DOCUMENT_NODE) {
      for (let i = 0, n = aParent.childNodes.length; i < n; ++i) {
        if (aParent.childNodes[i].nodeType == aChild.nodeType) {
          return false;
        }
      }
    }

    return true;
  };
}
