/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.DOMTreeView = DOMTreeView;

const { nsIDocShell } = Components.interfaces;
const { nsIDOMDocument } = Components.interfaces;
const { nsIDOMElement } = Components.interfaces;
const { nsIInterfaceRequestor } = Components.interfaces;
const { nsIWebProgress } = Components.interfaces;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");

var { DOMTreeDummyRowData } = require("./domTreeDummyRowData");
var { DOMTreeRowData } = require("./domTreeRowData");
var { EditAttributeTransaction } = require("transactions/editAttributeTransaction");
var { CommonTreeView } = require("utils/commonTreeView");
var { DeepIterator } = require("utils/deepIterator");
var { Logger } = require("utils/logger");

var { DOMUtils } = require("xpcom/domUtils");
var { MutationObserver } = require("xpcom/mutationObserver");

//////////////////////////////////////////////////////////////////////////////

function DOMTreeView(aRootNode, aCommandMediator, aViewer)
{
  CommonTreeView.call(this);

  this.mCommandMediator = aCommandMediator;
  this.mViewer = aViewer;

  // I'd use a doc shell-to-document WeakMap here, but it won't let me use doc
  // shells as a key. --crussell
  this.mDocumentPairs = [];
  this.mNodeToRowDataMap = new WeakMap();
  this.mDummyRowData = [];

  // We choose not use `mLevels` and `mOpenStates`.  We'll have to override
  // `getLevel`, `isContainerOpen`, and `setRowOpenState` below.
  this.mRowData = [ this._createRowDataForNode(aRootNode) ];
  this.addManagedRowData(this.mRowData);

  this.rowCount = 1;

  this._addProgressListener();
  this.mMutationObserver = 
    new MutationObserver(this._onTreeMutations.bind(this));
  this.mMutationObserver.observe(
    aRootNode,
    {
      childList: true,
      attributes: true,
      subtree: true,
      attributeFilter: [ "id" ]
    }
  );
}

{
  let proto = Object.create(CommonTreeView.prototype);
  DOMTreeView.prototype = proto;
  proto.constructor = DOMTreeView;

  proto.QueryInterface = XPCOMUtils.generateQI([
    "nsISupportsWeakReference",
    "nsITreeView",
    "nsIWebProgressListener"
  ]);

  proto.insertNodeFlow = null;
  proto.showAnonymousNodes = null;
  proto.showSubDocuments = null;

  proto.mDocumentPairs = null;
  proto.mDummyRowData = null;
  proto.mMutationObserver = null;
  proto.mNodeToRowDataMap = null;
  proto.mRowData = null;
  proto.mWebProgress = null;

  proto.destroy = function DTV_Destroy()
  {
    this.mMutationObserver.disconnect();
    this._removeProgressListener();
  };

  /**
   * Implementation of the optional method that's part of the interface
   * CommonTreeView uses of its subclasses.  The properties computed here will
   * be those that `getCellProperties` tells the tree box object about.
   *
   * @param aRow
   *        The index of the row the cell is in.
   * @param aColumn
   *        The nsITreeColumn the cell is in.
   * @return
   *        An array of strings naming the properties for cell.
   * @see CTV_GetCellProperties
   */
  proto.computeCellProperties =
    function DTV_ComputeCellProperties(aRow, aColumn)
  {
    let properties = [];
    switch (aColumn.id) {
      case "colNodeName":
        properties = this.mRowData[aRow].properties;
      break;
      case "colID":
        properties.push("attribute-value");
      break;
    }

    return properties;
  };

  /**
   * Fulfill the interface required of CommonTreeView subclasses.  When a
   * container row has been toggled open, this is called to fill in the row
   * data for the given row's children.
   *
   * @param aRow
   *        The index of row that has been toggled open.
   * @return
   *        The number of inserted child rows.
   */
  proto.expandRow = function DTV_ExpandRow(aRow)
  {
    // assert(!!this.getRowObject(aRow));
    let newRowData = [];
    let kids = this._getDescendants(this.getRowObject(aRow));
    for (let i = 0, n = kids.length; i < n; ++i) {
      newRowData.push(
        this._createRowDataForNode(kids[i], this.mRowData[aRow])
      );
    }

    CommonTreeView.arrayCopy(newRowData, this.mRowData, aRow + 1);

    return newRowData.length;
  };

  /**
   * @param aRow
   *        The index of some tree row.
   * @return
   *        The DOM Node represented in the tree at the given row.  NB: Even
   *        if the given index is valid, this method will return null if the
   *        row is a dummy row we created for the cmd_insertNode flow.
   */
  proto.getRowObject = function DTV_GetRowObject(aRow)
  {
    if (aRow < 0) {
      return null;
    }

    return this.mRowData[aRow].node;
  };

  proto.isDummyRow = function DTV_IsDummyRow(aRow)
  {
    return aRow >= 0 && !this.getRowObject(aRow);
  };

  proto.getRowDataForNode = function DTV_GetRowDataForNode(aNode)
  {
    return this.mNodeToRowDataMap.get(aNode) || null;
  };

  /**
   * Override CommonTreeView's method of the same name.  This is necessary
   * since we're not storing the open states in an `mOpenStates` array.
   *
   * @see CTV_ToggleOpenState
   */
  proto.setRowOpenState = function DTV_SetRowOpenState(aRow, aOpenState)
  {
    this.mRowData[aRow].isOpen = aOpenState;
  };

  proto.getRowIndexForNode = function DTV_GetRowIndexForNode(aNode)
  {
    // Fail fast.
    if (aNode && this.getRowDataForNode(aNode)) {
      for (let i = 0, n = this.mRowData.length; i < n; ++i) {
        if (this.mRowData[i].node == aNode) {
          return i;
        }
      }
    }

    return -1;
  };

  proto.getRowIndexForDummy = function DTV_GetRowIndexForDummy(aRowData)
  {
    let idx = this.selection.currentIndex;
    if (this.mRowData[idx] == aRowData) {
      return idx;
    }

    let low = idx - 1;
    let high = idx + 1;
    while (low >= 0 || high < this.rowCount) {
      if (low >= 0 && this.mRowData[low] == aRowData) {
        return low;
      }
      if (high < this.rowCount && this.mRowData[high] == aRowData) {
        return high;
      }

      --low;
      ++high;
    }

    return -1;
  };

  /**
   * Override CommonTreeView's method of the same name so we can prune
   * `this.mNodeToRowDataMap`.
   *
   * @param aRowStart
   * @param aHowMany
   */
  proto.exciseRowData = function DTV_ExciseRowData(aRowStart, aHowMany)
  {
    for (let i = aRowStart; i < aHowMany; ++i) {
      // For row data created for the cmd_insertNode flow, it's `node`
      // property will be null, and therefore won't be in the map.
      if (!this.isDummyRow(i)) {
        this.mNodeToRowDataMap.delete(this.getRowObject(i));
      }
    }

    CommonTreeView.prototype.exciseRowData.call(this, aRowStart, aHowMany);
  };

  /**
   * If the given node is rooted in this tree, progressively expand its
   * ancestors until it's got a corresponding row.  NB: It's not guaranteed
   * that the row will be visible; it may be scrolled out of view.
   *
   * @param aNode
   *        The node to show.
   * @param aOpenItToo [optional]
   * @return
   *        The index of the row corresponding to the node or the index of its
   *        closest, displayable ancestor, or -1 if the node is not in the
   *        subtree rooted at the viewer's inspected node.
   */
  proto.uncoverNode = function DTV_UncoverNode(aNode, aOpenItToo)
  {
    if (!aNode) {
      return -1;
    }

    let idx = this.getRowIndexForNode(aNode);
    if (idx < 0) {
      let parentRowObject =
        DOMUtils.getParentForNode(aNode, this.showAnonymousNodes);

      if (parentRowObject) {
        idx = this.uncoverNode(parentRowObject);
        if (idx >= 0) {
          if (!this.isContainerOpen(idx)) {
            this.toggleOpenState(idx);
          }
          // XXX `getRowIndexForNode` is a linear search.
          let actualIndex = this.getRowIndexForNode(aNode);
          // XXX Any reason why this *shouldn't* be >= 0?
          if (actualIndex >= 0) {
            idx = actualIndex;
          }
          // assert(idx >= 0);
        }
      }
    }

    if (this.getRowObject(idx) == aNode && aOpenItToo &&
        !this.isContainerEmpty(idx)) {
      this.toggleOpenState(idx);
    }

    return idx;
  };

  proto.targetNode = function DTV_TargetNode(aNode)
  {
    let idx = this.uncoverNode(aNode);
    if (idx < 0) {
      return false;
    }

    this.selection.select(idx);
    this.selection.tree.ensureRowIsVisible(idx);

    return true;
  };

  proto.createDummyRowData =
    function DTV_CreateDummyRowData(aParentNode, aNextSibling)
  {
    // NB: `aNextSibling` may be null, in which case we use the same semantics
    // as the `appendChild` DOM method.
    let parentIndex = this.getRowIndexForNode(aParentNode);
    let parentData = this.mRowData[parentIndex];
    let dummy = new DOMTreeDummyRowData(aNextSibling, parentData, parentData);
    this.mDummyRowData.push(dummy);

    if (!parentData.isOpen) {
      if (!this.isContainerEmpty(parentIndex)) {
        this.expandRow(parentIndex);
      }
      else {
        // We're going to have to fake it.
        parentData.isOpen = true;
        // We'll invalidate below, after actually inserting the dummy.
      }
    }

    // XXX Guard against anonymous subtrees.
    let insertionIndex;
    if (aNextSibling) {
      insertionIndex = this.getRowIndexForNode(aNextSibling);
    }
    else if (aParentNode.lastChild) {
      insertionIndex = this.getRowIndexForNode(aParentNode.lastChild) + 1;
    }
    else {
      insertionIndex = parentIndex + 1;
    }

    this.mRowData.splice(insertionIndex, 0, dummy);
    ++this.rowCount;
    this.mTreeBoxObject.rowCountChanged(insertionIndex, 1);
    // In case we had to fake the row expansion above.
    this.mTreeBoxObject.invalidateRow(parentIndex);

    return dummy;
  };

  proto.editDummyField = function DTV_EditDummyField(aRowData, aColumnID)
  {
    let idx = this.getRowIndexForDummy(aRowData);
    this.mTreeBoxObject.element.startEditing(
      idx, this.mTreeBoxObject.columns.getNamedColumn(aColumnID)
    );
  };

  proto.onFlowCancelled = function DTV_OnFlowCancelled(aRowData)
  {
    // If we faked the open state of the parent row, clean it up.
    if (!this._getDescendants(aRowData.rowParentData.node).length) {
      aRowData.rowParentData.isOpen = false;
    }

    // Get rid of the dummy row.
    let idx = this.getRowIndexForDummy(aRowData);
    this.mRowData.splice(idx, 1);
    --this.rowCount;
    this.mTreeBoxObject.rowCountChanged(idx, -1);

    // Stop keeping track of the dummy data, too.
    this.mDummyRowData.splice(this.mDummyRowData.indexOf(aRowData), 1);
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsITreeView

  proto.getCellText = function DTV_GetCellText(aRow, aColumn)
  {
    // NB: `rowData.node` will be null if we're in the middle of the
    // cmd_insertNode flow and `aRow` is the index of a dummy row we've
    // created that doesn't correspond to any node actually in the DOM.
    let rowData = this.mRowData[aRow];
    switch (aColumn.id) {
      case "colNodeName":
        return (rowData.node) ?
          rowData.node.nodeName :
          rowData.name;
      break;
      case "colID":
        if (rowData.node) {
          if (rowData.node instanceof nsIDOMElement) {
            return rowData.node.id;
          }
        }
        else {
          return rowData.id;
        }
      break;
    }

    return "";
  };

  /**
   * Override `CommonTreeView.prototype.getLevel` since we're not storing the
   * row level data in an `mLevels` array.
   *
   * @see CommonTreeView
   */
  proto.getLevel = function DTV_GetLevel(aRow)
  {
    return this.mRowData[aRow].level;
  };

  proto.isContainer = function DTV_IsContainer(aRow)
  {
    return true;
  };

  proto.isContainerEmpty = function DTV_IsContainerEmpty(aRow)
  {
    // Easy case.  NB: This is actually necessary.  We can't rely on the tests
    // below, since we have the notion of dummy rows, although we could
    // otherwise.
    if (this.mRowData[aRow].isOpen) {
      return false;
    }

    // This will return null if we're in the middle of cmd_insertNode and the
    // given row is our dummy.
    let node = this.getRowObject(aRow);
    return (node) ?
      !this._getDescendants(node).length :
      true;
  };

  /**
   * Override `CommonTreeView.prototype.isContainerOpen` since we're not
   * storing the row level data in an `mOpenStates` array.
   *
   * @see CommonTreeView
   */
  proto.isContainerOpen = function DTV_IsContainerOpen(aRow)
  {
    return this.mRowData[aRow].isOpen;
  };

  proto.isEditable = function DTV_IsEditable(aRow, aColumn)
  {
    // Dummy rows for cmd_insertNode are to be handled by `InsertNodeFlow`.
    let rowData = this.mRowData[aRow];
    let node = rowData.node;
    return (node) ?
      aColumn.id == "colID" && node && node instanceof nsIDOMElement :
      this.insertNodeFlow.isDummyRowCellEditable(rowData, aColumn);
  };

  proto.setCellText = function DTV_SetCellText(aRow, aColumn, aValue)
  {
    // This may be a dummy row for an ongoing cmd_insertNode flow.
    let rowData = this.mRowData[aRow];
    if (!rowData.node) {
      // assert(rowData instanceof DOMTreeDummyRowData);
      this.insertNodeFlow.setDummyRowCellText(rowData, aColumn, aValue);
      return;
    }

    this.mCommandMediator.doTransaction(
      new EditAttributeTransaction(rowData.node, null, "id", aValue)
    );

    this.mTreeBoxObject.invalidateCell(aRow, aColumn);
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIWebProgressListener

  proto.onStateChange =
  proto.onProgressChange =
  proto.onStatusChange =
  proto.onSecurityChange = function DTV_Stub() { };

  /**
   * @see _addProgressListener
   */
  proto.onLocationChange =
    function DTV_OnLocationChange(aProgress, aRequest, aURI, aFlags)
  {
    let docShell = this._getDocShell(aProgress.DOMWindow.document);
    let idx = this.getRowIndexForNode(this._getMappedDocument(docShell));
    if (idx < 0) {
      return;
    }

    this.mTreeBoxObject && this.mTreeBoxObject.beginUpdateBatch();

    if (idx == 0) {
      // XXX If it's still in the bfcache, we should probably continue to
      // allow inspecting it until it's purged.
      this.selection.select(-1);

      let count = this.rowCount;
      this.exciseRowData(0, count);
      this.rowCount = 0;
      this.mTreeBoxObject && this.mTreeBoxObject.rowCountChanged(0, -count);

      this._unmapDocShell(docShell);
      this._removeProgressListener();
    }
    else {
      let docRowParentIndex = this.getParentIndex(idx);

      // If the current target is in the replaced doc's subtree, we'll need to
      // make sure the new doc is selected after we make it available.
      let newSelectionIndex;
      let currentIndex = this.getRowIndexForNode(this.mViewer.target);
      while (this.getLevel(currentIndex) > 0) {
        let parentIndex = this.getParentIndex(currentIndex);
        if (parentIndex == docRowParentIndex) {
          newSelectionIndex = currentIndex;
          break;
        }
        currentIndex = parentIndex;
      }

      this.toggleOpenState(docRowParentIndex);
      this.toggleOpenState(docRowParentIndex);

      // assert(newSelectionIndex != 0);
      if (newSelectionIndex) {
        this.selection.select(newSelectionIndex);
      }
    }

    this.mTreeBoxObject && this.mTreeBoxObject.endUpdateBatch();
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  proto._createRowDataForNode =
    function DTV_CreateRowDataForNode(aNode, aRowParentData)
  {
    let domParentData =
      aNode.parentNode && this.getRowDataForNode(aNode.parentNode);
    let rowData = new DOMTreeRowData(aNode, domParentData, aRowParentData);
    this.mNodeToRowDataMap.set(aNode, rowData);

    if (aNode instanceof nsIDOMDocument) {
      this._mapDocShellToDocument(aNode);
    }

    return rowData;
  };

  proto._mapDocShellToDocument = function DTV_MapDocShellToDocument(aDocument)
  {
    let docShell = this._getDocShell(aDocument);
    let idx = this._getDocumentPairIndex(docShell);
    let entry;
    if (idx < 0) {
      entry = { docShell: docShell };
      this.mDocumentPairs.push(entry);
    }
    else {
      entry = this.mDocumentPairs[idx];
    }

    entry.document = aDocument;
  };

  proto._getDocumentPairIndex = function DTV_GetDocumentPairIndex(aDocShell)
  {
    for (let i = 0, n = this.mDocumentPairs.length; i < n; ++i) {
      if (this.mDocumentPairs[i].docShell == aDocShell) {
        return i;
      }
    }

    return -1;
  };

  proto._getDocShell = function DTV_GetDocShell(aDocument)
  {
    return aDocument.defaultView.QueryInterface(nsIInterfaceRequestor).
      getInterface(nsIDocShell);
  };

  proto._getMappedDocument = function DTV_GetMappedDocument(aDocShell)
  {
    let idx = this._getDocumentPairIndex(aDocShell);
    if (idx >= 0) {
      return this.mDocumentPairs[idx].document;
    }

    return null;
  };

  proto._unmapDocShell = function DTV_UnmapDocShell(aDocShell)
  {
    let idx = this._getDocumentPairIndex(aDocShell);
    if (idx >= 0) {
      this.mDocumentPairs.splice(idx, 1);
    }
  };

  /**
   * Mutation observer callback.
   *
   * @param aRecords
   *        An array of `MutationRecord`s.
   */
  proto._onTreeMutations = function DTV_OnTreeMutations(aRecords)
  {
    if (this.mTreeBoxObject) {
      this.mTreeBoxObject.beginUpdateBatch();
    }

    let newTarget = null;
    try {
      for (let i = 0, n = aRecords.length; i < n; ++i) {
        switch (aRecords[i].type) {
          case "attributes":
            this._onAttributeMutation(aRecords[i]);
          break;
          case "childList":
            newTarget = this._onChildListMutation(aRecords[i]);
          break;
          default:
            throw new Error(
              "DOMTreeView observed unexpected mutation type: " +
              aRecords[i].type
            );
          break;
        }
      }
    }
    finally {
      if (this.mTreeBoxObject) {
        this.mTreeBoxObject.endUpdateBatch();
      }
    }

    // Nodes created with cmd_insertNode get selected as the new viewer
    // target.
    let idx = this.getRowIndexForNode(newTarget);
    if (idx >= 0) {
      this.selection.select(idx);
    }
  };

  proto._onAttributeMutation = function DTV_OnAttributeMutation(aRecord)
  {
    if (aRecord.attributeName != "id") {
      throw new Error(
        "DOMTreeView observed non-id attribute mutation: " +
        aRecord.attributeName
      );
    }

    let idx = this.getRowIndexForNode(aRecord.target);
    if (this.mTreeBoxObject) {
      let column = this.mTreeBoxObject.columns.getNamedColumn("colID");
      this.mTreeBoxObject.invalidateCell(idx, column);
    }
  };

  /**
   * @param aRecord
   *        A DOM `MutationRecord`.
   * @return
   *        The node that should be the new viewer target, or null.  (Only
   *        used for mutations effected by cmd_insertNode.)
   */
  proto._onChildListMutation = function DTV_OnChildListMutation(aRecord)
  {
    // NB: I *think* mutation observers are designed so that if `addNodes` or
    // `removedNodes` are present and contain more than one node, those nodes
    // are guaranteed to be contiguous in the DOM tree.  (I don't see how it
    // could be any other way without making the record's `previousSibling`
    // and `nextSibling` attributes almost-but-not-quite-entirely useless.)
    //
    // It's moot, though.  XBL can arbitrarily reorder and reparent nodes wrt
    // the shadow tree.  Since we have to deal with the case of potentially
    // non-contiguous nodes, we always give them the same treatment.  It also
    // saves me from having to rely on the hunch I'm not sure about that I
    // just mentioned. --crussell

    // XXX Our `getRowIndexForNode` is a linear search, which means that the
    // algorithmic complexity here is terrible.  Trying to avoid premature
    // optimization, though.  We *could* store the row indexes in the row data
    // and use an adjustment list for mutations and toggled containers...
    {
      let n = aRecord.removedNodes && aRecord.removedNodes.length;
      for (let i = 0; i < n; ++i) {
        let idx = this.getRowIndexForNode(aRecord.removedNodes[i]);
        if (idx < 0) {
          continue;
        }

        // Make sure we remove descendant rows, too.
        this.rowCount -= this.collapseRow(idx);

        // If this was the parent row's last child, we have to update its
        // data's open state, because it's empty now.
        let level = this.getLevel(idx);
        if (this.getLevel(idx) > 0 &&
            !this._getDescendants(this.getRowObject(idx - 1)).length) {
          this.mRowData[idx - 1].isOpen = false;
        }

        --this.rowCount;
        this.mRowData.splice(idx, 1);
        if (this.mTreeBoxObject) {
          this.mTreeBoxObject.rowCountChanged(idx, -1);
        }
      }
    }

    let newTarget = null;

    // NB: Because of XBL, sibling row objects are not necessarily siblings in
    // the DOM.  We use "sibling group" below to refer to the nodes which
    // appear adjacent in the shadow tree.  (Unless we're not showing the
    // effects of XBL.  Then the "siblings" below really do refer to DOM
    // siblings.)
    let processed = this._processAddedNodes(aRecord.addedNodes);
    for (let i = 0, n = processed.length; i < n; ++i) {
      let p = processed[i];

      // Figure out where the current node's row needs to show up.
      let insertionIndex;
      // ... but first check if we already have a dummy row for it.  (This
      // will be the case when using cmd_insertNode.)
      if (p.rowData) {
        // Found one.  Just replace the dummy row with a proper
        // `DOMTreeRowData` object.
        insertionIndex = this.getRowIndexForDummy(p.rowData);
        this.mRowData[insertionIndex] = this._createRowDataForNode(
          p.node, p.parentRowData
        );

        newTarget = p.node;
        continue;
      }

      let position = p.siblingGroup.indexOf(p.node);
      if (position == 0) {
        // Special case: it's at the beginning, before all of its siblings.
        insertionIndex = this.getRowIndexForNode(p.parentRowData.node) + 1;
      }
      else {
        // Put it after its previous sibling row.
        let sibling = p.siblingGroup[position - 1];
        let siblingRowIndex = this.getRowIndexForNode(sibling);
        // Make sure to jump past the siblings descendants, if any are
        // visible.
        insertionIndex =
          this.getNextSiblingIndex(siblingRowIndex, siblingRowIndex, true);
      }

      ++this.rowCount;
      let rowData = this._createRowDataForNode(p.node, p.parentRowData);
      this.mRowData.splice(insertionIndex, 0, rowData);
      if (this.mTreeBoxObject) {
        this.mTreeBoxObject.rowCountChanged(insertionIndex, 1);
      }
    }

    return newTarget;
  };

  /**
   * XBL can arbitrarily re-order nodes.  Nodes which are in the same sibling
   * group need to be ordered such that those that appear earlier in the group
   * are nearer to the beginning of the list.
   *
   * Additionally we only need to process nodes whose sibling group is
   * visible; others won't be shown in the tree.
   *
   * @param aNodes
   * @return
   *        An array of objects, each with a `node`, `siblingGroup`, and
   *        `parentRowData` property, for every node that will be visible in
   *        the tree.  (I.e., all the rows corresponding to its ancestors are
   *        toggled open.)  Each object will also have a `rowData` property,
   *        but it will only be non-null if the node was one inserted by
   *        cmd_insertNode.
   */
  proto._processAddedNodes = function DTV_ProcessAddedNodes(aNodes)
  {
    let processed = [];
    if (!aNodes || !aNodes.length) {
      return processed;
    }

    // We start by building a map of all sibling groups for the given nodes.
    // Nodes which have the same parent row object are siblings, so we index
    // the map by the parent.  (We can't index it by the added node, because
    // we'll end up with different arrays of the same sibling group due to the
    // way `_getDescendants` works.)
    let parentToSiblingGroupMap = new WeakMap();
    for (let i = 0, n = aNodes.length; i < n; ++i) {
      // But first check if this is one inserted by cmd_insertNode.
      let node = aNodes[i];
      let dummyRowData = null;
      for (let i = 0, n = this.mDummyRowData.length; i < n; ++i) {
        let current = this.mDummyRowData[i];
        if (current.transaction && current.transaction.node == node) {
          this.mDummyRowData.splice(i, 1);
          dummyRowData = current;
          break;
        }
      }

      let parent =
        DOMUtils.getParentForNode(node, this.showAnonymousNodes);

      // We can skip nodes whose parent rows aren't open.
      let parentRowData = this.getRowDataForNode(parent);
      if (parentRowData && parentRowData.isOpen) {
        let siblingGroup = parentToSiblingGroupMap.get(parent) || null;
        if (!siblingGroup) {
          siblingGroup = this._getDescendants(parent);
          parentToSiblingGroupMap.set(parent, siblingGroup);
        }

        processed.push({
          node: node,
          parentRowData: parentRowData,
          rowData: dummyRowData,
          siblingGroup: siblingGroup,
        });
      }
    }

    // No need to sort them if we're not showing the effects of XBL.  In that
    // case, DOM order is the order we want them in.
    if (this.showAnonymousNodes) {
      processed.sort((function DTV_ProcessAddedNodes_Comparator(x, y)
      {
        let xParent = DOMUtils.getParentForNode(x.node, true);

        // For nodes not in the same sibling group, it doesn't matter what
        // order they appear in.
        if (xParent != DOMUtils.getParentForNode(y.node, true)) {
          return 0;
        }

        let siblingGroup = parentToSiblingGroupMap.get(xParent);
        return siblingGroup.indexOf(x.node) - siblingGroup.indexOf(y.node);
      }).bind(this));
    }

    return processed;
  };

  proto._getDescendants = function DTV_GetDescendants(aNode)
  {
    return DeepIterator.getDescendants(aNode, this.showAnonymousNodes,
                                       this.showSubDocuments);
  };

  proto._addProgressListener = function DTV_AddProgressListener()
  {
    let doc = this.getRowObject(0);
    try {
      this.mWebProgress = this._getDocShell(doc).QueryInterface(nsIWebProgress
      );
    }
    catch (ex) {
      Logger.print(
        "Couldn't get nsIWebProgress for document.  Exception:\n" + ex
      );
      return;
    }

    this.mWebProgress.addProgressListener(
      this, this.mWebProgress.NOTIFY_LOCATION
    );
  };

  proto._removeProgressListener = function DTV_RemoveProgressListener()
  {
    if (this.mWebProgress) {
      this.mWebProgress.removeProgressListener(this);
      this.mWebProgress = null;
    }
  };
}
