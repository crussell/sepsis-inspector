/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Oversees the sequence of steps to complete (or cancel) cmd_insertNode
 *
 * This is pretty convoluted.  It mostly has to do with how the APIs for the
 * tree view and tree binding cut orthogonally to the way we'd like to do
 * things.  The idea is that an `InsertNodeFlow` instance will be created when
 * it's time to carry out cmd_insertNode, and when the flow terminates (i.e.,
 * we've gathered all the info from the user, like the node name), we will
 * execute a transaction that actually creates the node and inserts it into
 * the tree.
 *
 * Requires cooperation on the part of the view.  The view must implement the
 * following methods:
 *
 *   getRowObject(row: number): any
 *     Return the node at the given index.
 *
 *   createDummyRowData(parent: DOMNode, nextSibling: DOMNode): DummyRowData
 *     The view should insert a row at the appropriate position (in the
 *     sibling group for `nextSibling`, as its immediate preceding sibling),
 *     and return the row data object.  The dummy row represents where the new
 *     node will be inserted, and upon successful completion of the flow, it
 *     should be purged and replaced by a row that represents the actual DOM
 *     node.
 *
 *   editDummyField(rowData: DummyRowData, columnID: string)
 *     Enter edit mode for the cell that corresponds to the given row and
 *     column.  (Hint: this should be as simple as getting the row index and
 *     column object and calling the tree binding's `startEditing` method.)
 *
 * The flow will send out two notifications (see `NotificationManager`):
 *
 *   flowCancelled { rowData: DummyRowData }
 *     Dispatched when the flow has been terminated before completion and the
 *     view should purge the dummy row.
 *
 *   flowCompleting { rowData: DummyRowData }
 *     Dispatched when the flow is ending due to success.
 *
 * The `DummyRowData` specified above must satisfy the following interface:
 *
 *   siblingObject: DOMNode
 *   parentObject: DOMNode
 *     These properties should be the same objects as those passed to the
 *     view's `createDummyRowData` method.
 *
 *   setField(columnID: string, value: string)
 *     Called when text has been entered for the dummy row from the tree's
 *     edit mode.
 *
 * Additionally, the view must make the appropriate calls to
 * `setDummyRowCellText` and `isDummyRowCellEditable` in the implementations
 * of the view's own `setCellText` and `isCellEditable` methods.
 *
 * TODO
 * - cmd_insertNode should support more node types (both for the parent that
 *   we're operating on and the child that will be inserted); right now we
 *   only support adding elements to other elements.
 * - cmd_insertNode should allow adding (multiple) attributes to an element.
 * - ensure anonymous subtrees are handled properly
 * - support other namespaces
 *
 * @see TreeEditWatcher
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.InsertNodeFlow = InsertNodeFlow;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");

var { NotificationManager } = require("utils/notificationManager");

//////////////////////////////////////////////////////////////////////////////

function InsertNodeFlow(aViewer, aTree, aView, aTreeEditWatcher)
{
  this.mNotificationManager = new NotificationManager(this);

  this.mViewer = aViewer;
  this.mTree = aTree;
  this.mView = aView;

  let $col = this.mTree.columns.getNamedColumn.bind(this.mTree.columns);
  this.mNodeNameColumn = $col("colNodeName");
  this.mIDColumn = $col("colID");

  this.mTreeEditWatcher = aTreeEditWatcher;
  this.mTreeEditWatcher.addObserver("editEnded", this);

  aTree.controllers.appendController(this);
}

{
  let proto = InsertNodeFlow.prototype;
  proto.mIDColumn = null;
  proto.mNodeNameColumn = null;
  proto.mNotificationManager = null;
  proto.mRowData = null;
  proto.mTree = null;
  proto.mTreeEditWatcher = null;
  proto.mView = null;
  proto.mViewer = null;

  proto.QueryInterface = XPCOMUtils.generateQI([ "nsIController" ]);

  proto.beginFlow = function INF_BeginFlow(aParent, aSibling)
  {
    this.mRowData = this.mView.createDummyRowData(aParent, aSibling);
    this.mTreeEditWatcher.state = this._computeNextStep();
    this._edit(this.mTreeEditWatcher.state);
    // Await text entry or cancellation.
  };

  proto.isDummyRowCellEditable =
    function INF_IsDummyRowCellEditable(aRowData, aColumn)
  {
    return (aColumn.id == "colNodeName" || aColumn.id == "colID") &&
           aRowData == this.mRowData;
  };

  proto.setDummyRowCellText =
    function INF_SetDummyRowCellText(aRowData, aColumn, aValue)
  {
    if (aRowData != this.mRowData) {
      throw new Error("Row not managed here.");
    }

    aRowData.setField(aColumn.id, aValue);

    this.mTreeEditWatcher.state = this._computeNextStep(
      this.mTreeEditWatcher.state, aColumn.id
    );
    // Don't call `_advanceToStep` here, because the "editEnded" notification
    // will fire soon, and it's taken care of in the notification handler.
  };

  ////////////////////////////////////////////////////////////////////////////
  //// Notifications

  proto.handleNotification = function INF_HandleNotification(aName, aData)
  {
    switch (aName) {
      case "editEnded":
        // There may be fields remaining to edit.
        this._edit(aData.state);
      break;
      default:
        throw new Error("wat.  Notification: " + aName);
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  /**
   * @param aCurrentStateString [optional]
   *        A JSON string of the form previously returned by this method.
   * @param aCurrentID [optional]
   *        A string giving the ID of the column the flow is leaving.
   * @return
   *        A JSON string encoding an object with the following fields:
   *
   *          awaitingIDs: string[]
   *            The IDs of columns where we are still waiting for input.
   *          keepActive: boolean
   *            Indicates that we aren't transitioning due to cancellation.
   *          passedIDs: string[]
   *            The IDs of the columns that have already been visited in this
   *            flow.
   */
  proto._computeNextStep =
    function INF_ComputeNextStep(aCurrentStateString, aCurrentID)
  {
    let currentState = (aCurrentStateString !== undefined) ?
      JSON.parse(aCurrentStateString) :
      null;
    let newState = {
      keepActive: true,
      awaitingIDs: [],
      passedIDs: (currentState) ?
        currentState.passedIDs :
        []
    };

    if (newState.passedIDs.indexOf(this.mIDColumn.id) < 0 &&
        aCurrentID != this.mIDColumn.id) {
      newState.awaitingIDs.push(this.mIDColumn.id);
    }
    if (!this.mRowData.name) {
      newState.awaitingIDs.push(this.mNodeNameColumn.id);
    }

    let $col = this.mTree.columns.getNamedColumn.bind(this.mTree.columns);
    newState.awaitingIDs = newState.awaitingIDs.sort(function (a, b)
    {
      return parseInt($col(a).element.ordinal) -
             parseInt($col(b).element.ordinal);
    });

    if (aCurrentID) {
      newState.passedIDs.push(aCurrentID);
      let idx = newState.awaitingIDs.indexOf(aCurrentID);
      if (idx >= 0) {
        // We just passed a required field.  Terminate the flow.
        newState.keepActive = false;
      }
    }

    return JSON.stringify(newState);
  };

  /**
   * @param aStateString
   *        A string of the form returned by `_computeNextStep`.
   * @return
   *        The updated state, or null if the flow is finished.
   */
  proto._advanceToStep = function INF_AdvanceToStep(aStateString)
  {
    let notificationData = { rowData: this.mRowData };
    let state = JSON.parse(aStateString);
    if (!state.keepActive) {
      this.mNotificationManager.notify("flowCancelled", notificationData);
      this.mRowData = null;
      return null;
    }

    if (state.awaitingIDs.length) {
      // Clear the keep-active bit.  This will allow us to figure out in
      // future calls whether we've been cancelled.
      state.keepActive = false;
      return state;
    }

    // We're not waiting for any more input.  Finish up.
    this.mNotificationManager.notify("flowCompleting", notificationData);

    return null;
  };

  /**
   * @param aStateString
   *        A JSON string in the form returned by `_computeNextStep`.
   */
  proto._edit = function INF_Edit(aStateString)
  {
    // Make sure we maintain a grasp on the dummy row data, because it may
    // disappear during the `_advanceToStep` call.
    let rowData = this.mRowData;

    let state = this._advanceToStep(aStateString);
    this.mTreeEditWatcher.state = state && JSON.stringify(state);
    if (state) {
      this.mView.editDummyField(rowData, state.awaitingIDs[0]);
    }
  };
}
