/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Extends `DOMTreeRowData` to accommodate dummy rows for cmd_insertNode.
 *
 * Ordinary rows are backed by real nodes in the DOM.  During the
 * cmd_insertNode flow, a dummy row is created and inserted into the view,
 * representing the pending operation.
 *
 * @see InsertNodeFlow
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.DOMTreeDummyRowData = DOMTreeDummyRowData;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { DOMTreeRowData } = require("./domTreeRowData");

//////////////////////////////////////////////////////////////////////////////

function DOMTreeDummyRowData(aNextSiblingNode, aDOMParentData, aRowParentData)
{
  DOMTreeRowData.call(this, null, aDOMParentData, aRowParentData);

  this.siblingObject = aNextSiblingNode;
  this.rowParentData = aRowParentData;

  this.id = "";
  this.name = "";
}

{
  let proto = Object.create(DOMTreeRowData.prototype);
  proto.constructor = DOMTreeDummyRowData;
  DOMTreeDummyRowData.prototype = proto;

  proto.id = null;
  proto.name = null;
  proto.transaction = null;

  ////////////////////////////////////////////////////////////////////////////
  //// Required to satisfy the dummy row data "interface"

  proto.siblingObject = null;

  proto.__defineGetter__("parentObject", function DTDRD_ParentObject_Get()
  {
    return this.mDOMParentData.node;
  });

  proto.setField = function DTDRD_SetField(aColumnID, aValue)
  {
    switch (aColumnID) {
      case "colNodeName":
        this.name = aValue;
      break;
      case "colID":
        this.id = aValue;
      break;
      default:
        throw new Error("wat.  Column: " + aColumnID);
      break;
    }
  };
}
