/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Viewer to show the node structure of a DOM document.
 *
 * @see registration/theViewerRegistry
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

const { nsIDOMDocument } = Components.interfaces;
const { nsIDOMElement } = Components.interfaces;
const { nsIDOMHTMLDocument } = Components.interfaces;
const { nsIDOMNodeFilter } = Components.interfaces;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");

var { ClipboardController } = require("./clipboardController");
var { DeleteNodeController } = require("./deleteNodeController");
var { DOMTreeView } = require("./domTreeView");
var { InsertNodeFlow } = require("./insertNodeFlow");
var { InsertNodeController } = require("./insertNodeController");

var { AttributesPasteHelper } = require("utils/attributesPasteHelper");
var { CommonTreeView } = require("utils/commonTreeView");
var { DeepIterator } = require("utils/deepIterator");
var { FindByClick } = require("utils/findByClick");
var { ThePrefManager } = require("utils/thePrefManager");

var { TheFakeClipboardHack } = require("hacks/theFakeClipboardHack");
var { TreeScrollHack } = require("hacks/treeScrollHack");

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aPanel
 *        The inspectorpanel XUL element that the viewer is being loaded into.
 */
function DOMTreeViewer(aPanel)
{
  aPanel.viewer = this;
  this.mPanel = aPanel;

  this.commandMediator = aPanel.commandMediator;
  this.commandMediator.decorate(this);

  this.mStringBundle = document.getElementById("domTreeStringBundle");
  aPanel.findbar.placeholder =
    this.mStringBundle.getString("findbarPlaceholder.label");

  aPanel.findbar.addControllerForCommand("cmd_find", this);
  aPanel.findbar.addControllerForCommand("cmd_findPrevious", this);
  aPanel.findbar.addControllerForCommand("cmd_findNext", this);
  aPanel.findbar.addControllerForCommand("cmd_findByClick", this);

  this.mTree = document.getElementById("olDOMTree");
  this.mTree.addEventListener("select", this, false);
  this.mTreeScrollHack = new TreeScrollHack(this.mTree);

  this.mAttributesPasteHelper =
    new AttributesPasteHelper(this.commandMediator);

  this.mInsertNodeController = new InsertNodeController(this, this.mTree);
  this.mDeleteNodeController = new DeleteNodeController(this, this.mTree);
  this.mClipboardController = new ClipboardController(
    this, this.mTree, this.mAttributesPasteHelper
  );

  TheFakeClipboardHack.addObserver("clipboardChange", this);
}

{
  let proto = DOMTreeViewer.prototype;
  proto.mAttributesPasteHelper = null;
  proto.mDeleteNodeController = null;
  proto.mDOMTreeView = null;
  proto.mFindIterator = null;
  proto.mFindQuery = null;
  proto.mInsertNodeController = null;
  proto.mPanel = null;
  proto.mStringBundle = null;
  proto.mSubject = null;
  proto.mTree = null;

  proto.QueryInterface = XPCOMUtils.generateQI([
    "nsIController"
  ]);

  proto.commandMediator = null;
  proto.target = null;

  /**
   * @param a_x_Document
   *        NB: `a_x_Document` is untrusted.
   */
  proto.inspectObject = function DTVr_InspectObject(a_x_Document)
  {
    if (this.mDOMTreeView) {
      this.mDOMTreeView.destroy();
    }

    this.mSubject = new XPCNativeWrapper(a_x_Document);

    this.mDOMTreeView =
      this.mClipboardController.view =
      this.mDeleteNodeController.view =
      this.mInsertNodeController.view =
      new DOMTreeView(this.mSubject, this.commandMediator, this);

    this.mDOMTreeView.showAnonymousNodes =
      ThePrefManager.getPref(".viewers.domTree.showAnonymousNodes");
    this.mDOMTreeView.showSubDocuments =
      ThePrefManager.getPref(".viewers.domTree.showSubDocuments");

    // NB: Don't directly add ourselves as XUL controller for anything in this
    // window without refactoring.  In order to support more commands, create
    // a new controller class or extend an existing one (e.g.,
    // `ClipboardController`).  See the note for `doCommand`.

    let uncoveredNode = this.mSubject;
    if (this.mSubject instanceof nsIDOMHTMLDocument) {
      uncoveredNode = this.mSubject.body;
    }
    else if (this.mSubject instanceof nsIDOMDocument) {
      uncoveredNode = this.mSubject.documentElement;
    }

    this.mDOMTreeView.uncoverNode(uncoveredNode, true);

    this.mTree.view = this.mDOMTreeView;
    this.mDOMTreeView.selection.select(0);
  };

  /**
   * Called by the Inspector framework when this viewer is going away.
   */
  proto.destroy = function DTVr_Destroy()
  {
    TheFakeClipboardHack.removeObserver("clipboardChange", this);

    // We don't need to destroy the clipboard controller, because the viewer
    // document is going away entirely.  The view, however, has observers/
    // listeners that need to be taken care of.
    this.mDOMTreeView.destroy();
    this.mClipboardController.destroy();
  };

  proto.updateContextMenuItems = function DTVr_UpdateContextMenuItems(aPopup)
  {
    let kids = aPopup.childNodes;
    for (let i = 0, n = kids.length; i < n; ++i) {
      if ("command" in kids[i] && kids[i].command) {
        this.updateCommand(kids[i].command);
      }
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIController

  proto.supportsCommand = function DTVr_SupportsCommand(aName)
  {
    switch (aName) {
      case "cmd_find":
      case "cmd_findNext":
      case "cmd_findPrevious":
      case "cmd_findByClick":
        return true;
      break;
    }

    return false;
  };

  proto.isCommandEnabled = function DTVr_IsCommandEnabled(aName)
  {
    switch (aName) {
      case "cmd_find":
      case "cmd_findNext":
      case "cmd_findPrevious":
      case "cmd_findByClick":
        return true;
      break;
    }

    return false;
  };

  proto.doCommand = function DTVr_DoCommand(aName)
  {
    switch (aName) {
      case "cmd_find":
        // No op.  The findbar binding handles this.  We should never get
        // called, because we aren't registered as a XUL controller directly;
        // Just in case that changes due to oversight (as in issue #102),
        // though...
        throw new Error("DOMTreeViewer cmd_find invoked");
      break;
      case "cmd_findNext":
      case "cmd_findPrevious":
        this._doFind(aName == "cmd_findPrevious");
      break;
      case "cmd_findByClick":
        this._toggleFindByClick();
      break;
    }
  };

  proto.onEvent = function DTVr_OnEvent(aName)
  {
  };

  ////////////////////////////////////////////////////////////////////////////
  //// notifications (see notificationManager.js)

  proto.handleNotification = function DTVR_HandleNotification(aName, aData)
  {
    switch (aName) {
      case "clipboardChange":
        // The Inspector will update cmd_paste for the Edit menu, but we have
        // other viewer-specifc paste commands (e.g., cmd_pasteChildNode) in
        // our context menu, and we need to make sure those get updated, too.
        this.updateCommandSet("cmdsDOMTree");
      break;
      default:
        throw new Error("wat.  notification: " + aName);
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIDOMNodeFilter

  /**
   * Used for the find iterator.  Just parse the findbar value as a selector
   * for now.
   * XXX This does mean we can't match non-elements right now, though...
   */
  proto.acceptNode = function DTVr_AcceptNode(aNode)
  {
    if (aNode instanceof nsIDOMElement) {
      if (aNode.mozMatchesSelector(this.mFindQuery)) {
        return nsIDOMNodeFilter.FILTER_ACCEPT;
      }
    }
    return nsIDOMNodeFilter.FILTER_REJECT;
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIObserver

  proto.observe = function DTVr_Observe(aSubject, aTopic, aData)
  {
    switch (aTopic) {
      case "find-by-click-finished":
        this._onFindByClickFinished();
      break;
      default:
        throw new Error("wat.  Topic: " + aTopic);
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIDOMEventListener

  proto.handleEvent = function DTVr_HandleEvent(aEvent)
  {
    switch (aEvent.type) {
      case "select":
        this._onSelect();
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// Internal methods

  proto._onSelect = function DTVr_OnSelect(aEvent)
  {
    // The select event fires when the selection changes.  NB: That includes
    // deselection.  The row at the current index is what we should be
    // operating on.
    let currentIndex = this.mDOMTreeView.selection.currentIndex;
    let currentNode = this.mDOMTreeView.getRowObject(currentIndex);

    // Was it a selection or deselection?
    if (this.mDOMTreeView.selection.isSelected(currentIndex)) {
      // It was the former.  Update the target.
      this.mPanel.changeTarget(currentNode);
    }
    else if (this.target == currentNode) {
      // It was a deselection.  The deselected row corresponds to our target,
      // and there are other rows selected, so we'll find another suitable
      // target.
      let indexes = this.mDOMTreeView.getSelectedIndexes();
      // XXX The nearest in the next-best chain (as in DOMi 2's dom viewer) is
      // probably a better idea than the dumb nearest neighbor that we use
      // both here and DOMi 2...
      let nearestIndex =
        CommonTreeView.getNearestIndex(currentIndex, indexes);
      this.mPanel.changeTarget(
        this.mDOMTreeView.getRowObject(nearestIndex)
      );
    }

    this.updateCommandSet("cmdsDOMTree");
  };

  proto._doFind = function DTVr_DoFind(aSearchBackward)
  {
    if (!this.mPanel.findbar.value) {
      this.mPanel.findbar.show();
      return;
    }

    this._initFindIterator();

    let lastNode = this.mFindIterator.referenceNode;
    let node = this._advanceFindIterator(aSearchBackward);

    let idx = this.mDOMTreeView.uncoverNode(node);
    if (idx < 0) {
      // assert(!node);
      // We reached the edge.  Wrap around.
      this.mFindIterator.reset(aSearchBackward);
      node = this._advanceFindIterator(aSearchBackward);

      if (!node) {
        // XXX show no-matches
        return;
      }

      idx = this.mDOMTreeView.uncoverNode(node);
      // XXX show continued-from-bottom/-top
    }

    this.mDOMTreeView.targetNode(this.mDOMTreeView.getRowObject(idx));
  };

  proto._initFindIterator = function DTVr_InitFindIterator()
  {
    let findbar = this.mPanel.findbar;
    // Three cases we have to be conscious of:
    //   - Find hasn't yet been used, so `mFindIterator` is uninitialized.
    //   - The target isn't what was last returned by the iterator.
    //   - The user changed the query from what was used last.
    //
    // We handle these all the same way: by creating a new iterator.
    // XXX Need to also create a new one if showAnonymousNodes or
    // showSubDocuments change, when we support toggling them from the UI.
    if (!this.mFindIterator ||
        this.target != this.mFindIterator.referenceNode ||
        findbar.value != this.mFindQuery) {
      this.mFindQuery = findbar.value;
      this.mFindIterator = new DeepIterator(
        this.mSubject,
        this,
        this.mDOMTreeView.showAnonymousNodes,
        this.mDOMTreeView.showSubDocuments,
        this.target
      );
    }
    // assert(this.mFindIterator)
  };

  proto._advanceFindIterator = function DTVr_AdvanceFindIterator(aBackward)
  {
    // NB: If we're switching directions, we need to prime the iterator so it
    // doesn't return what it did last time.
    let it = this.mFindIterator;
    if ((aBackward && !it.pointerBeforeReferenceNode ||
         !aBackward && it.pointerBeforeReferenceNode) &&
        it.filter.acceptNode(it.referenceNode) ==
          nsIDOMNodeFilter.FILTER_ACCEPT) {
      if (aBackward) {
        it.previousNode();
      }
      else {
        it.nextNode();
      }
    }

    let node = (aBackward) ?
      it.previousNode() :
      it.nextNode();

    return node;
  };

  proto._toggleFindByClick = function DTVr_ToggleFindByClick()
  {
    if (this.mFindByClick && this.mFindByClick.active) {
      this.mFindByClick.active = false;
    }
    else {
      let bloc = this.mPanel.synthBloc;
      this.mFindByClick = new FindByClick(
        bloc.mirror(this.mSubject), bloc, this
      );
    }
  };

  proto._onFindByClickFinished = function DTVr_OnFindByClickFinished()
  {
    let event = this.mFindByClick.event;
    let node = (ThePrefManager.getPref(".viewers.domTree.showAnonymousNodes")) ?
      this.mPanel.synthBloc.strip(event.get("originalTarget")) :
      this.mPanel.synthBloc.strip(event.get("target"));

    this.mDOMTreeView.targetNode(node);
  };
}
