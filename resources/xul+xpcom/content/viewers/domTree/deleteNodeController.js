/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Command controller for the domTree viewer's cmd_deleteNode.
 *
 * TODO
 * - When a node gets undeleted using Undo, show it in the view.
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.DeleteNodeController = DeleteNodeController;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");

var { DeleteNodeTransaction } = require("transactions/deleteNodeTransaction");
var { CommonTreeView } = require("utils/commonTreeView");

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aViewer
 *        `DOMTreeViewer` we are created for.
 * @param aTree
 *        The viewer's XUL tree element.
 * @param aView
 *        The `DOMTreeView` instance that lives in the tree.
 */
function DeleteNodeController(aViewer, aTree)
{
  this.mViewer = aViewer;
  this.mTree = aTree;

  aTree.controllers.appendController(this);
}

{
  let proto = DeleteNodeController.prototype;
  proto.mTree = null;
  proto.mViewer = null;

  proto.view = null;

  proto.NODE_NULL = DeleteNodeController.NODE_NULL = 1;
  proto.NO_PARENT = DeleteNodeController.NO_PARENT = 2;
  proto.NOT_EXPLICIT_CHILD = DeleteNodeController.NOT_EXPLICIT_CHILD = 3;

  proto.QueryInterface = XPCOMUtils.generateQI([ "nsIController" ]);

  proto.destroy = function DNC_Destroy()
  {
    this.mTree.controllers.removeController(this);
  };

  /**
   * Determine if a node is deletable by our deletion methods.
   *
   * @param aNode
   *        The node to check.
   * @param aFailure [optional]
   *        Outparam whose value will correspond to an error
   *        constant.  If the node is found to be deletable, aFailure will be
   *        not be altered.
   * @return
   *        True iff the given node can be deleted.
   */
  proto.isDeletable = function DNC_IsDeletable(aNode, aFailure)
  {
    let failure = aFailure || { };
    if (!aNode) {
      failure.value = this.NODE_NULL;
      return false;
    }

    if (!aNode.parentNode) {
      failure.value = this.NO_PARENT;
      return false;
    }

    if (Array.indexOf(aNode.parentNode.childNodes, aNode) < 0) {
      failure.value = this.NOT_EXPLICIT_CHILD;
      return false;
    }

    return true;
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIController

  proto.supportsCommand = function DNC_SupportsCommand(aName)
  {
    switch (aName) {
      case "cmd_delete":
        return true;
      break;
    }

    return false;
  };

  proto.isCommandEnabled = function DNC_IsCommandEnabled(aName)
  {
    switch (aName) {
      case "cmd_delete":
        return this.isDeletable(
          CommonTreeView.getSelectedRowObject(this.view)
        );
      break;
    }

    return false;
  };

  proto.doCommand = function DNC_DoCommand(aName)
  {
    switch (aName) {
      case "cmd_delete":
        this._deleteNode(
          CommonTreeView.getSelectedRowObject(this.view)
        );
      break;
    }
  };

  proto.onEvent = function DNC_OnEvent(aName)
  {
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  proto._deleteNode = function DNC_DeleteNode(aNode)
  {
    this.mViewer.commandMediator.doTransaction(
      new DeleteNodeTransaction(aNode)
    );
  };
}
