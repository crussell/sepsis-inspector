/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.StyleView = StyleView;

const Ci = Components.interfaces;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { CommonTreeView } = require("utils/commonTreeView");
var { StyleViewRowData } = require("./styleViewRowData");

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aCommandMediator
 * @param aRules
 *        An array of nsIDOMCSSRule objects.
 * @param aElement [optional]
 */
function StyleView(aCommandMediator, aRules, aElement)
{
  CommonTreeView.call(this);

  this.mRowData = [];

  if (aElement) {
    let inlineStyleData =
      StyleViewRowData.createRowDataForInlineStyle(aElement);
    if (inlineStyleData) {
      this.mRowData.push(inlineStyleData);
    }
  }

  for (let i = 0, n = aRules.length; i < n; ++i) {
    let ruleRowData = StyleViewRowData.createRowDataForRule(aRules[i]);
    this.mRowData.push(ruleRowData);
  }

  this.addManagedRowData(this.mRowData);

  this.rowCount = this.mRowData.length;

  // Iterate backwards so we can use `i` as the index without having to keep
  // track of how many rows are added.
  let i = 0;
  while (i < this.rowCount) {
    if (this.isContainer(i) && !this.isContainerEmpty(i)) {
      this.toggleOpenState(i);
    }

    ++i;
  }

  this.mCommandMediator = aCommandMediator;
  this.mCommandMediator.addTransactionListener(this);
}

{
  let proto = Object.create(CommonTreeView.prototype);
  StyleView.prototype = proto;
  proto.constructor = StyleView;

  proto.mCommandMediator = null;
  proto.mRowData = null;

  proto.destroy = function SV_Destroy()
  {
    this.mCommandMediator.removeTransactionListener(this);
  };

  proto.computeCellProperties =
    function SV_ComputeCellProperties(aRow, aColumn)
  {
    let rowData = this.mRowData[aRow];
    switch (aColumn.id) {
      case "colName":
        if (rowData.rule) {
          return [ "CSS_RULE" ];
        }
        if (rowData.isInlineStyle) {
          return [ "CSS_INLINE_STYLE" ];
        }
        return [ "CSS_PROPERTY_NAME" ];
      break;
      case "colValue":
        return [ "CSS_PROPERTY_VALUE" ];
      break;
      case "colSourceLocation":
        return [ "SOURCE_LOCATION" ];
      break;
      default:
        throw new Error("wat.  Column: " + aColumn.id);
      break;
    }

    return [];
  };

  proto.setRowOpenState = function CTV_SetRowOpenState(aRow, aOpenState)
  {
    this.mRowData[aRow].isOpen = aOpenState;
  };

  proto.expandRow = function CTV_ExpandRow(aRow)
  {
    let rowData = this.mRowData[aRow];
    let kids;
    if (rowData.rule instanceof Ci.nsIDOMCSSGroupingRule ||
        rowData.rule instanceof Ci.nsIDOMMozCSSKeyframesRule) {
      kids = StyleViewRowData.createChildRowDataForRuleList(
        rowData.rule.cssRules, rowData
      );
    }
    else {
      let declaration = this.getDeclaration(aRow);
      kids = StyleViewRowData.createChildRowDataForDeclaration(
        declaration, rowData
      );
    }

    CommonTreeView.arrayCopy(kids, this.mRowData, aRow + 1);

    return kids.length;
  };

  proto.getDeclaration = function SV_GetDeclaration(aRow)
  {
    let rowData = this.mRowData[aRow];
    if (rowData.declaration) {
      return rowData.declaration;
    }

    return ("style" in rowData.rule) ?
      rowData.rule.style :
      null;
  };

  proto.getRowObject = function SV_GetRowObject(aRow)
  {
    if (aRow < 0) {
      return null;
    }

    let rowData = this.mRowData[aRow];
    if (rowData.isInlineStyle) {
      return rowData.element.attributes.getNamedItemNS(null, "style");
    }

    return rowData.rule;
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsITreeView

  proto.getCellText = function SV_GetCellText(aRow, aColumn)
  {
    let rowData = this.mRowData[aRow];
    switch (aColumn.id) {
      case "colName":
        return rowData.name;
      break;
      case "colValue":
        return (rowData.priority) ?
          rowData.value + " !" + rowData.priority :
          rowData.value || "";
      break;
      case "colSourceLocation":
        return rowData.location || "";
      break;
      default:
        throw new Error("wat.  Column: " + aColumn.id);
      break;
    }

    return "";
  };

  proto.getLevel = function SV_GetLevel(aRow)
  {
    return this.mRowData[aRow].level;
  };

  proto.isContainer = function SV_IsContainer(aRow)
  {
    let rowData = this.mRowData[aRow];
    return rowData.isInlineStyle ||
           rowData.rule instanceof Ci.nsIDOMCSSFontFaceRule ||
           rowData.rule instanceof Ci.nsIDOMCSSPageRule ||
           rowData.rule instanceof Ci.nsIDOMCSSStyleRule ||
           rowData.rule instanceof Ci.nsIDOMCSSStyleRule ||
           rowData.rule instanceof Ci.nsIDOMMozCSSKeyframeRule ||
           rowData.rule instanceof Ci.nsIDOMMozCSSKeyframesRule ||
           rowData.rule instanceof Ci.nsIDOMCSSGroupingRule;
  };

  proto.isContainerEmpty = function SV_IsContainerEmpty(aRow)
  {
    let declaration = this.getDeclaration(aRow);
    if (declaration) {
      return !declaration.length;
    }

    let rule = this.mRowData[aRow].rule;
    // assert(rule instanceof Ci.nsIDOMMozCSSKeyframesRule ||
    //        rule instanceof Ci.nsIDOMCSSGroupingRule);
    return !rule.cssRules.length;
  };

  proto.isContainerOpen = function SV_IsContainerOpen(aRow)
  {
    return this.mRowData[aRow].isOpen;
  };

  proto.isEditable = function SV_IsEditable(aRow, aColumn)
  {
    return aRow >= 0 && aColumn.id == "colValue" &&
           !!this.mRowData[aRow].declaration;
  };

  proto.setCellText = function SV_SetCellText(aRow, aColumn, aValue)
  {
    let rowData = this.mRowData[aRow];
    if (!rowData.declaration || aColumn.id != "colValue") {
      throw new Error(
        "Bad setCellText call.  Row: " + aRow + " column: " + aColumn +
        " value:" + aValue
      );
    }

    let idx = aValue.indexOf(" !important");
    let priority = (idx == aValue.length - " !important".length - 1) ?
      "important" :
      "";
    let value = (priority) ?
      aValue.substr(0, idx) :
      aValue;

    let transaction = new EditPropertyTransaction(
      rowData.declaration, rowData.name, value, priority
    );

    // XXX check for errors
    this.mCommandMediator.doTransaction(transaction);
  };

  ////////////////////////////////////////////////////////////////////////////
  //// partial nsITransactionListener (see CommandMediator)

  proto.didDo =
  proto.didUndo =
  proto.didRedo =
    function SV_Did_Do_Or_Undo_Or_Redo(aManager, aTransaction, aResult)
  {
    let transaction = aTransaction.wrappedJSObject;
    if (!transaction) {
      return;
    }

    if (transaction instanceof EditPropertyTransaction &&
        Components.isSuccessCode(aResult)) {
      let parentRowIndex = -1;
      for (let i = 0, n = this.rowCount; i < n; ++i) {
        let rowData = this.mRowData[i];
        if (transaction.declaration == this.getDeclaration(i)) {
          if (parentRowIndex < 0) {
            parentRowIndex = i;
          }

          if (rowData.name != transaction.name) {
            continue;
          }

          let parentRowData = this.mRowData[parentRowIndex];
          if (parentRowData.isInlineStyle) {
            // The cell text in the "Name" column for inline style decs is a
            // serialization of the element's style attribute.  Since one of
            // its properties just changed, we need to update its `name` to
            // reflect the current state of the style attribute.
            let dummy = StyleViewRowData.createRowDataForInlineStyle(
              parentRowData.element
            );
            parentRowData.name = dummy.name;
          }

          rowData = StyleViewRowData.createRowDataForProperty(
            transaction.declaration, transaction.name, parentRowData
          );

          // If the new value was empty, the property will have been removed,
          // and `StyleViewRowData.createRowDataForProperty` will have
          // returned null.
          if (rowData) {
            this.mRowData[i] = rowData;
            this.mTreeBoxObject.invalidateRow(i);
            this.selection.select(i);
          }
          else {
            this.exciseRowData(i, 1);
            --this.rowCount;
            this.mTreeBoxObject.rowCountChanged(i, -1);

            // Update the twisty if it was the only property.
            if (parentRowIndex == i - 1 && !this.hasNextSibling(i)) {
              this.setRowOpenState(parentRowIndex, false);
              this.mTreeBoxObject.invalidateRow(parentRowIndex);
            }
          }

          break;
        }
      }
    }
  };
}
