/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Supplementary script for the cssStyle viewer.
 *
 * This file contains function definitions that are used by the Inspector
 * framework to determine whether the viewer is capable of inspecting a given
 * object and how to refer to the viewer by name in the Inspector UI.
 *
 * The viewer registry has some support for "statically" declaring the types
 * of objects the corresponding viewer will accept, but our filter criteria
 * are more advanced than can be expressed using the registry's declarative
 * primitives.
 *
 * @see viewers/cssStyle/info.json
 * @see registration/theViewerRegistry
 *      For more about info scripts and how they're used.  Look for
 *      occurrences of "scriptURI".
 */

//////////////////////////////////////////////////////////////////////////////

const Ci = Components.interfaces;

//////////////////////////////////////////////////////////////////////////////

/**
 * @param a_x_Object
 *        NB: `a_x_Object` is untrusted.
 * @see registration/viewerInfo
 * @see registration/magicFilter.jsm
 */
exports.filter = function filter(a_x_Object)
{
  if (a_x_Object instanceof Ci.nsIDOMCSSRule ||
      a_x_Object instanceof Ci.nsIDOMCSSRuleList ||
      a_x_Object instanceof Ci.nsIDOMElementCSSInlineStyle) {
    return true;
  }

  // If it's an element's "style" attribute, we can inspect its CSS
  // properties.
  if (a_x_Object instanceof Ci.nsIDOMAttr) {
    let attr = new XPCNativeWrapper(a_x_Object);
    if (attr.localName == "style" && attr.namespaceURI == null &&
        attr.ownerElement instanceof Ci.nsIDOMElementCSSInlineStyle) {
      return true;
    }
  }

  return false;
}

/**
 * @see registration/viewerInfo
 * @see registration/magicLocalizer.jsm
 */
exports.getLocalizedName = function getLocalizedName(aLocale)
{
  // XXX Hardcoded to the en-US localization (i.e., the only localization) for
  // now.
  return "CSS Rules and Properties";
}
