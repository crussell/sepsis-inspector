/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Viewer to show the CSS rules for DOM objects (e.g., elements, stylesheets).
 *
 * TODO:
 * - Make sure the source location is only underlined when hovering over that
 *   col's cell.
 * - Use a style attribute mutation observer to update the inline style
 *   properties.  (And a MozAfterPaint listener for the other stuff?)
 * - The part of the tree binding that deals with editable="true" doesn't seem
 *   to deal with overflow very well.
 * - Use JSCSSP on declarations' cssText to determine which properties are
 *   explicitly set and which are implicitly filled out by Gecko.
 *
 * @see registration/theViewerRegistry
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

const Ci = Components.interfaces;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");

var { StyleView } = require("./styleView");
var { EditPropertyTransaction } = require("transactions/editPropertyTransaction");

var { DOMUtils } = require("xpcom/domUtils");

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aPanel
 *        The inspectorpanel XUL element that the viewer is being loaded into.
 */
function CSSStyleViewer(aPanel)
{
  this.mPanel = aPanel;

  this.commandMediator = aPanel.commandMediator;
  this.commandMediator.decorate(this);

  this.mTree = document.getElementById("olCSSStyle");

  // XXX If we ever change the viewer to contain anything other than a
  // full-width, full-height tree listing the CSS properties, we'll need to
  // a) potentially refactor this to put the tree-specific commands in a
  //    separate controller
  // b) append to the tree element's controllers and not the window's
  window.controllers.appendController(this);

  this.mTree.addEventListener("select", this, false);
  // The treechildren binding uses a mousedown listener for selection, so
  // that's what we'll have to use, too, if we want to cancel it.
  this.mTree.addEventListener("mousedown", this, true);
}

{
  let proto = CSSStyleViewer.prototype;
  proto.mPanel = null;
  proto.mStyleView = null;
  proto.mSubject = null;
  proto.mTree = null;

  proto.QueryInterface = XPCOMUtils.generateQI([
    "nsIController"
  ]);

  proto.commandMediator = null;
  proto.target = null;

  /**
   * @param a_x_Object
   *        NB: `a_x_Object` is untrusted.
   */
  proto.inspectObject = function CSV_InspectObject(a_x_Object)
  {
    this.mSubject = new XPCNativeWrapper(a_x_Object);

    let element, rules = [];
    if (this.mSubject instanceof Ci.nsIDOMCSSRule) {
      rules.push(this.mSubject);
    }
    else if (this.mSubject instanceof Ci.nsIDOMCSSRuleList) {
      rules = rules.slice.call(this.mSubject);
    }
    else if (this.mSubject instanceof Ci.nsIDOMAttr) {
      // Attributes' `ownerElement` is deprecated, with (apparently) no
      // alternative.  Just... Fucking... *sigh* --crussell
      element = this.mSubject.ownerElement;
    }
    else {
      element = this.mSubject;
      let rulesSupportsArray = DOMUtils.getCSSStyleRules(element);
      for (let i = rulesSupportsArray.Count() - 1; i >= 0; --i) {
        let rule = rulesSupportsArray.GetElementAt(i);
        rules.push(rule.QueryInterface(Ci.nsIDOMCSSStyleRule));
      }
    }

    if (this.mStyleView) {
      this.mStyleView.destroy();
    }

    this.mStyleView = new StyleView(this.commandMediator, rules, element);
    this.mTree.view = this.mStyleView;

    let target = (this.mStyleView.rowCount) ?
      this.mStyleView.getRowObject(0) :
      null;

    this.mPanel.changeTarget(target);
  };

  proto.destroy = function CSV_Destroy()
  {
    if (this.mStyleView) {
      this.mStyleView.destroy();
    }

    window.controllers.removeController(this);
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIDOMEventListener

  proto.handleEvent = function CSV_HandleEvent(aEvent)
  {
    switch (aEvent.type) {
      case "mousedown":
        this._onMouseDown(aEvent);
      break;
      case "select":
        this._onSelect(aEvent);
      break;
      default:
        throw new Error("wat.  Event: " + aEvent.type);
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIController

  proto.supportsCommand = function CSV_SupportsCommand(aCommandName)
  {
    switch (aCommandName) {
      case "cmd_editPropertyValue":
        return true;
      break;
    }

    return false;
  };

  proto.isCommandEnabled = function CSV_IsCommandEnabled(aCommandName)
  {
    switch (aCommandName) {
      case "cmd_editPropertyValue":
        return this.mStyleView.isEditable(
          this.mStyleView.selection.currentIndex,
          this.mTree.columns.getNamedColumn("colValue")
        );
      break;
    }

    return false;
  };

  proto.doCommand = function CSV_DoCommand(aCommandName)
  {
    switch (aCommandName) {
      case "cmd_editPropertyValue":
        this._editPropertyValue();
      break;
    }
  };

  proto.onEvent = function CSV_OnEvent(aEventName)
  {
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  proto._onMouseDown = function CSV_OnClick(aEvent)
  {
    if (aEvent.target != this.mTree.body) {
      return;
    }

    let rowOut = {};
    let colOut = {};
    let itemOut = {};
    this.mTree.treeBoxObject.getCellAt(aEvent.clientX, aEvent.clientY, rowOut,
                                       colOut, itemOut);

    if (!colOut.value || colOut.value.id != "colSourceLocation" ||
        itemOut.value != "text") {
      return;
    }

    this._viewSource(
      this.mStyleView.getDeclaration(rowOut.value).parentRule
    );

    // Don't select the row.
    aEvent.stopPropagation();
    aEvent.preventDefault();
  };

  /**
   * @param aRule
   *        nsIDOMCSSStyleRule
   */
  proto._viewSource = function CSV_ViewSource(aRule)
  {
    let line = DOMUtils.getRuleLine(aRule);
    let uri = aRule.parentStyleSheet.href;
    gViewSourceUtils.viewSource(uri, null, null, line);
  };

  proto._editPropertyValue = function CSV_EditPropertyValue()
  {
    this.mTree.startEditing(this.mStyleView.selection.currentIndex,
                            this.mTree.columns.getNamedColumn("colValue"));
  };

  proto._onSelect = function CSV_OnSelect()
  {
    this.mPanel.changeTarget(
      this.mStyleView.getRowObject(this.mStyleView.selection.currentIndex)
    );

    this.updateCommand("cmd_editPropertyValue");
  };
}
