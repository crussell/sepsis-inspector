/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.StyleViewRowData = StyleViewRowData;

const Ci = Components.interfaces;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { DOMUtils } = require("xpcom/domUtils");

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aName
 *        The rule selector or the property name.
 * @param aParentRowData [optional]
 */
function StyleViewRowData(aName, aParentRowData)
{
  this.name = aName;
  this.isOpen = false;
  this.level = (aParentRowData) ?
    aParentRowData.level + 1 :
    0;
  this.isInlineStyle = false;
}

{
  let proto = StyleViewRowData.prototype;

  proto.declaration = null;
  proto.element = null;
  proto.isOpen = null;
  proto.level = null;
  proto.location = null;
  proto.name = null;
  proto.priority = null;
  proto.rule = null;
  proto.value = null;
  proto.isInlineStyle = null;

  proto.createRowDataForInlineStyle =
  StyleViewRowData.createRowDataForInlineStyle =
    function SVRD_CreateRowDataForInlineStyle(aElement)
  {
    if (!aElement.hasAttribute("style")) {
      return null;
    }

    let inlineStyleData = new StyleViewRowData(
      // XXX Escape this properly.
      "style=\"" + aElement.getAttribute("style") + "\""
    );

    inlineStyleData.element = aElement;
    inlineStyleData.declaration = aElement.style;
    inlineStyleData.isInlineStyle = true;

    return inlineStyleData;
  };

  /**
   * @param aRule
   * @param aParentRowData [optional]
   */
  proto.createRowDataForRule =
  StyleViewRowData.createRowDataForRule =
    function SVRD_CreateRowDataForRule(aRule, aParentRowData)
  {
    let rowData = null;
    switch (aRule.type) {
      case aRule.STYLE_RULE:
        rowData = this.createRowDataForStyleRule(aRule, aParentRowData);
      break;
      case aRule.PAGE_RULE:
      case aRule.FONT_FACE_RULE:
      case aRule.CHARSET_RULE:
      case aRule.IMPORT_RULE:
      case aRule.MEDIA_RULE:
      case aRule.NAMESPACE_RULE:
        rowData = this.createRowDataForAtRule(aRule, aParentRowData);
      break;
      default:
        // NB: These rules aren't recognized in all supported versions of
        // Gecko, so we check that they're recognizable first, before
        // checking against them, to prevent warnings.
        if (("SUPPORTS_RULE" in aRule && aRule.type == aRule.SUPPORTS_RULE) ||
            ("MOZ_KEYFRAMES_RULE" in aRule &&
              aRule.type == aRule.MOZ_KEYFRAMES_RULE) ||
            ("FONT_FEATURE_VALUES_RULE" in aRule &&
              aRule.type == aRule.FONT_FEATURE_VALUES_RULE) ||
            ("MOZ_KEYFRAME_RULE" in aRule &&
             aRule.type == aRule.MOZ_KEYFRAME_RULE) ||
            (aRule instanceof Ci.nsIDOMCSSMozDocumentRule)) {
          rowData = this.createRowDataForAtRule(aRule, aParentRowData);
        }
        else {
          throw new Error("What rule is this?");
        }
      break;
    }

    return rowData;
  };

  proto.createRowDataForStyleRule =
  StyleViewRowData.createRowDataForStyleRule =
    function SV_CreateRowDataForStyleRule(aRule)
  {
    let ruleData = new StyleViewRowData(aRule.selectorText);
    if (aRule.parentStyleSheet) {
      // XXX l10n?
      ruleData.location =
        aRule.parentStyleSheet.href + ":" +
        DOMUtils.getRuleLine(aRule);
    }

    ruleData.rule = aRule;

    return ruleData;
  };

  proto.createRowDataForAtRule =
  StyleViewRowData.createRowDataForAtRule =
    function SVRD_CreateRowDataForAtRule(aRule, aParentRowData)
  {
    let ruleData;
    if ("MOZ_KEYFRAME_RULE" in aRule &&
        aRule.type == aRule.MOZ_KEYFRAME_RULE) {
      ruleData = new StyleViewRowData(aRule.keyText, aParentRowData);
    }
    else {
      ruleData = new StyleViewRowData(
        aRule.cssText.replace(/\s*{[\s\S]*/, ""), aParentRowData
      );
    }

    ruleData.rule = aRule;

    return ruleData;
  };

  proto.createChildRowDataForRuleList =
  StyleViewRowData.createChildRowDataForRuleList =
    function SVRD_CreateChildRowDataForRuleList(aRuleList, aParentRowData)
  {
    let rowData = [];
    for (let i = 0, n = aRuleList.length; i < n; ++i) {
      rowData.push(
        this.createRowDataForRule(aRuleList[i], aParentRowData)
      );
    }

    return rowData;
  };

  proto.createChildRowDataForDeclaration =
  StyleViewRowData.createChildRowDataForDeclaration =
    function SVRD_CreateChildRowDataForDeclaration(aDeclaration,
                                                   aParentRowData)
  {
    let rowData = [];

    for (let i = 0, n = aDeclaration.length; i < n; ++i) {
      let propertyRowData = this.createRowDataForProperty(
        aDeclaration, aDeclaration[i], aParentRowData
      );

      rowData.push(propertyRowData)
    }

    return rowData;
  };

  proto.createRowDataForProperty =
  StyleViewRowData.createRowDataForProperty =
    function SVRD_CreateRowDataForProperty(aDeclaration, aPropertyName,
                                           aParentRowData)
  {
    let rowData = new StyleViewRowData(aPropertyName, aParentRowData);
    rowData.value = aDeclaration.getPropertyValue(aPropertyName);
    if (!rowData.value) {
      return null;
    }

    rowData.declaration = aDeclaration;
    rowData.priority = aDeclaration.getPropertyPriority(rowData.name);

    return rowData;
  };
}
