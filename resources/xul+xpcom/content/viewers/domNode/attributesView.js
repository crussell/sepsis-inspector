/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Implementation of the nsITreeView for the domNode viewer's attribute tree.
 *
 * The tree is the one shown when an element is being inspected in the viewer.
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.AttributesView = AttributesView;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { AttributeMutationInfo } = require("./attributeMutationInfo");
var { AttributeRowData } = require("./attributeRowData");
var { EditAttributeTransaction } = require("transactions/editAttributeTransaction");
var { CommonTreeView } = require("utils/commonTreeView");
var { TreeEditWatcher } = require("utils/treeEditWatcher");
var { StubTreeView } = require("utils/stubTreeView");

//////////////////////////////////////////////////////////////////////////////

function AttributesView(aCommandMediator, aElement)
{
  this.mRowData = [];
  this.mCommandMediator = aCommandMediator;
  this.mElement = aElement;

  // `aElement.attributes` is live.  We don't want to use that.
  this.mRowData = this._getRowData(aElement);
  this.rowCount = this.mRowData.length;

  let $MutationObserver = aElement.ownerDocument.defaultView.MutationObserver;
  this.mMutationObserver = 
    new $MutationObserver(this._onAttributeMutations.bind(this));
  this.mMutationObserver.observe(aElement, { attributes: true,
                                             attributeOldValue: true });

  this.mCommandMediator.addTransactionListener(this);
}

{
  let proto = Object.create(StubTreeView.prototype);
  AttributesView.prototype = proto;
  proto.constructor = AttributesView;

  proto.mCommandMediator = null;
  proto.mElement = null;
  proto.mRowData = null;
  proto.mMutationObserver = null;
  proto.mTreeBoxObject = null;
  proto.mTreeEditWatcher = null;

  proto.destroy = function AV_Destroy()
  {
    this.mMutationObserver.disconnect();
    delete this.mMutationObserver;

    this.mTreeEditWatcher.destroy();

    this.mCommandMediator.removeTransactionListener(this);
  };

  proto.getRowObject = function AV_GetRowObject(aRow)
  {
    if (aRow < 0 || aRow >= this.rowCount) {
      return null;
    }

    let rowData = this.mRowData[aRow];
    return this.mElement.attributes.getNamedItemNS(
      rowData.namespaceURI, rowData.localName
    );
  };

  proto.computeCellProperties =
    function AV_ComputeCellProperties(aRow, aColumn)
  {
    let properties = [];
    switch (aColumn.id) {
      case "colName":
        properties.push("attribute-name");
      break;
      case "colValue":
        properties.push("attribute-value");
      break;
    }

    return properties;
  };

  /**
   * For adding an attribute to the element (e.g., using Insert from the
   * context menu).
   *
   * A temporary row is created in the view, and we enter edit mode for the
   * name field, then the value field.  Accepting both causes the attribute to
   * be added, but canceling doesn't affect the element and the temporary
   * placeholder row goes away.
   */
  proto.editNewAttribute =
    function AV_EditNewAttribute()
  {
    // XXX What if the value column isn't visible?
    this.rowCount = this.mRowData.push(
      new AttributeRowData(this.mElement, null, null, null)
    );
    this.mTreeBoxObject.rowCountChanged(this.rowCount - 1, 1);

    this.mTreeEditWatcher.state =
      this._nextInsertionStep(this.rowCount - 1, true);
    this._advanceInsertionFlow(this.mTreeEditWatcher.state);
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsITreeView

  proto.getCellText = function AV_GetCellText(aRow, aColumn)
  {
    let rowData = this.mRowData[aRow];
    switch (aColumn.id) {
      case "colName":
        return rowData.name;
      break;
      case "colValue":
        return rowData.value;
      break;
    }

    return "";
  };

  proto.getCellProperties =
    function AV_GetCellProperties(aRow, aColumn, aProperties)
  {
    return CommonTreeView.prototype.getCellProperties.call(
      this, aRow, aColumn, aProperties
    );
  };

  proto.isEditable = function AV_IsEditable(aRow, aColumn)
  {
    switch (aColumn.id) {
      case "colName":
        return this.mRowData[aRow].pending;
      break;
      case "colValue":
        return true;
      break;
      default:
        throw new Error("wat.  Column: " + aColumn.id);
      break;
    }

    return false;
  };

  proto.setCellText = function AV_SetCellText(aRow, aColumn, aValue)
  {
    let rowData = this.mRowData[aRow];

    // If we're being called as a result of the cmd_insertAttribute flow, we
    // we'll have a dummy row (not backed by the DOM) in the view representing
    // the new attribute.  We want to make sure we get all the data for it
    // upfront before touching the DOM.
    if (rowData.pending) {
      switch (aColumn.id) {
        case "colName":
          rowData.name = rowData.localName = aValue;
        break;
        case "colValue":
          rowData.value = aValue;
        break;
        default:
          throw new Error("wat.  Column: " + aColumn.id);
        break;
      }

      this.mTreeBoxObject.invalidateCell(aRow, aColumn);
      // Just after we return, the tree binding is going to cause our
      // "editEnded" notification handler to fire, so we don't call
      // `_advanceInsertionFlow` here.
      this.mTreeEditWatcher.state = this._nextInsertionStep(aRow, true);
      return;
    }

    // This is an actual, DOM-backed attribute.  Go ahead and make the change.
    let transaction = new EditAttributeTransaction(
      this.mElement, rowData.namespaceURI, rowData.localName, aValue
    );
    this.mCommandMediator.doTransaction(transaction);
    return;
  };

  proto.setTree = function AV_SetTree(aTreeBoxObject)
  {
    if (this.mTreeBoxObject && aTreeBoxObject) {
      throw new Error("View was given another tree.");
    }

    this.mTreeBoxObject = aTreeBoxObject;

    if (this.mTreeBoxObject) {
      this.mTreeEditWatcher = new TreeEditWatcher(aTreeBoxObject.element);
      this.mTreeEditWatcher.addObserver("editEnded", this);
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// Notifications

  /**
   * @see utils/notificationManager.js
   */
  proto.handleNotification = function AV_HandleNotification(aName, aData)
  {
    switch (aName) {
      case "editEnded":
        this._onEditEnded(aData.state);
      break;
      default:
        throw new Error("wat.  Notification: " + aName);
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// partial nsITransactionListener (see CommandMediator)

  proto.didDo =
  proto.didUndo =
  proto.didRedo =
    function AV_Did_Do_Or_Undo_Or_Redo(aManager, aTransaction, aResult)
  {
    let transaction = aTransaction.wrappedJSObject;
    if (!transaction) {
      return;
    }

    if (transaction.element == this.mElement &&
        transaction instanceof EditAttributeTransaction) {
      let idx = this._getAttributeIndex(transaction.localName,
                                        transaction.namespaceURI);
      this.selection.select(idx);
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  /**
   * Given an element, return an array of Attr-like objects for the element's
   * attributes at the time of invokation.  This is helpful for avoiding the
   * "live" nature of the attribute list accessed through
   * `aElement.attributes`.
   *
   * @param aElement
   *        The element to get the attributes for.
   * @return
   *        An array of Attr-like objects, each with a `localName`, `name`,
   *        and `namespaceURI` property.
   */
  proto._getRowData = function AV_GetRowData(aElement)
  {
    let rowData = [];
    for (let i = 0, n = aElement.attributes.length; i < n; ++i) {
      let attr = aElement.attributes[i];
      let { localName, namespaceURI } = attr;
      rowData.push(
        new AttributeRowData(aElement, localName, namespaceURI, attr)
      );
    }

    return rowData;
  };

  /**
   * Mutation observer callback for attribute mutations on `this.mElement`.
   *
   * @param aRecords
   *        An array of `MutationRecord`s.
   */
  proto._onAttributeMutations = function AV_OnAttributeMutations(aRecords)
  {
    this.mTreeBoxObject.beginUpdateBatch();

    let mutations = AttributeMutationInfo.processMutationRecords(aRecords);
    // XXX Optimize me? Use an attribute-to-index map and an adjustment list.
    // The use of `_getAttributeIndex` makes this O(n*m) where m >= n.
    for (let i = 0, n = mutations.length; i < n; ++i) {
      if (mutations[i].isDeletion) {
        this._onAttributeRemoved(mutations[i]);
      }
      else {
        this._onAttributeSet(mutations[i]);
      }
    }

    this.mTreeBoxObject.endUpdateBatch();
  };

  /**
   * Given a name and namespace, perform a linear search for that attribute in
   * this view's working copy of the element's attributes.
   *
   * @param aName
   *        The unqualified attribute name.  NB: Pass in `localName` rather
   *        than `name`, since the latter may return the name qualified by a
   *        namespace prefix.
   * @param aNamespace
   *        The attribute namespace.
   * @return
   *        The index of the attribute.
   */
  proto._getAttributeIndex = function AV_GetAttributeIndex(aName, aNamespace)
  {
    for (let i = 0, n = this.mRowData.length; i < n; ++i) {
      let rowData = this.mRowData[i];
      if (rowData.localName == aName && rowData.namespaceURI == aNamespace) {
        return i;
      }
    }

    return -1;
  };

  proto._onAttributeRemoved = function AV_OnAttributeRemoved(aMutation)
  {
    let idx = this._getAttributeIndex(aMutation.name, aMutation.namespace);
    
    // Select the next attribute if possible, otherwise, the previous one.
    // NB: If the tree changes away from being single seltype, `currentIndex`
    // isn't as reliable and things get a lot more complicated.
    let selectIdx = idx;
    if (this.selection.currentIndex == idx && idx == this.rowCount - 1) {
      --selectIdx;
    }

    this.mRowData.splice(idx, 1);
    --this.rowCount;
    this.mTreeBoxObject.rowCountChanged(idx, -1);

    this.selection.select(selectIdx);
  };

  proto._onAttributeSet = function AV_OnAttributeSet(aMutation)
  {
    if (aMutation.isAddition) {
      // If this attribute is one that has just been added through the
      // viewer's "Insert New Attribute" UI, then it will already be
      // represented in the tree with its own row.
      let idx = this._getAttributeIndex(aMutation.name, aMutation.namespace);
      if (idx >= 0) {
        // assert(this.mRowData[idx].pending);
        // assert(!this.mTreeEditWatcher.state);
        this.mRowData[idx].pending = false;
        this.selection.select(idx);
        // XXX What happens if some other attribute has been added (through a
        // sidechannel, e.g., content script) by the time the insertion flow
        // has finished?  Should we re-order the rows to reflect the
        // attributes' natural order?
        return;
      }

      // Otherwise, this attribute was added through some other means (e.g.,
      // content script, the "Insert New Attribute" UI in another window...).
      idx = this.rowCount;
      // XXX A `MutationInfo` actually isn't sufficiently `Attr`-like.  Its
      // `name` corresponds to an `Attr`'s `localName`.  After processing all
      // records in a batch, we should probably go through and verify the row
      // data's `name` matches the real `Attr`'s `name`, updating it if it
      // doesn't.
      this.mRowData.push(new AttributeRowData(
        this.mElement, aMutation.name, aMutation.namespace, aMutation
      ));

      ++this.rowCount;
      this.mTreeBoxObject.rowCountChanged(idx, 1);
    }
    else {
      // It was neither an addition nor a deletion.  The value just changed. 
      // Figure out which row it is, and update it.
      let idx = this._getAttributeIndex(aMutation.name, aMutation.namespace);
      let rowData = this.mRowData[idx];
      rowData.value = aMutation.value;
      this.mTreeBoxObject.invalidateRow(idx);
    }
  };

  /**
   * @param aRowIndex
   * @param aKeepActive [optional]
   * @return
   *        A string giving the row index, followed by a space separated list
   *        of column IDs where we are still awaiting input.  The first column
   *        ID listed will be the one to move to next.
   */
  proto._nextInsertionStep =
    function AV_NextInsertionStep(aRowIndex, aKeepActive)
  {
    let awaiting = [];
    let rowData = this.mRowData[aRowIndex];
    if (rowData.name == null) {
      awaiting.push("colName");
    }
    if (rowData.value == null) {
      awaiting.push("colValue");
    }

    if (awaiting.length) {
      // In our state string, the first ID indicates the one to move to next.
      // Make sure we don't move to the one we're currently at.
      let currentColumn = this.mTreeBoxObject.element.editingColumn;
      if (currentColumn && currentColumn.id == awaiting[0]) {
        awaiting.push(awaiting.shift());
      }
    }

    return this._buildState(aRowIndex, awaiting, !!aKeepActive);
  }

  /**
   * @param aState
   *        A string in the form returned by `_buildState`.  If the state as
   *        the keep-active bit set, it indicates that the caller knows we're
   *        moving away from the current step for some reason other than
   *        cancelation.  If not set, any artifacts of the cmd_insertAttribute
   *        flow (e.g., the dummy row) will be cleaned up and the flow will be
   *        terminated.
   * @return
   *        Boolean indicating whether the cmd_insertAttribute flow is
   *        finished.
   */
  proto._advanceInsertionFlow = function AV_AdvanceInsertionFlow(aState)
  {
    let { rowIndex, columnIDs, keepActive } = this._decomposeState(aState);
    if (!keepActive) {
      // We were canceled.
      this._removePendingRow(rowIndex);
      return true;
    }

    if (columnIDs.length) {
      // Make sure to clear the keep-active bit here, otherwise it messes up
      // our ability to know when we've been canceled.
      this.mTreeEditWatcher.state = this._buildState(rowIndex, columnIDs);
      this.mTreeBoxObject.element.startEditing(
        rowIndex,
        this.mTreeBoxObject.columns.getNamedColumn(columnIDs[0])
      );

      return false;
    }

    // We're not awaiting any more input.  Go ahead and finalize things by
    // setting the attribute in the DOM.
    let rowData = this.mRowData[rowIndex];

    // ... but first, if they entered a duplicate with an existing attribute,
    // we'll want to overwrite it...
    // assert(rowData.name == rowData.localName);
    if (this.mElement.hasAttributeNS(rowData.namespaceURI, rowData.name)) {
      // ... but we don't want two rows representing it, so just get rid of
      // the dummy row.
      this._removePendingRow(rowIndex);
    }

    let transaction = new EditAttributeTransaction(
      this.mElement, rowData.namespaceURI, rowData.name, rowData.value
    );
    this.mCommandMediator.doTransaction(transaction);

    return true;
  };

  proto._removePendingRow = function AV_RemovePendingRow(aRowIndex)
  {
    // assert(this.mRowData[aRowIndex].pending);
    this.mRowData.splice(aRowIndex, 1);
    --this.rowCount;
    this.mTreeBoxObject.rowCountChanged(aRowIndex, -1);
  };

  /**
   * @param aRowIndex
   * @param aColumns
   * @param aKeepActive [optional]
   *        The caller should specify true if the state should indicate that
   *        it shouldn't be canceled when advancing in the cmd_insertAttribute
   *        flow.
   */
  proto._buildState =
    function AV_BuildState(aRowIndex, aColumns, aKeepActive)
  {
    let result = String(aRowIndex);
    if (aColumns.length) {
      result += " " + aColumns.join(" ");
    }
    if (aKeepActive) {
      result += " *";
    }

    return result;
  };

  /**
   * The inverse of `_buildState`.
   *
   * @param aState
   *        A string in the form returned by `_buildState`.
   * @return
   *        An object with `rowIndex`, `columnIDs`, and `keepActive` fields.
   */
  proto._decomposeState = function AV_DecomposeState(aState)
  {
    let tokens = aState.split(" ");
    let rowIndex = tokens.shift();
    let keepActive = tokens[tokens.length - 1] == "*";
    if (keepActive) {
      tokens.pop();
    }

    return {
      rowIndex: rowIndex,
      columnIDs: tokens,
      keepActive: keepActive
    };
  };

  proto._onEditEnded = function AV_OnEditEnded(aState)
  {
    let finished = this._advanceInsertionFlow(aState);
    if (finished) {
      this.mTreeEditWatcher.state = null;
    }
  };
}
