/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Command controller for the domNode viewer's attribute tree.
 *
 * The tree is the one shown when an element is being inspected in the viewer.
 *
 * TODO
 * - Multiple selection/paste.
 * - cmd_cut
 * - cmd_pasteAsValue
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.AttributesController = AttributesController;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { DeleteAttributeTransaction } = require("transactions/deleteAttributeTransaction");
var { SetTextContentTransaction } = require("transactions/setTextContentTransaction");
var { AttributeParser } = require("utils/attributeParser");
var { AttributesPasteHelper } = require("utils/attributesPasteHelper");
var { XMLNames } = require("utils/xmlNames");

var { TheFakeClipboardHack } = require("hacks/theFakeClipboardHack");

//////////////////////////////////////////////////////////////////////////////

function AttributesController(aViewer, aTree, aView)
{
  this.mViewer = aViewer;
  this.mTree = aTree;
  this.view = aView;

  this.mXMLNames = new XMLNames();
  this.mPasteHelper =
    new AttributesPasteHelper(aViewer.commandMediator, this.mXMLNames);

  aTree.controllers.appendController(this);
}

{
  let proto = AttributesController.prototype;
  proto.mPasteHelper = null;
  proto.mTree = null;
  proto.mViewer = null;
  proto.mXMLNames = null;

  proto.view = null;

  proto.destroy = function DNV_Destroy()
  {
    this.mTree.controllers.removeController(this);
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIController

  proto.supportsCommand = function AC_SupportsCommand(aCommandName)
  {
    switch (aCommandName) {
      case "cmd_copy":
      case "cmd_paste":
      case "cmd_delete":
      case "cmd_editAttributeValue":
      case "cmd_insertAttribute":
        return true;
      break;
    }

    return false;
  };

  proto.isCommandEnabled = function AC_IsCommandEnabled(aCommandName)
  {
    switch (aCommandName) {
      case "cmd_copy":
      case "cmd_delete":
        return this.view.selection.count > 0;
      break;
      case "cmd_paste":
        return this.mPasteHelper.isClipboardPasteable();
      break;
      case "cmd_editAttributeValue":
        return this.view.isEditable(
                 this.view.selection.currentIndex,
                 this.mTree.columns.getNamedColumn("colValue")
               );
      break;
      case "cmd_insertAttribute":
        return !!this.view;
      break;
      default:
        throw new Error("wat.  Command: " + aCommandName);
      break;
    }

    return false;
  };

  proto.doCommand = function AC_DoCommand(aCommandName)
  {
    switch (aCommandName) {
      case "cmd_copy":
        this._copyAttributes();
      break;
      case "cmd_paste":
        this.mPasteHelper.paste(this.mViewer.subject);
      break;
      case "cmd_delete":
        this._deleteAttributes();
      break;
      case "cmd_editAttributeValue":
        this.mTree.startEditing(
          this.view.selection.currentIndex,
          this.mTree.columns.getNamedColumn("colValue")
        );
        // The view's `setCellText` method will make sure any transaction gets
        // onto the undo/redo stack.
      break;
      case "cmd_insertAttribute":
        this.view.editNewAttribute();
      break;
      default:
        throw new Error("wat.  Command: " + aCommandName);
      break;
    }
  };

  proto.onEvent = function AC_OnEvent(aEventName)
  {
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  proto._copyAttributes = function AC_CopyAttributes()
  {
    // XXX Hardcoded for now to assume a single row is selected.
    let attr = this.view.getRowObject(this.view.selection.currentIndex);
    let prefix = this._getPrefix(attr);
    if (prefix) {
      prefix += ":";
    }

    TheFakeClipboardHack.copyContents(
      prefix + attr.localName + "=" + AttributeParser.quoteValue(attr.value)
    );

    this.mViewer.updateCommand("cmd_paste");
  };

  /**
   * @param aAttr
   *        An attribute DOM node.
   * @return
   *        A string naming a suitable prefix to use, based on the given
   *        attribute's namespace.  If the attribute exists in a familiar
   *        namespace, we prefer to return a prefix based on common
   *        conventions over more exotic ones.  Only in the event that we have
   *        no better choice will we resort to using the prefix that's
   *        actually associated with the attribute.  See the notes above.
   */
  proto._getPrefix = function AC_GetPrefix(aAttr)
  {
    let commonPrefix = this.mXMLNames.lookupPrefix(aAttr.namespaceURI);
    return (commonPrefix === null) ?
      aAttr.prefix || "" :
      commonPrefix;
  };

  proto._deleteAttributes = function AC_DeleteAttributes()
  {
    // assert(this.mTree.selType == "single");
    let attr = this.view.getRowObject(this.view.selection.currentIndex);
    let transaction = new DeleteAttributeTransaction(
      this.mViewer.subject, attr.namespaceURI, attr.localName
    );
    this.mViewer.commandMediator.doTransaction(transaction);
  };
}
