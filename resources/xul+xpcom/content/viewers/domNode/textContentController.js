/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Command controller for the domNode viewer's `textContent` editor.
 *
 * The editor in question is the one you get when inspecting some non-element
 * node that has a usable `textContent` property, like attributes or comments.
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.TextContentController = TextContentController;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { SetTextContentTransaction } = require("transactions/setTextContentTransaction");

//////////////////////////////////////////////////////////////////////////////

function TextContentController(aViewer, aInput, aButton)
{
  this.mViewer = aViewer;
  this.mInput = aInput;
  this.mButton = aButton;

  // Attaching ourselves to the parent groupbox doesn't work for some reason,
  // so we do this individually for the controls we're... controlling.
  aInput.controllers.appendController(this);
  aButton.controllers.appendController(this);
}

{
  let proto = TextContentController.prototype;
  proto.mButton = null;
  proto.mInput = null;
  proto.mViewer = null;

  proto.destroy = function DNV_Destroy()
  {
    this.mInput.controllers.removeController(this);
    this.mButton.controllers.removeController(this);
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIController

  proto.supportsCommand = function DNV_SupportsCommand(aCommandName)
  {
    switch (aCommandName) {
      case "cmd_setTextContent":
        return true;
      break;
    }

    return false;
  };

  proto.isCommandEnabled = function DNV_IsCommandEnabled(aCommandName)
  {
    switch (aCommandName) {
      case "cmd_setTextContent":
        // We won't be called unless there's an appropriate element being
        // inspected, since we're only in the "controller sequence" to begin
        // with because the correct panel in the deck is showing.
        return true;
      break;
      default:
        throw new Error("wat.  Command: " + aCommandName);
      break;
    }

    return false;
  };

  proto.doCommand = function DNV_DoCommand(aCommandName)
  {
    switch (aCommandName) {
      case "cmd_setTextContent":
        {
          let transaction;
          transaction = new SetTextContentTransaction(
            this.mViewer.subject, this.mInput.value
          );
          this.mViewer.commandMediator.doTransaction(transaction);
        }
        this.mInput.focus();
      break;
      default:
        throw new Error("wat.  Command: " + aCommandName);
      break;
    }
  };

  proto.onEvent = function DNV_OnEvent(aEventName)
  {
  };
}
