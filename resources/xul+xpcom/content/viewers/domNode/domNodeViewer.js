/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Viewer to show details about a DOM node (e.g., document, element, attr).
 *
 * TODO
 * - User selectable namespace when inserting attributes.
 * - During the cmd_insertAttribute flow, allow tabbing an incomplete field to
 *   the other.  Only accept when both fields have a value and they press
 *   Enter.
 * - Make sure the shortcut for cmd_insertAttribute works from anywhere in the
 *   viewer, not just when the tree has focus.
 *
 * @see registration/theViewerRegistry
 */

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { AttributesController } = require("./attributesController");
var { AttributesView } = require("./attributesView");
var { NodeTypeHelper } = require("utils/nodeTypeHelper");
var { TextContentController } = require("./textContentController");

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aPanel
 *        The inspectorpanel XUL element that the viewer is being loaded into.
 */
function DOMNodeViewer(aPanel)
{
  this.mPanel = aPanel;

  this.commandMediator = aPanel.commandMediator;
  this.commandMediator.decorate(this);

  let $ = document.getElementById.bind(document);
  this.mStringBundle = $("domNodeStringBundle");

  this.mLocalNameInfo = $("txLocalName");
  this.mNamespaceURIInfo = $("txNamespaceURI");
  this.mNodeTypeInfo = $("txNodeType");

  this.mDetailsDeck = $("bxDetailsDeck");

  this.mTextContentBox = $("bxTextContent");
  this.mTextContentInput = $("txTextContent");
  this.mTextContentButton = $("btSetTextContent");

  this.mDocumentDetailsBox = $("bxDocumentDetails");
  this.mDocumentURIInfo = $("txDocumentURI");
  this.mContentTypeInfo = $("txContentType");
  this.mCompatModeInfo = $("txCompatMode");
  this.mCharacterSetInfo = $("txCharacterSet");
  this.mReadyStateInfo = $("txReadyState");

  this.mAttributesBox = $("bxAttributes");
  this.mTree = $("olAttributes");

  this.mTree.addEventListener("focus", this, false);
  this.mTree.addEventListener("blur", this, false);
  this.mTree.addEventListener("select", this, false);
  this.mTree.addEventListener("dblclick", this, false);

  this.commandMediator.addTransactionListener(this);

  this.mAttributesController =
    new AttributesController(this, this.mTree, this.mAttributesView);

  this.mTextContentController = new TextContentController(
    this, this.mTextContentInput, this.mTextContentButton
  );
}

{
  let proto = DOMNodeViewer.prototype;
  proto.mAttributesBox = null;
  proto.mAttributesController = null;
  proto.mAttributesView = null;
  proto.mCharacterSetInfo = null;
  proto.mCompatModeInfo = null;
  proto.mContentTypeInfo = null;
  proto.mDetailsDeck = null;
  proto.mDocumentURIInfo = null;
  proto.mDocumentDetailsBox = null;
  proto.mLocalNameInfo = null;
  proto.mNamespaceURIInfo = null;
  proto.mNodeTypeInfo = null;
  proto.mPanel = null;
  proto.mReadyStateInfo = null;
  proto.mStringBundle = null;
  proto.mSubject = null;
  proto.mTextContentController = null;
  proto.mTextContentBox = null;
  proto.mTextContentInput = null;
  proto.mTree = null;

  proto.commandMediator = null;

  ////////////////////////////////////////////////////////////////////////////
  //// interface sepsisIViewer

  proto.target = null;

  proto.__defineGetter__("subject", function DNV_Subject_Get()
  {
    return this.mSubject;
  });

  /**
   * @param a_x_Node
   *        NB: `a_x_Node` is untrusted.
   */
  proto.inspectObject = function DNV_InspectObject(a_x_Node)
  {
    this.mSubject = new XPCNativeWrapper(a_x_Node);

    if (this.mAttributesView) {
      this.mAttributesView.destroy();
    }

    this._updateNodeInfo(this.mSubject);
    this._updateDetailsDeck(this.mSubject);

    this._retarget();

    this._updateAllCommands();

    return this.mSubject;
  };

  proto.destroy = function DNV_Destroy()
  {
    if (this.mAttributesView) {
      this.mAttributesView.destroy();
    }

    this.commandMediator.removeTransactionListener(this);

    this.mAttributesController.destroy();
    this.mTextContentController.destroy();
  };

  proto.updateContextMenuItems = function DNV_UpdateContextMenuItems(aPopup)
  {
    let kids = aPopup.childNodes;
    for (let i = 0, n = kids.length; i < n; ++i) {
      if ("command" in kids[i] && kids[i].command) {
        this.updateCommand(kids[i].command);
      }
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIDOMEventListener

  proto.handleEvent = function DNV_HandleEvent(aEvent)
  {
    switch (aEvent.type) {
      case "focus":
      case "blur":
        this._updateAllCommands();
      break;
      case "select":
        this._onSelect();
      break;
      case "dblclick":
        this._onDoubleClick(aEvent);
      break;
      case "readystatechange":
        this._onReadyStateChange(aEvent.target);
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// partial nsITransactionListener (see CommandMediator)

  proto.didDo =
  proto.didUndo =
  proto.didRedo =
    function DNV_Did_Do_Or_Undo_Or_Redo(aManager, aTransaction, aResult)
  {
    let transaction = aTransaction.wrappedJSObject;
    if (!transaction) {
      return;
    }

    if (transaction.node == this.mSubject &&
        transaction instanceof SetTextContentTransaction) {
      this._updateTextContentInput(this.mSubject);
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// Internal methods

  proto._updateNodeInfo = function DNV_UpdateNodeInfo(aNode)
  {
    this.mLocalNameInfo.value = aNode.localName;
    this.mNamespaceURIInfo.value = aNode.namespaceURI;
    this.mNodeTypeInfo.value = NodeTypeHelper.getName(aNode.nodeType);
  };


  proto._updateDetailsDeck = function DNV_UpdateDetailsDeck(aNode)
  {
    // Our options are to show the attributes tree, the text content input, or
    // nothing at all.
    switch (aNode.nodeType) {
      // things with useful nodeValues
      case aNode.ATTRIBUTE_NODE:
      case aNode.CDATA_SECTION_NODE:
      case aNode.COMMENT_NODE:
      case aNode.PROCESSING_INSTRUCTION_NODE:
      case aNode.TEXT_NODE:
        this.mDetailsDeck.hidden = false;
        this.mDetailsDeck.selectedPanel = this.mTextContentBox;
        this._updateTextContentInput(aNode);
      break;
      case aNode.DOCUMENT_NODE:
        this.mDetailsDeck.hidden = false;
        this.mDetailsDeck.selectedPanel = this.mDocumentDetailsBox;
        this._updateDocumentDetails(aNode);
      break;
      case aNode.ELEMENT_NODE:
        this.mDetailsDeck.hidden = false;
        this.mDetailsDeck.selectedPanel = this.mAttributesBox;
      break;
      default:
        this.mDetailsDeck.hidden = true;
      break;
    }

    // We we call this unconditionally, because it will null out the view if
    // it's not an element, and that's what we want.
    this._updateAttributesView(aNode);
  };

  proto._updateTextContentInput = function DNV_UpdateTextContentInput(aNode)
  {
    // Circumvent any "attributes' textContent is deprecated" warnings.
    this.mTextContentInput.value = (aNode.nodeType == aNode.ATTRIBUTE_NODE) ?
      aNode.value :
      aNode.textContent;
    // Clear the Undo/Redo stack, so that leaning on the Undo keyboard
    // shortcut won't take the input value back to the text content of any
    // unrelated nodes that we may have been inspecting before `aNode`.
    try {
      // XXX Sometimes this fails when called from the `subject` setter, maybe
      // due to race conditions?
      this.mTextContentInput.editor.transactionManager.clear();
    }
    catch (ex) {
    }
  };

  proto._updateAttributesView = function DNV_UpdateAttributesView(aNode)
  {
    if (aNode.nodeType == aNode.ELEMENT_NODE) {
      this.mAttributesView = new AttributesView(this.commandMediator, aNode);
      this.mAttributesController.view = this.mAttributesView;
      this.mTree.view = this.mAttributesView;
      if (this.mAttributesView.rowCount) {
        this.mAttributesView.selection.select(0);
      }
    }
    else {
      this.mTree.view = this.mAttributesView = null;
    }
  };

  proto._updateDocumentDetails = function DNV_UpdateDocumentDetails(aNode)
  {
    this.mDocumentURIInfo.value = aNode.documentURI;
    this.mContentTypeInfo.value = aNode.contentType;
    this.mCompatModeInfo.value = (aNode.compatMode == "BackCompat") ?
      this.mStringBundle.getString("quirksMode.value") :
      this.mStringBundle.getString("standardsMode.value");
    this.mCharacterSetInfo.value = aNode.characterSet;
    this.mReadyStateInfo.value = aNode.readyState;

    if (aNode.readyState != "complete") {
      aNode.addEventListener("readystatechange", this, false);
    }
  };

  /**
   * Handle the attribute tree's "select" event.
   */
  proto._onSelect = function DNV_OnSelect()
  {
    this._updateAllCommands();
    this._retarget();
  };

  /**
   * Handle the attribute tree's "dblclick" event.
   *
   * @param aEvent
   *        The "dblclick" DOM event.
   */
  proto._onDoubleClick = function DNV_OnDoubleClick(aEvent)
  {
    let rowOut = {};
    this.mTree.treeBoxObject.getCellAt(
      aEvent.clientX, aEvent.clientY, rowOut, {}, {}
    );

    if (rowOut.value < 0) {
      this.dispatchCommand("cmd_insertAttribute");
    }
  };

  proto._updateAllCommands = function DNV_UpdateAllCommands()
  {
    this.updateCommandSet("cmdsDOMNode");
  };

  proto._retarget = function DNV_Retarget()
  {
    let newTarget = null;
    if (!this.mDetailsDeck.hidden &&
        this.mDetailsDeck.selectedPanel == this.mAttributesBox) {
      newTarget = this.mAttributesView.getRowObject(
        this.mAttributesView.selection.currentIndex
      );
    }

    this.mPanel.changeTarget(newTarget);
  };

  proto._onReadyStateChange = function DNV_OnReadyStateChange(aDocument)
  {
    if (aDocument.readyState == "complete") {
      aDocument.removeEventListener("readystatechange", this, false);
    }

    this._updateDocumentDetails(aDocument);
  };
}
