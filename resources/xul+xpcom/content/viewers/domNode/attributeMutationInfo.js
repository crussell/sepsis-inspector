/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 *
 * For processing DOM Mutation Records into a form useful enough for us.
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.AttributeMutationInfo = AttributeMutationInfo;

//////////////////////////////////////////////////////////////////////////////

/**
 * The `name` and `namespace` properties describe those of the mutated
 * attribute.  `next` is non-null iff it's not the final mutation in the
 * mutation sequence for the affected attribute in a batch of mutation
 * records.  If non-null, it's the `AttributeMutationInfo` for the next
 * mutation in that sequence.  `isAddition` and `isDeletion` are for what they
 * sound like.
 *
 * @param aName
 * @param aNamespace
 * @param aNext [optional]
 */
function AttributeMutationInfo(aName, aNamespace, aNext)
{
  this.name = aName;
  this.namespace = aNamespace;
  if (aNext !== undefined) {
    this.next = aNext;
  }
}

/**
 * Go through the given records and create a `AttributeMutationInfo` instance,
 * classifying each mutation as an addition, deletion, or simple modification.
 *
 * NB: This *must* be called synchronously from a DOM Mutation Observer
 * callback.  Otherwise, this is of no use if other mutations are allowed to
 * occur before we do our thing.
 *
 * @param aRecords
 *        An array of DOM `MutationRecord`s.
 * @return
 *        An array of `AttributeMutationInfo` objects.
 */
AttributeMutationInfo.processMutationRecords =
  function AMI_ProcessMutationRecords(aRecords)
{
  // The way this works is, given a record for a mutated attribute, look
  // ahead at the next record affecting that attribute and use its
  // `oldValue` property to figure out how the mutation corresponding to
  // this record changed the attribute in question.  If the next record for
  // the affected attribute has a null `oldValue`, that means the mutation
  // for the current record had to have been a deletion.  If it's non-null,
  // it's the value this mutation set the attribute to.  Additionally, if
  // it's non-null but the *current* record's `oldValue` is null, that means
  // that the current record is for an insertion. 
  //
  // In order to efficiently do this lookahead stuff, first we need to sift
  // the mutations by the attribute affected by each mutation.  This will
  // allow the aforementioned categorization to be done in two passes.

  // For that last bit, we'll keep track of the attributes here.
  // `recordsByAttr`'s properties will be named by a string describing the
  // attribute, and the properties' values will be arrays of mutations of
  // those attributes, ordered chronologically.
  let recordsByAttr = Object.create(null);
  let mutationPairs = [];

  for (let i = 0, n = aRecords.length; i < n; ++i) {
    if (aRecords[i].type != "attributes") {
      continue;
    }

    // We need to verify that the attribute matches on name *and* namespace.
    // We use JSON as a quick and easy encoding method.  We could just
    // delimit them--with a colon, for example--but then we'd have to deal
    // with escaping and unescaping the colons in those fields, and then the
    // edge cases like null namespaces versus namespaces that are the string
    // string "null".
    let attr = JSON.stringify({ namespace: aRecords[i].attributeNamespace,
                                name: aRecords[i].attributeName });
    if (!(attr in recordsByAttr)) {
      recordsByAttr[attr] = [];
    }

    let mutationNumber = recordsByAttr[attr].push(aRecords[i]) - 1;
    mutationPairs[i] = { attribute: attr, mutationNumber: mutationNumber };
  }

  let recordToInfoMap = new WeakMap();
  let mutations = [];
  // NB: We're going backwards, so we can set the infos' `next` at
  // construction.
  for (let i = mutationPairs.length - 1; i >= 0; --i) {
    let pair = mutationPairs[i];

    let attrRecords = recordsByAttr[pair.attribute];

    let record = attrRecords[pair.mutationNumber];
    let nextRecord, nextInfo;
    if (pair.mutationNumber < attrRecords.length - 1) {
      nextRecord = attrRecords[pair.mutationNumber + 1];
      nextInfo = recordToInfoMap.get(nextRecord);
    }

    let info = new AttributeMutationInfo(
      record.attributeName,
      record.attributeNamespace,
      nextInfo
    );

    if (nextRecord) {
      // See comments above regarding the significance of `oldValue`.
      info.value = nextRecord.oldValue;
      info.isDeletion = nextRecord.oldValue == null;
    }
    else {
      // This was the final mutation affecting this attribute in this batch of
      // records, so the relevant details can be found by looking at the
      // current state of the target element.
      info.isDeletion =
        !record.target.hasAttributeNS(info.namespace, info.name);
      if (!info.isDeletion) {
        info.value =
          record.target.getAttributeNS(info.namespace, info.name);
      }
    }

    info.isAddition = !info.isDeletion && record.oldValue == null;

    mutations.unshift(info);
  }

  return mutations;
};

{
  let proto = AttributeMutationInfo.prototype;
  proto.isAddition = null;
  proto.isDeletion = null;
  proto.name = null;
  proto.namespace = null;
  proto.next = null;
  proto.value = null;
}
