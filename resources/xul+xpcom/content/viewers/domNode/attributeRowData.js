/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.AttributeRowData = AttributeRowData;

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aElement
 * @param aLocalName
 * @param aNamespace
 * @param aAttr [optional]
 *        An `Attr` or `Attr`-like object, with `name` and `value` properties.
 */
function AttributeRowData(aElement, aLocalName, aNamespace, aAttr)
{
  let attr = (aAttr === undefined) ?
    aElement.attributes.getNamedItemNS(aNamespace, aLocalName) :
    aAttr;

  if (attr) {
    this.name = attr.name;
    this.value = attr.value;
  }

  this.element = aElement;
  this.localName = aLocalName;
  this.namespaceURI = aNamespace;

  this.pending = !attr;
}

{
  let proto = AttributeRowData.prototype;
  proto.element = null;
  proto.localName = null;
  proto.name = null;
  proto.namespaceURI = null;
  proto.pending = null;
  proto.value = null;
}
