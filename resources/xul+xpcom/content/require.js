/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Implementation of `require` for the XUL+XPCOM platform.
 *
 * In some places, we use the same terminology as NodeJS (e.g., `Loader`s'
 * execution contexts are called "modules") and we use a similar resolution
 * strategy, but the only thing that should be relied on is the ability to
 * call `require`.  This is not an attempt at full compatibility with NodeJS
 * or CommonJS and there is no guarantee that we match the characteristics of
 * those environments—not even the properties available on `require`.
 *
 * The idea is that the loader parts can be used for a minimal `require`
 * implementation that can be easily supported on any other (i.e., non-Gecko)
 * platform that we target.
 *
 * This was forked from Mozilla's toolkit/loader.js to fix some bugs and,
 * along the way, generally make it possible to understand just what the hell
 * is going on in here, exactly.  Dead code paths and those paths that are
 * specific to the Add-on SDK (including its attempt at a security model) have
 * been dropped.
 *
 * Compare to addon-sdk/source/lib/toolkit/loader.js changeset 01332e9ad851
 * from http://hg.mozilla.org/mozilla-central
 */

var require = (function __sepsis_loader_bootstrap__()
{
  let $ = Object.create.bind(Object, null);
  const Cu = Components.utils;
  const { Loader } =
    Cu.import("chrome://sepsis-inspector/content/loader/loader.jsm", $());
  const { Module } =
    Cu.import("chrome://sepsis-inspector/content/loader/module.jsm", $());
  const { Require } =
    Cu.import("chrome://sepsis-inspector/content/loader/require.jsm", $());
  const { Resolver } =
    Cu.import("chrome://sepsis-inspector/content/loader/resolver.jsm", $());

  // We may be getting sourced with `Cu.import` instead of script inclusion.
  let module, resolver, loader, loaderID;
  if (~String(this).indexOf("BackstagePass")) {
    this.EXPORTED_SYMBOLS = [ "require" ];

    module = new Module();
    loaderID = "sepsis JSM loader";
  }
  else if ("document" in Cu.getGlobalForObject(__sepsis_loader_bootstrap__)) {
    let docURI = document.location.href;

    module = new Module(docURI);
    loaderID = "sepsis DOMWindow loader :: " + docURI;
  }
  else {
    // For sandboxes, we punt.  We'll end up on the JSM path above, in a
    // recursive quasi-"call".
    return Cu.import("chrome://sepsis-inspector/content/require.js").require;
  }

  resolver = new Resolver( "chrome://sepsis-inspector/content/");
  loader = new Loader(resolver, { id: loaderID });
  require = new Require(resolver, loader, module);

  return require;
})();
