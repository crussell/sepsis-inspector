/* Any copyright on this file's contents is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/ */

Cu.import("chrome://sepsis-inspector/content/require.js");

var { TheViewerRegistry } = require("registration/theViewerRegistry");

let uids = TheViewerRegistry.getUIDs();
Test.ok(uids.length, "No viewers registered...?");

// It's not important that this figure be up-to-date.  It's just a ball-park
// sanity check.
Test.ok(uids.length >= 9 , "There should be several viewers...");

function checkName(aUID)
{
  let error;
  try {
    TheViewerRegistry.getViewerName(aUID);
  }
  catch (ex) {
    error = ex;
  }

  Test.error(error, "Couldn't get name for UID: " + aUID);
}

for (let i = 0, n = uids.length; i < n; ++i) {
  checkName(uids[i]);
}
