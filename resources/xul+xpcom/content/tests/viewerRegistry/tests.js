/* Any copyright on this file's contents is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/ */

/**
 * @fileoverview
 * TODO:
 * - test IsInspectable
 * - test MetaDataParseError, BadURIError, and InfoScriptError
 */

Cu.import("chrome://sepsis-inspector/content/require.js");

var { ViewerInfo } = require("registration/viewerInfo")
var { TheViewerRegistry } = require("registration/theViewerRegistry");
var JSMirrors = require("jsmirrors");

let $ = JSMirrors.fullLocal;

let uidBase = "org.example.mockuid.";
let uriBase = "chrome://sepsis-inspector/content/tests/viewerRegistry/";

function UID(aUIDPart) { return uidBase + aUIDPart; }
function URI(aUIDPart) { return uriBase + aUIDPart + "Info.json"; }

function registerViewer(aRegistry, aUIDPart)
{
  let info, error;
  try {
    info = aRegistry._buildViewerInfo(UID(aUIDPart), URI(aUIDPart));
  }
  catch (ex) {
    error = ex;
  }

  if (error instanceof ViewerInfo.MetadataParseError ||
      error instanceof ViewerInfo.BadURIError ||
      error instanceof TheTestViewerRegistry.InfoScriptError) {
    Test.error(error.innerException, "Inner exception:", true);
  }

  Test.error(error);

  return info;
}

let loadedAdapterURIs = [];
let adapterLoadCount = 0;

let TheTestViewerRegistry = new TheViewerRegistry.constructor();
TheTestViewerRegistry.adapterRegistry = {
  load: function (aURI) { loadedAdapterURIs.push(aURI); }
};

// We are not necessarily testing the initializer/localizer/filter
// functionality here; what we're testing is mostly that the viewer gets
// assigned the kind of initializer/localizer/filter that it should

let info;
info = registerViewer(TheTestViewerRegistry, "interdentalShark");
Test.is(info.uid, uidBase + "interdentalShark");
Test.is(info.filter.constructor.name, "SyntheticFilter");
Test.is(info.initializer.constructor.name, "ViewerConstructorInitializer");
Test.is(info.localizer.constructor.name, "StringBundleLocalizer");
Test.is(loadedAdapterURIs.length, adapterLoadCount);

adapterLoadCount = loadedAdapterURIs.length;

info = registerViewer(TheTestViewerRegistry, "deisticPanther");
Test.is(info.uid, uidBase + "deisticPanther");
Test.is(info.filter.constructor.name, "CustomCallFilter");
Test.is(info.initializer.constructor.name, "CustomCallInitializer");
Test.is(info.localizer.constructor.name, "CustomCallLocalizer");
Test.is(loadedAdapterURIs.length, adapterLoadCount);

adapterLoadCount = loadedAdapterURIs.length;

let uri;
info = registerViewer(TheTestViewerRegistry, "lemonishDragon");
Test.is(info.uid, uidBase + "lemonishDragon");
Test.is(info.filter.constructor.name, "MagicFilter");
Test.is(info.initializer.constructor.name, "MagicInitializer");
Test.is(info.localizer.constructor.name, "MagicLocalizer");
Test.is(loadedAdapterURIs.length, adapterLoadCount + 2);
uri = "chrome://sepsis-inspector/content/adapters/bonnieBraeAdapter.js";
Test.ok(loadedAdapterURIs.indexOf(uri) >= 0);
uri = "chrome://sepsis-inspector/content/adapters/yenBenAdapter.js";
Test.ok(loadedAdapterURIs.indexOf(uri) >= 0);

adapterLoadCount = loadedAdapterURIs.length;

info = registerViewer(TheTestViewerRegistry, "effeminateGorilla");
Test.is(info.uid, uidBase + "effeminateGorilla");
Test.is(info.filter.constructor.name, "InterfaceFilter");
Test.is(info.initializer.constructor.name, "ViewerConstructorInitializer");
Test.is(info.localizer.constructor.name, "StringBundleLocalizer");
Test.is(loadedAdapterURIs.length, adapterLoadCount);

adapterLoadCount = loadedAdapterURIs.length;

info = registerViewer(TheTestViewerRegistry, "explosiveSeacow");
Test.is(info.uid, uidBase + "explosiveSeacow");
Test.is(info.filter.constructor.name, "ExplosiveSeacowFilter");
Test.ok(info.filter.isReallyESIFilter);
Test.is(info.initializer.constructor.name, "ExplosiveSeacowInitializer");
Test.ok(info.initializer.isReallyESIInitializer);
Test.is(info.localizer.constructor.name, "ExplosiveSeacowLocalizer");
Test.ok(info.localizer.isReallyESILocalizer);
Test.is(loadedAdapterURIs.length, adapterLoadCount + 1);
uri = "chrome://sepsis-inspector/content/adapters/seacowAdapter.js";
Test.ok(loadedAdapterURIs.indexOf(uri) >= 0);

Test.ok(!info.localizer.used);
Test.is(TheTestViewerRegistry.getViewerName(UID("explosiveSeacow")), "Manatee");
Test.ok(info.localizer.used);

let mockAppTarget = { };
let appSID = "org.mozilla.sepsis.inspector.synthetic.appSynthetic";
let mockSM = {
  isSynthetic: function VRT_IS(aThing, aSID)
  {
    return aSID == appSID && aThing == mockAppTarget;
  },
  isAppSynthetic: function VRT_IS(aThing)
  {
    return this.isSynthetic(aThing, appSID);
  },
  strip: function VRT_Strip(aThing) {
    return JSMirrors.strip(aThing);
  }
};

let uid = UID("explosiveSeacow");
Test.ok(!info.filter.used);
Test.ok(!TheTestViewerRegistry.viewerSupportsSubject(uid, $({}), mockSM));
Test.ok(info.filter.used);

// Test `canInspectApplication`.
function supportsIA(aUID)
{
  return TheTestViewerRegistry.viewerSupportsSubject(
    UID(aUID), mockAppTarget, mockSM
  );
}

info.filter.used = false;
Test.ok(TheTestViewerRegistry.viewerSupportsSubject(uid, mockAppTarget, mockSM));
Test.ok(!info.filter.used);

Test.ok(!info.initializer.used);
Test.is(TheTestViewerRegistry.initializeViewer(uid, { }),
        info.scriptModule.exports.mockViewer);
Test.ok(info.initializer.used);

let uids = TheTestViewerRegistry.getUIDs();
Test.ok(uids.indexOf(UID("explosiveSeacow")) >= 0);
Test.ok(uids.indexOf(UID("effeminateGorilla")) >= 0);
Test.ok(uids.indexOf(UID("deisticPanther")) >= 0);
Test.ok(uids.indexOf(UID("lemonishDragon")) >= 0);
Test.ok(uids.indexOf(UID("interdentalShark")) >= 0);
Test.is(uids.length, 5);

Test.ok(!supportsIA("effeminateGorilla"));
Test.ok(!supportsIA("deisticPanther"));
Test.ok(!supportsIA("lemonishDragon"));
Test.ok(!supportsIA("interdentalShark"));

function checkNoViewer(aUID)
{
  let error;
  try {
    TheTestViewerRegistry.getViewerName(aUID);
  }
  catch (ex) {
    error = ex;
  }

  Test.ok(error instanceof TheTestViewerRegistry.NoSuchViewerError,
          "UID shouldn't be registered " + aUID);
}

checkNoViewer(UID("sacreligiousRam"));
checkNoViewer(UID("fickleMarmoset"));

// Go ahead and test these, too.  Note that we're checking, e.g., the UID
// "explosiveSeacow", not the full "org.example.mockuid.explosiveSeacow",
// which should be correct while the former shouldn't.
checkNoViewer("explosiveSeacow");
checkNoViewer("effeminateGorilla");
checkNoViewer("deisticPanther");
checkNoViewer("lemonishDragon");
checkNoViewer("interdentalShark");

