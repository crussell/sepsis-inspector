exports.mockViewer = { };

exports.ExplosiveSeacowInitializer =
  function ExplosiveSeacowInitializer(aInfo)
{
  this.isReallyESIInitializer = true;
  this.used = false;
  this.initializeViewer = function ()
  {
    this.used = true;
    return exports.mockViewer;
  };
}

exports.ExplosiveSeacowLocalizer =
  function ExplosiveSeacowLocalizer(aInfo)
{
  this.isReallyESILocalizer = true;
  this.used = false;
  this.getName = function () {
    this.used = true;
    return "Manatee";
  };
}

exports.ExplosiveSeacowFilter =
  function ExplosiveSeacowFilter(aInfo)
{
  this.isReallyESIFilter = true;
  this.used = false;
  this.test = function () { this.used = true; return false; };
}
