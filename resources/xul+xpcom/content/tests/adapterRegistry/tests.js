/* Any copyright on this file's contents is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/ */

Cu.import("chrome://sepsis-inspector/content/require.js");

var { TheAdapterRegistry } = require("registration/theAdapterRegistry");

function loadAdapter(aRegistry, aURI)
{
  let error;
  try {
    aRegistry.load(aURI);
  }
  catch (ex) {
    error = ex;
  }

  Test.ok(!error, "adapter load error: " + String(error));
}

let registry = new TheAdapterRegistry.constructor();
let baseURI = "chrome://sepsis-inspector/content/adapters/";

loadAdapter(registry, baseURI + "eventTargetAdapter.js");
loadAdapter(registry, baseURI + "styleSheetRuleListAdapter.js");
loadAdapter(registry, baseURI + "declarationRuleAdapter.js");
loadAdapter(registry, baseURI + "objectDetailsAdapter.js");
