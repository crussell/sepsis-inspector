/* Any copyright on this file's contents is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/ */

var { __X1 } = require("./second/__X1");

exports._X11 = { value: "_X11", sub: __X1 };
