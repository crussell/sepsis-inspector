/* Any copyright on this file's contents is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/ */

Cu.import("chrome://sepsis-inspector/content/require.js");

/**
 * A description of each module's dependencies is encoded in its name.  For
 * example, X101 depends on _X01, which itself depends on ___X.   The number
 * of leading underscores should match the directory depth.  X001 is located
 * here in the test top-level dir, while __X1 is located in "./first/second/".
 */
var { X000 } = require("./X000");
var { X001 } = require("./X001");
var { X010 } = require("./X010");
var { X011 } = require("./X011");
var { X100 } = require("./X100");
var { X101 } = require("./X101");
var { X110 } = require("./X110");
var { X111 } = require("./X111");

Test.is(X000.value, "X000");
Test.ok(!("sub" in X000));

Test.is(X001.value, "X001");
Test.is(X001.sub.value, "___X");

Test.is(X010.value, "X010");
Test.is(X010.sub.value, "__X0");

Test.is(X011.value, "X011");
Test.is(X011.sub.value, "__X1");

Test.is(X100.value, "X100");
Test.is(X100.sub.value, "_X00");

Test.is(X101.value, "X101");
Test.is(X101.sub.value, "_X01");

Test.is(X110.value, "X110");
Test.is(X110.sub.value, "_X10");

Test.is(X111.value, "X111");
Test.is(X111.sub.value, "_X11");
