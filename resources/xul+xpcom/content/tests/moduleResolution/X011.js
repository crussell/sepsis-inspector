/* Any copyright on this file's contents is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/ */

var { __X1 } = require("./first/second/__X1");

exports.X011 = { value: "X011", sub: __X1 };
