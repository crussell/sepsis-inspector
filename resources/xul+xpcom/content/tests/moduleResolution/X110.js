/* Any copyright on this file's contents is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/ */

var { _X10 } = require("./first/_X10");

exports.X110 = { value: "X110", sub: _X10 };
