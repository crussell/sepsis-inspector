/* Any copyright on this file's contents is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/ */

var { _X11 } = require("./first/_X11");

exports.X111 = { value: "X111", sub: _X11 };
