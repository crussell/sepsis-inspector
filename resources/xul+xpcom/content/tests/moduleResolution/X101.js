/* Any copyright on this file's contents is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/ */

var { _X01 } = require("./first/_X01");

exports.X101 = { value: "X101", sub: _X01 };
