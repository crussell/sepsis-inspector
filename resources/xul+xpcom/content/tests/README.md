Using the sepsis test-runner
============================

Summary/quick start
-------------------

Here's how we do tests:

1. Write a testfile and save it as chrome/content/tests/[name-of-test]/tests.js

2. Add [name-of-test] to chrome/content/tests/config.json

3. After re-installing the newly built add-on, run the tests and be amazed.

Running tests
-------------

There are two ways to do that last step:

- After making sure sepsis.inspector.tests.commandEnabled is true, press the
  test runner keyboard shortcut from within the Inspector.  The shortcut is
  Cmd+Shift+F7 on Mac and Ctrl+Shift+F7 elsewhere.

- Source chrome://sepsis-inspector/content/harness/testRunner.js somehow and
  manually call `TestRunner.go("chrome://sepsis-inspector/content/tests/")`.
  This may be necessary if things are so badly broken that for some reason you
  aren't even able to bring up the Inspector window.

About the test harness
----------------------

The methods available for use within the testfiles are those defined in
testUtils.js.  Look at that file for an overview.  It bears some superficial
resemblance to Mochitest, with the availability of `Test.is` and `Test.ok`.

The testfiles are not intended to be distributed with the package made
available on AMO; the tests directory is blown away before uploading to AMO,
so neither of the above methods for running the tests will work for
installations made through AMO, unless the testfiles are explicitly re-added
after installation (e.g., by copying them over from sepsis-inspector repo).

The test harness is not specific to sepsis-inspector.  It grew out of the
testRunner.js and testUtils.js files that were originally created for sepsis-
inspector, but was refactored to be suitable for use with other add-ons (so
long as they follow the conventions).  The harness is pretty small: it's all
contained within around 500 LOC embodied by the two files testRunner.js and
testUtils.js.

There's some Inspector-specific glue within the Inspector codebase to make
sure the .tests.commandEnabled pref and the magic key combo work; these things
are not features provided by the test-runner.
