/* Any copyright on this file's contents is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/ */

Cu.import("chrome://sepsis-inspector/content/require.js");
var { Enumerator } = require("utils/enumerator");

let xs, enumerator, error, key;

////////////////////////////////////////////////////////////////////////////

xs = [ 0, 1, 1, 2, 3, 5, 8, 13, 21 ];

enumerator = new Enumerator(xs);

Test.is(enumerator.key, null);

error = null;
try {
  enumerator.getValue();
}
catch (ex) {
  error = ex;
}
Test.ok(!!error);

for (key = 0; key < xs.length; ++key) {
  Test.is(enumerator.next(), xs[key]);
  Test.is(enumerator.key, key);
  Test.ok(!enumerator.isDead);
  if (key < xs.length - 1) {
    Test.ok(enumerator.hasNext());
  }
}

Test.ok(!enumerator.hasNext());

enumerator.destroy();
Test.ok(enumerator.isDead);

Test.is(enumerator.key, null);

error = null;
try {
  enumerator.getValue();
}
catch (ex) {
  error = ex;
}
Test.ok(!!error);
