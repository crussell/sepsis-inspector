/**
 * @fileoverview
 * XXX:
 *   Inspect this:
 *     let o = { x: true };
 *   Then open __defineGetter__ and toggle off Object.prototype properties.
 *   Need a tooltip for proto box.
 */

Cu.import("chrome://sepsis-inspector/content/require.js");
var { Enumerator } = require("utils/enumerator");
var { ObjectDetailsSupplier } = require("synthetic/objectDetailsSupplier");
var { FilteredPropertiesView } = require("viewers/objectProperties/filteredPropertiesView");
var { SynthBloc } = require("synthetic/synthBloc");

let $, $strip, p1, p2, p3, obj, view, idx;

// For dumping the view contents, e.g., to the console, during debugging.
function viewNamesToString(aView)
{
  let str = "rows: " + aView.rowCount + "\n\n";
  for (let i = 0, n = aView.rowCount; i < n; ++i) {
    for (let j = 0, m = aView.getLevel(i); j < m; ++j) {
      str += "  ";
    }
    let openState = (aView.isContainer(i) && aView.isContainerOpen(i)) ?
      "+" :
      "-";
    str += aView.getCellText(i, { id: "colName" }) + " " + openState + "\n";
  }

  return str;
};

function checkRow(aView, aRow, aName, aValue, aLevel)
{
  Test.is(aView.rowStore[aRow].details.name, aName);
  Test.is($strip(aView.rowStore[aRow].details.value), aValue);
  Test.is(aView.rowStore[aRow].level, aLevel);
}

function checkIsNonEmpty(aView, aRow)
{
  Test.ok(aView.isContainer(aRow));
  Test.ok(!aView.isContainerEmpty(aRow));
}

function checkIsOpen(aView, aRow, aState)
{
  let state = (aState === undefined) ?
    true :
    aState;

  if (state) {
    checkIsNonEmpty(aView, aRow);
  }

  Test.is(aView.isContainerOpen(aRow), state);
}

function checkOpenStates(aView)
{
  /*
  // Uncomment this and watch stdout when debugging.
  if ("mBackingView" in aView) {
    let logStr = String((new Error()).stack).match(/runTests.*\n/).toString();
    dump("\n" + logStr + "\n");

    dump(viewNamesToString(aView.mBackingView) + "\n");
    dump(viewNamesToString(aView) + "\n");
  }
  */

  for (let i = 0, n = aView.rowCount; i < n; ++i) {
    if (i > 0 && aView.isContainer(i - 1)) {
      let shouldBeOpen = aView.getLevel(i) > aView.getLevel(i - 1);
      checkIsOpen(aView, i - 1, shouldBeOpen);
    }
  }
}

function makeView(aObject)
{
  // We need some mock objects.  We want a functioning `supplier`.  Suppliers
  // are per-bloc, and synthetic blocs are per-global, so we'll need a sandbox
  // to get a new global.
  let sandbox = Cu.Sandbox(
    Cc["@mozilla.org/systemprincipal;1"].createInstance(Ci.nsIPrincipal)
  );

  // ... and `ObjectDetailsSupplier` expects something that looks like a
  // panel.
  Cu.evalInSandbox(
    "const panel = {" +
    "  panelManager: {" +
    "    addObserver: function () {}," +
    "    isLoadPending: function () { return false; }" +
    "  }," +
    "  viewer: {" +
    "    getTargetSynthetic: function () {}" +
    "  }" +
    "};",
    sandbox
  );

  let bloc = new SynthBloc();
  $ = bloc.mirror.bind(bloc);
  $strip = bloc.strip.bind(bloc);
  sandbox.panel.synthBloc = bloc;
  let supplier = ObjectDetailsSupplier.create(sandbox.panel);

  let view = new FilteredPropertiesView(bloc.mirror(aObject), supplier);

  // Only needed for `viewNamesToString` to work.
  view.stringBundle = { getString: function MV_X_GetString() "(unknown)" };

  view.toggleOpenState(0);
  return view;
}

obj = Object.create(null);

obj.a = Object.create(null);
obj.b = Object.create(null);
obj.c = Object.create(null);

view = makeView(obj);

checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "a", obj.a, 1);
checkRow(view, ++idx, "b", obj.b, 1);
checkRow(view, ++idx, "c", obj.c, 1);
Test.is(view.rowCount, ++idx);

//////////////////////////////////////////////////////////////////////////////

p1 = Object.create(null);
obj = Object.create(p1);

p1.p = Object.create(null);
p1.q = Object.create(null);
p1.r = Object.create(null);

obj.a = Object.create(null);
obj.b = Object.create(null);
obj.c = Object.create(null);
obj.x = Object.create(null);
obj.y = Object.create(null);
obj.z = Object.create(null);

view = makeView(obj);

checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "a", obj.a, 1);
checkRow(view, ++idx, "b", obj.b, 1);
checkRow(view, ++idx, "c", obj.c, 1);
checkRow(view, ++idx, "p", p1.p, 1);
checkRow(view, ++idx, "q", p1.q, 1);
checkRow(view, ++idx, "r", p1.r, 1);
checkRow(view, ++idx, "x", obj.x, 1);
checkRow(view, ++idx, "y", obj.y, 1);
checkRow(view, ++idx, "z", obj.z, 1);
Test.is(view.rowCount, ++idx);

//////////////////////////////////////////////////////////////////////////////

p1 = Object.create(null);
obj = Object.create(p1);

p1.a = Object.create(null);
p1.b = Object.create(null);
p1.c = Object.create(null);
p1.x = Object.create(null);
p1.y = Object.create(null);
p1.z = Object.create(null);

obj.p = Object.create(null);
obj.q = Object.create(null);
obj.r = Object.create(null);

view = makeView(obj);

checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "a", p1.a, 1);
checkRow(view, ++idx, "b", p1.b, 1);
checkRow(view, ++idx, "c", p1.c, 1);
checkRow(view, ++idx, "p", obj.p, 1);
checkRow(view, ++idx, "q", obj.q, 1);
checkRow(view, ++idx, "r", obj.r, 1);
checkRow(view, ++idx, "x", p1.x, 1);
checkRow(view, ++idx, "y", p1.y, 1);
checkRow(view, ++idx, "z", p1.z, 1);
Test.is(view.rowCount, ++idx);

//////////////////////////////////////////////////////////////////////////////
//// Toggling heritage objects' property visibility

p1 = Object.create(null);

p1.a = Object.create(null);
p1.b = Object.create(null);
p1.c = Object.create(null);
p1.x = Object.create(null);
p1.y = Object.create(null);
p1.z = Object.create(null);

p2 = Object.create(p1);

p2.j = Object.create(null);
p2.k = Object.create(null);
p2.l = Object.create(null);

obj = Object.create(p2);

obj.p = Object.create(null);
obj.q = Object.create(null);
obj.r = Object.create(null);

view = makeView(obj);

//   p1   p2   obj
//   X     X    X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "a", p1.a, 1);
checkRow(view, ++idx, "b", p1.b, 1);
checkRow(view, ++idx, "c", p1.c, 1);
checkRow(view, ++idx, "j", p2.j, 1);
checkRow(view, ++idx, "k", p2.k, 1);
checkRow(view, ++idx, "l", p2.l, 1);
checkRow(view, ++idx, "p", obj.p, 1);
checkRow(view, ++idx, "q", obj.q, 1);
checkRow(view, ++idx, "r", obj.r, 1);
checkRow(view, ++idx, "x", p1.x, 1);
checkRow(view, ++idx, "y", p1.y, 1);
checkRow(view, ++idx, "z", p1.z, 1);
Test.is(view.rowCount, ++idx);

view.togglePropertyVisibility($(p1));

//   p1   p2   obj
//         X    X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "j", p2.j, 1);
checkRow(view, ++idx, "k", p2.k, 1);
checkRow(view, ++idx, "l", p2.l, 1);
checkRow(view, ++idx, "p", obj.p, 1);
checkRow(view, ++idx, "q", obj.q, 1);
checkRow(view, ++idx, "r", obj.r, 1);
Test.is(view.rowCount, ++idx);

view.togglePropertyVisibility($(p1));

//   p1   p2   obj
//   X     X    X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "a", p1.a, 1);
checkRow(view, ++idx, "b", p1.b, 1);
checkRow(view, ++idx, "c", p1.c, 1);
checkRow(view, ++idx, "j", p2.j, 1);
checkRow(view, ++idx, "k", p2.k, 1);
checkRow(view, ++idx, "l", p2.l, 1);
checkRow(view, ++idx, "p", obj.p, 1);
checkRow(view, ++idx, "q", obj.q, 1);
checkRow(view, ++idx, "r", obj.r, 1);
checkRow(view, ++idx, "x", p1.x, 1);
checkRow(view, ++idx, "y", p1.y, 1);
checkRow(view, ++idx, "z", p1.z, 1);
Test.is(view.rowCount, ++idx);

view.togglePropertyVisibility($(p2));

//   p1   p2   obj
//   X          X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "a", p1.a, 1);
checkRow(view, ++idx, "b", p1.b, 1);
checkRow(view, ++idx, "c", p1.c, 1);
checkRow(view, ++idx, "p", obj.p, 1);
checkRow(view, ++idx, "q", obj.q, 1);
checkRow(view, ++idx, "r", obj.r, 1);
checkRow(view, ++idx, "x", p1.x, 1);
checkRow(view, ++idx, "y", p1.y, 1);
checkRow(view, ++idx, "z", p1.z, 1);
Test.is(view.rowCount, ++idx);

view.togglePropertyVisibility($(p2));

//   p1   p2   obj
//   X     X    X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "a", p1.a, 1);
checkRow(view, ++idx, "b", p1.b, 1);
checkRow(view, ++idx, "c", p1.c, 1);
checkRow(view, ++idx, "j", p2.j, 1);
checkRow(view, ++idx, "k", p2.k, 1);
checkRow(view, ++idx, "l", p2.l, 1);
checkRow(view, ++idx, "p", obj.p, 1);
checkRow(view, ++idx, "q", obj.q, 1);
checkRow(view, ++idx, "r", obj.r, 1);
checkRow(view, ++idx, "x", p1.x, 1);
checkRow(view, ++idx, "y", p1.y, 1);
checkRow(view, ++idx, "z", p1.z, 1);
Test.is(view.rowCount, ++idx);

view.toggleOpenState(0);

//   p1   p2   obj
//   X     X    X
checkIsOpen(view, idx = 0, false);
Test.is(view.rowCount, ++idx);

view.togglePropertyVisibility($(p1));

view.toggleOpenState(0);

//   p1   p2   obj
//         X    X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "j", p2.j, 1);
checkRow(view, ++idx, "k", p2.k, 1);
checkRow(view, ++idx, "l", p2.l, 1);
checkRow(view, ++idx, "p", obj.p, 1);
checkRow(view, ++idx, "q", obj.q, 1);
checkRow(view, ++idx, "r", obj.r, 1);
Test.is(view.rowCount, ++idx);

view.togglePropertyVisibility($(obj));

//   p1   p2   obj
//         X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "j", p2.j, 1);
checkRow(view, ++idx, "k", p2.k, 1);
checkRow(view, ++idx, "l", p2.l, 1);
Test.is(view.rowCount, ++idx);

view.togglePropertyVisibility($(p1));

//   p1   p2   obj
//   X     X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "a", p1.a, 1);
checkRow(view, ++idx, "b", p1.b, 1);
checkRow(view, ++idx, "c", p1.c, 1);
checkRow(view, ++idx, "j", p2.j, 1);
checkRow(view, ++idx, "k", p2.k, 1);
checkRow(view, ++idx, "l", p2.l, 1);
checkRow(view, ++idx, "x", p1.x, 1);
checkRow(view, ++idx, "y", p1.y, 1);
checkRow(view, ++idx, "z", p1.z, 1);
Test.is(view.rowCount, ++idx);

view.togglePropertyVisibility($(p2));

//   p1   p2   obj
//   X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "a", p1.a, 1);
checkRow(view, ++idx, "b", p1.b, 1);
checkRow(view, ++idx, "c", p1.c, 1);
checkRow(view, ++idx, "x", p1.x, 1);
checkRow(view, ++idx, "y", p1.y, 1);
checkRow(view, ++idx, "z", p1.z, 1);
Test.is(view.rowCount, ++idx);

view.togglePropertyVisibility($(p1));

//   p1   p2   obj
//
checkIsOpen(view, idx = 0, false);
Test.is(view.rowCount, ++idx);

//////////////////////////////////////////////////////////////////////////////

p1 = Object.create(null);
p2 = Object.create(p1);
p3 = Object.create(p2);
obj = Object.create(p3);

p1.a = Object.create(null);
p2.x = Object.create(null);
p2.y = Object.create(null);
p3.y = Object.create(null);
p3.z = Object.create(null);
obj.y = Object.create(null);

view = makeView(obj);

//   p1   p2   p3   obj
//   X     X    X    X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "a", p1.a, 1);
checkRow(view, ++idx, "x", p2.x, 1);
checkRow(view, ++idx, "y", obj.y, 1);
checkRow(view, ++idx, "z", p3.z, 1);
Test.is(view.rowCount, ++idx);

view.togglePropertyVisibility($(p1));

//   p1   p2   p3   obj
//         X    X    X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "x", p2.x, 1);
checkRow(view, ++idx, "y", obj.y, 1);
checkRow(view, ++idx, "z", p3.z, 1);
Test.is(view.rowCount, ++idx);

view.togglePropertyVisibility($(p1));

//   p1   p2   p3   obj
//   X     X    X    X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "a", p1.a, 1);
checkRow(view, ++idx, "x", p2.x, 1);
checkRow(view, ++idx, "y", obj.y, 1);
checkRow(view, ++idx, "z", p3.z, 1);
Test.is(view.rowCount, ++idx);

view.togglePropertyVisibility($(p2));

//   p1   p2   p3   obj
//   X          X    X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "a", p1.a, 1);
checkRow(view, ++idx, "y", obj.y, 1);
checkRow(view, ++idx, "z", p3.z, 1);
Test.is(view.rowCount, ++idx);

view.togglePropertyVisibility($(p2));

//   p1   p2   p3   obj
//   X     X    X    X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "a", p1.a, 1);
checkRow(view, ++idx, "x", p2.x, 1);
checkRow(view, ++idx, "y", obj.y, 1);
checkRow(view, ++idx, "z", p3.z, 1);
Test.is(view.rowCount, ++idx);

view.togglePropertyVisibility($(p3));

//   p1   p2   p3   obj
//   X     X         X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "a", p1.a, 1);
checkRow(view, ++idx, "x", p2.x, 1);
checkRow(view, ++idx, "y", obj.y, 1);
Test.is(view.rowCount, ++idx);

view.togglePropertyVisibility($(p3));

//   p1   p2   p3   obj
//   X     X    X    X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "a", p1.a, 1);
checkRow(view, ++idx, "x", p2.x, 1);
checkRow(view, ++idx, "y", obj.y, 1);
checkRow(view, ++idx, "z", p3.z, 1);
Test.is(view.rowCount, ++idx);

view.togglePropertyVisibility($(obj));

//   p1   p2   p3   obj
//   X     X    X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "a", p1.a, 1);
checkRow(view, ++idx, "x", p2.x, 1);
checkRow(view, ++idx, "z", p3.z, 1);
Test.is(view.rowCount, ++idx);

view.togglePropertyVisibility($(obj));

//   p1   p2   p3   obj
//   X     X    X    X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "a", p1.a, 1);
checkRow(view, ++idx, "x", p2.x, 1);
checkRow(view, ++idx, "y", obj.y, 1);
checkRow(view, ++idx, "z", p3.z, 1);
Test.is(view.rowCount, ++idx);

view.togglePropertyVisibility($(p1));
view.togglePropertyVisibility($(p2));
view.togglePropertyVisibility($(p3));

//   p1   p2   p3   obj
//                   X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "y", obj.y, 1);
Test.is(view.rowCount, ++idx);

view.togglePropertyVisibility($(p3));
view.togglePropertyVisibility($(obj));

//   p1   p2   p3   obj
//              X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "z", p3.z, 1);
Test.is(view.rowCount, ++idx);

view.togglePropertyVisibility($(p2));
view.togglePropertyVisibility($(p3));

//   p1   p2   p3   obj
//         X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "x", p2.x, 1);
Test.is(view.rowCount, ++idx);

view.togglePropertyVisibility($(p1));
view.togglePropertyVisibility($(p2));

//   p1   p2   p3   obj
//   X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "a", p1.a, 1);
Test.is(view.rowCount, ++idx);

view.togglePropertyVisibility($(p3));

//   p1   p2   p3   obj
//   X          X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "a", p1.a, 1);
checkRow(view, ++idx, "z", p3.z, 1);
Test.is(view.rowCount, ++idx);

view.togglePropertyVisibility($(p2));
view.togglePropertyVisibility($(obj));
view.togglePropertyVisibility($(p1));
view.togglePropertyVisibility($(p3));

//   p1   p2   p3   obj
//         X         X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "x", p2.x, 1);
checkRow(view, ++idx, "y", obj.y, 1);
Test.is(view.rowCount, ++idx);

view.togglePropertyVisibility($(p1));
view.togglePropertyVisibility($(p2));

//   p1   p2   p3   obj
//   X               X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "a", p1.a, 1);
checkRow(view, ++idx, "y", obj.y, 1);
Test.is(view.rowCount, ++idx);

//////////////////////////////////////////////////////////////////////////////
//// Testing show/hide inherited properties in multi-level views

p1 = Object.create(null);
p1.a = Object.create(null);
p1.z = Object.create(null);

p2 = Object.create(p1);
p2.p = Object.create(p1);

obj = Object.create(p2);
obj.q = Object.create(p1);
obj.r = Object.create(p2);
obj.s = Object.create(p1);

view = makeView(obj);

//   p1   p2   obj
//   X     X    X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "a", p1.a, 1);
checkRow(view, ++idx, "p", p2.p, 1);
checkRow(view, ++idx, "q", obj.q, 1);
checkRow(view, ++idx, "r", obj.r, 1);
checkRow(view, ++idx, "s", obj.s, 1);
checkRow(view, ++idx, "z", p1.z, 1);
Test.is(view.rowCount, ++idx);

view.toggleOpenState(3); // obj.q

//   p1   p2   obj
//   X     X    X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "a", p1.a, 1);
checkRow(view, ++idx, "p", p2.p, 1);
checkRow(view, ++idx, "q", obj.q, 1);
checkRow(view, ++idx,   "a", p1.a, 2);
checkRow(view, ++idx,   "z", p1.z, 2);
checkRow(view, ++idx, "r", obj.r, 1);
checkRow(view, ++idx, "s", obj.s, 1);
checkRow(view, ++idx, "z", p1.z, 1);
Test.is(view.rowCount, ++idx);
checkOpenStates(view);

view.toggleOpenState(6); // obj.r

//   p1   p2   obj
//   X     X    X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "a", p1.a, 1);
checkRow(view, ++idx, "p", p2.p, 1);
checkRow(view, ++idx, "q", obj.q, 1);
checkRow(view, ++idx,   "a", p1.a, 2);
checkRow(view, ++idx,   "z", p1.z, 2);
checkRow(view, ++idx, "r", obj.r, 1);
checkRow(view, ++idx,   "a", p1.a, 2);
checkRow(view, ++idx,   "p", p2.p, 2);
checkRow(view, ++idx,   "z", p1.z, 2);
checkRow(view, ++idx, "s", obj.s, 1);
checkRow(view, ++idx, "z", p1.z, 1);
Test.is(view.rowCount, ++idx);
checkOpenStates(view);

view.togglePropertyVisibility($(p2));

//   p1   p2   obj
//   X          X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "a", p1.a, 1);
checkRow(view, ++idx, "q", obj.q, 1);
checkRow(view, ++idx,   "a", p1.a, 2);
checkRow(view, ++idx,   "z", p1.z, 2);
checkRow(view, ++idx, "r", obj.r, 1);
checkRow(view, ++idx,   "a", p1.a, 2);
checkRow(view, ++idx,   "z", p1.z, 2);
checkRow(view, ++idx, "s", obj.s, 1);
checkRow(view, ++idx, "z", p1.z, 1);
Test.is(view.rowCount, ++idx);
checkOpenStates(view);

view.togglePropertyVisibility($(p2));

//   p1   p2   obj
//   X     X    X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "a", p1.a, 1);
checkRow(view, ++idx, "p", p2.p, 1);
checkRow(view, ++idx, "q", obj.q, 1);
checkRow(view, ++idx,   "a", p1.a, 2);
checkRow(view, ++idx,   "z", p1.z, 2);
checkRow(view, ++idx, "r", obj.r, 1);
checkRow(view, ++idx,   "a", p1.a, 2);
checkRow(view, ++idx,   "p", p2.p, 2);
checkRow(view, ++idx,   "z", p1.z, 2);
checkRow(view, ++idx, "s", obj.s, 1);
checkRow(view, ++idx, "z", p1.z, 1);
Test.is(view.rowCount, ++idx);
checkOpenStates(view);

view.togglePropertyVisibility($(p1));

//   p1   p2   obj
//         X    X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "p", p2.p, 1);
checkRow(view, ++idx, "q", obj.q, 1);   checkIsOpen(view, idx, false);
checkRow(view, ++idx, "r", obj.r, 1);
checkRow(view, ++idx,   "p", p2.p, 2);
checkRow(view, ++idx, "s", obj.s, 1);
Test.is(view.rowCount, ++idx);
checkOpenStates(view);

view.togglePropertyVisibility($(p1));

//   p1   p2   obj
//   X     X    X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "a", p1.a, 1);
checkRow(view, ++idx, "p", p2.p, 1);
checkRow(view, ++idx, "q", obj.q, 1);   checkIsOpen(view, idx, false);
checkRow(view, ++idx, "r", obj.r, 1);
checkRow(view, ++idx,   "a", p1.a, 2);
checkRow(view, ++idx,   "p", p2.p, 2);
checkRow(view, ++idx,   "z", p1.z, 2);
checkRow(view, ++idx, "s", obj.s, 1);
checkRow(view, ++idx, "z", p1.z, 1);
Test.is(view.rowCount, ++idx);
checkOpenStates(view);

view.toggleOpenState(6); // obj.r.p

//   p1   p2   obj
//   X     X    X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "a", p1.a, 1);
checkRow(view, ++idx, "p", p2.p, 1);
checkRow(view, ++idx, "q", obj.q, 1);
checkRow(view, ++idx, "r", obj.r, 1);   checkIsOpen(view, idx);
checkRow(view, ++idx,   "a", p1.a, 2);
checkRow(view, ++idx,   "p", p2.p, 2);
checkRow(view, ++idx,     "a", p1.a, 3);
checkRow(view, ++idx,     "z", p1.z, 3);
checkRow(view, ++idx,   "z", p1.z, 2);
checkRow(view, ++idx, "s", obj.s, 1);
checkRow(view, ++idx, "z", p1.z, 1);
Test.is(view.rowCount, ++idx);
checkOpenStates(view);

view.togglePropertyVisibility($(p2));

//   p1   p2   obj
//   X          X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "a", p1.a, 1);
checkRow(view, ++idx, "q", obj.q, 1);
checkRow(view, ++idx, "r", obj.r, 1);
checkRow(view, ++idx,   "a", p1.a, 2);
checkRow(view, ++idx,   "z", p1.z, 2);
checkRow(view, ++idx, "s", obj.s, 1);
checkRow(view, ++idx, "z", p1.z, 1);
Test.is(view.rowCount, ++idx);
checkOpenStates(view);

view.togglePropertyVisibility($(p1));

//   p1   p2   obj
//              X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "q", obj.q, 1);
checkRow(view, ++idx, "r", obj.r, 1);   checkIsOpen(view, idx, false);
checkRow(view, ++idx, "s", obj.s, 1);
Test.is(view.rowCount, ++idx);
checkOpenStates(view);

view.togglePropertyVisibility($(p2));

//   p1   p2   obj
//         X    X
checkIsOpen(view, idx = 0);
checkRow(view, ++idx, "p", p2.p, 1);
checkRow(view, ++idx, "q", obj.q, 1);
checkRow(view, ++idx, "r", obj.r, 1);   checkIsOpen(view, idx, false);
checkRow(view, ++idx, "s", obj.s, 1);
Test.is(view.rowCount, ++idx);
checkOpenStates(view);
