/* Any copyright on this file's contents is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/ */

Cu.import("chrome://sepsis-inspector/content/require.js");

var { Logger } = require("utils/logger");
var { MagicFilter } = require("registration/magicFilter");
var { MagicLocalizer } = require("registration/magicLocalizer");
var { MagicInitializer } = require("registration/magicInitializer");

function makeMockInfo(aFilterFn, aLocalizerFn, aInitializerFn)
{
  return {
    uid: "org.example.mockuid",
    scriptModule: {
      exports: {
        filter: aFilterFn,
        getLocalizedName: aLocalizerFn,
        initializeViewer: aInitializerFn
      }
    }
  };
}

{
  let mockViewer = { };
  let mockPanel = { };
  let mockInspectee = { };

  let fUsed = false;
  let lUsed = false;
  let iUsed = false;

  let filter = function filter(a_x_Object)
  {
    Test.is(a_x_Object, mockInspectee);
    fUsed = true;
    return true;
  }

  let getLocalizedName = function getLocalizedName(aLocale)
  {
    Test.is(aLocale, "en-US");
    lUsed = true;
    return "Mock Viewer";
  }

  let initializeViewer = function initializeViewer(aPanel)
  {
    Test.is(aPanel, mockPanel);
    iUsed = true;
    return mockViewer;
  }

  let mockInfo = makeMockInfo(filter, getLocalizedName, initializeViewer);

  let magicFilter = new MagicFilter(mockInfo);
  let magicLocalizer = new MagicLocalizer(mockInfo);
  let magicInitializer = new MagicInitializer(mockInfo);

  let fResult = magicFilter.test(mockInspectee);
  Test.ok(fUsed);
  Test.ok(fResult);

  let lResult = magicLocalizer.getName("en-US");
  Test.ok(lUsed);
  Test.is(lResult, "Mock Viewer");

  let iResult = magicInitializer.initializeViewer(mockPanel);
  Test.ok(iUsed);
  Test.is(iResult, mockViewer);
}

// Now test magic initializer function pecking order.

{
  let mockPanel = { };
  let mockViewer1 = { };
  let mockViewer2 = { };
  let mockViewer3 = { };

  let iResult;
  let i1Used = false;
  let i2Used = false;
  let i3Used = false;

  let mockInfo = makeMockInfo(null, null, null);
  mockInfo.scriptModule.exports.initializeViewer = function initializeViewer(aPanel)
  {
    Test.is(aPanel, mockPanel);
    i1Used = true;
    return mockViewer1;
  };

  mockPanel.viewerContext = {
    initializeViewer: function initializeViewer(aPanel)
    {
      Test.is(aPanel, mockPanel);
      i2Used = true;
      return mockViewer2;
    }
  };

  let magicInitializer = new MagicInitializer(mockInfo);

  iResult = magicInitializer.initializeViewer(mockPanel);
  Test.ok(i1Used);
  Test.is(iResult, mockViewer1);

  delete mockInfo.scriptModule.exports.initializeViewer;

  iResult = magicInitializer.initializeViewer(mockPanel);
  Test.ok(i2Used);
  Test.is(iResult, mockViewer2);

  // XXX Remove this for sepsis-inspector 0.6.
  {
    delete mockPanel.viewerContext.initializeViewer;
    mockInfo.scriptModule.exports.initialize = function initialize(aPanel)
    {
      Test.is(aPanel, mockPanel);
      i3Used = true;
      return mockViewer3;
    };

    Logger.print(
      "Seeing this no-initializeViewer/initialize-deprecated warning for " +
      "org.example.mockuid from magicInitializer.jsm is situation normal.  " +
      "In fact, if you don't get a warning, there's a problem.\n\n" +
      "[[ IGNORE [["
    );
    iResult = magicInitializer.initializeViewer(mockPanel);
    Logger.print("]] IGNORE ]]");
    Test.ok(i3Used);
    Test.is(iResult, mockViewer3);
  }
}
