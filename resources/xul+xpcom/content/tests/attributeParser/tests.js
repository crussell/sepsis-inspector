/* Any copyright on this file's contents is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/ */
 
Components.utils.import("chrome://sepsis-inspector/content/require.js");

let { AttributeParser } = require("utils/attributeParser");

let parser, input;

parser = new AttributeParser(input = "");
Test.ok(parser.state != AttributeParser.FINISHED);
Test.is(parser.input, input);
Test.ok(parser.prefix === null);
Test.ok(parser.localName === null);
Test.ok(parser.value === null);

parser = new AttributeParser(input = "foo=\"bar\"");
Test.is(parser.state, AttributeParser.FINISHED);
Test.is(parser.input, input);
Test.ok(parser.prefix === null);
Test.is(parser.localName, "foo");
Test.is(parser.value, "bar");

parser = new AttributeParser(input = "baz:foo=\"bar\"");
Test.is(parser.state, AttributeParser.FINISHED);
Test.is(parser.input, input);
Test.is(parser.prefix, "baz");
Test.is(parser.localName, "foo");
Test.is(parser.value, "bar");

parser = new AttributeParser(input = "foo='bar'");
Test.is(parser.state, AttributeParser.FINISHED);
Test.is(parser.input, input);
Test.ok(parser.prefix === null);
Test.is(parser.localName, "foo");
Test.is(parser.value, "bar");

parser = new AttributeParser(input = "baz:foo='bar'");
Test.is(parser.state, AttributeParser.FINISHED);
Test.is(parser.input, input);
Test.is(parser.prefix, "baz");
Test.is(parser.localName, "foo");
Test.is(parser.value, "bar");

parser = new AttributeParser(input = "foo='bar(\"baz\")'");
Test.is(parser.state, AttributeParser.FINISHED);
Test.is(parser.input, input);
Test.is(parser.prefix, null);
Test.is(parser.localName, "foo");
Test.is(parser.value, "bar(\"baz\")");

parser = new AttributeParser(input = "foo=\"bar('baz')\"");
Test.is(parser.state, AttributeParser.FINISHED);
Test.is(parser.input, input);
Test.is(parser.prefix, null);
Test.is(parser.localName, "foo");
Test.is(parser.value, "bar('baz')");

let error;

try {
  parser = new AttributeParser("foo=\"bar(\"baz\")\"");
}
catch (ex) {
  error = ex;
}
Test.ok(error);

error = null;
try {
  parser = new AttributeParser("foo='bar('baz')'");
}
catch (ex) {
  error = ex;
}
Test.ok(error);

Test.is(AttributeParser.quoteValue("foo'bar"), "\"foo'bar\"");
Test.is(AttributeParser.quoteValue("foo\"bar"), "'foo\"bar'");
Test.is(AttributeParser.quoteValue("fo'ob\"ar"), "\"fo'ob&quot;ar\"");

parser = new AttributeParser(input = "baz=\"fo'ob&quot;ar\"");
Test.is(parser.state, AttributeParser.FINISHED);
Test.is(parser.input, input);
Test.is(parser.prefix, null);
Test.is(parser.localName, "baz");
// XXX We don't support entities right now, so this fails:
// Test.is(parser.value, "fo'ob\"ar");

// We support parsing only a single pair/triplet at a time.
error = null;
try {
  parser = new AttributeParser("foo='bar' fum='baz'");
}
catch (ex) {
  error = ex;
}

error = null;
try {
  parser = new AttributeParser("foo=\"bar\" fum=\"baz\"");
}
catch (ex) {
  error = ex;
}
Test.ok(error);

error = null;
try {
  parser = new AttributeParser("foo='b'ar'");
}
catch (ex) {
  error = ex;
}
Test.ok(error);

error = null;
try {
  parser = new AttributeParser("foo=\"b\"ar\"");
}
catch (ex) {
  error = ex;
}
Test.ok(error);

error = null;
try {
  parser = new AttributeParser("foo=\"b\"ar'");
}
catch (ex) {
  error = ex;
}
Test.ok(error);

error = null;
try {
  parser = new AttributeParser("foo='b'ar\"");
}
catch (ex) {
  error = ex;
}
Test.ok(error);
