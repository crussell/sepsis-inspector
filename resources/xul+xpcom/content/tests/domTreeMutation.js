/**
 * @fileoverview
 * Requirements:
 *   content/utils/stubTreeView.js
 *   content/utils/commonTreeView.js
 *   content/utils/nodeTypeHelper.js
 *   content/viewers/domTree/domTreeView.js
 */
Cu.import("chrome://sepsis-inspector/content/require.js");

var { Logger } = require("utils/logger");

let nodeTypes = {
  "document": document.DOCUMENT_NODE,
  "doctype": document.DOCUMENT_TYPE_NODE,
  "element": document.ELEMENT_NODE,
  "text": document.TEXT_NODE
};

function checkDOM(aNode, aStructure, aView, aRow)
{
  Test.is(aNode.nodeType, nodeTypes[aStructure.type]);
  Test.is(aView.getRowObject(aRow), aNode);
  if ("name" in aStructure) {
    Test.is(aNode.localName.toLowerCase(), aStructure.name);
  }
  let idx = aRow + 1;
  if ("children" in aStructure && aStructure.children) {
    for (let i = 0, n = aStructure.children.length; i < n; ++i) {
      // XXX Using childNodes means anonymous content and subdocuments don't
      // get tested.
      idx +=
        checkDOM(aNode.childNodes[i], aStructure.children[i], aView, idx);
    }
  }

  return idx - aRow;
}

function runTests()
{
  let doc = document.implementation.createHTMLDocument("");
  let view = new DOMTreeView(doc);

  for (let i = 0; i < view.rowCount; ++i) {
    if (!view.isContainerEmpty(i) && !view.isContainerOpen(i)) {
      view.toggleOpenState(i);
    }
  }

  let structure = {
    type: "document", children: [
      { type: "doctype" },
      { name: "html", type: "element", children: [
        { name: "head", type: "element", children: [
          { name: "title", type: "element", children: [
            { type: "text" } ] } ] },
        { name: "body", type: "element", children: null } ] } ]
  };

  checkDOM(doc, structure, view, 0);

  doc.body.appendChild(doc.createElement("div"));
  // It should be the same.  The body element had no child nodes before, so it
  // wasn't toggled open.
  Test.ok(!view.isContainerOpen(6));
  checkDOM(doc, structure, view, 0);

  view.toggleOpenState(6);
  Test.ok(view.isContainerOpen(6));
  structure = {
    type: "document", children: [
      { type: "doctype" },
      { name: "html", type: "element", children: [
        { name: "head", type: "element", children: [
          { name: "title", type: "element", children: [
            { type: "text" } ] } ] },
        { name: "body", type: "element", children: [
          { name: "div", type: "element", children: null } ] } ] } ]
  };
  checkDOM(doc, structure, view, 0);

  structure = {
    name: "body", type: "element", children: [
      { name: "div", type: "element", children: null } ]
  };
  checkDOM(doc.body, structure, view, 6);

  // From here on, we'll use the abridged structure, as above.

  // Mutations and observer callbacks aren't synchronous.  That's the whole
  // point, actually.  So we chain these tests together via setTimeout-invoked
  // functions so everything in this call stack can run through and unwind.
  doc.body.appendChild(doc.createElement("span"));
  let testChunk1 = function() {
    Test.ok(view.isContainerOpen(6));
    structure = {
      name: "body", type: "element", children: [
        { name: "div", type: "element", children: null },
        { name: "span", type: "element", children: null } ]
    };
    checkDOM(doc.body, structure, view, 6);
    testChunk2();
  };

  let testChunk2 = function() {
    doc.body.removeChild(doc.body.firstChild);
    setTimeout(function() {
      Test.ok(view.isContainerOpen(6));
      structure = {
        name: "body", type: "element", children: [
          { name: "span", type: "element", children: null } ]
      };
      checkDOM(doc.body, structure, view, 6);
      testChunk3();
    }, 100);
  };

  let testChunk3 = function() {
    doc.body.removeChild(doc.body.firstChild);
    setTimeout(function() {
      Test.ok(!view.isContainerOpen(6));
      structure = {
        name: "body", type: "element", children: null
      };
      checkDOM(doc.body, structure, view, 6);

      alert("all tests finished");
    }, 100);
  };

  setTimeout(testChunk1, 100);
}

try {
  runTests();
}
catch (ex) {
  Logger.print(ex.stack);
  throw ex;
}
