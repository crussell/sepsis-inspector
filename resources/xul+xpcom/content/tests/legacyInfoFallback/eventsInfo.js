/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

const viewerURI =
  "chrome://sepsis-inspector/content/viewers/events/events.xul";

const config = {
  adapterURIs: [
    "chrome://sepsis-inspector/content/adapters/eventTargetAdapter.js"
  ]
};

function filter(a_x_Object)
{
  return a_x_Object instanceof Components.interfaces.nsIDOMEventTarget;
}

function getLocalizedName(aLocale)
{
  return "Events";
}

function initializeViewer(aPanel)
{
  return new aPanel.viewerContext.EventsViewer(aPanel);
}
