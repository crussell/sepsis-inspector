/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

const viewerURI =
  "chrome://sepsis-inspector/content/viewers/objectDetails/objectDetails.html";

const config = {
  wantsSynthetic: true,
  adapterURIs: [
    "chrome://sepsis-inspector/content/adapters/objectDetailsAdapter.js"
  ]
}

function getLocalizedName(aLocale)
{
  return "Object Details";
}

function filter(a_x_Object, aSynthBloc)
{
  let sid = "org.mozilla.sepsis.inspector.synthetic.objectDetails";
  if (!aSynthBloc || !aSynthBloc.isSynthetic(a_x_Object, sid)) {
    return false;
  }

  return a_x_Object.name != null;
}

function initializeViewer(aPanel)
{
  return new aPanel.viewerContext.ObjectDetailsViewer(aPanel);
}
