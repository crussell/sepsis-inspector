/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

const Ci = Components.interfaces;

const viewerURI =
  "chrome://sepsis-inspector/content/viewers/cssStyle/cssStyle.xul";

const config = {
  adapterURIs: [
    "chrome://sepsis-inspector/content/adapters/declarationRuleAdapter.js",
    "chrome://sepsis-inspector/content/adapters/styleSheetRuleListAdapter.js"
  ]
};

/**
 * @param a_x_Object
 *        NB: `a_x_Object` is untrusted.
 */
function filter(a_x_Object)
{
  if (a_x_Object instanceof Ci.nsIDOMCSSRule ||
      a_x_Object instanceof Ci.nsIDOMCSSRuleList ||
      a_x_Object instanceof Ci.nsIDOMElementCSSInlineStyle) {
    return true;
  }

  // If it's an element's "style" attribute, we support that.
  if (a_x_Object instanceof Ci.nsIDOMAttr) {
    let attr = new XPCNativeWrapper(a_x_Object);
    if (attr.localName == "style" && attr.namespaceURI == null &&
        attr.ownerElement instanceof Ci.nsIDOMElementCSSInlineStyle) {
      return true;
    }
  }

  return false;
}

function getLocalizedName(aLocale)
{
  return "CSS Rules and Properties";
}

function initializeViewer(aPanel)
{
  return new aPanel.viewerContext.CSSStyleViewer(aPanel);
}
