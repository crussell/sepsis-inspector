/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

const viewerURI =
  "chrome://sepsis-inspector/content/viewers/objectProperties/objectProperties.xul";

const config = {
  useMirrors: true
};

function filter(a_x_Object)
{
  return true;
}

function getLocalizedName(aLocale)
{
  return "Object Properties and Methods";
}

function initializeViewer(aPanel)
{
  return new aPanel.viewerContext.ObjectPropertiesViewer(aPanel);
}
