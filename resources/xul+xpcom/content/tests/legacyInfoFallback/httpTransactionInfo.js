/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

const viewerURI =
  "chrome://sepsis-inspector/content/viewers/httpTransaction/httpTransaction.xul";

const config = {
  wantsSynthetic: true
};

function filter(a_x_Object, aSynthBloc)
{
  let sid = "org.mozilla.sepsis.inspector.synthetic.networkActivity";
  return (aSynthBloc) ?
    aSynthBloc.isSynthetic(a_x_Object, sid) :
    false;
}

function getLocalizedName(aLocale)
{
  return "HTTP Transaction";
}

function initializeViewer(aPanel)
{
  return new aPanel.viewerContext.HTTPTransactionViewer(aPanel);
}
