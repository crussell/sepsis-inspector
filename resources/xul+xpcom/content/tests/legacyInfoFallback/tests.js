/* Any copyright on this file's contents is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/ */

Cu.import("chrome://sepsis-inspector/content/require.js");

var { ViewerInfo } = require("registration/viewerInfo")
var { LegacyInfoFallback } = require("hacks/legacyInfoFallback")

let info;
let uidBase = "org.example.fakeuid.";
let uriBase = "chrome://sepsis-inspector/content/tests/legacyInfoFallback/";

function _do(aUID, aURI)
{
  let info = LegacyInfoFallback.createViewerInfo(aUID, aURI);
  Test.ok(info.metadata.fixedUpFromLegacyInfo);
  Test.is(info.uid, aUID);
  Test.is(info.jsonURI, aURI);

  return info;
}

info = _do(uidBase + "categoryManager", uriBase + "categoryManagerInfo.js");
Test.ok(info.metadata.canInspectApplication);
Test.is(info.metadata.adapterURIs.length, 0);
Test.is(info.metadata.wantsSynthetic, false);
Test.is(info.metadata.useMirrors, false);

info = _do(uidBase + "cssStyle", uriBase + "cssStyleInfo.js");
Test.is(info.metadata.canInspectApplication, false);
Test.is(info.metadata.adapterURIs.length, 2);
Test.is(info.metadata.wantsSynthetic, false);
Test.is(info.metadata.useMirrors, false);

info = _do(uidBase + "domNode", uriBase + "domNodeInfo.js");
Test.is(info.metadata.canInspectApplication, false);
Test.is(info.metadata.adapterURIs.length, 0);
Test.is(info.metadata.wantsSynthetic, false);
Test.is(info.metadata.useMirrors, false);

info = _do(uidBase + "domTree", uriBase + "domTreeInfo.js");
Test.is(info.metadata.canInspectApplication, false);
Test.is(info.metadata.adapterURIs.length, 0);
Test.is(info.metadata.wantsSynthetic, false);
Test.is(info.metadata.useMirrors, false);

info = _do(uidBase + "eventListeners", uriBase + "eventListenersInfo.js");
Test.is(info.metadata.canInspectApplication, false);
Test.is(info.metadata.adapterURIs.length, 0);
Test.is(info.metadata.wantsSynthetic, false);
Test.is(info.metadata.useMirrors, false);

info = _do(uidBase + "events", uriBase + "eventsInfo.js");
Test.is(info.metadata.canInspectApplication, false);
Test.is(info.metadata.adapterURIs.length, 1);
Test.is(info.metadata.wantsSynthetic, false);
Test.is(info.metadata.useMirrors, false);

info = _do(uidBase + "httpTransaction", uriBase + "httpTransactionInfo.js");
Test.is(info.metadata.canInspectApplication, false);
Test.is(info.metadata.adapterURIs.length, 0);
Test.ok(info.metadata.wantsSynthetic);
Test.is(info.metadata.useMirrors, false);

info = _do(uidBase + "networkActivity", uriBase + "networkActivityInfo.js");
Test.ok(info.metadata.canInspectApplication);
Test.is(info.metadata.adapterURIs.length, 0);
Test.is(info.metadata.wantsSynthetic, false);
Test.is(info.metadata.useMirrors, false);

info = _do(uidBase + "objectDetails", uriBase + "objectDetailsInfo.js");
Test.is(info.metadata.canInspectApplication, false);
Test.is(info.metadata.adapterURIs.length, 1);
Test.ok(info.metadata.wantsSynthetic);
Test.is(info.metadata.useMirrors, false);

info = _do(uidBase + "objectProperties", uriBase + "objectPropertiesInfo.js");
Test.is(info.metadata.canInspectApplication, false);
Test.is(info.metadata.adapterURIs.length, 0);
Test.is(info.metadata.wantsSynthetic, false);
Test.ok(info.metadata.useMirrors);

info = _do(uidBase + "observerService", uriBase + "observerServiceInfo.js");
Test.ok(info.metadata.canInspectApplication);
Test.is(info.metadata.adapterURIs.length, 0);
Test.is(info.metadata.wantsSynthetic, false);
Test.is(info.metadata.useMirrors, false);
