const viewerURI =
  "chrome://sepsis-inspector/content/viewers/domNode/domNode.xul";

function filter(a_x_Object)
{
  return a_x_Object instanceof Components.interfaces.nsIDOMNode;
}

function getLocalizedName(aLocale)
{
  return "DOM Node";
}

function initializeViewer(aPanel)
{
  return new aPanel.viewerContext.DOMNodeViewer(aPanel);
}
