/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

const viewerURI =
  "chrome://sepsis-inspector/content/viewers/networkActivity/networkActivity.xul";

const config = {
  canInspectApplication: true
};

function filter(a_x_Object)
{
  // TODO Accept documents.
  // NB `a_x_Object` is untrusted.
  let object = new XPCNativeWrapper(a_x_Object);
  return object instanceof Components.interfaces.nsIHttpActivityDistributor;
}

function getLocalizedName(aLocale)
{
  return "Network Activity";
}

function initializeViewer(aPanel)
{
  return new aPanel.viewerContext.NetworkActivityViewer(aPanel);
}
