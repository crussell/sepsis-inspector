/* Any copyright on this file's contents is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/ */

/**
 * @fileoverview
 * Quickly open the inspector, for manually testing objectDetails viewer.
 *
 * There aren't any automated tests here; this is just for convenience, to
 * make visual inspection easier.
 */

Cu.import("chrome://sepsis-inspector/content/require.js");

var { NotificationManager } = require("utils/notificationManager");
var { ObserverService } = require("xpcom/observerService");

// Inspect `thing` using the "JS Object" viewer, and wait for
//   "subjectChanged".
// Select "constructor" in the "JS Object" viewer, and wait for
//   "subjectChanged".
// Focus the second panel.

function Super() { }

function Thing() { }

{
  let proto = Object.create(Super.prototype);
  Thing.prototype = proto;
  proto.constructor = Thing;
}

let thing = new Thing();

//////////////////////////////////////////////////////////////////////////////

let observer = {
  observe: function O_Observe(aSubject, aTopic, aData)
  {
    switch (aTopic) {
      case "si-instantiated":
        this.selectConstructor(aSubject.app);
        ObserverService.removeObserver(this, "si-instantiated");
      break;
      default:
        throw new Error("wat.  Topic: " + aTopic);
      break;
    }
  },

  selectConstructor: function O_SelectConstructor(aInspectorApp)
  {
    let rootPanel = aInspectorApp.panelManager.rootPanel;

    if (aInspectorApp.panelManager.isLoadPending(rootPanel) ||
        aInspectorApp.mSynthBloc.strip(rootPanel.subject) != thing) {
      aInspectorApp.panelManager.addObserver(
        "subjectChanged", this.notificationManager
      );
    }
    else {
      this.makeSelection(rootPanel);
    }
  },

  handleNotification: function O_HandleNotification(aName, aData)
  {
    switch (aName) {
      case "subjectChanged":
        if (aData.panel != aData.panel.panelManager.rootPanel) {
          aData.panel.panelManager.removeObserver(
            "subjectChanged", this.notificationManager
          );

          if (aData.panel.panelManager.rootPanel.target !=
              thing.constructor) {
            this.makeSelection(aData.panel.panelManager.rootPanel);
          }
          else {
            this.focusPanel(aData.panel);
          }
        }
      break;
      default:
        throw new Error("wat.  Notification: " + aName);
      break;
    }
  },

  makeSelection: function O_MakeSelection(aPanel)
  {
    aPanel.panelManager.addObserver(
      "subjectChanged", this.notificationManager
    );

    let tree = aPanel.browser.contentDocument.getElementById("olJSObject");
    let column = tree.columns.getNamedColumn("colName");
    for (let i = 0, n = tree.view.rowCount; i < n; ++i) {
      if (tree.view.getCellText(i, column) == "constructor") {
        tree.view.selection.select(i);
        break;
      }
    }
  },

  focusPanel: function O_FocusPanel(aPanel)
  {
    aPanel.ownerDocument.commandDispatcher.advanceFocusIntoSubtree(
      aPanel.browser.contentDocument.documentElement
    );
  }
};

observer.notificationManager = new NotificationManager(observer);

ObserverService.addObserver(observer, "si-instantiated", false);

sepsis.openInspector(thing, "org.mozilla.sepsis.viewers.jsObject");
