/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Each Inspector window is run by an `InspectorActivity` instance.
 *
 * The `InspectorActivity` constructor is one of the first things to run in
 * each Inspector window.  `ThePrefManager` is initialized the first time any
 * Inspector window is opened after the host app starts up.
 *
 * `InspectorActivity` manages most of the window UI, including handling most
 * commands that aren't viewer-specific and building up dynamic menus. 
 * (Except for the "Attach to Chrome [...]"/"Attach to Content Document"
 * menus.  That's entirely self-contained and split out into the shared
 * attachmenus binding.)  Some panel management happens here, but mostly in a
 * shallow sort of way; much of that is handled by delegating to the
 * (appropriately named) `PanelManager` (and, by extension, `PanelMaker`).
 *
 * The `InspectorActivity` instance created for each window is accessible from
 * the global `inspector` variable in that window's scope.
 *
 * `InspectorActivity` instances participate in the observe/notify system used
 * heavily throughout the project.  Understanding that subsystem is very
 * nearly vital to understanding how components (e.g., `PanelManager`
 * instances, `SynthBloc` instances) communicate with other components.  For
 * some examples of notifications that Inspector components send out, see the
 * `addObserver` calls in the `InspectorActivity` constructor below.
 *
 * @see utils/notificationManager.js
 * @see bindings/attachmenus.xml
 * @see registration/theViewerRegistry
 */

//////////////////////////////////////////////////////////////////////////////
//// Global Symbols

let inspector;

const kXULNSURI =
  "http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul";

//////////////////////////////////////////////////////////////////////////////
//// Imported Symbols

//  "I have moved to naming global singletons with a The* prefix --
//   ThePacketServer, TheMasterServer, TheVoip, etc. Feels pretty good."
//
// <https://twitter.com/ID_AA_Carmack/status/575788622554628096>
//
// The viewer registry and adapter registry are *basically* singletons.  Not
// really, because we can multiple ones, e.g., for tests, but for all the
// non-test parts of the codebase cares, both `AdapterRegistry` and
// `ViewerRegistry` have only got one, canonical instance each that ever gets
// used.
var { TheAdapterRegistry } = require("registration/theAdapterRegistry");
var { ThePrefManager } = require("utils/thePrefManager");
var { TheViewerRegistry } = require("registration/theViewerRegistry");
var { TheSupplierRegistry } = require("registration/theSupplierRegistry");

var { CommandMediator } = require("utils/commandMediator");
var { ForwardingTransactionListener } = require("utils/forwardingTransactionListener");
var { Logger } = require("utils/logger");
var { InspectorHooks } = require("integration/inspectorHooks");
var { NotificationManager } = require("utils/notificationManager");
var { PanelMaker } = require("utils/panelMaker");
var { PanelManager } = require("utils/panelManager");
var { SynthBloc } = require("synthetic/synthBloc");
var { TitleChanger } = require("integration/titleChanger");
var { ViewerLoad } = require("utils/viewerLoad");
var { ViewerBarController } = require("utils/viewerBarController");

var { AppStartup } = require("xpcom/appStartup");
var { DOMUtils } = require("xpcom/domUtils");
var { FocusManager } = require("xpcom/focusManager");
var { ObserverService } = require("xpcom/observerService");

var { TheFakeClipboardHack } = require("hacks/theFakeClipboardHack");
var { InitializeWidthsHack } = require("hacks/initializeWidthsHack");

Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");

var { nsIDOMDocument } = Components.interfaces;
var { nsIDOMWindow } = Components.interfaces;

//////////////////////////////////////////////////////////////////////////////

window.addEventListener("load", inspectorLoadListener, false);

function inspectorLoadListener()
{
  inspector = new InspectorActivity();

  let phantomBloc, target, uid, fromPanel;
  if (window.arguments && window.arguments.length) {
    [ phantomBloc, target, uid, fromPanel ] = window.arguments;
  }

  if (phantomBloc) phantomBloc = phantomBloc.wrappedJSObject;
  if (target) target = target.wrappedJSObject;

  let load = inspector.handleHookArguments(
    phantomBloc, target, uid, fromPanel
  );

  if (fromPanel) inspector._setSize(
    fromPanel.ownerDocument.defaultView, load.subject, load.uid
  );

  ObserverService.notifyObservers(window, "si-instantiated", null);
}

//////////////////////////////////////////////////////////////////////////////

/**
 * Runs the Inspector window.  See above.
 *
 * @see integration/hooks.js.
 * @constructor
 */
function InspectorActivity()
{
  // Inspector components send notifications (e.g., "panelAdded",
  // "viewerLoaded", etc.) to other interested components that have chosen to
  // observe them.  See the `addObserver` calls below and the
  // `NotificationManager` implementation to see this in action.
  this.mNotificationManager = new NotificationManager(this);

  this.mSynthBloc = SynthBloc.create();

  // Make sure we obey platform conventions for the window title.
  this.mTitleChanger = new TitleChanger(window);

  TheViewerRegistry.registerAllViewers(TheAdapterRegistry);

  let $ = document.getElementById.bind(document);

  this.mStringBundle = $("inspectorStringBundle");
  this.mPanelWell = $("bxPanelWell");
  this.mPanelMaker = new PanelMaker(this.mPanelWell);
  this.mRefreshItem = $("mnRefresh");

  // Mediates access to our undo/redo stack.
  this.mCommandMediator = new CommandMediator(this, document);
  this.mCommandMediator.decorate(this);

  this.panelManager = new PanelManager(
    this, this.mPanelMaker, this.mSynthBloc, this.mCommandMediator
  );

  this.mViewerBarController = new ViewerBarController(
    this.mPanelWell, this.panelManager
  );

  let inititializeWidthsHack = new InitializeWidthsHack(
    this.panelManager.rootPanel, this.mPanelMaker
  );

  this.panelManager.addObserver("viewerLoaded", this.mNotificationManager);
  this.panelManager.addObserver("targetChanged", this.mNotificationManager);
  this.panelManager.addObserver("panelAdded", this.mNotificationManager);
  this.panelManager.addObserver("panelRemoved", this.mNotificationManager);
  this.panelManager.addObserver("subjectChanged", this.mNotificationManager);

  // The root panel will have been created before we had an opportunity to
  // begin observing panelAdded, so we invoke `_onPanelAdded` manually.
  this.mLastFocusedPanel = this.panelManager.rootPanel;
  this._onPanelAdded(this.panelManager.rootPanel);

  this.mEditViewerItem = $("mnEditViewerBar");

  let controllers = document.commandDispatcher.getControllers();
  controllers.appendController(this);

  TheFakeClipboardHack.addObserver("clipboardChange", this.mNotificationManager);

  // cmd_runTests is hardcoded to be able run tests no matter what, so that we
  // can still easily run them even with a broken Inspector.  In the event
  // that we're not broken, though, we actually want to make sure we respect
  // the pref that controls whether the shortcut should be enabled or not.
  $("cmd_runTests").setAttribute(
    "oncommand", "inspector.dispatchCommand(this.id);"
  );

  window.addEventListener("unload", this, true);
}

{
  let proto = InspectorActivity.prototype;
  proto.mCachedRootRelation = null;
  proto.mCommandMediator = null;
  proto.mLastFocusedPanel = null;
  proto.mNotificationManager = null;
  proto.mPanelMaker = null;
  proto.mPanelWell = null;
  proto.mStringBundle = null;
  proto.mSynthBloc = null;
  proto.mTitleChanger = null;
  proto.mViewerBarController = null;
  proto.mEditViewerItem = null;

  proto.QueryInterface = XPCOMUtils.generateQI([
    "nsIController",
    "nsIDOMEventListener",
    "nsISupportsWeakReference"
  ]);

  /**
   * Handles window.arguments.  Reads the .defaultViewer pref to determine
   * which viewer to load initially.
   *
   * This is separate from the constructor because we need the global
   * `inspector` to be set up by the time `ViewerLoad.inspectParentTarget`
   * gets called.  That won't have happened yet if we call it inside the
   * constructor, since the `InspectorActivity` instance hasn't been returned
   * to the caller at that point.
   *
   * @param aPhantomBloc
   * @param aTarget
   * @param aUID [optional]
   * @param aFromPanel [optional]
   * @see InspectorHooks.launch
   */
  proto.handleHookArguments =
    function IA_HandleHookArguments(aPhantomBloc, aTarget, aUID, aFromPanel)
  {
    let root = this.panelManager.rootPanel;
    if (aFromPanel) {
      this.panelManager.setParentPanel(aFromPanel);
    }
    else {
      this.panelManager.addPanel(root);
    }

    let inspectee;
    if (aTarget.sameAs(null) || aTarget.sameAs(undefined)) {
      inspectee = this.mSynthBloc.appSynthetic;
    }
    else {
      // The target to be inspected definitely isn't a synthetic or mirror
      // created from our synth bloc, but note that we just reparented the
      // root panel.  (Well, maybe.  If `aFromPanel` was given.)  So the
      // target may belong to one of the bloc's ancestors.
      //
      // Three possibilities:
      //   1. The target to be inspected is a synthetic from an ancestor bloc.
      //   2. The target to be inspected is a mirror from an ancestor bloc.
      //   3. The target to be inspected is an object (or primitive)
      //      completely unrelated to any of the blocs in our lineage.
      //
      // Case 3 should be the common case--any wholly new window just opened
      // up or anything created as a result of hooks.js, while cases 1 and 2
      // are what will happen when cmd_inspectInNewWindow is used from within
      // an existing Inpector.
      try {
        // Just assume that it's case 3.  If it's not, this will fail, and we
        // can be sure it's either case 1 or case 2, which need to be handled
        // the same anyway.
        inspectee = this.mSynthBloc.mirror(aPhantomBloc.strip(aTarget));
      }
      catch (ex) {
        // It's got to be a synthetic or mirror we recognize.  Just use it.
        // assert(!!this.mSynthBloc.parent);
        inspectee = aPhantomBloc.strip(aTarget);
      }
    }

    let relation = this.panelManager.getRelationForPanel(root, inspectee);
    if (!relation.getUIDs().length) throw new Error(
      "Could not find a way to inspect the given target"
    );

    this.panelManager.inspectee = inspectee;

    if (aUID) {
      // NB: `inspectParentTarget` has an optional third parameter to indicate
      // that the UID is merely a hint, and therefore prevent it from
      // throwing.  However, we don't use it here, because it doesn't give us
      // fine enough control to let us choose our own fallback.  So we risk it
      // throwing here, and we can fall back to the default path below if the
      // given UID doesn't work.
      try {
        return ViewerLoad.inspectParentTarget(root, aUID);
      }
      catch (ex) {
        Logger.print(ex);
      }
    }

    return ViewerLoad.inspectParentTarget(
      root, ThePrefManager.getPref(".defaultViewer"), true
    );
  };

  proto.newWindow = function IA_NewWindow()
  {
    InspectorHooks.launch();
  };

  proto.closeWindow = function IA_CloseWindow()
  {
    window.close();
  };

  proto.closePanel = function IA_ClosePanel()
  {
    if (this.mLastFocusedPanel == this.panelManager.rootPanel) {
      this.closeWindow();
      return;
    }

    this.panelManager.removePanel(this.mLastFocusedPanel);
  };

  proto.quit = function IA_Quit()
  {
    AppStartup.quit(AppStartup.eAttemptQuit);
  };

  /**
   * Switch to the panel with the (1-based) index given by the event target's
   * "key" attribute.  For a target with key="9", we interpret it as "Switch
   * to the last active panel." (for parity with Firefox).  For a target with
   * key="0", we select to interpret it the same (because it makes way more
   * sense than key="9").
   *
   * @param aEvent
   *        The event that fired, causing the switch call.  The event target
   *        should have a "key" attribute with a value 0-9.
   */
  proto.onSwitchPanelKey = function IA_OnSwitchPanelKey(aEvent)
  {
    // XXX Assumes one child per panel, max.
    let panelNumber = parseInt(aEvent.target.getAttribute("key"));
    if (panelNumber == 9 || panelNumber == 0) {
      panelNumber = Infinity;
    }
    else {
      --panelNumber;
    }

    let panel = this.panelManager.rootPanel;
    for (let i = 0; i < panelNumber; ++i) {
      let kids = this.panelManager.getChildPanels(panel);
      let kid = (kids.length) ?
        kids[0] :
        null;

      if (panelNumber == Infinity && (!kid || !kid.active)) {
        break;
      }

      if (!kid) return;

      panel = kid;
    }

    if (panel.active) {
      document.commandDispatcher.advanceFocusIntoSubtree(
        panel.browser.contentDocument.documentElement
      );
    }
  };

  /**
   * Disable attach menu entries that correspond to this Inspector window or
   * any of this inspector instance's viewer documents.
   *
   * @param aPopup
   *        Either the "Attach to Content Document" or "Attach to Chrome
   *        Document" menupopup node.
   */
  proto.onAttachMenuShown = function IA_OnAttachMenuShown(aPopup)
  {
    let menu = document.getElementById(aPopup.getAttribute("whichmenu"));
    if (menu.hasNone()) return;

    for (let item = aPopup.firstChild; item; item = item.nextSibling) {
      // XXX There are maybe more appropriate docshell-related methods to do
      // this, but I don't know what they are. --crussell
      let parentFrame = DOMUtils.getParentForNode(item.targetDocument, true);
      if (item.targetDocument == document ||
          this._isPanelBrowser(parentFrame)) {
        item.setAttribute("disabled", true);
      }
    }
  };

  /**
   * Command handler for the "Attach [...]" menus.
   *
   * @param aItem
   *        The menuitem selected.  It must have a `targetDocument` property.
   *        NB: `aItem.targetDocument` is untrusted.
   */
  proto.onSelectDocumentItem = function IA_OnSelectDocumentItem(aItem)
  {
    this.setObjectAsTarget(
      this.mSynthBloc.mirror(new XPCNativeWrapper(aItem.targetDocument))
    );
  };

  /**
   * Inspect the given object in the root panel.
   *
   * @param aObject
   *        A mirror for the object to be used as the root panel's new
   *        subject.
   */
  proto.setObjectAsTarget = function IA_SetObjectAsTarget(aObject)
  {
    this.panelManager.setParentPanel(null);
    this.panelManager.clearRelationForPanel(this.panelManager.rootPanel);

    this.panelManager.inspectee = aObject;

    let uid = this._getDefaultOnAttach();
    ViewerLoad.inspectParentTarget(this.panelManager.rootPanel, uid, true);
  };

  /**
   * Command handler for the "Inspect Application" menu item.
   */
  proto.setApplicationAsTarget = function IA_SetApplicationAsTarget()
  {
    this.setObjectAsTarget(this.mSynthBloc.appSynthetic);
  };

  /**
   * Keep track of which panel has focus.  Update the enabled state of viewer-
   * sensitive commands.
   */
  proto.updateLastFocusedPanel = function IA_UpdateLastFocusedPanel()
  {
    let panel = this._getPanelForFocusedElement();
    if (!panel) return;

    this.mLastFocusedPanel = panel;

    this.updateCommandSet("cmdsInspectorFocus");
    this._updateRefreshItemText();
  };

  /**
   * Dynamically build up the viewer menu items in the View menu.  These items
   * reflect the viewers available to be loaded in the current panel.
   *
   * @param aPopup
   *        The menupopup XUL element for the View menu.
   * @see updateLastFocusedPanel
   */
  proto.populateViewerList = function IA_PopulateViewerList(aPopup)
  {
    // Get rid of the old items.
    let kid = this.mEditViewerItem.nextSibling;
    while (kid.nodeName != "menuseparator") {
      // We can't set `kid` to `kid.nextSibling` after removing it, because it
      // won't be in the tree anymore, so `nextSibling` won't work.
      kid = kid.nextSibling;
      kid.parentNode.removeChild(kid.previousSibling);
    }

    let currentViewerUID = this.mLastFocusedPanel.viewerUID;

    // Build the new ones.
    // XXX When showing entries for viewers that require an adapter, we should
    // indicate that.
    let uids = this.mLastFocusedPanel.getRelation().getUIDs();
    for (let i = 0, n = uids.length; i < n; ++i) {
      let item = aPopup.ownerDocument.createElementNS(kXULNSURI, "menuitem");

      item.setAttribute("uid", uids[i]);
      item.setAttribute("label", TheViewerRegistry.getViewerName(uids[i]));
      item.setAttribute("name", "viewerList");
      item.setAttribute("type", "radio");

      if (uids[i] == currentViewerUID) {
        item.setAttribute("checked", "true");
      }

      // At this point, `kid` is the menu separator where we stopped at above.
      aPopup.insertBefore(item, kid);
    }
  };

  /**
   * Invoked when a menu item in the viewer list is clicked.
   *
   * @see populateViewerList
   */
  proto.onSelectViewerListItem = function IA_OnSelectViewerListItem(aItem)
  {
    let uid = aItem.getAttribute("uid");
    // There's the "Enter Viewer" menu item, which obviously won't have a
    // UID.
    if (!uid) return;

    ViewerLoad.inspectParentTarget(this.mLastFocusedPanel, uid);
  };

  /**
   * Put the last focused panel's viewer bar in "edit" mode, to allow
   * filtering and selecting viewer.
   */
  proto.editViewerBar = function IA_EditViewerBar()
  {
    this.mViewerBarController.startEditing(this.mLastFocusedPanel.viewerBar);
  };

  proto.inspectTargetGlobal = function IA_InspectTargetGlobal()
  {
    this.setObjectAsTarget(
      this._getObjectGlobal(this.panelManager.inspectee)
    );
  };

  /**
   * Create a new child panel of the currently focused panel and inspect the
   * currently focused panel's target there.
   *
   * @see updateLastFocusedPanel
   */
  proto.inspectInNewPanel = function IA_InspectInNewPanel()
  {
    let panel = this.panelManager.addPanel(this.mLastFocusedPanel);
    ViewerLoad.inspectParentTarget(panel);
  };

  /**
   * Open a new Inspector window, inspecting the currently focused panel's
   * target there.
   *
   * @see updateLastFocusedPanel
   */
  proto.inspectInNewWindow = function IA_InspectInNewWindow()
  {
    // Try to use the same viewer as the child, if any.
    let uid, kids = this.panelManager.getChildPanels(this.mLastFocusedPanel);
    // XXX Check `isLoadPending`?
    if (kids.length && kids[0].active) {
      uid = kids[0].viewerUID;
    }

    let inspectee = this.mLastFocusedPanel.target;
    InspectorHooks.launch(
      this.mSynthBloc.strip(inspectee), uid, this.mLastFocusedPanel
    );
  };

  /**
   * Attach to the last focused panel's current target and begin inspecting it
   * in this Inspector window.
   *
   * @see updateLastFocusedPanel
   */
  proto.inspectInSameWindow = function IA_InspectInSameWindow()
  {
    this.setObjectAsTarget(this.mLastFocusedPanel.target);
  };

  proto.refresh = function IA_Refresh(aPanel)
  {
    if ("refresh" in aPanel.viewer) {
      try {
        aPanel.viewer.refresh();
        return;
      }
      catch (ex) {
        Logger.print(
          "refresh method threw an error for viewer with UID " +
          aPanel.viewerUID
        );
        Logger.print(ex)
      }
    }

    this.panelManager.changePanelSubject(aPanel, aPanel.subject);
  };

  proto.getSubordinateCommandMediator =
    function IA_GetSubordinateCommandMediator()
  {
    // `getCommandMediatorForPanel` can throw when our caller is triggered by
    // certain command update requests that fire asynchronous to viewer loads.
    // We don't actually care about these errors.
    try {
      return this.panelManager.getCommandMediatorForPanel(
        this.mLastFocusedPanel, true
      );
    }
    catch (ex) {
      // Don't ignore exceptions that aren't of the expected type.
      if (!(ex instanceof PanelManager.CommandMediatorAccessError)) {
        throw ex;
      }
    }

    return null;
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIController

  proto.isCommandEnabled = function IA_IsCommandEnabled(aName)
  {
    switch (aName) {
      case "cmd_inspectGlobal":
        return this.panelManager.rootPanel.getRelation().hasAdapter(null) &&
               !this.mSynthBloc.isSynthetic(this.panelManager.inspectee);
      break;
      case "cmd_undo":
        return !!this.mCommandMediator.getTransactionManager().
               numberOfUndoItems;
      break;
      case "cmd_redo":
        return !!this.mCommandMediator.getTransactionManager().
               numberOfRedoItems;
      break;
      case "cmd_inspectInNewPanel":
        if (this.panelManager.getChildPanels(this.mLastFocusedPanel).length) {
          // Can't insert a new panel between the current one and its child.
          return false;
        }
      // Fall through.
      case "cmd_inspectInNewWindow":
      case "cmd_inspectInSameWindow":
        return !!this.panelManager.getTargetRelation(this.mLastFocusedPanel).
               getUIDs().length;
      break;
      case "cmd_find":
      case "cmd_findPrevious":
      case "cmd_findNext":
        return this.mLastFocusedPanel.findbar.isCommandEnabled(aName);
      break;
      case "cmd_refresh":
        // We don't ever bother updating this, because it's rare that it
        // should be disabled, the duration for its disabled state is so
        // short.
        return !this.panelManager.isLoadPending(this.mLastFocusedPanel);
      break;
      case "cmd_runTests":
        return ThePrefManager.getPref(".tests.commandEnabled");
      break;
    }

    return false;
  };

  proto.supportsCommand = function IA_SupportsCommand(aName)
  {
    switch (aName) {
      case "cmd_inspectGlobal":
      case "cmd_undo":
      case "cmd_redo":
      case "cmd_inspectInNewPanel":
      case "cmd_inspectInNewWindow":
      case "cmd_inspectInSameWindow":
      case "cmd_find":
      case "cmd_findPrevious":
      case "cmd_findNext":
      case "cmd_refresh":
      case "cmd_runTests":
        return true;
      break;
    }

    return false;
  };

  proto.doCommand = function IA_DoCommand(aName)
  {
    switch (aName) {
      case "cmd_inspectGlobal":
        this.inspectTargetGlobal();
      break;
      case "cmd_undo":
        {
          let tm = this.mCommandMediator.getTransactionManager();
          tm.undoTransaction();
        }
      break;
      case "cmd_redo":
        {
          let tm = this.mCommandMediator.getTransactionManager();
          tm.redoTransaction();
        }
      break;
      case "cmd_inspectInNewPanel":
        this.inspectInNewPanel();
      break;
      case "cmd_inspectInNewWindow":
        this.inspectInNewWindow();
      break;
      case "cmd_inspectInSameWindow":
        this.inspectInSameWindow();
      break;
      case "cmd_find":
      case "cmd_findPrevious":
      case "cmd_findNext":
        this.mLastFocusedPanel.findbar.doCommand(aName);
      break;
      case "cmd_refresh":
        this.refresh(this.mLastFocusedPanel);
      break;
      case "cmd_runTests":
        TestRunner.go("sepsis-inspector");
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// Notifications

  /**
   * @see utils/notificationManager.js
   */
  proto.handleNotification = function IA_HandleNotification(aName, aData)
  {
    switch (aName) {
      case "clipboardChange":
        // This ends up refreshing cmd_delete, too, but oh well.
        this.updateCommandSet("cmdsSelectEditMenuItems");
      break;
      case "panelAdded":
        this._onPanelAdded(aData.panel);
      break;
      case "panelRemoved":
        this._onPanelRemoved(aData.parentPanel);
      break;
      case "targetChanged":
        this._onTargetChanged(aData.origin);
      break;
      case "viewerLoaded":
        this._onViewerLoaded(aData.origin);
      break;
      case "subjectChanged":
        this._onSubjectChanged(aData.origin);
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIDOMEventListener

  proto.handleEvent = function IA_HandleEvent(aEvent)
  {
    switch (aEvent.type) {
      case "click":
        this._onClick(aEvent);
      break;
      case "unload":
        this._onUnload(aEvent);
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  proto._setSize = function IA_SetSize(aOpener, aTarget, aUID) {
    // When opened using "Inspect Target in New Window", we'll try to resize
    // to the same size as the child of the panel whose target we're
    // inspecting, if it has one.  If not, we'll just use the size of the root
    // panel.
    let doppelganger;
    let panels = [];
    {
      let root = aOpener.inspector.panelManager.rootPanel;
      if (!root.panelManager.isLoadPending(root)) {
        panels.push(root);
      }
    }

    doppelgangerSearch:
    while (panels.length) {
      let panel = panels.shift();
      let bloc = panel.synthBloc;
      let target = panel.target;
      let parentHasSameTarget = (!bloc.isSynthetic(target)) ?
        bloc.getSynthBlocForMirror(target) && target.sameAs(aTarget) :
        target == aTarget;

      let kids = panel.panelManager.getChildPanels(panel);
      for (let i = 0, n = kids.length; i < n; ++i) {
        if (kids[i].active && !panel.panelManager.isLoadPending(kids[i])) {
          if (parentHasSameTarget) {
            // If there was no child panel with the given UID, but there was a
            // child panel, we choose to fall back to trying to mimick it
            // before defaulting to the root panel.
            doppelganger = kids[i];
            if (kids[i].viewerUID == aUID) {
              break doppelgangerSearch;
            }
          }

          panels.push(kids[i]);
        }
      }
    }

    if (!doppelganger) {
      doppelganger = aOpener.inspector.panelManager.rootPanel;
    }

    document.documentElement.removeAttribute("persist");

    window.resizeTo(doppelganger.boxObject.width, aOpener.outerHeight);
  };

  proto._isPanelBrowser = function IA_IsPanelBrowser(aEl) {
    return aEl && "ownerPanel" in aEl && aEl.ownerPanel.browser == aEl &&
           this.panelManager.isManagingPanel(aEl.ownerPanel);
  };

  proto._getPanelForFocusedElement = function IA_GetPanelForFocusedElement()
  {
    let el = FocusManager.getFocusedElementForWindow(window, false, {});

    if (el) {
      // It can be in the panel subtree, including the panel browser or one of
      // the findbar's descendants.
      let bindingParent;
      do {
        bindingParent = el.ownerDocument.getBindingParent(el);
        if (bindingParent) {
          el = bindingParent;
        }
      } while (bindingParent);
    }

    if (!this.panelManager.isManagingPanel(el)) return null;

    return el;
  };

  /**
   * Update the text of the "Refresh[...]" menu item in the View menu to
   * reflect the given panel's viewer's name.
   */
  proto._updateRefreshItemText = function IA_UpdateRefreshItemText()
  {
    if (this.panelManager.isLoadPending(this.mLastFocusedPanel)) {
      // Trying to access the panel's `viewer` while a load is pending will
      // cause it to throw, so just bail.  Our handler for the "viewerLoaded"
      // handler will take care of this when it's ready anyway.
      return;
    }

    let uid = this.mLastFocusedPanel.viewerUID;
    let labelText = this.mStringBundle.getFormattedString(
      "refreshPanel.label", [ TheViewerRegistry.getViewerName(uid) ]
    );
    this.mRefreshItem.setAttribute("label", labelText);
  };

  /**
   * Handle the "targetChanged" notification.
   *
   * @param aPanel
   *        The panel containing the viewer that dispatched the notification.
   */
  proto._onTargetChanged = function IA_OnTargetChanged(aPanel)
  {
    if (aPanel.panelManager != this.panelManager) return;

    if (aPanel == this.mLastFocusedPanel) {
      this.updateCommand("cmd_inspectInNewPanel");
      this.updateCommand("cmd_inspectInNewWindow");
      this.updateCommand("cmd_inspectInSameWindow");
    }

    this._updateChildPanels(aPanel);
  };

  /**
   * Update the given panel's child panels based on its current target.
   *
   * @param aPanel
   *        The panel whose target has changed.
   */
  proto._updateChildPanels = function IA_UpdateChildPanels(aPanel)
  {
    // If there is a valid target, we need to update all the linked child
    // panels' subjects.  (That may or may not recursively update other
    // descendant panels; it depends on their current subjects, how the
    // viewers in the child panels are implemented, and `aPanel`'s new
    // target.)  If there isn't a valid target, we need to empty all the
    // linked descendant panels.
    let kids = this.panelManager.getChildPanels(aPanel, true);
    for (let i = 0, n = kids.length; i < n; ++i) {
      if (!kids[i].linked) {
        continue;
      }

      if (aPanel.target !== null) {
        try {
          var load = ViewerLoad.inspectParentTarget(kids[i]);
        }
        catch (ex) { }

        if (load) {
          kids[i].active = true;
          continue;
        }
      }

      // NB: No panel in this sequence should be marked inactive before its
      // parent, otherwise, `_emptyPanel` will display the wrong message.
      let panels = [ kids[i] ];
      while (panels.length) {
        let current = panels.pop();
        this._emptyPanel(current);
        panels = panels.concat(this.panelManager.getChildPanels(current));
      }
    }
  };

  /**
   * @return
   *        The value of the .defaultViewer pref (a string), if that viewer is
   *        can be used in the root panel with the Inspector target, or
   *        undefined otherwise.
   */
  proto._getDefaultUIDIfCompatible = function IA_GetDefaultUIDIfCompatible()
  {
    let defaultUID = ThePrefManager.getPref(".defaultViewer");
    return (this.panelManager.rootPanel.getRelation().hasUID(defaultUID)) ?
      defaultUID :
      undefined;
  };

  /**
   * @return
   *        The value of the .defaultViewer pref (a string), if that viewer is
   *        can be used in the root panel with the Inspector target and the
   *        .useDefaultOnAttach pref is set to true, or undefined otherwise.
   */
  proto._getDefaultOnAttach = function IA_GetDefaultOnAttach()
  {
    return (ThePrefManager.getPref(".useDefaultOnAttach")) ?
      this._getDefaultUIDIfCompatible() :
      undefined;
  };

  /**
   * Make the panel inactive and show the helper text.  This is the "Select a
   * valid target..." text.
   *
   * @param aPanel
   */
  proto._emptyPanel = function IA_EmptyPanel(aPanel) {
    let parentPanel = this.panelManager.getParentPanel(aPanel);
    if (parentPanel.active) {
      let uid = parentPanel && parentPanel.viewer && parentPanel.viewerUID;
      aPanel.emptyLabel.textContent = this.mStringBundle.getFormattedString(
        "inactivePanelWithActiveParent.label",
        [ TheViewerRegistry.getViewerName(uid) ]
      );
    }
    else {
      aPanel.emptyLabel.textContent =
        this.mStringBundle.getString("inactivePanel.label");
    }

    // XXX PanelManager probably needs a method to unload the viewers of
    // inactive panels, and should be implicitly called when setting this to
    // false.  Maybe even allow viewers with a `destroy` method to keep their
    // viewer documents loaded, and then just re-initialize `viewer`?
    aPanel.active = false;
  }

  /**
   * Handle the "viewerLoaded" notification.
   *
   * @param aPanel
   *        The panel it was loaded into.
   */
  proto._onViewerLoaded = function IA_OnViewerLoaded(aPanel)
  {
    if (aPanel.panelManager != this.panelManager) return;

    if (!aPanel.findbar.hidden) {
      aPanel.findbar.hide();
    }

    // XXX We *have* to do this before calling `advanceFocusIntoSubtree`,
    // because if the panel browser isn't in the deck's foreground, when we
    // call it, the app hangs.  Platform bug?
    aPanel.active = true;

    aPanel.viewerBar.value = TheViewerRegistry.getViewerName(aPanel.viewerUID);

    if (this.mLastFocusedPanel != aPanel) return;
    
    this._updateRefreshItemText();

    document.commandDispatcher.advanceFocusIntoSubtree(
      aPanel.browser.contentDocument.documentElement
    );
  };

  /**
   * Handle the "panelAdded" notification.
   *
   * @param aPanel
   *        The newly inserted panel node.
   */
  proto._onPanelAdded = function IA_OnPanelAdded(aPanel)
  {
    if (aPanel.panelManager != this.panelManager) return;

    this.updateCommand("cmd_inspectInNewPanel");

    let splitter = this.mPanelMaker.getPanelSplitter(aPanel);
    if (!splitter) return;

    splitter.addEventListener("click", this, false);
  };

  /**
   * Handle the "panelRemoved" notification.
   *
   * @param aParentPanel
   *        The parent panel of the newly removed panel.  It will be null if
   *        the panel's parent is not managed by this panel manager.
   */
  proto._onPanelRemoved = function IA_OnPanelRemoved(aParentPanel)
  {
    if (aParentPanel.panelManager != this.panelManager) return;

    // We want to set the focus to the parent panel, but only if the focus was
    // previously in the removed panel.  `isManagingPanel` will return false
    // at this point if `mLastFocusedPanel` is the removed panel, becuse the
    // panel manager will have already stopped tracking it.
    if (this.panelManager.isManagingPanel(this.mLastFocusedPanel)) {
      // The focuse was elsewhere.  Don't mess with it.
      return;
    }

    FocusManager.focusedWindow = aParentPanel.viewerContext;
  };

  /**
   * Handle the "subjectChanged" notification.
   *
   * @param aPanel
   *        The panel whose subject has just changed.
   */
  proto._onSubjectChanged = function IA_OnSubjectChanged(aPanel)
  {
    if (aPanel.panelManager != this.panelManager) return;

    // For viewers that immediately export some target (i.e., those that set a
    // target when their `inspectObject` or `inspectApplication` is called),
    // we'll receive a "targetChanged" notification.  The child panels'
    // subjects will be updated appropriately.  For those that don't, however,
    // we won't receive a notification, but we still need to make sure the
    // descendant panels show the empty box.
    if (!aPanel.target) {
      this._updateChildPanels(aPanel);
    }

    this.updateCommand("cmd_inspectGlobal");

    // Update the title.
    if (aPanel == this.panelManager.rootPanel) {
      let inspectee = this.panelManager.inspectee;
      let bloc = this.mSynthBloc.getSynthBlocForMirror(inspectee);
      if ((bloc && bloc.mirror(nsIDOMWindow).hasInstance(inspectee)) ||
          (bloc && bloc.mirror(nsIDOMDocument).hasInstance(inspectee))) {
        let win = this._getObjectGlobal(inspectee, bloc);
        this.mTitleChanger.changeTitle(
          win.get("document").get("title") || win.get("document").get("URL")
        );
      }
      else {
        // XXX Pick something appropriate for the title when the target isn't
        // a document or DOM window.  There's a similar bug (552219) on file
        // against DOM Inspector.
        this.mTitleChanger.changeTitle(null);
      }
    }
  };

  proto._getObjectGlobal = function IA_GetObjectGlobal(aThing, aSynthBloc)
  {
    let bloc = aSynthBloc;
    if (!bloc) {
      bloc = this.mSynthBloc.getSynthBlocForMirror(aThing);
      if (!bloc) return null;
    }

    return bloc.mirror(
      Components.utils.getGlobalForObject(bloc.strip(aThing))
    );
  };

  /**
   * Handle the "click" event for panel splitters to close them on middle
   * click.  Ignore non-splitter clicks and clicks on the splitter that aren't
   * middle clicks.
   *
   * @param aEvent
   *        The "click" DOM event.
   */
  proto._onClick = function IA_OnClick(aEvent)
  {
    let panel = this.mPanelMaker.getPanelForSplitter(aEvent.target);
    if (panel && aEvent.button == 1) {
      // Middle click closes panel.
      this.panelManager.removePanel(panel);
    }
  };

  /**
   * Handle the "unload" event for inspector.xul; initiate the destruction
   * sequence when the window closes.
   *
   * @param aEvent
   *        The "unload" DOM event.
   */
  proto._onUnload = function IA_OnUnload(aEvent)
  {
    if (aEvent.target != document) {
      // It's not for us.  We receive unload events for viewer documents, too,
      // because they'll bubble up to us.
      return;
    }

    TheFakeClipboardHack.removeObserver(
      "clipboardChange", this.mNotificationManager
    );

    this.panelManager.destroy();
  };
}
