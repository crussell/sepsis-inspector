/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 *
 * Translates an object into a details synthetic.
 *
 * @see synthetic/objectDetailsSupplier
 * @see adapters/syntheticAdapter
 */

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { SyntheticAdapter } = require("adapters/syntheticAdapter");

var { ObjectDetailsSupplier } = require("synthetic/objectDetailsSupplier");

//////////////////////////////////////////////////////////////////////////////

// Our `translateTarget` method is actually implemented by a common class.
exports = new SyntheticAdapter(ObjectDetailsSupplier.sid);
