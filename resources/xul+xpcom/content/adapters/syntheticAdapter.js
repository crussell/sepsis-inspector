/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Translates a value into a supplier's preferred synthetic.
 *
 * Meant to be a helper for the convenience of "proper" adapter
 * implementations; there is no "Synthetic Adapter" itself that gets
 * registered directly for use in the Inspector.
 *
 * Interested parties should create a supplier that implements a
 * `getTargetSynthetic` method:
 *
 *   getTargetSynthetic(aPanel)
 *     Return a synthetic (of the type the supplier specializes in) that
 *     corresponds to the panel's current target.
 *
 * Suppliers that use `SupplierHelper` can use the helper's method of the same
 * name.
 *
 * @see registration/theAdapterRegistry
 * @see registration/theSupplierRegistry
 * @see synthetic/synthBloc
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.SyntheticAdapter = SyntheticAdapter;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { TheSupplierRegistry } = require("registration/theSupplierRegistry");

//////////////////////////////////////////////////////////////////////////////

/**
 * NB: `SyntheticAdapter` instances should only be used for synthetics from
 * suppliers that implement `getTargetSynthetic`.
 *
 * @param aSID
 *        A string naming the kind of synthetic the adapter should seek.
 */
function SyntheticAdapter(aSID)
{
  this.mSID = aSID
}

{
  let proto = SyntheticAdapter.prototype;
  proto.mSID = null;

  proto.translateTarget =
    function SA_TranslateTarget(aTarget, aSynthBloc, aParentPanel)
  {
    if (!aParentPanel) return null;
    let supplier = TheSupplierRegistry.getSupplier(
      aSynthBloc, this.mSID, true
    );

    return supplier && supplier.getTargetSynthetic(aParentPanel);
  }
}
