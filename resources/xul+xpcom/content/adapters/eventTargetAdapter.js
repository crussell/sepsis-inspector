/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 *
 * Translates a DOM event to its event target.  NB: The "target" in
 * `translateTarget` and `aTarget` is not referring to the same type of target
 * as the "target" in "event target".  They come from overlapping domains of
 * discourse, where terminology was chosen that happens to collide.
 *
 * @see utils/syntheticAdapterBase.jsm
 * @see registration/theAdapterRegistry
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.translateTarget = translateTarget;

//////////////////////////////////////////////////////////////////////////////

function translateTarget(aTarget, aSynthBloc, aParentPanel)
{
  let bloc = aSynthBloc.getSynthBlocForMirror(aTarget);
  if (bloc && bloc.mirror(Ci.nsIDOMEvent).hasInstance(aTarget)) {
    return aTarget.get("target");
  }
  return null;
}
