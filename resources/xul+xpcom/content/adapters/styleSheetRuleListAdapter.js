/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 *
 * Translates a style sheet to its rule list.
 *
 * @see registration/theAdapterRegistry
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.translateTarget = translateTarget;

//////////////////////////////////////////////////////////////////////////////

function translateTarget(aTarget, aSynthBloc, aParentPanel)
{
  let bloc = aSynthBloc.getSynthBlocForMirror(aTarget);
  if (bloc && bloc.mirror(Ci.nsIDOMCSSStyleSheet).hasInstance(aTarget)) {
    return aParentTarget.get("cssRules");
  }
  return null;
}
