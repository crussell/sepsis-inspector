/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.IOService =
  Components.classes["@mozilla.org/network/io-service;1"].
  getService(Components.interfaces.nsIIOService2);

//////////////////////////////////////////////////////////////////////////////
