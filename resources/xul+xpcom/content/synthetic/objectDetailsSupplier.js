/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Supplier for object details synthetics.
 *
 * An object details synthetic represents a value or an exception that
 * occurred when attempting to get at the value.
 *
 * If the value (or exception) that a details synthetic represents is the
 * result of accessing a property, and the name of that property and the base
 * object it belongs to are known, the synthetic has a few optional fields for
 * information about the property, like:
 *
 * - Its name.
 * - Its property descriptor.
 * - The details synthetic corresponding to the base object, i.e., the object
 *   that the property belongs to.
 *
 * @see synthetic/objectDetails.js
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.ObjectDetailsSupplier = ObjectDetailsSupplier;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { SupplierHelper } = require("utils/supplierHelper");

//////////////////////////////////////////////////////////////////////////////

function ObjectDetailsSupplier(aSynthBloc)
{
  this.mSynthBloc = aSynthBloc;
  this.ObjectDetails = aSynthBloc.getRegisteredSID(this.sid);
}

{
  let proto = ObjectDetailsSupplier.prototype;

  proto.mHelper = null;
  proto.mSynthBloc = null;

  proto.ObjectDetails = null;

  ObjectDetailsSupplier.sid = proto.sid =
    "org.mozilla.sepsis.inspector.synthetic.objectDetails";

  ObjectDetailsSupplier.create = function ODS_Create(aPanel)
  {
    let helper = new SupplierHelper(this.sid);
    return helper.createOrGetSupplier(aPanel, true);
  };

  /**
   * @param aHelper
   *        The `SupplierHelper` instance created above.
   */
  proto.initializeWithHelper = function ODS_InitializeWithHelper(aHelper)
  {
    this.mHelper = aHelper;
  };

  /**
   * @see adapters/syntheticAdapter
   */
  proto.getTargetSynthetic = function ODS_GetTargetSynthetic(aPanel)
  {
    return this.mHelper.getTargetSynthetic(aPanel);
  };

  ////////////////////////////////////////////////////////////////////////////
  //// Supplier-specific methods

  /**
   * @param aThing
   *        A mirror around the value that the details synthetic is supposed to
   *        represent, or an exception wrapper.  If the details object should
   *        represent an exception that was caught when trying to obtain this
   *        value, this should be an `ObjectDetails.ExceptionWrapper`.
   * @param aName [optional]
   * @param aBaseDetails [optional]
   * @param aProperty [optional]
   * @return
   *        An `ObjectDetails` synthetic.
   */
  proto.createDetails =
    function ODS_CreateDetails(aThing, aName, aBaseDetails, aProperty)
  {
    return new this.ObjectDetails(
      aThing, this.mSynthBloc, aName, aBaseDetails, aProperty
    );
  };

  /**
   * @param aName
   *        A string naming some property of the base object.
   * @param aBaseDetails
   *        An `ObjectDetails` synthetic for the base object.
   * @return
   *        An `ObjectDetails` synthetic for the property.
   */
  proto.createPropertyDetails =
    function ODS_CreatePropertyDetails(aName, aBaseDetails)
  {
    let value, base = aBaseDetails.value;
    try {
      value = base.get(aName);
    }
    catch (_x_ex) {
      value = new this.ObjectDetails.ExceptionWrapper(
        this.mSynthBloc.mirror(_x_ex)
      );
    }

    return this.createDetails(value, aName, aBaseDetails, base.lookUp(aName));
  };
}
