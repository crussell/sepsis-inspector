/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Welcome to the world of synthetics.
 *
 * XXX Explain synthetics themselves and the point of the whole thing.
 *
 * "Synthetic privileges" are held by `InspectorActivity` instances and viewers,
 * and refers to the ability to inspect a set of synthetics; to be capable of
 * inspecting a given synthetic means to have the synthetic privileges for all
 * synthetics managed by the associated synthetic bloc.
 *
 * `SynthBloc` instances are per-`InspectorActivity` instance.  When
 * synthetics are created, they are always created with respect to a specific
 * `SynthBloc` instance.  Inspectors spun off another (e.g.,
 * cmd_inspectInNewWindow, but not cmd_newWindow) automatically inherit the
 * synthetic privileges of the parent.  If the Inspector target (read: root
 * panel subject) ever changes, though, that Inspector's inherited privileges
 * will disappear, leaving only the privileges for the synthetics created for
 * that synthetic bloc specifically; it effectively creates a new privilege
 * domain.  This keeps synthetics working sensibly so viewers that don't opt-
 * in with `wantsSynthetic` in their info files don't see synthetics normally,
 * but the Inspector itself can still be used to inspect other Inspector
 * windows.
 *
 * Synthetics should be created by a supplier.  Implementing a supplier on
 * your own and getting it right can be tricky, so it's best to use
 * `SupplierHelper`.  XXX Explain more.
 *
 * `SynthBloc` instances are themselves suppliers for the "app synthetic",
 * which is considered core Inspector infrastructure.  (NB: Even so, this is
 * *NOT* an example of how suppliers should be ordinarily written; `SynthBloc`
 * is special.)
 *
 * The problem: we want to allow the Inspector framework to use its existing
 * infrastructure to inspect objects and re-use it for when the Inspector is
 * put into "Inspect Application" mode.  So we have a sentinel object that we
 * use internally to signify when this is the case.  It's the app subject
 * synthetic, and the root panel's subject is internally represented by this
 * synthetic when the Inspector is in "Inspect Application" mode.
 *
 * TODO:
 * - Watch sepsis-inspector-synthetic category.
 *
 * @see registration/theSupplierRegistry
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.SynthBloc = SynthBloc;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { Module } = require("loader/module");
var { Resolver } = require("loader/resolver");
var { Loader } = require("loader/loader");

var { TheSupplierRegistry } = require("registration/theSupplierRegistry");
var { TheStaticStore } = require("utils/theStaticStore");

//////////////////////////////////////////////////////////////////////////////

/**
 * @constructor
 */
function SynthBloc()
{
  this.mSIDToConstructorMap = new Map();

  let resolver = new Resolver("chrome://sepsis-inspector/content/");
  this.mLoader = new Loader(
    resolver, { id: "sepsis-inspector synthetic loader" }
  );

  this.appSynthetic = new (this.getRegisteredSID(this.APPSYNTHETIC_SID))();

  // NB: Despite the fact that we give it an SID, `JSMirrors` is not really a
  // synthetic constructor.  (It's more like a factory--you can't get any
  // meaningful info about `JSMirrors` from the `instanceof` operator.)  We're
  // conveniently able to re-use some of the infrastructure that we have for
  // synthetics to load the mirrors module, though, so we do.
  this.JSMirrors = this._load(
    TheStaticStore.readValue(this.JSMIRRORS_SID, "sepsis-inspector-synthetic")
  );
}

{
  let proto = SynthBloc.prototype;
  proto.mLoader = null;
  proto.mParent = null;
  proto.mSIDToConstructorMap = null;

  SynthBloc.APPSYNTHETIC_SID = proto.APPSYNTHETIC_SID =
    TheSupplierRegistry.APPSYNTHETIC_SID;

  SynthBloc.JSMIRRORS_SID = proto.JSMIRRORS_SID =
    "org.mozilla.sepsis.inspector.jsMirrors";

  proto.appSynthetic = null;

  SynthBloc.create = function SB_Create()
  {
    return TheSupplierRegistry.createSupplier(this.APPSYNTHETIC_SID, null);
  };

  proto.__defineGetter__("parent", function SM_Parent_Get()
  {
    return this.mParent;
  });

  proto.__defineSetter__("parent", function SM_Parent_Set(aVal)
  {
    this.mParent = aVal;
  });

  proto.getRegisteredSID = function SM_GetRegisteredSID(aSID)
  {
    if (!this.mSIDToConstructorMap.has(aSID)) {
      let uri = TheStaticStore.readValue(aSID, "sepsis-inspector-synthetic");
      let exports = this._load(uri);

      let symbols = Object.keys(exports);
      if (symbols.length != 1) throw new Error(
        "Synthetic must export exactly one symbol " + uri
      );

      this.mSIDToConstructorMap.set(aSID, exports[symbols[0]]);
    }

    return this.mSIDToConstructorMap.get(aSID);
  };

  /**
   * @param a_x_Object
   *        NB: `a_x_Object` is untrusted.
   */
  proto.isAppSynthetic = function SB_IsSynthetic(a_x_Val)
  {
    return this.isSynthetic(a_x_Val, this.APPSYNTHETIC_SID);
  };

  /**
   * Determine if synthetic bloc has synthetic privileges for an object.
   *
   * Always use this method instead of instanceof, even if you've got a
   * reference to the constructor of a synthetic registered with the SID that
   * you would otherwise call this method with.  The instanceof operator will
   * give false negatives, because it doesn't take into account synthetics
   * registered with ancestor synthetic blocs and that this bloc has inherited
   * privileges for.
   *
   * NB: The synthetic's constructor *must* have been registered with
   * `registerSynthetic` on this instance (or the parent) to get a true return
   * value.
   *
   * @param a_x_Val
   *        An object which may be a synthetic for which this bloc has
   *        privileges.
   *        NB: `a_x_Val` is untrusted.
   * @param aSID [optional]
   *        A string naming a SID.  The value will be checked against the
   *        synthetic constructor registered with this SID.  Useful for
   *        checking synthetic types without having to load and evaluate the
   *        synthetic's source file if it wasn't already loaded.  Defaults to
   *        checking against all registered synthetic types if no SID is
   *        provided.
   * @return
   *        True if `a_x_Val` is an instance of a synthetic for which this
   *        synthetic bloc has privileges.
   */
  proto.isSynthetic = function SM_IsSynthetic(a_x_Val, aSID)
  {
    if (this.parent && this.parent.isSynthetic(a_x_Val, aSID)) return true;

    if (aSID != undefined) {
      return this.mSIDToConstructorMap.has(aSID) &&
             a_x_Val instanceof this.mSIDToConstructorMap.get(aSID);
    }

    for (let [sid, constructor] of this.mSIDToConstructorMap) {
      if (a_x_Val instanceof constructor) return true;
    }

    return false;
  };

  /**
   * @param a_x_Val
   *        NB: `a_x_Val` is untrusted
   * @return
   *        A JS mirror to perform trusted operations on the given value.
   */
  proto.mirror = function SB_Mirror(a_x_Val) {
    let bloc = this.getSynthBlocForMirror(a_x_Val);
    if ((bloc && a_x_Val !== null) || this.isSynthetic(a_x_Val)) {
      throw new Error(
        "Mirrors cannot be created for synthetics or existing mirrors"
      );
    }
    return this.JSMirrors.fullLocal(a_x_Val);
  };

  /**
   * @param aMirror
   * @return
   *        NB: The `getReflectee` method's return value is untrusted.
   */
  proto.strip = function SB_Strip(aMirror) {
    try {
      if (this.parent) return this.parent.strip(aMirror);
    }
    catch (ex) { }

    return this.JSMirrors.strip(aMirror);
  };

  proto.getSynthBlocForMirror = function SB_GetSynthBlocForMirror(aMirror)
  {
    let bloc = this.parent && this.parent.getSynthBlocForMirror(aMirror);
    if (bloc) return bloc;
    try {
      this.JSMirrors.strip(aMirror);
    }
    catch (ex) {
      return null;
    }

    return this;
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  /**
   * Load the module at the given URI.
   *
   * @param aURI
   *        A string containing the URI to the script.
   * @return
   *        The module's exports.
   */
  proto._load = function SM_Load(aURI)
  {
    let module = this.mLoader.getCachedModule(aURI);
    if (!module) {
      module = new Module(aURI);
      try {
        this.mLoader.load(module);
      }
      catch (ex) {
        throw new Error("Error loading synthetic with URI " + aURI);
        // XXX Drop module
      }
    }

    return module.exports;
  };
}
