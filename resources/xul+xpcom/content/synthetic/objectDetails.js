/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * @see synthetic/objectDetailsSupplier
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.ObjectDetails = ObjectDetails;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { ObjectDetailsSupplier } = require("synthetic/objectDetailsSupplier");

var { Enumerator } = require("utils/enumerator");

//////////////////////////////////////////////////////////////////////////////

/**
 * This is a bit of a misnomer.  This is intended to represent primitive
 * values, too.
 *
 * @param aThing
 *        A mirror around the value this synthetic is supposed to represent,
 *        or a wrapped exception.  If the consumer caught an exception when
 *        trying to obtain this value, this should be an
 *        `ObjectDetails.ExceptionWrapper`.
 * @param aSynthBloc
 * @param aName [optional]
 * @param aBaseDetails [optional]
 * @param aProperty [optional]
 * @see ObjectDetails.ExceptionWrapper
 */
function ObjectDetails(aThing, aSynthBloc, aName, aBaseDetails, aProperty)
{
  if (aThing instanceof ObjectDetails.ExceptionWrapper) {
    this.exception = aThing.exception;
  }
  else {
    this.value = aThing;
  }

  this.bloc = aSynthBloc;

  if (aName !== undefined) {
    this.name = aName;
  }

  if (aBaseDetails !== undefined) {
    this.baseDetails = aBaseDetails;
  }

  if (aProperty !== undefined) {
    this.property = aProperty;
  }
}

{
  let proto = ObjectDetails.prototype;
  proto.baseDetails = null;
  proto.bloc = null;
  proto.exception = null;
  proto.name = null;
  proto.property = null;
  proto.value = null;

  proto.sid = ObjectDetails.sid = ObjectDetailsSupplier.sid;

  ////////////////////////////////////////////////////////////////////////////
  //// wrappers

  /**
   * A wrapper for exceptions.
   *
   * Suppose you, the consumer, want to get the details for some object's
   * properties.  But suppose further that when you tried to get the values
   * for the object's properties, one or more exceptions occurred.  Let's
   * assume you caught them, but still, you've got these exceptions.  If
   * somewhere in your UI you want to be able to expose the fact that these
   * errors occurred, then these wrappers are helpful.
   *
   * When this happens, take your exception, wrap it up with one of these, and
   * use *that* to get your details synthetic.  The `ObjectDetails`
   * constructor is aware of these wrappers and will check to see if the thing
   * that it's given is one of them.
   *
   * NB: Exception wrappers are only for representing that an exception
   * occurred as a byproduct of attempting to get the thing that you actually
   * wanted the details for.  If the exception itself is what you want the
   * details for, then pass the exception to `ObjectDetails` as usual, without
   * wrapping it up.
   *
   * @param aExceptionMirror
   */
  proto.ExceptionWrapper = ObjectDetails.ExceptionWrapper =
    function OD_ExceptionWrapper(aExceptionMirror)
  {
    this.exception = aExceptionMirror;
  };

  {
    let ew_proto = ObjectDetails.ExceptionWrapper.prototype;
    ew_proto.exception = null;
  }

  ////////////////////////////////////////////////////////////////////////////

  /**
   * Figure out what to call the given prototype object.
   *
   * Besides any default value provided, this method will return a string in
   * one of two formats:
   *
   *   "Constructor.prototype" and "[InternalPrototype]"
   *
   * The first format is preferred.  When this method resorts to returning a
   * string matching the second format, the name inside the brackets will
   * reflect the given object's internal [[Class]] name.  The latter format
   * will usually show up when the given object is something from Gecko's DOM
   * implementation internals.
   *
   * The default value is unchecked, and so callers relying on either of these
   * formats to signify something should either constrain the fallback value
   * they pass in, or simply leave it unspecified and check for null.
   *
   * @param aHeritageItem
   *        A mirror around some object in exists in the prototype chain of
   *        some other object.
   * @param aDefault [optional]
   *        A string this method should return if it can't figure out what to
   *        call the object.
   * @return
   *        A string naming the given object and suitable for printing, as
   *        described above, or null if one can't be determined and the caller
   *        provides no default to fall back on.
   */
  proto.getProtoName = ObjectDetails.getProtoName =
    function OD_GetProtoName(aHeritageItem, aDefault)
  {
    let constructorProp = aHeritageItem.lookUpOwn("constructor");
    if (constructorProp && !constructorProp.isAccessor) {
      let constructor = constructorProp.value;
      if (constructor.typeof === "function" &&
          constructor.get("prototype").sameAs(aHeritageItem) &&
          constructor.name) {
        return constructor.name + ".prototype";
      }
    }

    let classText = aHeritageItem.internalClass;
    if (classText !== "Object") return "[" + classText + "]";
    if (aDefault !== undefined) return aDefault;
    return null;
  };
}
