/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Transaction for deleting the attribute for some element.
 *
 * @see utils/commandMediator.jsm
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.DeleteAttributeTransaction = DeleteAttributeTransaction;

const Cu = Components.utils;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { TransactionBase } = require("utils/transactionBase");

//////////////////////////////////////////////////////////////////////////////

function DeleteAttributeTransaction(aElement, aNamespace, aLocalName)
{
  this.mElementWeak = Cu.getWeakReference(aElement);

  this.namespaceURI = aNamespace;
  this.localName = aLocalName;

  if (!aElement.hasAttributeNS(this.namespaceURI, this.localName)) {
    throw new Error("No such attribute");
  }

  this.oldValue = aElement.getAttributeNS(this.namespaceURI, this.localName);

  this.wrappedJSObject = this;
}

{
  let proto = Object.create(TransactionBase.prototype);
  DeleteAttributeTransaction.prototype = proto;
  proto.constructor = DeleteAttributeTransaction;

  proto.mElementWeak = null;

  proto.localName = null;
  proto.namespaceURI = null;
  proto.oldValue = null;

  proto.__defineGetter__("element", function DAT_Element_Get()
  {
    return this.mElementWeak.get();
  });

  proto.doTransaction = function DAT_DoTransaction()
  {
    let el = this.element;
    if (el) {
      el.removeAttributeNS(this.namespaceURI, this.localName);
    }
  };

  proto.undoTransaction = function DAT_UndoTransaction()
  {
    let el = this.element;
    if (el) {
      el.setAttributeNS(this.namespaceURI, this.localName, this.oldValue);
    }
  };
}
