/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Transaction for changing a DOM node's value/text content.  Use, for
 * example, to edit a text or comment node in the DOM Node viewer.
 *
 * @see utils/commandMediator.jsm
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.SetTextContentTransaction = SetTextContentTransaction;

const Cu = Components.utils;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { TransactionBase } = require("utils/transactionBase");

//////////////////////////////////////////////////////////////////////////////

function SetTextContentTransaction(aNode, aNewValue)
{
  this.mNodeWeak = Cu.getWeakReference(aNode);

  this.oldValue = aNode.textContent;
  this.newValue = aNewValue;

  this.wrappedJSObject = this;
}

{
  let proto = Object.create(TransactionBase.prototype);
  SetTextContentTransaction.prototype = proto;
  proto.constructor = SetTextContentTransaction;

  proto.mNodeWeak = null;

  proto.newValue = null;
  proto.oldValue = null;

  proto.__defineGetter__("node", function EAVT_Node_Get()
  {
    return this.mNodeWeak.get();
  });

  proto.doTransaction = function EAVT_DoTransaction()
  {
    this._setTextContent(this.newValue);
  };

  proto.undoTransaction = function EAVT_UndoTransaction()
  {
    this._setTextContent(this.oldValue);
  };

  proto._setTextContent = function DNV_UpdateNodeTextContent(aValue)
  {
    let node = this.node;
    if (node) {
      switch (node.nodeType) {
        // Circumvent any "attributes' textContent is deprecated" warnings.
        case node.ATTRIBUTE_NODE:
          node.value = aValue;
        break;
        case node.CDATA_SECTION_NODE:
        case node.COMMENT_NODE:
        case node.PROCESSING_INSTRUCTION_NODE:
        case node.TEXT_NODE:
          node.textContent = aValue;
        break;
        default:
          throw new Error("wat.  Node type: " + node.nodeType);
        break;
      }
    }
  };
}
