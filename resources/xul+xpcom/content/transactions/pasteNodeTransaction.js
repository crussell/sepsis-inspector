/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Transaction for pasting some node into the inspected document.
 *
 * @see utils/commandMediator.jsm
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.PasteNodeTransaction = PasteNodeTransaction;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { TransactionBase } = require("utils/transactionBase");

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aNode
 *        The node to be pasted.  NB: The caller probably wants to use the
 *        result of a `cloneNode` call here.  Otherwise, if the given node
 *        already exists in the document, it will be moved from the old
 *        location to the new one, because that's how the DOM is specced.
 * @param aParentNode
 * @param aNextSibling
 */
function PasteNodeTransaction(aNode, aParentNode, aNextSibling)
{
  this.node = aNode;
  this.parentNode = aParentNode;
  this.nextSibling = aNextSibling;

  this.wrappedJSObject = this;
}

{
  let proto = Object.create(TransactionBase.prototype);
  PasteNodeTransaction.prototype = proto;
  proto.constructor = PasteNodeTransaction;

  proto.nextSibling = null;
  proto.node = null;
  proto.parentNode = null;

  proto.doTransaction = function PNT_DoTransaction()
  {
    this.parentNode.insertBefore(this.node, this.nextSibling);
  };

  proto.undoTransaction = function PNT_UndoTransaction()
  {
    this.parentNode.removeChild(this.node);
  };
}
