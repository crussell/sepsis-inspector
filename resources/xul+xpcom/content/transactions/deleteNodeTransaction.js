/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Transaction for deleting a DOM node.
 *
 * @see utils/commandMediator.jsm
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.DeleteNodeTransaction = DeleteNodeTransaction;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { TransactionBase } = require("utils/transactionBase");

//////////////////////////////////////////////////////////////////////////////

function DeleteNodeTransaction(aNode)
{
  this.node = aNode;

  this.parent = aNode.parentNode;
  this.sibling = aNode.nextSibling;

  this.wrappedJSObject = this;
}

{
  let proto = Object.create(TransactionBase.prototype);
  DeleteNodeTransaction.prototype = proto;
  proto.constructor = DeleteNodeTransaction;

  proto.node = null;
  proto.parent = null;
  proto.sibling = null;

  proto.doTransaction = function DAT_DoTransaction()
  {
    this.parent.removeChild(this.node);
  };

  proto.undoTransaction = function DAT_UndoTransaction()
  {
    this.parent.insertBefore(this.node, this.sibling);
  };
}
