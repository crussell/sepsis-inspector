/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Transaction for commands that add non-attribute nodes to the DOM.
 *
 * TODO:
 * - Expiry.  We don't want to hold on to references forever, but we also
 *   don't want to use weak references.  We want a document watcher to notify
 *   us when it's time to expunge references.
 *
 * @see utils/commandMediator.jsm
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.InsertNodeTransaction = InsertNodeTransaction;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { TransactionBase } = require("utils/transactionBase");

//////////////////////////////////////////////////////////////////////////////

function InsertNodeTransaction(aParentNode, aNamespace, aName, aAttributes,
                               aNextSibling)
{
  this.parentNode = aParentNode;
  this.namespaceURI = aNamespace;
  this.name = aName;
  this.attributes = aAttributes;
  this.nextSibling = aNextSibling;

  this.wrappedJSObject = this;
}

{
  let proto = Object.create(TransactionBase.prototype);
  InsertNodeTransaction.prototype = proto;
  proto.constructor = InsertNodeTransaction;

  proto.attributes = null;
  proto.name = null;
  proto.namespaceURI = null;
  proto.nextSibling = null;
  proto.node = null;
  proto.parentNode = null;

  proto.doTransaction = function INT_DoTransaction()
  {
    this.node = this.parentNode.ownerDocument.createElementNS(
      this.namespaceURI, this.name
    );

    // NB: `this.attributes` might not inherit from `Object.prototype`.
    let $hasOwn = Object.prototype.hasOwnProperty.bind(this.attributes);
    for (let prop in this.attributes) {
      if ($hasOwn(prop)) {
        this.node.setAttribute(prop, this.attributes[prop]);
      }
    }

    this._insert();
  };

  proto.undoTransaction = function INT_UndoTransaction()
  {
    this.parentNode.removeChild(this.node);
  };

  proto.redoTransaction = function INT_RedoTransaction()
  {
    this._insert();
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  proto._insert = function INT_Insert()
  {
    this.parentNode.insertBefore(this.node, this.nextSibling);
  };
}
