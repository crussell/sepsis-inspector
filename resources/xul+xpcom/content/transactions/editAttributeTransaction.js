/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Transaction for commands that change an attribute's value. Used, for
 * example, when editing an attribute inline in the DOM Node viewer's
 * Attributes tree.
 *
 * @see utils/commandMediator.jsm
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.EditAttributeTransaction = EditAttributeTransaction;

const Cu = Components.utils;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { TransactionBase } = require("utils/transactionBase");

//////////////////////////////////////////////////////////////////////////////

function EditAttributeTransaction(aElement, aNamespace, aLocalName, aNewValue)
{
  this.mElementWeak = Cu.getWeakReference(aElement);

  this.namespaceURI = aNamespace;
  this.localName = aLocalName;
  this.newValue = aNewValue;

  if (aElement.hasAttributeNS(this.namespaceURI, this.localName)) {
    this.oldValue = aElement.getAttributeNS(
      this.namespaceURI, this.localName
    );
  }

  this.wrappedJSObject = this;
}

{
  let proto = Object.create(TransactionBase.prototype);
  EditAttributeTransaction.prototype = proto;
  proto.constructor = EditAttributeTransaction;

  proto.mElementWeak = null;

  proto.localName = null;
  proto.namespaceURI = null;
  proto.newValue = null;
  proto.oldValue = null;

  proto.__defineGetter__("element", function EAT_Element_Get()
  {
    return this.mElementWeak.get();
  });

  proto.doTransaction = function EAT_DoTransaction()
  {
    let el = this.element;
    if (el) {
      el.setAttributeNS(this.namespaceURI, this.localName, this.newValue);
    }
  };

  proto.undoTransaction = function EAT_UndoTransaction()
  {
    let el = this.element;
    if (!el) {
      return;
    }

    if (this.oldValue === null) {
      el.removeAttributeNS(this.namespaceURI, this.localName);
    }
    else {
      el.setAttributeNS(this.namespaceURI, this.localName, this.oldValue);
    }
  };
}
