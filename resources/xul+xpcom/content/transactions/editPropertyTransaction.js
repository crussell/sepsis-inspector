/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Transaction for commands that change a CSS property's value.  Used, for
 * example, for inline editing in the CSS Style viewer's tree view.
 *
 * @see utils/commandMediator.jsm
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.EditPropertyTransaction = EditPropertyTransaction;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { TransactionBase } = require("utils/transactionBase");

//////////////////////////////////////////////////////////////////////////////

function EditPropertyTransaction(aDeclaration, aName, aNewValue, aNewPriority)
{
  // We can't rely on an `mDeclarationWeak`, because they aren't guaranteed to
  // stay alive for the duration of the style's life.  (When accessing stuff
  // stuff from the DOM style interfaces, you're not always getting an object
  // that is used internally; some of it is generated on the fly from the
  // actual internal structures.  This means that if we get a reference to an
  // nsIDOMCSSStyleDeclaration, it's possible that we're the only consumer,
  // and it'll get GCed if we only keep a weak reference to it.
  this.declaration = aDeclaration;
  this.name = aName;
  this.oldValue = aDeclaration.getPropertyValue(aName);
  this.oldPriority = aDeclaration.getPropertyPriority(aName);
  this.newValue = aNewValue;
  this.newPriority = aNewPriority;

  this.wrappedJSObject = this;
}

{
  let proto = Object.create(TransactionBase.prototype);
  EditPropertyTransaction.prototype = proto;
  proto.constructor = EditPropertyTransaction;

  proto.declaration = null;
  proto.name = null;
  proto.newPriority = null;
  proto.newValue = null;
  proto.oldPriority = null;
  proto.oldValue = null;

  proto.doTransaction = function EPT_DoTransaction()
  {
    this._setProperty(this.newValue, this.newPriority);
  };

  proto.undoTransaction = function EPT_UndoTransaction()
  {
    this._setProperty(this.oldValue, this.oldPriority);
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  proto._setProperty = function EPT_SetProperty(aValue, aPriority)
  {
    // Circumvent "can't access dead object" errors".  See the comments about
    // weak refs above.
    try {
      String(this.declaration);
    }
    catch (ex) {
      // Go ahead and get rid of it, then.
      delete this.declaration;
    }

    if (this.declaration) {
      // XXX Guard against errors?
      this.declaration.setProperty(this.name, aValue, aPriority);
    }
  };
}
