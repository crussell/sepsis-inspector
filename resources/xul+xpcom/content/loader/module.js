/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.Module = Components.utils.import(
  "chrome://sepsis-inspector/content/loader/module.jsm", Object.create(null)
).Module;

//////////////////////////////////////////////////////////////////////////////
