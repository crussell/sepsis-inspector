/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Creates a function suitable for a global `require`.
 *
 * Not guaranteed to match the characteristics of NodeJS or CommonJS.
 *
 * @see resources/xul+xpcom/content/require.js
 * @see resources/xul+xpcom/content/loader/module.jsm
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

const EXPORTED_SYMBOLS = [ "Require" ];

const Cc = Components.classes;

//////////////////////////////////////////////////////////////////////////////

Components.utils.import("chrome://sepsis-inspector/content/loader/module.jsm");

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aLoader
 *        The `Loader` instance that manages the execution context the
 *        `require` function will operate within.
 * @param aResolver
 *        Satisfies the resolver interface to perform module lookups.  Must
 *        implement the `mapToURI` method.
 * @param aModule
 *        The `Module` where the `require` call is happening.
 * @return
 *        An implementation of the `require` function.
 */
function Require(aResolver, aLoader, aModule)
{
  // Make the [[Call]] behavior match [[Construct]]
  if (!Require.prototype.isPrototypeOf(this)) {
    return new Require(aResolver, aLoader, aModule);
  }

  this._resolver = aResolver;
  this._loader = aLoader;
  this._module = aModule;

  return this._requireImplementation.bind(this);
}

{
  let $proto = Require.prototype;

  $proto._requireImplementation = function R_RequireImplementation(aID)
  {
    if (!aID) throw new Error("No name given\n\n" + this._module.uri);

    let uri = this._resolver.mapToURI(aID, this._module.uri);
    let module = this._loader.getCachedModule(uri);
    if (module) return module.exports;

    if (uri.endsWith(".json")) {
      // First attempt to load and parse JSON uri, e.g., "test.json".  If that
      // doesn't exist, check for "test.json.js" for NodeJS parity.
      let fileText;
      try { fileText = Resolver.readText(uri); } catch (ex) {};
      if (fileText !== undefined) {
        module = new Module(uri);
        // NB: This may throw.  Let it.
        module.exports = JSON.parse(fileText);
        this._loader.cacheModule(module);

        return module.exports;
      }

      // Keep looking.  Try to find a "*.json.js" file instead.
      module = this._loader.getCachedModule(uri + ".js");
      if (module) return module.exports;
    }

    module = new Module(uri);
    return this._loader.load(module);
  }
}
