/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

const EXPORTED_SYMBOLS = [ "StackUtils" ];

//////////////////////////////////////////////////////////////////////////////

var StackUtils =
{
  parseStack: function SU_ParseStack(stack) {
    let lines = String(stack).split("\n");
    return lines.reduce(function(frames, line) {
      if (line) {
        let atIndex = line.indexOf("@");
        let columnIndex = line.lastIndexOf(":");
        let lineIndex = line.lastIndexOf(":", columnIndex - 1);
        let fileName = line.slice(atIndex + 1, lineIndex).split(" -> ").pop();
        let lineNumber = parseInt(line.slice(lineIndex + 1, columnIndex));
        let columnNumber = parseInt(line.slice(columnIndex + 1));
        let name = line.slice(0, atIndex).split("(").shift();
        frames.unshift({
          fileName: fileName,
          name: name,
          lineNumber: lineNumber,
          columnNumber: columnNumber
        });
      }
      return frames;
    }, []);
  },

  serializeStack: function SU_SerializeStack(frames) {
    return frames.reduce(function(stack, frame) {
      return frame.name + "@" +
             frame.fileName + ":" +
             frame.lineNumber + ":" +
             frame.columnNumber + "\n" +
             stack;
    }, "");
  }
}
