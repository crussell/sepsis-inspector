/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

const EXPORTED_SYMBOLS = [ "Module" ];

//////////////////////////////////////////////////////////////////////////////

function Module(uri)
{
  return Object.create(null, {
    singleton: { writable: true, value: false },
    exports: { enumerable: true, writable: true, value: Object.create(null) },
    uri: { value: uri }
  });
}
