/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Controls module lookups
 *
 * The `require` implementation can use this to map a requirement ID to URI
 * that can be loaded by the loader by calling the `mapToURI` method.
 *
 * The loader can use this to create new `require` implementations to be
 * exposed to nested modules by calling the `createRequire` method.
 *
 * In theory, these two roles don't necessarily need to be fulfilled by the
 * same object; consumers are totally free to implement some hypothetical
 * "ModuleFactory" and "RequireFactory" for these purposes, respectively.
 * However, in practice, both needs are handled by `Resolver` class for
 * simplicity.
 *
 * @see resources/xul+xpcom/content/require.js
 * @see resources/xul+xpcom/content/loader/require.jsm
 */

////////////////////////////////////////////////////////////////////////////
//// Global symbols

const EXPORTED_SYMBOLS = [ "Resolver" ];

const Cc = Components.classes;
const Ci = Components.interfaces;
const Cu = Components.utils;

////////////////////////////////////////////////////////////////////////////
//// Imported symbols

Cu.import("chrome://sepsis-inspector/content/loader/require.jsm");
Cu.import("chrome://sepsis-inspector/content/loader/stackUtils.jsm");

const OSFile = Cu.import("resource://gre/modules/osfile/ospath_unix.jsm");

////////////////////////////////////////////////////////////////////////////

/**
 * @param aRootURI
 *        string
 * @param aCustomPaths [optional]
 *        JSON key/value store where the key is a special prefix that should
 *        be mapped to the path given by the value string in all module
 *        resolutions.
 */
function Resolver(aRootURI, aCustomPaths)
{
  this.rootURI = this._addTrailingSlash(aRootURI);

  this._pathToURIMap = (aCustomPaths !== undefined) ?
    this._buildPathToURIMap(aCustomPaths) :
    new Map();
}

{
  let $proto = Resolver.prototype;

  // XXX Not exactly a great place for this to live, but oh well.
  Resolver.readText = $proto._readText = function R_ReadText(aURI)
  {
    let request =
      Cc["@mozilla.org/xmlextras/xmlhttprequest;1"].createInstance();
    request.open("GET", aURI, false);
    request.overrideMimeType("text/plain");
    request.send(null);

    return request.responseText;
  }

  Resolver.NODE_MODULES = $proto.NODE_MODULES = [
    "assert", "buffer_ieee754", "buffer", "child_process", "cluster",
    "console", "constants", "crypto", "_debugger", "dgram", "dns", "domain",
    "events", "freelist", "fs", "http", "https", "_linklist", "module",
    "net", "os", "path", "punycode", "querystring", "readline", "repl",
    "stream", "string_decoder", "sys", "timers", "tls", "tty", "url",
    "util", "vm", "zlib"
  ];

  Resolver.isNodeBuiltin = $proto.isNodeBuiltin =
    function R_IsNodeBuiltin(aName)
  {
    return !!~this.NODE_MODULES.indexOf(aName);
  }

  /**
   * @param aID
   *        A string naming the ID of the module to resolve.
   * @param aParentURI
   *        A string giving the URI of the parent module.
   * @return
   *        A string containing the URI to the module with the given ID.
   */
  $proto.mapToURI = function R_MapToURI(aID, aParentURI)
  {
    if (this.isNodeBuiltin(aID)) throw new Error("Host is not NodeJS");

    // The module's `uri` is expected to be undefined if it represents a JSM.
    let relativeTo = aParentURI || this._getCallerURI();
    let uri = this._resolveToURI(aID, relativeTo);
    if (!uri) {
      throw new Error(
        'Cannot resolve "' + aID + '" module required by', aParentURI
      );
    }

    return uri;
  }

  $proto.createRequire = function R_CreateRequire(aLoader, aModule)
  {
    return new Require(this, aLoader, aModule);
  }

  ////////////////////////////////////////////////////////////////////////////
  //// Internal methods

  $proto._addTrailingSlash = function R_AddTrailingSlash(aPath)
  {
    return !aPath ? null : !aPath.endsWith('/') ? aPath + '/' : aPath;
  }

  $proto._buildPathToURIMap = function R_BuildPathToURIMap(aPaths)
  {
    // We want more deeply nested custom paths to override more general ones.
    // XXX Doesn't handle absolute and relative custom paths.
    let map = new Map();
    Object.keys(aPaths).
      map(path => path.split("/")).
      sort((a, b) => b.length - a.length).
      map(dirs => dirs.join("/")).
      forEach(path => map.set(path, aPaths[path]));

    return map;
  }

  $proto._resolveToURI = function R_ResolveToURI(aID, aParentURI)
  {
    let uri = this._resolveCustomPaths(aID, this._pathToURIMap);
    if (uri) return uri;

    return this._nodeResolve(aID, aParentURI);
  }

  $proto._resolveCustomPaths =
    function R_ResolveCustomPaths(aID, aPathsToURIMap)
  {
    // Do not resolve if already a resource URI
    if (aID.startsWith("resource://")) return this._normalizeExt(aID);

    for (let [alias, uri] of aPathsToURIMap.keys()) {
      if (aID.startsWith(alias)) {
        return this._normalizeExt(aID.replace(alias, uri));
      }
    }

    return void 0; // otherwise we raise a warning, see bug 910304
  }

  /**
   * @param aModuleID
   *        A string naming the desired module.
   * @param aParentURI
   *        A string containing the URI for the module where the resolution
   *        for the above module should be relative to (e.g., the one that
   *        would be desired module's parent).
   * @see http://nodejs.org/api/modules.html#modules_all_together
   */
  $proto._nodeResolve = function R_NodeResolve(aModuleID, aParentURI)
  {
    let ambiguousURI = (this._isRelative(aModuleID)) ?
      this._resolveRelative(aModuleID, aParentURI) :
      this._join(this.rootURI, aModuleID);

    let resolvedPath = this._lookForFile(ambiguousURI);
    if (resolvedPath) return resolvedPath;

    resolvedPath = this._lookForDirectory(ambiguousURI);
    if (resolvedPath) return resolvedPath;

    // Walk our way up to the root and check to see if it's in some
    // "node_module" subdirectory.
    if (!this._isRelative(aModuleID) &&
        aParentURI.startsWith(this.rootURI)) {
      // assert(this.rootURI[this.rootURI.length - 1] == "/");
      let deepDir = OSFile.dirname(aParentURI.substr(this.rootURI.length));
      // assert(deepDir.charAt(0) != "/");
      let dirs = this._getNodeModulePaths(deepDir);

      for (let i = 0; i < dirs.length; i++) {
        ambiguousURI = this._join(this.rootURI, dirs[i], aModuleID);

        resolvedPath = this._lookForFile(ambiguousURI);
        if (resolvedPath) return resolvedPath;

        resolvedPath = this._lookForDirectory(ambiguousURI)
        if (resolvedPath) return resolvedPath;
      }
    }

    return void 0;
  }

  $proto._resolveRelative = function R_ResolveRelative(aID, aBase)
  {
    if (!this._isRelative(aID)) return aID;

    let basePaths = aBase.split('/');
    // Pop the last element in the `base`, because it is from a
    // relative file
    // '../mod.js' from '/path/to/file.js' should resolve to '/path/mod.js'
    basePaths.pop();
    if (!basePaths.length) return OSFile.normalize(aID);

    let resolved = this._join(basePaths.join('/'), aID);

    // Joining and normalizing removes the './' from relative files.
    // We need to ensure the resolution still has the root
    if (this._isRelative(aBase)) resolved = './' + resolved;

    return resolved;
  }

  $proto._isRelative = function R_IsRelative(aID)
  {
    return aID.charAt(0) === '.';
  }

  // Combines all arguments into a resolved, normalized path
  $proto._join = function R_Join(...aPaths)
  {
    let resolved = OSFile.normalize(OSFile.join(...aPaths))
    // OS.File `normalize` strips out the second slash in
    // `resource://` or `chrome://`, and third slash in
    // `file:///`, so we work around this
    resolved = resolved.replace(/^resource\:\/([^\/])/, 'resource://$1');
    resolved = resolved.replace(/^file\:\/([^\/])/, 'file:///$1');
    resolved = resolved.replace(/^chrome\:\/([^\/])/, 'chrome://$1');
    return resolved;
  }

  // Attempts to load `path` and then `path.js`
  // Returns `path` with valid file, or `undefined` otherwise
  $proto._lookForFile = function R_LookForFile(aPath)
  {
    // XXX As per node's loader spec,
    // we first should try and load 'path' (with no extension)
    // before trying 'path.js'. We will not support this feature
    // due to performance, but may add it if necessary for adoption.
    try {
      // Append '.js' to path name unless it's another support filetype
      let tmpPath = this._normalizeExt(aPath);
      this._readText(tmpPath);
      return tmpPath;
    } catch (e) {}

    return void 0;
  }

  $proto._lookForDirectory = function R_LookForDirectory(aPath)
  {
    // If `path/package.json` exists, parse the `main` entry.
    try {
      let manifestText = this._readText(aPath + '/package.json');
      let main = this._getMainPath(JSON.parse(manifestText));
      let tmpPath = this._join(aPath, main);
      let found = this._lookForFile(tmpPath);
      if (found) return found;
    } catch (e) { }

    let tmpPath = aPath + '/index.js'; // XXX Use `this._join`?
    try {
      this._readText(tmpPath);
      return tmpPath;
    } catch (e) {}

    return void 0;
  }

  // Utility function to normalize module URIs so they have ".js" extension.
  $proto._normalizeExt = function R_NormalizeExt(aURI)
  {
    return aURI.endsWith(".js") ? aURI :
           aURI.endsWith(".json") ? aURI :
           aURI + '.js';
  }

  // From `resolve` module
  // https://github.com/substack/node-resolve/blob/master/lib/node-modules-paths.js
  $proto._getNodeModulePaths = function R_GetNodeModulePaths(aDeepDir)
  {
    // Configurable in node -- do we need this to be configurable?
    let moduleDir = 'node_modules';

    let parts = aDeepDir.split('/');
    let dirs = [];
    for (let i = parts.length - 1; i >= 0; i--) {
      if (parts[i] === moduleDir) continue;
      let dir = this._join(parts.slice(0, i + 1).join('/'), moduleDir);
      dirs.push(dir);
    }

    if (aDeepDir.charAt(0) != "/") dirs.push(moduleDir);

    return dirs;
  }

  /**
   * @param aParsedManifest
   *        An addressable manifest store (i.e., the package.json parsed)
   * @return
   *        A string containing the path for the package's entry point.
   * @see https://docs.npmjs.com/files/package.json#main
   */
  $proto._getMainPath = function R_GetMainPath(aParsedManifest)
  {
    let path = aParsedManifest.main || './index.js';
    return (!this._isRelative(path)) ?
      './' + path :
      path;
  }

  // XXX Hack.  Only works if caller is from a JSM.  But it turns out that
  // JSMs are the whole reason this hack has to exist in the first place.
  $proto._getCallerURI = function R_GetCallerURI() {
    let frames = StackUtils.parseStack((new Error()).stack);
    let loaderURIPrefix = OSFile.dirname(__URI__);
    let currentFrame = frames.pop();
    while (frames.length && currentFrame.fileName.startsWith(loaderURIPrefix)) {
      currentFrame = frames.pop();
    }

    return currentFrame.fileName;
  }
}
