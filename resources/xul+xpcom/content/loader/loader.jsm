/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Loader suitable for managing `require`-able execution contexts ("modules").
 *
 * Not guaranteed to match the characteristics of NodeJS or CommonJS.
 *
 * @see resources/xul+xpcom/content/require.js
 * @see resources/xul+xpcom/content/loader/require.jsm
 */

////////////////////////////////////////////////////////////////////////////
//// Global symbols

const EXPORTED_SYMBOLS = [ "Loader" ];

const Cc = Components.classes;
const Ci = Components.interfaces;
const Cu = Components.utils;

const singletonModuleCache = new Map();
const singletonSandboxCache = new Map();

////////////////////////////////////////////////////////////////////////////
//// Imported symbols

Cu.import("chrome://sepsis-inspector/content/loader/stackUtils.jsm");

const { ConsoleAPI } = Cu.import("resource://gre/modules/devtools/Console.jsm");

////////////////////////////////////////////////////////////////////////////

/**
 * @param aRequireMaker
 *        Implements the `createRequire` method.
 * @param aOptions
 *        Object containing configuration options for the loader.  Must define
 *        an `id` for this loader.
 */
function Loader(aRequireMaker, aOptions)
{
  this.id = aOptions.id;
  if (undefined === this.id) throw new Error("Loader must have ID");

  this._requireMaker = aRequireMaker;
  if (undefined === this._requireMaker) throw new Error("`require` required");

  this.internalURI = aOptions.internalURI;

  this.invisibleToDebugger = !!aOptions.invisibleToDebugger;

  this.globals = aOptions.globals;
  if (undefined === this.globals) {
    this.globals = {
      console: new ConsoleAPI({ consoleID: this.id })
    };
  }

  this._uriToSandboxMap = new Map();
  this._cachedModules = new Map();
}

{
  let $proto = Loader.prototype;

  Loader.DuplicateModuleError = $proto.DuplicateModuleError =
    function L_DuplicateModuleError(aURI)
  {
    Components.utils.reportError((new Error()).stack); // XXX
    return new Error("Module already cached: " + aURI);
  }

  Loader.ExportsEvalError = $proto.ExportsEvalError =
    function L_ExportsEvalError(aInnerException, aID, aURI)
  {
    let { message, fileName, lineNumber } = aInnerException;
    let toString = String(aInnerException);
    let file = String(fileName).split(" -> ").pop();
    let stack = aInnerException.stack || Error().stack;
    let frames = StackUtils.parseStack(stack).filter(
      function isntLoaderFrame(frame)
    {
      return frame.fileName !== this.internalURI;
    });

    // Note that `String(error)` where error is from subscript loader does
    // not puts `:` after `"Error"` unlike regular errors thrown by JS code.
    // If there is a JS stack then this error has already been handled by an
    // inner module load.
    if (String(aInnerException) === "Error opening input stream (invalid filename?)") {
      let caller = frames.slice(0).pop();
      fileName = caller.fileName;
      lineNumber = caller.lineNumber;
      message = "Module `" + aID + "` is not found at " + aURI;
      toString = message;
    }
    // Workaround for a Bug 910653. Errors thrown by subscript loader
    // do not include `stack` field and above created error won't have
    // fileName or lineNumber of the module being loaded, so we ensure
    // it does.
    else if (frames[frames.length - 1].fileName !== file) {
      frames.push({ fileName: file, lineNumber: lineNumber, name: "" });
    }

    let prototype = (typeof(aInnerException) === "object") ?
      aInnerException.constructor.prototype :
      Error.prototype;

    return Object.create(prototype, {
      message: { value: message, writable: true, configurable: true },
      fileName: { value: fileName, writable: true, configurable: true },
      lineNumber: { value: lineNumber, writable: true, configurable: true },
      stack: { value: StackUtils.serializeStack(frames), writable: true, configurable: true },
      toString: { value: () => toString, writable: true, configurable: true },
    });
  }

  $proto.load = function L_Load(aModule)
  {
    // The load implementation may be dependent on the module being in the
    // cache before loading, so we set it and remove it if we have any errors.
    this.cacheModule(aModule);

    let exports;
    try {
      exports = this._loadExports(aModule);
    }
    catch (ex) {
      this._dropCachedModule(aModule);
      throw ex;
    }

    if (aModule.singleton) {
      this._promoteSingleton(aModule);
    }

    return exports;
  }

  $proto.getCachedModule = function L_GetCachedModule(aURI)
  {
    if (singletonModuleCache.has(aURI)) return singletonModuleCache.get(aURI);
    return (this._cachedModules.has(aURI)) ?
      this._cachedModules.get(aURI) :
      null;
  }

  $proto.cacheModule = function L_CacheModule(aModule)
  {
    let { uri } = aModule;
    if (this.getCachedModule(uri)) throw new Loader.DuplicateModuleError(uri);
    if (aModule.singleton) {
      singletonModuleCache.set(uri, aModule);
    }
    else {
      this._cachedModules.set(uri, aModule);
    }

    return aModule;
  }

  $proto.unload = function L_Unload(aModule)
  {
    this._dropCachedModule(aModule);
  };

  ////////////////////////////////////////////////////////////////////////////
  //// Internal methods

  /**
   * Evaluates the code associated with the given module so it can populate
   * the module's `exports`.
   *
   * @param aModule
   *        A `Module` instance.
   * @return
   *        The given module, after the code has been successfully loaded.
   */
  $proto._loadExports = function L_LoadExports(aModule)
  {
    // We expose set of properties defined by `CommonJS` specification via
    // prototype of the sandbox. Also globals are deeper in the prototype
    // chain so that each module has access to them as well.
    let proto = Object.create(this.globals);
    proto.require = this._requireMaker.createRequire(this, aModule);
    proto.module = aModule;
    
    // Need to make sure when scripts make direct assignments to `exports`,
    // then the effect gets propogated to the actual module instead of
    // temporarily overriding the symbol in that scope.
    Object.__defineGetter__.call(proto, "exports",
      function L_Exports_Get()
    {
      return aModule.exports;
    });

    Object.__defineSetter__.call(proto, "exports",
      function L_Exports_Set(aNewValue)
    {
      aModule.exports = aNewValue;
    });

    let sandbox = this._createSandbox({
      name: aModule.uri,
      prototype: proto,
      wantXrays: false,
      invisibleToDebugger: this.invisibleToDebugger,
      metadata: {
        URI: aModule.uri
      }
    });
    this._uriToSandboxMap.set(aModule.uri, sandbox);

    let { loadSubScript } = Cc['@mozilla.org/moz/jssubscript-loader;1'].
      getService(Ci.mozIJSSubScriptLoader);
    try {
      loadSubScript(aModule.uri, sandbox, "UTF-8");
    }
    catch (ex) {
      throw new Loader.ExportsEvalError(ex, aModule.id, aModule.uri);
    }

    if (aModule.exports && typeof(aModule.exports) === 'object') {
      this._freeze(aModule.exports);
    }

    return aModule.exports;
  }

  /**
   * @see https://developer.mozilla.org/en/Components.utils.Sandbox
   */
  $proto._createSandbox = function L_CreateSandbox(aOptions)
  {
    // Normalize options and rename to match `Cu.Sandbox` expectations.
    let systemPrincipal = Components.Constructor(
      '@mozilla.org/systemprincipal;1', 'nsIPrincipal'
    )();

    // XXX This should eventually use `wantComponents: false`
    let options = {
      sandboxName: aOptions.name,
      principal: 'principal' in aOptions ? aOptions.principal : systemPrincipal,
      wantXrays: 'wantXrays' in aOptions ? aOptions.wantXrays : true,
      sandboxPrototype: 'prototype' in aOptions ? aOptions.prototype : {},
      invisibleToDebugger: 'invisibleToDebugger' in aOptions ?
                           aOptions.invisibleToDebugger : false,
      metadata: 'metadata' in aOptions ? aOptions.metadata : {}
    };

    let sandbox = Cu.Sandbox(options.principal, options);

    // Each sandbox at creation gets its own set of properties that will be
    // shadowing ones from its prototype.  We want to avoid that.
    delete sandbox.Iterator;
    delete sandbox.importFunction;
    delete sandbox.debug;

    return sandbox;
  }

  $proto._dropCachedModule = function L_DropCachedModule(aModule)
  {
    let { uri } = aModule;
    let sandbox = this._uriToSandboxMap.get(uri);;
    this._uriToSandboxMap.delete(uri);
    this._cachedModules.delete(uri);

    return sandbox;
  }

  /**
   * Move the given module out of the loader instance's cache and into the
   * global cache shared by all loaders.  The shared cache is how we ensure
   * modules marked as singletons never get initialized more than once.
   * Modules may mark themselves as singletons by setting `module.singleton`
   * to true.
   *
   * @param aModule
   * @throws
   *        Error if the given module isn't actually marked as a singleton.
   */
  $proto._promoteSingleton = function L_PromoteSingleton(aModule)
  {
    let { uri } = aModule;
    if (!aModule.singleton) throw Error("Not a singleton module: " + uri);
    if (!this._cachedModules.has(uri)) throw Error("foreign module: " + uri);

    // assert(this._uriToSandboxMap.has(uri));
    let sandbox = this._dropCachedModule(aModule);
    singletonSandboxCache.set(uri, sandbox);
    singletonModuleCache.set(uri, aModule);
  }

  // Workaround for bug 674195. Freezing objects from other compartments fail,
  // so we use `Object.freeze` from the same compartment instead.
  $proto._freeze = function L_Freeze(object)
  {
    if (Object.getPrototypeOf(object) === null) {
      Object.freeze(object);
    }
    else {
      Object.getPrototypeOf(Object.getPrototypeOf(object.isPrototypeOf)).
        constructor. // `Object` from the owner compartment.
        freeze(object);
    }

    return object;
  }
}
