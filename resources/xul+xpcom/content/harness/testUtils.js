/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Used from within test files
 *
 * `Test.is` and `Test.ok` are available.
 *
 * `Test.error` prints the given error if non-null and counts it as a failure.
 *
 * @see harness/testRunner.js
 */

function TestUtils()
{
  this.successCount = 0;
  this.failureCount = 0;
}

{
  let proto = TestUtils.prototype;

  proto.failureCount = null;
  proto.successCount = null;

  proto.is = function T_Is(a, b, msg)
  {
    if (a === b) {
      ++this.successCount;
      return;
    }

    let err;
    try {
      err = "got: " + a + "\nexpected: " + b;
    }
    catch (ex) {
      err = "got unexpected value; encountered error coercing arguments";
    }

    if (msg) {
      err += "\n" + msg;
    }
    this._checkFailed(new Error(err));
  },

  proto.ok = function T_Ok(expression, msg)
  {
    if (expression) {
      ++this.successCount;
      return;
    }

    let err = "expected true expression";
    if (msg) {
      err += "\n" + msg;
    }
    this._checkFailed(new Error(err));
  },

  /**
   * @param aError
   * @param aMessage [optional]
   * @param aNoCount [optional]
   *        True iff this is being called from within any conditional/branch.
   */
  proto.error = function T_Error(aError, aMessage, aNoCount)
  {
    if (!aError && !aNoCount) {
      ++this.successCount;
      return;
    }

    if (aNoCount) {
      this._logError(aError, aMessage);
    }
    else {
      this._checkFailed(aError, aMessage);
    }
  },

  /**
   * Useful if some test code is known to currently throw.  E.g., suppose you
   * have some line:
   *
   *   Test.ok(foo.bar());
   *
   * If `foo` is null due to an implementation bug, this will throw an error,
   * which will suspend execution of the test file.  As a temporary
   * workaround, you can wrap it in a callback:
   *
   *   Test.try(() =>
   *     Test.ok(foo.bar())
   *   );
   *
   * `Test.try` will attempt to execute the callback, counting an exception as
   * a failure, but otherwise allow further execution of the test file to
   * proceed.
   */
  proto.try = function T_Try(aCallback)
  {
    let error = null;
    try {
      aCallback();
    }
    catch (ex) {
      error = ex;
    }

    if (error) {
      this._checkFailed(error, "Encountered error with test");
    }
    else {
      ++this.successCount;
    }
  },

  /**
   * For testing that your API throws when it's supposed to.
   *
   * @param aCallback
   *        Function that when called with no arguments is expected to throw.
   *        Failure to throw is counted as a test failure.
   */
  proto.throws = function T_Throws(aCallback)
  {
    let error = null;
    try {
      aCallback();
    }
    catch (ex) {
      error = ex;
    }

    this.ok(error);
  };

  proto._checkFailed = function T_CheckFailed(aError, aMessage)
  {
    ++this.failureCount;
    this._logError(aError, aMessage);
  };

  proto._logError = function T_LogError(aError, aMessage)
  {
    let stackString = "\n" + String(aError.stack);

    let preamble =  (aMessage) ?
      preamble = aMessage + "\n":
      "";
    dump("\n" + preamble + aError + stackString + "\n");
    console.error(aError);
  };
}
