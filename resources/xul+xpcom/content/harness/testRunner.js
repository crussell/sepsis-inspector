/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

function TestRunner(aTestSetName)
{
  window.addEventListener("keypress", this, false);

  this.mainTestSet = aTestSetName;

  let testSetMap = new Map();

  let pair = this._getRegistrationPair(aTestSetName);
  if (!pair) {
    this._showError(new Error("No tests registered for " + aTestSetName));
    return;
  }

  let { testUtilsURI, testsDirectory } = pair;

  testSetMap.set(this.mainTestSet, {
    config: null,
    testsDirectory: testsDirectory,
    name: this.mainTestSet
  });

  this._runAllSets(testSetMap, testUtilsURI);
}

{
  let Cc = Components.classes;
  let Ci = Components.interfaces;
  let Cu = Components.utils;

  let proto = TestRunner.prototype;

  proto.mResultsTable = null;

  TestRunner.instance = null;

  proto.mainTestSet = null;

  /**
   * @param aTestSetName
   *        A string specifying which tests should be run.  E.g., the URI to
   *        the root test directory.
   * @return
   *        The `nsIDOMWindow` that the new `TestRunner` runs in.
   */
  TestRunner.go = function TR_Go(aTestSetName)
  {
    const XULNS =
      "http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul";

    let runnerDocURI =
      "data:application/vnd.mozilla.xul+xml," + escape(
        "<?xml version=\"1.0\"?>\n" +
        "<?xml-stylesheet href=\"chrome://global/skin\"?>\n" +
        "<window id=\"winTests\"\n" +
        "        xmlns=\"" + XULNS + "\"\n" +
        "        onload=\"TestRunner.go('" + aTestSetName + "');\">\n" +
        "  <script type=\"application/javascript\"\n" +
        "          src=\"" + this.getOwnScriptURI() + "\"/>\n" +
        "</window>\n"
      );

    // NB: This needs to work even if no DOM exists.
    if ("document" in Cu.getGlobalForObject(this) && document &&
        document.documentURI == runnerDocURI) {
      TestRunner.instance = new TestRunner(aTestSetName);

      // Not used, but just for consistency.
      return window;
    }

    let scope = Object.create(null);
    Cu.import("resource://gre/modules/Services.jsm", scope);
    return scope.Services.ww.openWindow(null, runnerDocURI, null, null, null);
  };

  TestRunner.getOwnScriptURI = function TR_GetOwnScriptURI()
  {
    // The stack should be in the form
    //
    //   TR_GetOwnURI@chrome://example/content/testRunner.js:XX:YY
    //   [...]
    //
    // Where XX and YY are the line and column numbers.  (The column number
    // may not be available in older runtimes.)
    let line = (new Error()).stack.split("\n")[0];
    let lineDelimiterIndex = line.indexOf(":", line.lastIndexOf("/"));
    return line.substring(line.indexOf("@") + 1, lineDelimiterIndex);
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIDOMEventListener

  proto.handleEvent = function TR_HandleEvent(aEvent)
  {
    switch (aEvent.type) {
      case "keypress":
        if (aEvent.key == "F5" || (aEvent.key == "r" && aEvent.ctrlKey)) {
          window.removeEventListener("keypress", this);
          TestRunner.go(this.mainTestSet);
        }
      break;
      default:
        throw new Error("wat.  Event: " + aEvent.type);
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// Internal methods

  /**
   * @param aTestSetMap
   *        A `Map` whose key is the name of a test set and whose values are
   *        objects with `config`, and `testsDirectory` properties.
   *
   *        NB: `aTestSetMap` isn't guaranteed to contain the name of every
   *        test set yet.  A test set's config.json is allowed to contain
   *        mention of another set's directory or its config.json.  If that's
   *        the case, then *that* set should be run, too.
   *
   *        For any given test set, discovery of its recursively linked sets
   *        is allowed to happen during execution of the original set's tests.
   * @param aTestUtilsURI
   *        A string giving the path to an implementation of a Mochitest-like
   *        API.  Will be used by the `TestRunner` to instantiate the `Test`
   *        object visible to tests from their global scope.
   */
  proto._runAllSets = function TR_RunAllSets(aTestSetMap, aTestUtilsURI)
  {
    this.mResultsTable = this._buildTable();

    // ... to explain it another way, `aTestSetMap` may be modified during
    // loop execution.  This is no problem; ES6's iterators handle this
    // correctly.
    for (let [ name, info ] of aTestSetMap) {
      let contrib = this._runTestSet(name, info, aTestUtilsURI);
      for (let subordinate of contrib) {
        if (!aTestSetMap.has(subordinate.name)) {
          aTestSetMap.set(subordinate.name, subordinate);
        }
      }
    }
  };

  /**
   * @param aName
   *        A string giving the name of a particular test set to run.
   * @param aInfo
   * @param aTestUtilsURI
   * @return
   *        An array of info objects for subordinate test sets, so that the
   *        caller may handle them recursively.
   * @see TR_RunAllTests
   */
  proto._runTestSet = function TR_RunTestSet(aName, aInfo, aTestUtilsURI)
  {
    let result = [];
    let config;
    try {
      config = this._readAndParseConfig(aInfo.testsDirectory);
    }
    catch (ex) {
      this._showError(ex);
      return result;
    }

    // If we're running this test set because it appeared in the "contrib"
    // section of another test set's config.json, that other test set may have
    // specified some override config values (e.g., the location of a
    // `require` implementation).
    if (aInfo.config) {
      for (let override of Object.keys(aInfo.config)) {
        config[override] = aInfo.config[override];
      }
    }

    aInfo.config = config;

    for (let testName of config.tests) {
      if (!testName.length) continue;
      let testFileURI = aInfo.testsDirectory + testName + "/tests.js";
      this._runTestFile(testFileURI, aTestUtilsURI, config);
    }

    if (!("contrib" in config)) return result;

    for (let key of Object.keys(config.contrib)) {
      let name = (key.endsWith("/") || key.endsWith("/config.json")) ?
        name = key.substring(0, key.lastIndexOf("/")) :
        key;

      result.push({
        config: config.contrib[key],
        testsDirectory: aInfo.testsDirectory + name + "/",
        name: name
      });
    }

    return result;
  };

  /**
   * Look at the registered test sets, find the one we were asked for, and
   * return its registration info.
   *
   * @param aTestSetName
   * @return
   *        An object with properties `testUtilsURI` and `testsDirectory`.
   *        Their values will be strings.
   */
  proto._getRegistrationPair = function TR_GetRegistrationPair(aTestSetName)
  {
    const category = "sepsis-tests";

    let categoryManager = Cc["@mozilla.org/categorymanager;1"].getService(
      Ci.nsICategoryManager
    );

    let e = categoryManager.enumerateCategory(category);
    while (e.hasMoreElements()) {
      let result = { };

      result.testUtilsURI = String(
        e.getNext().QueryInterface(Ci.nsISupportsCString)
      );

      result.testsDirectory = this._fixUpDirectoryURI(
        categoryManager.getCategoryEntry(category, result.testUtilsURI)
      );

      if (!aTestSetName.startsWith("chrome://")) {
        let prefix = "chrome://" + aTestSetName + "/";
        if (result.testsDirectory.startsWith(prefix)) {
          return result;
        }
      }
      else if (result.testsDirectory == this._fixUpDirectoryURI(aTestSetName)) {
        return result;
      }
    }

    return null;
  };

  proto._fixUpDirectoryURI = function TR_FixUpDirectoryURI(aURI)
  {
    if (aURI.substr(-1, 1) != "/") {
      return aURI + "/";
    }

    return aURI;
  };

  /**
   * @param aTestDirectoryURI
   *        A string containing the URI of the root test directory.
   *        Subdirectories will be expected to contain a testfile named
   *        "tests.js".
   * @return
   *        A parsed JSON object with a "tests" array and an optional
   *        "contrib" array.
   */
  proto._readAndParseConfig =
    function TR_ReadAndParseConfig(aTestDirectoryURI)
  {
    let request =
      Cc["@mozilla.org/xmlextras/xmlhttprequest;1"].createInstance();

    let uri = aTestDirectoryURI + "config.json";
    request.open("GET", uri, false);
    request.overrideMimeType("application/json");
    try {
      request.send(null);
    }
    catch (ex) {
      let error = new Error("Couldn't open " + uri);
      error.innerException = ex;
      throw error;
    }

    let parsedConfig;
    try {
      parsedConfig = JSON.parse(request.responseText);
    }
    catch (ex) {
      let error = Error("Couldn't parse tests' config.json");
      error.innerException = ex;
      throw error;
    }
    
    return this._validateConfig(parsedConfig, uri);
  };

  proto._validateConfig = function TR_ValidateConfig(aConfigJSON, aURI)
  {
    let message = "Validation failed for " + aURI +
                  ": expected an array of strings for property ";
    if (!("tests" in aConfigJSON)) {
      throw new Error(message);
    }

    if (!Array.isArray(aConfigJSON.tests)) {
      throw new Error(message + JSON.stringify("tests"));
    }

    let hasTests = false;
    for (let i = 0, n = aConfigJSON.tests.length; i < n; ++i) {
      if (typeof(aConfigJSON.tests[i]) != "string") {
        throw new Error(message + JSON.stringify("tests"));
      }
      if (aConfigJSON.tests[i] != "") {
        hasTests = true;
      }
    }

    if (!hasTests) {
      throw new Error("No tests listed in " + aURI);
    }

    return aConfigJSON;
  };

  proto._runTestFile =
    function TR_RunTestFile(aTestURI, aTestUtilsURI, aConfig)
  {
    this._showRunning(aTestURI);

    let principal =
      Cc["@mozilla.org/systemprincipal;1"].createInstance(Ci.nsIPrincipal);
    let sandbox = Cu.Sandbox(principal);

    // Set up the global `Test` object and `assert` function.
    sandbox.console = console;
    let $ = this._initLoaderWithScript(sandbox, aTestUtilsURI);
    sandbox.Test = new $.scope.TestUtils();
    sandbox.assert = sandbox.Test.ok.bind(sandbox.Test);

    // `Components` constants.
    sandbox.Cc = Cc;
    sandbox.Ci = Ci;
    sandbox.Cu = Cu;

    if (aConfig && "requireURI" in aConfig) {
      $.source(aConfig.requireURI);
      sandbox.exports = {};
    }

    // Now we can do the test file.
    let error;
    try {
      $.source(aTestURI);
    }
    catch (ex) {
      error = ex;
    }

    // XXX Need to close any content windows the script opened?
    this._showResults(sandbox.Test, aTestURI, error);
  };

  /**
   * @param aSandbox
   *        Components.utils.Sandbox
   * @param aURI
   *        A string describing the URI of a script to load.
   * @return
   *        An object with a `scope` property whose keys are those that
   *        correspond to globals from the script at the given URI, and a
   *        `source` method that can load further scripts.
   *
   * NB: Globals from the given script are hidden and only accessible from
   * `scope`, but globals from any script passed to the `source` method are
   * loaded as normal (i.e., not hidden away in `scope`).
   *
   * @see "convoluted mess" above.
   */
  proto._initLoaderWithScript =
    function TR_InitLoaderWithScript(aSandbox, aURI)
  {
    let loaderString =
      "(function() {" +
      "  const C = Components;" +
      "  var loader = C.classes[\"@mozilla.org/moz/jssubscript-loader;1\"]" +
      "    .getService(C.interfaces.mozIJSSubScriptLoader);" +
      "  var scope = Object.create(null);" +
      "  loader.loadSubScriptWithOptions(" +
           JSON.stringify(aURI) + ", { target: scope, ignoreCache: true }" +
      "  );" +
      "  return {" +
      "    scope: scope," +
      "    source: function (aURI) {" +
      "      loader.loadSubScriptWithOptions(aURI, { ignoreCache: true });" +
      "    }" +
      "  };" +
      "})();";
    return Cu.evalInSandbox(loaderString, aSandbox, "1.8");
  };

  proto._showError = function TR_ShowError(aError)
  {
    const XHTMLNS = "http://www.w3.org/1999/xhtml";

    // The data URI is way too long.
    let stackString = aError.stack.replace(/@data:.*/, "@TestRunner dialog");

    dump("\n" + aError + "\n" + stackString + "\n");
    Cu.reportError(aError + "\n" + stackString);

    let output = document.createElementNS(XHTMLNS, "div");
    output.setAttribute("style", "white-space: pre; max-width: 80ch;");
    output.textContent = aError + "\n" + stackString;
    document.documentElement.appendChild(output);
  };

  proto._buildTable = function TR_BuildTable()
  {
    const XHTMLNS = "http://www.w3.org/1999/xhtml";

    while (document.documentElement.childNodes.length) {
      document.documentElement.removeChild(
        document.documentElement.lastChild
      );
    }

    let $ = document.createElementNS.bind(document, XHTMLNS);
    let table = $("table");
    table.setAttribute("width", "100%");

    table.appendChild($("thead"));

    let row = table.tHead.appendChild($("tr"));
    row.appendChild($("td")).textContent = "Test";
    row.appendChild($("td")).textContent = "Successes";
    row.appendChild($("td")).textContent = "Failures";
    row.appendChild($("td")).textContent = "Passing";

    table.appendChild($("tbody"));

    return document.documentElement.appendChild(table);
  };

  proto._showRunning = function TR_ShowRunning(aURI)
  {
    const XHTMLNS = "http://www.w3.org/1999/xhtml";
    let row = document.createElementNS(XHTMLNS, "tr")
    row.appendChild(document.createElementNS(XHTMLNS, "td"));
    row.appendChild(document.createElementNS(XHTMLNS, "td"));
    row.firstChild.textContent = aURI;
    row.lastChild.textContent = "Running...";
    row.lastChild.setAttribute("colspan", "3");
    this.mResultsTable.tBodies[0].appendChild(row);
  };

  proto._showResults = function TR_ShowResults(aTestUtils, aURI, aError)
  {
    const XHTMLNS = "http://www.w3.org/1999/xhtml";

    let totalTests = aTestUtils.successCount + aTestUtils.failureCount;
    let permille = Math.floor((aTestUtils.successCount * 1000) / totalTests);

    let body = this.mResultsTable.tBodies[0];
    let row = body.lastChild.cloneNode(true);

    if (aError) {
      // The test file itself threw an error (or let one escape).  Ideally,
      // we'd be showing any errors ourselves, but at the moment, we just rely
      // on the test utils, which reports them to the console.
      aTestUtils.error(aError);
      row.lastChild.textContent = "Uncaught error (see console)";
    }
    else {
      row.lastChild.removeAttribute("colspan");
      row.lastChild.textContent = aTestUtils.successCount;

      row.appendChild(document.createElementNS(XHTMLNS, "td"));
      row.lastChild.textContent = aTestUtils.failureCount;

      row.appendChild(document.createElementNS(XHTMLNS, "td"));
      row.lastChild.textContent =
        Math.floor(permille / 10) + "." + (permille % 10) + "%";
    }

    body.replaceChild(row, body.lastChild);
  };
}
