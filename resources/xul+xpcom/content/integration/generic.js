/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Augment the `sepsis` object to provide a top-level command handler.  To
 * determine the context that the inspector should load when opened, we read
 * the pref:
 *
 *   sepsis.inspector.integration.<aAppName>.context
 *
 * Possible values are:
 *   0: Don't inspect any context initially.
 *   1: Use the current content context.
 *   2: Inspect the application's own window.
 *   3: Inspect application.
 *   4: Like 1, but use the content document instead of the content window.
 *      Similar to the DOM Inspector and the Web Console.
 *   5: Like 2, but open inspecting the application's document instead of its
 *      `window` object.
 */

sepsis.openInspectorByPref = function GO_OpenInspectorByPref(aAppName)
{
  if (aAppName == undefined) {
    throw new Error("app name must be provided");
  }

  this.initLogger();
  this.initPrefManager();
    
  let context;
  let pref =
    this.prefManager.getPref(".integration." + aAppName + ".context");
  switch (pref) {
    // Blank.
    case 0:
      // No op; context undefined.
    break;

    // Page context
    case 1:
      context = content;
    break;

    // App window context
    case 2:
      context = window;
    break;

    // Application context
    case 3:
      context = null;
    break;

    // Page document
    case 4:
      context = content.document;
    break;

    // App window document
    case 5:
      context = document;
    break;

    default:
      this.logger.print(
        "unrecognized value for pref sepsis.inspector.integration." +
        aAppName + ".context: " + pref
      );
    break;
  }

  sepsis.openInspector(context);
};

sepsis.initLogger = function GO_InitLogger()
{
  if ("logger" in this) return;

  let $ = Object.create;
  let { require } = Components.utils.import(
    "chrome://sepsis-inspector/content/require.js", $(null)
  );

  this.logger = require("utils/logger").Logger;
};

sepsis.initPrefManager = function GO_InitPrefmanager()
{
  if ("prefManager" in this) {
    return;
  }

  let $ = Object.create;
  let { require } = Components.utils.import(
    "chrome://sepsis-inspector/content/require.js", $(null)
  );

  let { ThePrefManager } = require("utils/thePrefManager");
  this.prefManager = ThePrefManager;
};
