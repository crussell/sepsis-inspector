/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Encapsulate platform differences wrt conventions for application titles.
 *
 * On Windows and Linux, it is the convention to display the document name
 * followed by the name application's own name, whereas on Mac, platform
 * convention is to display only the document name in the title bar.
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.TitleChanger = TitleChanger;

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aWindow
 *        The DOM window whose title should be changed.  The "default" title
 *        (that is, the application's name) should already be set.
 * @constructor
 */
function TitleChanger(aWindow)
{
  this.mWindow = aWindow;
  this.mOriginalTitle = aWindow.document.title;
  this.mIsMac = /Mac/.test(aWindow.navigator.platform);
}

{
  let proto = TitleChanger.prototype;
  proto.mIsMac = null;
  proto.mOriginalTitle = null;
  proto.mWindow = null;

  /**
   * @param aDocumentTitle
   *        The document title.
   */
  proto.changeTitle = function TC_ChangeTitle(aDocumentTitle)
  {
    let newTitle;
    if (aDocumentTitle) {
      newTitle = (this.mIsMac) ?
        aDocumentTitle :
        aDocumentTitle + " - " + this.mOriginalTitle;
    }
    else {
      newTitle = this.mOriginalTitle;
    }

    this.mWindow.document.title = newTitle;
  };
}
