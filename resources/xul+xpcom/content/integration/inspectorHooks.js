/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Launches the Inspector, e.g., from the host app or other Inspector windows
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.InspectorHooks = InspectorHooks;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { WindowWatcher } = require("xpcom/windowWatcher");
var { SupportsString } = require("xpcom/supportsString");
var { MutableArray } = require("xpcom/mutableArray");

var { SynthBloc } = require("synthetic/synthBloc");

//////////////////////////////////////////////////////////////////////////////

function InspectorHooks() { }

{
  let $proto = InspectorHooks.prototype;

  /**
   * Launch the inspector window.
   *
   * @param a_x_Target [optional]
   *        The object to use as the inspector target.  This will be used as the
   *        root panel's subject, if provided.  If null or undefined, and
   *        `aFromePanel` isn't specified, the Inspector will default to opening
   *        in "Inspect Application" mode.
   * @param aUID [optional]
   *        The UID of the viewer to be loaded in the root panel.  The viewer
   *        must be valid for the provided target, otherwise the inspector will
   *        complain about it and refuse to load the viewer.  Defaults to the
   *        viewer in the ".defaultViewer" pref.
   * @param aFromPanel [optional]
   *        If the Inspector is being opened to inspect the target in some
   *        existing Inspector window, this should be the panel `aTarget` came
   *        from.
   * @see synthetic/synthBloc
   */
  InspectorHooks.launch = $proto.launch =
    function IH_Launch(a_x_Target, aUID, aFromPanel)
  {
    // If `a_x_Target` is a primitive, then the `appendElement` method for
    // `MutableArray` would choke on it.  So we want some sort of wrapper.  As
    // it turns out, we already have mirrors and synth blocs, so we (ab)use
    // them for our immediate purpose here (even though that purpose doesn't
    // belong to the same set of problems that blocs and mirrors were created
    // to solve).  Corollary: if you're trying to wrap your head around synth
    // blocs, what they're for, and how they work, then look elsewhere; this
    // has nothing to do with their raison d'etre.
    let phantomBloc = new SynthBloc();
    phantomBloc.wrappedJSObject = phantomBloc;

    let target = Object.create(phantomBloc.mirror(a_x_Target));
    target.wrappedJSObject = target;

    let uid = new SupportsString();
    uid.data = aUID;

    let windowDotArguments = new MutableArray();
    windowDotArguments.appendElement(phantomBloc, false);
    windowDotArguments.appendElement(target, false);
    windowDotArguments.appendElement(uid, false);
    windowDotArguments.appendElement(aFromPanel, false);

    return WindowWatcher.openWindow(
      null, "chrome://sepsis-inspector/content/inspector.xul", "_blank",
      "chrome,all,dialog=no", windowDotArguments
    );
  }
}
