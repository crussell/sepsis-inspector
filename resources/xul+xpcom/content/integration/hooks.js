/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Launches the Inspector, e.g., from the host app
 *
 * Hooks to help integrate the Inspector with the host app.  Import this file
 * to begin using the `sepsis.openInspector` method to open up the Inspector
 * window, e.g., from a keyboard shortcut or a menu item in your app.
 *
 * Alternatively, use this file in conjunction with integration/generic.js and
 * a pref (with a name in the form expected by `sepsis.openInspectorByPref`)
 * in order to provide a user controllable preference in about:config that
 * determines how the Inspector chooses its initial target.
 *
 * @see integration/generic.js
 * @deprecated
 */

if (!("sepsis" in window)) {
  window.sepsis = {};
}

/**
 * @see integration/inspectorHooks
 * @deprecated
 */
sepsis.openInspector =
  function Sepsis_OpenInspector(aTarget, aUID, aFromPanel)
{
  if (!("_hooks" in sepsis)) {
    let require = Components.utils.import(
      "chrome://sepsis-inspector/content/require.js", Object.create(null)
    ).require;

    sepsis._hooks = require("integration/inspectorHooks").InspectorHooks;
  }

  return sepsis._hooks.launch(aTarget, aUID, aFromPanel);
}
