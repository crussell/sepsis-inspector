/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var Mirrors = function() {
    // XXX Bulletproof this.
    var HAS_PROTO_SET = ({}).__proto__ !== undefined;

    var $ = {};
    ["create", "defineProperty", "getOwnPropertyDescriptor", "isExtensible",
     "getPrototypeOf", "getOwnPropertyNames", "keys", "preventExtensions",
     "isSealed", "seal", "isFrozen", "freeze"].forEach(
        (p) => $[p] = Object[p]
    );
    $._quote = function(words) {
        return JSON.stringify(words);
    };
    $._className = function(val) {
        return $.toString.call(val).split(/\s|]/)[1];
    };
    $._mixin = function(original, ...descriptors) {
        return descriptors.reduce((proto, descriptor) => (
            $.create(proto, descriptor)
        ), original);
    };
    $._inheritedGetter = function(obj, name) {
        let descriptor;
        while (obj && !descriptor) {
            descriptor = $.getOwnPropertyDescriptor(obj, name);
            obj = $.getPrototypeOf(obj);
        }
        return descriptor.get;
    };
    $._lookUpOwnProperty = function(name, owner, base) {
        if (typeof(name) !== "string") throw new TypeError("String expected");
        var obj = $._getReflectee(owner);
        var desc = $.getOwnPropertyDescriptor(obj, name);
        if (desc === undefined) return null;
        let createPropertyMirror = $._getCreatorForProperties(owner);
        return createPropertyMirror(name, owner, base);
    };
    $._lookUpProperty = function(name, owner, base) {
        if (typeof(name) !== "string") throw new TypeError("String expected");
        var p = $._lookUpOwnProperty(name, owner, base);
        if (p) return p;
        var parent = owner.prototype;
        return parent && $._lookUpProperty(name, parent, base);
    };
    $._cloneOriginMirror = function(propertyMirror, which) {
        let origin = $._getPropertyOrigin(propertyMirror);
        let baseOrOwner = origin[which];
        let createValueMirror = $._getCreatorForValues(baseOrOwner)
        return createValueMirror($._getReflectee(baseOrOwner));
    };
    $._hasLiveAccessor = function(owner, name) {
        let descriptor = $.getOwnPropertyDescriptor(owner, name);
        return !!descriptor && !("value" in descriptor);
    };
    $._checkOwner = function(origin) {
        if (origin.owner !== origin.base) return new Error(
            "Property " + $._quote(origin.name) + " base differs from owner"
        );
        return null;
    };
    $._checkDescriptor = function(origin) {
        let owner = $._getReflectee(origin.owner);
        let property = $.getOwnPropertyDescriptor(owner, origin.name);
        if (!property || ("value" in property) === origin.isAccessor) {
            return new Error(
                "Property " + $._quote(origin.name) +
                " descriptor type has changed"
            );
        }
        return null;
    };
    $._changeField = function(error, origin, attribute, value) {
        if (error) throw error;
        let obj = $._getReflectee(origin.owner);
        let descriptor = $.getOwnPropertyDescriptor(obj, origin.name);
        descriptor[attribute] = value;
        $.defineProperty(obj, origin.name, descriptor);
    };
    $._generateFieldBooleanGetter = function(attribute) {
        return function() {
            let origin = $._getPropertyOrigin(this);
            let error = $._checkDescriptor(origin);
            if (error) throw error;
            let obj = $._getReflectee(origin.owner);
            return $.getOwnPropertyDescriptor(obj, origin.name)[attribute];
        }
    };
    $._generateFieldBooleanSetter = function(attribute) {
        return function(p) {
            p = $._dissolveMirror(p);
            if (typeof(p) !== "boolean") throw new TypeError(
                "Boolean expected"
            );
            let origin = $._getPropertyOrigin(this);
            let error = $._checkOwner(origin) || $._checkDescriptor(origin);
            $._changeField(error, origin, attribute, p);
        }
    };
    $._generateFieldPayloadGetter = function(attribute, coerceToNull) {
        return function() {
            let origin = $._getPropertyOrigin(this);
            let error = $._checkDescriptor(origin);
            if (error) throw error;
            let create = $._getCreatorForValues(origin.owner);
            let obj = $._getReflectee(origin.owner);
            let descriptor = $.getOwnPropertyDescriptor(obj, origin.name);
            return (!coerceToNull || descriptor[attribute] !== undefined) ?
                create(descriptor[attribute]) :
                null;
        }
    };
    $._generateFieldPayloadSetter = function(attribute, coerceNull) {
        return function(p) {
            p = $._dissolveMirror(p);
            if (coerceNull && p === null) p = undefined;
            let origin = $._getPropertyOrigin(this);
            let error = $._checkOwner(origin) || $._checkDescriptor(origin);
            $._changeField(error, origin, attribute, p);
        }
    };
    $._isPrimitive = function(input) {
        if (input === null) return true;
        switch (typeof(input)) {
            case "undefined":
            case "number":
            case "boolean":
            case "string": return true;
        }
        return false;
    };
    $._dissolveMirror = function(input) {
        if ($._isPrimitive(input)) return input;
        return $._stripMirror(input);
    }
    $._stripMirror = function(mirror) {
        let primitive = PrimitiveMirror.clone(mirror);
        if (primitive) return mirror.valueOf();

        return $._getReflectee(mirror);
    };
    $._getTrusted = function(type, mirror) {
        // NB: Grab hold to prevent `mirror.trust` from tricking us by
        // overwriting itself and fooling our equality comparison below.
        let trust = mirror.trust;
        let info = $._openChannel(type);
        trust(info.key);
        if (info.trust !== trust) throw new Error("Can't establish trust");
        return $._transfer(info.key);
    };
    $._openChannel = function(type) {
        let key = $.create(null);
        trustedChannels.keys.push(key);
        let count = trustedChannels.info.push({
            key: key,
            type: type
        });
        return trustedChannels.info[count - 1];
    };
    $._transfer = function(key, payload) {
        // NB: `undefined` is a plausible payload
        let hasPayload = arguments.length > 1;
        let info = $._grabChannel(key, !hasPayload);
        if (hasPayload) {
            info.content = { value: payload };
            return null;
        }
        else {
            return info.content.value;
        }
    }
    $._grabChannel = function(key, remove) {
        let idx = trustedChannels.keys.indexOf(key);
        if (idx < 0) throw new Error("Invalid key");
        let info = trustedChannels.info[idx];
        if (remove) {
            trustedChannels.info.splice(idx, 1);
            trustedChannels.keys.splice(idx, 1);
        }
        return info;
    };
    $._generateTrust = function(spec, fallback) {
        return function $$(key) {
            let info = $._grabChannel(key);
            let names = $.getOwnPropertyNames(spec);
            if (names.indexOf(info.type) < 0) {
                if (fallback && fallback != $$) fallback(key);
            }
            else {
                $._transfer(key, spec[info.type]);
            }
            info.trust = $$;
        };
    };
    $._getReflectee = function(mirror) {
        return $._getTrusted("reflectee", mirror);
    };
    $._getPropertyOrigin = function(mirror) {
        return $._getTrusted("propertyOrigin", mirror);
    };
    $._getCreatorForValues = function(mirror) {
        return $._getTrusted("valueMirrorCreator", mirror);
    };
    $._getCreatorForProperties = function(mirror) {
        return $._getTrusted("propertyMirrorCreator", mirror);
    };
    $._checkPrivilege = function(mirror, privilege) {
        let capabilities = $._getTrusted("capabilities", mirror);
        if (capabilities.indexOf(privilege) < 0) return new Error(
            "Incorrect privilege level"
        );
        return null;
    };
    $._enforceCapabilities = function(privilege, descriptor) {
        let result = $.create(null);
        for (let name in descriptor) {
            result[name] = $.create(null);
            for (let attribute in descriptor[name]) {
                let callee = descriptor[name][attribute];
                result[name][attribute] = function() {
                    let error = $._checkPrivilege(this, privilege);
                    if (error) throw error;
                    return callee.apply(this, arguments);
                };
            }
        }
        return result;
    };

    var trustedChannels = { keys: [], info: [] };

    var Allows = {
        introspection: 1,
        mutation: 2,
        evaluation: 4
    };

    var serialNumber = 0;

    function PrimitiveMirror(value) {
        if (!$._isPrimitive(value)) throw TypeError("Not a primitive");

        this.__val = value;
        this.__id = serialNumber++;

        return $.freeze(this);
    }

    PrimitiveMirror.from = function(value) {
        try {
            return new PrimitiveMirror(value);
        }
        catch (ex) {
            return null;
        }
    };

    PrimitiveMirror.clone = function(other) {
        if (!(other instanceof PrimitiveMirror)) return null;
        return new PrimitiveMirror(other.valueOf());
    };

    PrimitiveMirror.prototype = $.freeze({
        sameAs: function(input) {
            let other = PrimitiveMirror.clone(input);
            if (!other) other = PrimitiveMirror.from(input);
            return !!other && other.__val === this.__val;
        },
        get typeof() {
            return typeof(this.__val);
        },
        get internalClass() {
            return $._className(this.__val);
        },
        valueOf: function() {
            return this.__val;
        },
        toString: function() {
            return "Primitive Mirror #" + this.__id;
        }
    });

    PrimitiveMirror = $.freeze(PrimitiveMirror);

    var basicMirrorDescriptor = {
        sameAs: {
            value: function(other) {
                return (
                    !$._isPrimitive(other) &&
                    other.sameAs === this.sameAs &&
                    $._getReflectee(other) === $._getReflectee(this)
                );
            }
        },
        typeof: {
            get: function() {
                return typeof($._getReflectee(this));
            }
        },
        toString: {
            value: function() {
                return "Object Basic Local Mirror #" + this.__id;
            }
        }
    };

    var objBasicMirrorProto = $.create(null, basicMirrorDescriptor);

    var objMirrorDescriptor = $._enforceCapabilities(
        Allows.introspection,
    {
        prototype: {
            get: function() {
                let prototype = $.getPrototypeOf($._getReflectee(this));
                let createValueMirror = $._getCreatorForValues(this);
                return prototype && createValueMirror(prototype);
            }
        },
        isExtensible: {
            get: function() {
                return $.isExtensible($._getReflectee(this));
            }
        },
        isFrozen: {
            get: function() {
                return $.isFrozen($._getReflectee(this));
            }
        },
        isSealed: {
            get: function() {
                return $.isSealed($._getReflectee(this));
            }
        },
        keys: {
            get: function() {
                return $.keys($._getReflectee(this)).sort();
            }
        },
        propertyNames: {
            get: function() {
                var names = [];
                var obj = $._getReflectee(this);
                while (obj) {
                    names = names.concat($.getOwnPropertyNames(obj));
                    obj = $.getPrototypeOf(obj);
                }
                return names.sort().filter((key, idx, names) =>
                    idx === 0 || names[idx - 1] !== key
                );
            }
        },
        lookUpOwn: {
            value: function(name) {
                return $._lookUpOwnProperty(
                    $._dissolveMirror(name), this, this
                );
            }
        },
        lookUp: {
            value: function(name) {
                return $._lookUpProperty(
                    $._dissolveMirror(name), this, this
                );
            }
        },
        has: {
            value: function(name) {
                // NB: We pass `name` to `lookUp` unaltered instead of using
                // `$._dissolveMirror(name)`, because it could double unwrap.
                if (arguments.length) return this.lookUp(name) !== null;
                if (this.hasOwn()) return true;
                let parent = this.prototype;
                return !!parent && parent.has();
            }
        },
        hasOwn: {
            value: function(name) {
                // NB: Don't double unwrap `name` with `$._dissolveMirror`.
                if (arguments.length) return this.lookUpOwn(name) !== null;
                return $.getOwnPropertyNames($._getReflectee(this)).length > 0;
            }
        },
        internalClass: {
            get: function() {
                return $._className($._getReflectee(this));
            }
        },
        toString: {
            value: function() {
                return "Object Introspection Mirror #" + this.__id;
            }
        }
    });

    var objMirrorProto = $.create(objBasicMirrorProto, objMirrorDescriptor);

    var functionMirrorDescriptor = $._enforceCapabilities(
        Allows.introspection,
    {
        name: {
            get: function() {
                let descriptor = $.getOwnPropertyDescriptor(
                    $._getReflectee(this), "name"
                );
                return (descriptor && "value" in descriptor) ?
                    String(descriptor.value) :
                    null;
            }
        },
        source: {
            get: function() {
                var obj = $._getReflectee(this);
                var source = Function.prototype.toString.apply(obj);
                var pattern = /\)\s*\{\s*\[native code\]\s*\}$/;
                return (source.match(pattern) === null) ?
                    source :
                    null;
            }
        },
        toString: {
            value: function() {
                return "Function Introspection Mirror #" + this.__id;
            }
        }
    });

    var propertyMirrorDescriptor = $._enforceCapabilities(
        Allows.introspection,
    {
        sameAs: {
            value: function(other) {
                let otherOrigin;
                let origin = $._getPropertyOrigin(this);
                try {
                    otherOrigin = $._getPropertyOrigin(other);
                }
                finally {
                    return (
                        !!otherOrigin &&
                        this.sameAs === other.sameAs &&
                        origin.isAccessor === otherOrigin.isAccessor &&
                        origin.name === otherOrigin.name &&
                        origin.base.sameAs(otherOrigin.base) &&
                        origin.owner.sameAs(otherOrigin.owner)
                    );
                }
            }
        },
        base: {
            get: function() {
                return $._cloneOriginMirror(this, "base");
            }
        },
        owner: {
            get: function() {
                return $._cloneOriginMirror(this, "owner");
            }
        },
        name: {
            get: function() {
                return $._getPropertyOrigin(this).name;
            }
        },
        isAccessor: {
            get: function() {
                let origin = $._getPropertyOrigin(this);
                let error = $._checkDescriptor(origin);
                if (error) {
                    error.isAccessor = origin.isAccessor;
                    throw error;
                }
                return origin.isAccessor;
            }
        },
        isConfigurable: {
            get: $._generateFieldBooleanGetter("configurable")
        },
        isEnumerable: {
            get: $._generateFieldBooleanGetter("enumerable")
        },
        toString: {
            value: function() {
                let origin = $._getPropertyOrigin(this);
                return (
                    "Property Introspection Mirror name: " + origin.name +
                    " #" + this.__id
                );
            }
        }
    });

    var propertyMirrorProto = $.create(null, propertyMirrorDescriptor);

    var accessorPropertyMirrorDescriptor = $._enforceCapabilities(
        Allows.introspection,
    {
        getter: {
            get: $._generateFieldPayloadGetter("get", true)
        },
        setter: {
            get: $._generateFieldPayloadGetter("set", true)
        },
        toString: {
            value: function() {
                let origin = $._getPropertyOrigin(this);
                return (
                    "Accessor Property Introspection Mirror name: " +
                    origin.name + " #" + this.__id
                );
            }
        }
    });

    var accessorPropertyMirrorProto =
        $.create(propertyMirrorProto, accessorPropertyMirrorDescriptor);

    var dataPropertyMirrorDescriptor = $._enforceCapabilities(
        Allows.introspection,
    {
        isWritable: {
            get: $._generateFieldBooleanGetter("writable")
        },
        value: {
            get: $._generateFieldPayloadGetter("value")
        },
        toString: {
            value: function() {
                let origin = $._getPropertyOrigin(this);
                return (
                    "Data Property Introspection Mirror name: " + origin.name +
                    " #" + this.__id
                );
            }
        }
    });

    var dataPropertyMirrorProto =
        $.create(propertyMirrorProto, dataPropertyMirrorDescriptor);

    var introspectionMirrorProto = $.create(objMirrorProto, {
        trust: {
            value: $._generateTrust({
                valueMirrorCreator: createIntrospectionMirrorOn,
                propertyMirrorCreator: createPropertyIntrospectionMirrorOn,
                capabilities: [
                    Allows.introspection
                ]
            })
        }
    });

    var introspectionFunctionMirrorProto = $.create(
        introspectionMirrorProto,
        functionMirrorDescriptor
    );

    function createObjectMirrorOn(obj, objProto, functionProto) {
        let mirror = PrimitiveMirror.from(obj);
        if (mirror) return mirror;

        let proto = (typeof(obj) === "function") ?
            functionProto :
            objProto;

        var fields = {
            __id: { value: serialNumber++ },
            trust: { value: $._generateTrust({ reflectee: obj }, proto.trust) }
        };
        return $.create(proto, fields);
    };

    function createIntrospectionMirrorOn(obj) {
        return createObjectMirrorOn(
            obj, introspectionMirrorProto, introspectionFunctionMirrorProto
        );
    };

    function createPropertyMirrorOn(name, owner, base, aTypeProto, dTypeProto) {
        // NB: `owner` and `base` are mirrors.
        let createValueMirror = $._getCreatorForValues(owner);
        // assert($._getCreatorForValues(base) === createValueMirror);
        base = createValueMirror($._getReflectee(base));
        owner = createValueMirror($._getReflectee(owner));
        if (base.sameAs(owner)) base = owner;
        let isAccessor = $._hasLiveAccessor($._getReflectee(owner), name);
        let proto = isAccessor ?
            aTypeProto :
            dTypeProto;
        let origin = {
            isAccessor: isAccessor,
            name: name,
            base: base,
            owner: owner
        };
        return $.create(proto, {
            __id: {
                value: serialNumber++
            },
            trust: {
                value: $._generateTrust({
                    propertyOrigin: origin,
                    capabilities: $._getTrusted("capabilities", owner)
                })
            }
        });
    };

    function createPropertyIntrospectionMirrorOn(name, owner, base) {
        return createPropertyMirrorOn(
            name, owner, base,
            accessorPropertyMirrorProto,
            dataPropertyMirrorProto
        );
    };

    var objMutableMirrorDescriptor = $._enforceCapabilities(
        Allows.mutation,
    {
        prototype: {
            get: $._inheritedGetter(objMirrorProto, "prototype"),
            set: function(p) {
                if (!HAS_PROTO_SET) throw new Error(
                    "Changing the prototype is not supported in this engine"
                );
                $._getReflectee(this).__proto__ = $._dissolveMirror(p);
            }
        },
        isExtensible: {
            get: $._inheritedGetter(objMirrorProto, "isExtensible"),
            set: function(b) {
                b = $._dissolveMirror(b);
                if (typeof(b) !== "boolean") throw new TypeError(
                    "Boolean expected"
                );
                if (!this.isExtensible && b) throw Error(
                    "A non-extensible object cannot be made extensible"
                );
                if (!b) $.preventExtensions($._getReflectee(this));
            }
        },
        isFrozen: {
            get: $._inheritedGetter(objMirrorProto, "isFrozen"),
            set: function(b) {
                b = $._dissolveMirror(b);
                if (typeof(b) !== "boolean") throw new TypeError(
                    "Boolean expected"
                );
                if (this.isFrozen && !b) throw Error(
                    "An object cannot be unfrozen"
                );
                if (b) $.freeze($._getReflectee(this));
            }
        },
        isSealed: {
            get: $._inheritedGetter(objMirrorProto, "isSealed"),
            set: function(b) {
                b = $._dissolveMirror(b);
                if (typeof(b) !== "boolean") throw new TypeError(
                    "Boolean expected"
                );
                if (this.isSealed && !b) throw Error(
                    "An object cannot be unsealed"
                );
                if (b) $.seal($._getReflectee(this));
            }
        },
        define: {
            value: function(name, spec) {
                if (!this.isExtensible) throw Error(
                    "Can't add property to a non-extensible object"
                );
                if (this.hasOwn(name)) throw Error(
                    "Property " + $._quote(name) + " already exists"
                );
                // NB: Doing this here rather than above means that we avoid
                // double unwrapping in the `hasOwn` call.
                name = $._dissolveMirror(name);
                if (typeof(name) !== "string") throw new TypeError(
                    "String expected"
                );
                var desc = {};
                let $$ = $._dissolveMirror;
                if ("getter" in spec) desc.get = $$(
                    spec.getter
                );
                if ("setter" in spec) desc.set = $$(
                    spec.setter
                );
                if ("isConfigurable" in spec) desc.configurable = $$(
                    spec.isConfigurable
                );
                if ("isEnumerable" in spec) desc.enumerable = $$(
                    spec.isEnumerable
                );
                if ("isWritable" in spec) desc.writable = $$(
                    spec.isWritable
                );
                if ("value" in spec) desc.value = $$(
                    spec.value
                );
                $.defineProperty($._getReflectee(this), name, desc);
                let createPropertyMirror = $._getCreatorForProperties(this);
                return createPropertyMirror(name, this, this);
            }
        },
        toString: {
            value: function() {
                return "Object Introspection+Mutation Mirror #" + this.__id;
            }
        }
    });

    var mutableFactoryDescriptor = {
        trust: {
            value: $._generateTrust({
                valueMirrorCreator: createMutationMirrorOn,
                propertyMirrorCreator: createPropertyMutationMirrorOn,
                capabilities: [
                    Allows.introspection,
                    Allows.mutation
                ]
            })
        }
    };

    var objMutableMirrorProto = $.create(
        objMirrorProto,
        objMutableMirrorDescriptor
    );

    var mutableLocalMirrorProto = $.create(
        objMutableMirrorProto,
        mutableFactoryDescriptor
    );

    var mutableLocalFunctionMirrorProto = $.create(
        mutableLocalMirrorProto,
        functionMirrorDescriptor
    );

    function createMutationMirrorOn(obj) {
        return createObjectMirrorOn(
            obj, mutableLocalMirrorProto, mutableLocalFunctionMirrorProto
        );
    };

    var propertyMutableMirrorDescriptor = $._enforceCapabilities(
        Allows.mutation,
    {
        delete: {
            value: function(strict) {
                let origin = $._getPropertyOrigin(this);
                let error = $._checkOwner(origin);
                if (error) throw error;
                if (strict === undefined) strict = false;
                strict = $._dissolveMirror(strict);
                if (typeof(strict) !== "boolean") throw new TypeError(
                    "Boolean expected"
                );
                let obj = $._getReflectee(origin.base);
                var result;
                if (!strict) result = delete obj[origin.name];
                else (function() {
                    "use strict";
                    result = delete obj[origin.name];
                })();
                return result;
            }
        },
        becomeAccessorProperty: {
            value: function(getter, setter) {
                let origin = $._getPropertyOrigin(this);
                let error = $._checkOwner(origin);
                if (error) throw error;
                let obj = $._getReflectee(origin.owner);
                var desc = $.getOwnPropertyDescriptor(obj, origin.name);
                if (desc) {
                    if (!desc.configurable) throw Error(
                        "Can't mutate a non-configurable property"
                    );
                    delete desc.value;
                    delete desc.writable;
                }
                else {
                    desc = {
                        configurable: true,
                        enumerable: true
                    };
                }
                desc.get = $._dissolveMirror(getter);
                desc.set = $._dissolveMirror(setter);
                if (desc.get === null) desc.get = undefined;
                if (desc.set === null) desc.set = undefined;
                $.defineProperty(obj, origin.name, desc);
                return origin.owner.lookUpOwn(origin.name);
            }
        },
        becomeDataProperty: {
            value: function(value, writable) {
                let origin = $._getPropertyOrigin(this);
                let error = $._checkOwner(origin);
                if (error) throw error;
                if (writable === undefined) writable = true;
                writable = $._dissolveMirror(writable);
                if (typeof(writable) !== "boolean") throw new TypeError(
                    "Boolean expected"
                );
                let obj = $._getReflectee(origin.owner);
                var desc = $.getOwnPropertyDescriptor(obj, origin.name);
                if (desc) {
                    if (!desc.configurable) throw Error(
                        "Can't mutate a non-configurable property"
                    );
                    delete desc.get;
                    delete desc.set;
                }
                else {
                    desc = {
                        configurable: true,
                        enumerable: true
                    };
                }
                desc.value = $._dissolveMirror(value);
                desc.writable = writable;
                $.defineProperty(obj, origin.name, desc);
                return origin.owner.lookUpOwn(origin.name);
            }
        },
        isEnumerable: {
            get: $._inheritedGetter(propertyMirrorProto, "isEnumerable"),
            set: $._generateFieldBooleanSetter("enumerable")
        },
        isConfigurable: {
            get: $._inheritedGetter(propertyMirrorProto, "isConfigurable"),
            set: $._generateFieldBooleanSetter("configurable")
        },
        toString: {
            value: function() {
                return (
                    "Property Introspection+Mutation Mirror #" + this.__id
                );
            }
        }
    });

    var dataPropertyMutableMirrorDescriptor = $._enforceCapabilities(
        Allows.mutation,
    {
        isWritable: {
            get: $._inheritedGetter(dataPropertyMirrorProto, "isWritable"),
            set: $._generateFieldBooleanSetter("writable")
        },
        value: {
            get: $._inheritedGetter(dataPropertyMirrorProto, "value"),
            set: $._generateFieldPayloadSetter("value")
        },
        toString: {
            value: function() {
                return (
                    "Data Property Introspection+Mutation Mirror #" +
                    this.__id
                );
            }
        }
    });

    var accessorPropertyMutableMirrorDescriptor = $._enforceCapabilities(
        Allows.mutation,
    {
        getter: {
            get: $._inheritedGetter(accessorPropertyMirrorProto, "getter"),
            set: $._generateFieldPayloadSetter("get", true)
        },
        setter: {
            get: $._inheritedGetter(accessorPropertyMirrorProto, "setter"),
            set: $._generateFieldPayloadSetter("set", true)
        },
        toString: {
            value: function() {
                return (
                    "Accessor Property Introspection+Mutation Mirror #" +
                    this.__id
                );
            }
        }
    });

    var propertyMutableMirrorProto = $.create(
        propertyMirrorProto,
        propertyMutableMirrorDescriptor
    );

    var dataPropertyMutableMirrorProto = $.create(
        propertyMutableMirrorProto,
        dataPropertyMutableMirrorDescriptor
    );

    var accessorPropertyMutableMirrorProto = $.create(
        propertyMutableMirrorProto,
        accessorPropertyMutableMirrorDescriptor
    );

    function createPropertyMutationMirrorOn(name, owner, base) {
        return createPropertyMirrorOn(
            name, owner, base,
            accessorPropertyMutableMirrorProto,
            dataPropertyMutableMirrorProto
        );
    };

    var objEvalMirrorDescriptor = $._enforceCapabilities(
        Allows.evaluation,
    {
        hasInstance: {
            value: function(arg) {
                return $._dissolveMirror(arg) instanceof $._getReflectee(this);
            }
        },
        put: {
            value: function(name, value, strict) {
                name = $._dissolveMirror(name);
                value = $._dissolveMirror(value);
                var target = $._getReflectee(this);
                if (strict === undefined) strict = false;
                strict = $._dissolveMirror(strict);
                if (typeof(strict) !== "boolean") throw new TypeError(
                    "Boolean expected"
                );
                if (!strict) target[name] = value;
                else (function() {
                    "use strict";
                    target[name] = value;
                })();
            }
        },
        get: {
            value: function(name) {
                let createValueMirror = $._getCreatorForValues(this);
                return createValueMirror(
                    $._getReflectee(this)[$._dissolveMirror(name)]
                );
            }
        },
        callMethod: {
            value: function(name, arg0) {
                // NB: Don't use `$._dissolveMirror(name)`, because it can
                // lead to a double unwrapping bug when calling `this.get`.
                let func = this.get(name);
                return func.invoke.apply(
                    func, [ this ].concat([].slice.call(arguments, 1))
                );
            }
        },
        toString: {
            value: function() {
                return "Object Eval Mirror #" + this.__id;
            }
        }
    });

    var functionEvalMirrorDescriptor = $._enforceCapabilities(
        Allows.evaluation,
    {
        invoke: {
            value: function(thisArg, arg0) {
                var func = $._getReflectee(this);
                var thisArg = $._dissolveMirror(thisArg);
                let createValueMirror = $._getCreatorForValues(this);
                return createValueMirror(func.apply(
                    thisArg, [].slice.call(arguments, 1).map(
                        (v) => $._dissolveMirror(v))
                    )
                );
            }
        },
        execute: {
            value: function(arg0) {
                return this.invoke.apply(
                    this, [ null ].concat([].slice.call(arguments))
                );
            }
        },
        toString: {
            value: function() {
                return "Function Eval Mirror #" + this.__id;
            }
        }
    });

    var evalMirrorProto = $._mixin(
        objBasicMirrorProto,
        objEvalMirrorDescriptor,
        {
            trust: {
                value: $._generateTrust({
                    valueMirrorCreator: createEvaluationMirrorOn,
                    capabilities: [
                        Allows.evaluation
                    ]
                })
            }
        }
    );

    var evalFunctionMirrorProto = $.create(
        evalMirrorProto,
        functionEvalMirrorDescriptor
    );

    function createEvaluationMirrorOn(obj) {
        return createObjectMirrorOn(
            obj, evalMirrorProto, evalFunctionMirrorProto
        );
    };

    var introspectionEvalMirrorProto = $._mixin(
        introspectionMirrorProto,
        objEvalMirrorDescriptor,
        {
            toString: {
                value: function() {
                    return "Object Introspection+Eval Mirror #" + this.__id;
                }
            },
            trust: {
                value: $._generateTrust({
                    valueMirrorCreator: createIntrospectionEvaluationMirrorOn,
                    capabilities: [
                        Allows.introspection,
                        Allows.evaluation
                    ]
                }, introspectionMirrorProto.trust)
            }
        }
    );

    var introspectionEvalFunctionMirrorProto = $._mixin(
        introspectionEvalMirrorProto,
        functionMirrorDescriptor,
        functionEvalMirrorDescriptor,
        {
            toString: {
                value: function() {
                    return "Function Introspection+Eval Mirror #" + this.__id;
                }
            }
        }
    );

    function createIntrospectionEvaluationMirrorOn(obj) {
        return createObjectMirrorOn(
            obj, introspectionEvalMirrorProto,
            introspectionEvalFunctionMirrorProto
        );
    };

    var introspectionMutationEvalMirrorProto = $._mixin(
        mutableLocalMirrorProto,
        objEvalMirrorDescriptor,
        {
            toString: {
                value: function() {
                    return "Local Object Full Access Mirror #" + this.__id;
                }
            },
            trust: {
                value: $._generateTrust({
                    valueMirrorCreator: createIntrospectionMutationEvalMirrorOn,
                    capabilities: [
                        Allows.introspection,
                        Allows.mutation,
                        Allows.evaluation
                    ]
                }, mutableLocalMirrorProto.trust)
            }
        }
    );

    var introspectionMutationEvalFunctionMirrorProto = $._mixin(
        introspectionMutationEvalMirrorProto,
        functionMirrorDescriptor,
        functionEvalMirrorDescriptor,
        {
            toString: {
                value: function() {
                    return "Local Function Full Access Mirror #" + this.__id;
                }
            }
        }
    );

    function createIntrospectionMutationEvalMirrorOn(obj) {
        return createObjectMirrorOn(
            obj, introspectionMutationEvalMirrorProto,
            introspectionMutationEvalFunctionMirrorProto
        );
    };

    //--------------------------------------------------------------------------

    var exports = {
        introspect: createIntrospectionMirrorOn,
        mutate: createMutationMirrorOn,
        evaluation: createEvaluationMirrorOn,
        introspectEval: createIntrospectionEvaluationMirrorOn,
        fullLocal: createIntrospectionMutationEvalMirrorOn,
        strip: function (mirror) {
            return $._stripMirror(mirror);
        }
    };
    return exports;
}();

// Our minimal effort to work as a NodeJS module.
if (typeof(module) !== "undefined") module.exports = Mirrors;
