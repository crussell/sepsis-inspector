/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * NodeJS-style module wrapper for jsmirrors implementation.
 *
 * Wirfs-Brock mirrors doesn't ship as a NodeJS-style module.  (It defines no
 * top-level `exports`.)  So we ship a wrapper for it ourselves, so that it
 * may be `require`d.
 *
 * @see jsmirrors/tests/fakeRequire.js
 */

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { SubScriptLoader } = require("xpcom/subScriptLoader");
SubScriptLoader.loadSubScript( // defines `Mirror`
  "chrome://sepsis-inspector/content/jsmirrors/mirrors.js"
);

//////////////////////////////////////////////////////////////////////////////
//// Exported symbols

exports = Mirrors;

//////////////////////////////////////////////////////////////////////////////
