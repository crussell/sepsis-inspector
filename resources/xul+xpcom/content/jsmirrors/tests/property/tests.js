var Test = exports.Test = new (require('../testUtils'));

var $ = require('jsmirrors').introspect;
var x, y, z, p;

x = Object.create(null);
y = Object.create(x);
z = Object.create(y);

x.p = 0;

Test.ok($(x).lookUp("p").sameAs($(x).lookUp("p")));

Test.ok($(x).lookUp("p").base.sameAs($(x)));
Test.ok($(x).lookUp("p").owner.sameAs($(x)));

Test.ok($(y).lookUp("p").base.sameAs($(y)));
Test.ok($(y).lookUp("p").owner.sameAs($(x)));

Test.ok($(z).lookUp("p").base.sameAs($(z)));
Test.ok($(z).lookUp("p").owner.sameAs($(x)));

p = $(x).lookUp("p");
Test.is(p.name, "p");
Test.is(p.isAccessor, false);
Test.is(p.isConfigurable, true);
Test.is(p.isEnumerable, true);
Test.is(p.isWritable, true);
Test.ok(!("getter" in p));
Test.ok(!("setter" in p));

x = {};
Object.defineProperty(x, "p", { value: 0 });

p = $(x).lookUp("p");
Test.is(p.isAccessor, false);
Test.is(p.isConfigurable, false);
Test.is(p.isEnumerable, false);
Test.is(p.isWritable, false);
Test.ok(!("getter" in p));
Test.ok(!("setter" in p));

x = {};
Object.defineProperty(x, "p", { value: 0, configurable: true });

p = $(x).lookUp("p");
Test.is(p.isAccessor, false);
Test.is(p.isConfigurable, true);
Test.is(p.isEnumerable, false);
Test.is(p.isWritable, false);
Test.ok(!("getter" in p));
Test.ok(!("setter" in p));

x = {};
Object.defineProperty(x, "p", { value: 0, enumerable: true });

p = $(x).lookUp("p");
Test.is(p.isAccessor, false);
Test.is(p.isConfigurable, false);
Test.is(p.isEnumerable, true);
Test.is(p.isWritable, false);
Test.ok(!("getter" in p));
Test.ok(!("setter" in p));

x = {};
Object.defineProperty(x, "p", { value: 0, writable: true });

p = $(x).lookUp("p");
Test.is(p.isAccessor, false);
Test.is(p.isConfigurable, false);
Test.is(p.isEnumerable, false);
Test.is(p.isWritable, true);
Test.ok(!("getter" in p));
Test.ok(!("setter" in p));

x = {};
x.__defineGetter__("p", () => 0);

p = $(x).lookUp("p");
Test.is(p.isAccessor, true);
Test.ok(p.getter !== null);
Test.ok(p.setter === null);
Test.ok(!("isWritable" in p));

x = {};
x.__defineSetter__("p", () => 0);

p = $(x).lookUp("p");
Test.is(p.isAccessor, true);
Test.ok(p.getter === null);
Test.ok(p.setter !== null);
Test.ok(!("isWritable" in p));

x = {};
x.__defineGetter__("p", () => 0);
x.__defineSetter__("p", () => 0);

p = $(x).lookUp("p");
Test.is(p.isAccessor, true);
Test.ok(p.getter !== null);
Test.ok(p.setter !== null);
Test.ok(!("isWritable" in p));

x = {};
x.p = 0;

p = $(x).lookUp("p");

Test.ok( p);

delete x.p;

Test.ok(!("p" in x));
Test.is(p.name, "p");
Test.ok( p.base.sameAs($(x)));
Test.ok( p.owner.sameAs($(x)));
Test.throws(() =>
  !!p.isAccessor
);
Test.throws(() =>
  !!p.isConfigurable
);
Test.throws(() =>
  !!p.isEnumerable
);
Test.throws(() =>
  !!p.isWritable
);
Test.throws(() =>
  String(p.value)
);

x.p = 0;

Test.ok( ("p" in x));
Test.is(p.name, "p");
Test.ok( p.base.sameAs($(x)));
Test.ok( p.owner.sameAs($(x)));
Test.ok(!p.isAccessor);
Test.ok(p.isConfigurable !== null);
Test.ok(p.isEnumerable !== null);
Test.ok(p.isWritable !== null);
Test.ok(p.value !== null);

x = {};
x.__defineGetter__("p", () => 0);

p = $(x).lookUp("p");

Test.ok( p);

delete x.p;

Test.ok(!("p" in x));
Test.is(p.name, "p");
Test.ok( p.base.sameAs($(x)));
Test.ok( p.owner.sameAs($(x)));
Test.throws(() =>
  !!p.isAccessor
);
Test.throws(() =>
  !!p.isConfigurable
);
Test.throws(() =>
  !!p.isEnumerable
);
Test.throws(() =>
  String(p.getter)
);
Test.throws(() =>
  String(p.setter)
);

x.__defineGetter__("p", () => 0);

Test.ok( ("p" in x));
Test.is(p.name, "p");
Test.ok( p.base.sameAs($(x)));
Test.ok( p.owner.sameAs($(x)));
Test.ok( p.isAccessor);
Test.ok(p.isConfigurable !== null);
Test.ok(p.isEnumerable !== null);
Test.ok(p.getter !== null);
Test.ok(p.setter === null);

x = { p: null };

p = $(x).lookUp("p");
Test.ok(!p.sameAs({
    sameAs: p.sameAs,
    name: "p",
    base: { sameAs: () => true },
    owner: { sameAs: () => true },
    isAccessor: false
}));
