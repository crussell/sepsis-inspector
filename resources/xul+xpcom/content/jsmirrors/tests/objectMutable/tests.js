var Test = exports.Test = new (require('../testUtils'));

var $ = require('jsmirrors').mutate;
var x, y, z;

x = { foo: 'bar' };
y = {};
z = Object.create(y);

Test.is(Object.getPrototypeOf(z), y);

$(z).prototype = $(x);

Test.is(Object.getPrototypeOf(z), x);

Test.ok(Object.isExtensible(x));
Test.ok($(x).isExtensible);

$(x).isExtensible = false;

Test.ok(!Object.isExtensible(x));
Test.ok(!$(x).isExtensible);

Test.ok(!$(x).isSealed);
Test.ok(!Object.isSealed(x));

$(x).isSealed = true;

Test.ok($(x).isSealed);
Test.ok(Object.isSealed(x));

Test.ok(!$(x).isFrozen);
Test.ok(!Object.isFrozen(x));

$(x).isFrozen = true;

Test.ok($(x).isFrozen);
Test.ok(Object.isFrozen(x));
