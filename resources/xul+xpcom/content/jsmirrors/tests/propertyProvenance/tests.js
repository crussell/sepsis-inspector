var Test = exports.Test = new (require('../testUtils'));

var $ = require('jsmirrors').mutate;
var x, y, xp, yp;

x = Object.create(null);
y = Object.create(x);
x.p = 0;

xp = $(y).lookUp("p");

Test.ok(xp.base.sameAs($(y)));
Test.ok(xp.owner.sameAs($(x)));

yp = $(y).define("p", { value: 1, isConfigurable: true });

Test.is(y.p, 1);
Test.ok(yp.base.sameAs($(y)));
Test.ok(yp.owner.sameAs($(y)));

// Because the mirror was created to manage y's "p" but it turned out the
// property was contributed through the prototype chain (the one on x), then
// this is invalid:

Test.throws(() => xp.delete());
Test.is(y.p, 1);
Test.ok($(y).lookUp("p").sameAs(yp));

// And check that the prototype's property doesn't get deleted, because the
// mirror we're using was created for y's own "p".

yp.delete();
Test.is(y.p, 0);
Test.ok($(y).lookUp("p").sameAs(xp));

// Check for idempotence/no exceptions thrown

yp.delete();
Test.is(y.p, 0);
Test.ok($(y).lookUp("p").sameAs(xp));

// Still by the principle of idempotence:

Test.throws(() => xp.delete());
Test.is(y.p, 0);
Test.ok($(y).lookUp("p").sameAs(xp));

// Check that properly removing a property on the prototoype leaves others
// intact.

yp = $(y).define("p", { value: 1, isConfigurable: true });
Test.is(y.p, 1);

xp.owner.lookUp("p").delete();
Test.ok( xp.base.has("p"));
Test.ok(!xp.owner.has("p"));
Test.is(y.p, 1);

// Clean removal

yp.delete();
Test.ok(!yp.base.has("p"));
Test.ok(!yp.owner.has("p"));
