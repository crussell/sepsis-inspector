var Test = exports.Test = new (require('../testUtils'));

var $ = require('jsmirrors').evaluation;
var x, y, v;

x = function () { return this.p; };
v = Object.create(null);

Test.ok($(x).invoke($({ p: v })).sameAs($(v)));

x = (z, zz) => { zz.p = z.p; return zz; };
y = Object.create(null);

Test.ok($(x).invoke(null, $({ p: v }), $(y)).sameAs($(y)));
Test.is(y.p, v);

y = Object.create(null);

Test.ok($(x).execute($({ p: v }), $(y)).sameAs($(y)));
Test.is(y.p, v);
