#!/usr/bin/env node

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Driver for the NodeJS test harness
 *
 * Tests specified in config.json can be run by executing this file, or by
 * manually running it under Node from the commandline:
 *
 *   node ./testRunner-node.js
 *
 * Tests must be written as NodeJS modules.  A test with the name "example"
 * should be implemented in ./example/tests.js and "example" specified in the
 * config.json.  They must export the MochiTest-like test API implemented in
 * the file distributed along with this file.  Include this at the head of any
 * test module:
 *
 *   exports.Test = new (require('../testUtils'));
 *
 * @see config.json
 * @see testRunner-browser.html
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.TestRunner = TestRunner;

//////////////////////////////////////////////////////////////////////////////

function TestRunner(configPath) {
  if (!(this instanceof TestRunner)) return new TestRunner(configPath);

  this._results = new Map();

  let names = this._validateConfig(require(configPath));
  for (let i = 0, n = names.length; i < n; ++i) {
    // The empty string is allowed as a dummy entry that also acts a sentinel
    // we interpret to mean "stop running tests"; it's useful for debugging.
    if (!names[i]) break;
    let result = this._runTest(names[i]);
    this._results.set(names[i], result);
    console.log(result, names[i]);
  }
}

{
  let $proto = TestRunner.prototype;

  $proto._results = null;

  $proto._validateConfig = function(json, path) {
    let message = "Expected \"tests\" to be an array of strings in " + path;
    if (!("tests" in json)) throw new Error(message);

    if (!Array.isArray(json.tests)) throw new Error(message);

    let noTests = true;
    for (let i = 0, n = json.tests.length; i < n; ++i) {
      if (typeof(json.tests[i]) !== "string") throw new Error(message);
      if (json.tests[i] !== "") noTests = false;
    }

    if (noTests) throw new Error(
      "No tests listed in " + path
    );

    return json.tests;
  }

  $proto._runTest = function(name) {
    let exports = null;
    try {
      // Because `path.join` is busted.
      exports = require("./" + name + "/tests.js");
    }
    catch (ex) {
      console.error(ex.stack);
    }

    return exports && exports.Test;
  }
}

if (!module.parent) TestRunner("./config.json");
