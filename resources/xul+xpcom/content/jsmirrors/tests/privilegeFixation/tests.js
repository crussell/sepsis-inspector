var Test = exports.Test = new (require('../testUtils'));

var $f, $r, pe, pf, pr;

$f = require('jsmirrors').fullLocal;
$r = require('jsmirrors').introspect;

pr = $r({ s: 1 }).lookUp("s");
pf = $f({ t: 0 }).lookUp("t");

Test.ok(!("delete" in pr));
Test.is(Number(pr.value), 1);
pr.value = -1;
Test.is(Number(pr.value), 1);

Test.ok( ("delete" in pf));
Test.is(Number(pf.value), 0);
pf.value = null;
Test.is(pf.value.valueOf(), null);

// Given that we have resricted privileges for some object X and full
// privileges for some object Y, we want to know if there's a way to forge
// some object X' from the two that will give us unrestricted access to the
// underlying X.

pe = Object.create(pf, {
    name: {
        value: "s"
    },
    base: {
        value: pr.base
    },
    owner: {
        value: pr.owner
    }
});

Test.is(Number(pr.value), 1);
pe.value = -1;
Test.is(Number(pr.value), 1);

// What about an "apply" attack?  I.e., apply privileged methods to a
// restricted mirror.

Test.throws(() => pf.delete.call(pr));
Test.throws(() => pf.becomeDataProperty.call(pr, -1));

// Can't alter or impersonate trust, either.

let originalTrust = pr.trust;
pr.trust = pf.trust;
Test.is(pr.trust, originalTrust);
Test.ok(pr.trust !== pf.trust);

// And no way to MITM trust.

pe = Object.create(pf, {
  trust: {
    value: function() {
      return pr.trust.apply(pr, arguments);
    }
  }
});

Test.throws(() => pe.delete());
