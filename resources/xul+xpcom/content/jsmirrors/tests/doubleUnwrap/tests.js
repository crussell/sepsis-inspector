var Test = exports.Test = new (require('../testUtils'));

let $, x, i, m, n;

$ = require("jsmirrors").fullLocal;

x = Object.create(null);
m = $(x);
i = $("p");
n = $(i);

Test.throws(() => m.define(n, { value: -1, isEnumerable: true }));

Test.ok(!("p" in x));

m.define(i, { value: -1, isEnumerable: true });

Test.ok( ("p" in x));

$ = require('jsmirrors').introspect;

x = { p: false };

// XXX These tests were created because a review/audit found a double-
// unwrapping bug with the implementation of `has` and `hasOwn` at the time of
// this writing.  However the inclusion of these tests is not enough; need to
// rigorously test that no double-unwrapping is occurring for *any* methods
// that accept a primitive--i.e., not just these two, and not even just those
// that accept strings.
Test.throws(() => $(x).has($($("p"))));
Test.throws(() => $(x).hasOwn($($("p"))));
