/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Driver for the browser-based test harness
 *
 * Tests are written as NodeJS-style modules and so can run from the command
 * line (see testRunner-node.js), or in the browser.  They're supported in the
 * browser due to a stubbed out `require` and `exports` implementation
 * supplied by this TestRunner implementation.  This is to allow certain
 * downstream consumers to integrate the tests into their own test set merely
 * by dumping this project source tree into the including project's source
 * tree.  Caveats: in order for the project to integrate this project's tests,
 * it must have...
 *
 *   a) a passable `require` implementation
 *   b) support a Mochitest-like API like that found in our testUtils.js
 *
 * To run the tests, open harness.html in your browser.  NB: This is known not
 * to work on Chrome/Chromium due to XHR restrictions for file:-based URIs.
 * Tests for V8 can be performed by running the NodeJS test-runner at the
 * commandline.
 *
 * For (non-test) modules to be resolvable using `require` within the test
 * files, the test config needs to supply certain information:
 *
 *  1. the name as it should appear to test files trying to `require` the
 *     script to be tested.
 *  2. the URI to the script relative to this directory containing the test
 *     harness implementation
 *  3. the name of the symbol defined in the script from (2) that should be
 *     returned to test modules that `require` the name from (1)
 *
 * Note that this is only necessary for tests that actually intend to consume
 * the implementation as a NodeJS-style module.  It's perfectly feasible,
 * though for tests to source the required scripts by using the DOM e.g. to
 * manually create and insert script elements into the test's document.  This
 * is not recommended, however, because it prevents the scripts from being
 * usable through the NodeJS test-runner.
 *
 * @see harness.html
 * @see testRunner-node.js
 */

function TestRunner(aTestsDirURI)
{
  this.mTestsDirURI = aTestsDirURI;

  this.mResultsTable = this._buildTable();

  this.mTestFrame = document.createElement("iframe");
  document.body.appendChild(this.mTestFrame);
  this._runTests(aTestsDirURI);
}

{
  let proto = TestRunner.prototype;

  proto.mResultsTable = null;

  TestRunner.go = function TR_Go()
  {
    // We may be loaded as a framed document to run an individual test.  Make
    // sure to only continue if we're being loaded as the topmost test runner.
    if (document.location.search) return;

    uri = document.location.href;
    uri = uri.substring(0, uri.lastIndexOf("/"));

    return new TestRunner(uri);
  };

  ////////////////////////////////////////////////////////////////////////////
  //// Internal methods

  proto._runTests = function TR_RunTests(aTestsDirURI)
  {
    this._parseConfig(aTestsDirURI).then((names) => {
      this.mPendingTests = names;
      this._runNextTest(aTestsDirURI, names);
    });
  };

  proto._runNextTest = function TR_RunNextTest(aTestsDirURI, aTestNames)
  {
    // We allow config.json to contain a dummy "" test that a) gets ignored, and
    // b) is used as a signal to mean "stop running tests here", which is useful
    // when adding new tests to the config or when trying to fix broken tests.
    if (!aTestNames.length || !aTestNames[0]) {
      this.mTestFrame.parentNode.removeChild(this.mTestFrame);
      return;
    }

    let testFileURI = aTestsDirURI + "/" + aTestNames.shift() + "/tests.js";

    this._showRunning(testFileURI);
    this._loadFrame(document.documentURI + "?" + testFileURI).
      then((scope) => this._initTest(scope)).
      then((scriptElement) => {
        this._showResults(this.mTestFrame.contentWindow.Test);
        this._runNextTest(this.mTestsDirURI, this.mPendingTests);
      }).
      catch((event) => {
        this._showError(event.error);
        this._runNextTest(this.mTestsDirURI, this.mPendingTests);
      });
  };

  proto._loadFrame = function TR_LoadFrame(aURI) {
    let listener;
    return new Promise((resolve) => {
      listener = resolve;
      this.mTestFrame.addEventListener("load", listener);
      this.mTestFrame.src = aURI;
    }).then((event) => {
      this.mTestFrame.removeEventListener("load", listener);
      return event.target.contentWindow;
    });
  };

  proto._initTest = function TR_InitTest(aScope)
  {
    let special = this.config.script;

    aScope.require = function (aID)
    {
      switch (aID) {
        case "../testUtils": return aScope.TestUtils;
        case special.name: return aScope[special.symbol];
        default: throw new Error(
          "Can only require '" + special.name + "' and '../testUtils'"
        );
      }
    };

    let testURI = aScope.document.location.search.substr(1);
    return this._loadScript(aScope, "./testUtils.js").
      then(() => this._loadScript(aScope, special.uri)).
      then(() => this._loadScript(aScope, testURI));
  };

  proto._loadScript = function TR_LoadScript(aScope, aURI)
  {
    let el = aScope.document.createElement("script");
    el.setAttribute("type", "application/javascript;version=1.7");
    el.setAttribute("src", aURI);

    let listener;
    return new Promise((resolve, reject) => {
      listener = resolve;
      aScope.addEventListener("afterscriptexecute", listener, false);
      aScope.addEventListener("error", reject, false);
      aScope.document.head.appendChild(el);
    }).then((event) => {
      aScope.removeEventListener("afterscriptexecute", listener, false);
      return event.target;
    });
  };

  /**
   * @param aTestDirectoryURI
   *        A string containing the URI of the root test directory.  Must
   *        contain "config.json", and each test named in the config must
   *        corresponde to a subdirectory that contains its own "tests.js".
   * @return
   *        A promise for an array of strings giving the name of the tests
   *        specified by the config file.
   */
  proto._parseConfig = function TR_ParseConfig(aTestDirectoryURI)
  {
    let uri = aTestDirectoryURI + "/config.json";
    let request = new XMLHttpRequest();
    request.open("GET", uri, true);
    request.overrideMimeType("application/json");
    return new Promise((resolve, reject) => {
      request.addEventListener("error", reject);
      request.addEventListener("load", () => {
        try {
          this.config = JSON.parse(request.responseText);
        }
        catch (ex) {
          let error = Error("Couldn't parse tests' config.json: " + uri);
          error.innerException = ex;
          throw error;
        }
        
        resolve(this._validateConfig(this.config, uri));
      });

      request.send(null);
    });
  };

  proto._validateConfig = function TR_ValidateConfig(aConfigJSON, aURI)
  {
    let message = "Validation failed for " + aURI +
                  ": \"tests\" must be an array of strings";
    if (!("tests" in aConfigJSON)) {
      throw new Error(message);
    }

    if (!Array.isArray(aConfigJSON.tests)) {
      throw new Error(message);
    }

    let hasTests = false;
    for (let i = 0, n = aConfigJSON.tests.length; i < n; ++i) {
      if (typeof(aConfigJSON.tests[i]) != "string") {
        throw new Error(message);
      }
      if (aConfigJSON.tests[i] != "") {
        hasTests = true;
      }
    }

    if (!hasTests) {
      throw new Error("No tests listed in " + aURI);
    }

    if (typeof(aConfigJSON.script) !== "object" ||
        typeof(aConfigJSON.script.name) !== "string" ||
        typeof(aConfigJSON.script.uri) !== "string" ||
        typeof(aConfigJSON.script.symbol) !== "string") {
      throw new Error(
        "Validation failed for " + aURI + ": bad \"script\" spec"
      );
    }

    return aConfigJSON.tests;
  };

  proto._showError = function TR_ShowError(aError)
  {
    const XHTMLNS = "http://www.w3.org/1999/xhtml";

    let body = this.mResultsTable.tBodies[0];
    let row = body.lastChild;

    row.lastChild.textContent = "Uncaught error";

    // We sometimes get null when an error occurs in the test file.
    if (aError) {
      let output = document.createElementNS(XHTMLNS, "div");
      output.setAttribute("style", "white-space: pre; max-width: 80ch;");
      output.textContent = aError + "\n" + aError.stack;
      document.documentElement.appendChild(output);

      console.error(aError);
    }
  };

  proto._buildTable = function TR_BuildTable()
  {
    const XHTMLNS = "http://www.w3.org/1999/xhtml";

    while (document.body.childNodes.length) {
      document.body.removeChild(document.body.lastChild);
    }

    let $ = document.createElementNS.bind(document, XHTMLNS);
    let table = $("table");
    table.setAttribute("width", "100%");

    table.appendChild($("thead"));

    let row = table.tHead.appendChild($("tr"));
    row.appendChild($("td")).textContent = "Test";
    row.appendChild($("td")).textContent = "Successes";
    row.appendChild($("td")).textContent = "Failures";
    row.appendChild($("td")).textContent = "Passing";

    table.appendChild($("tbody"));

    return document.documentElement.appendChild(table);
  };

  proto._showRunning = function TR_ShowRunning(aURI)
  {
    const XHTMLNS = "http://www.w3.org/1999/xhtml";
    let row = document.createElementNS(XHTMLNS, "tr")
    row.appendChild(document.createElementNS(XHTMLNS, "td"));
    row.appendChild(document.createElementNS(XHTMLNS, "td"));
    row.firstChild.textContent = aURI;
    row.lastChild.textContent = "Running...";
    row.lastChild.setAttribute("colspan", "3");
    this.mResultsTable.tBodies[0].appendChild(row);
  };

  proto._showResults = function TR_ShowResults(aTestUtils)
  {
    const XHTMLNS = "http://www.w3.org/1999/xhtml";

    debugger; // XXX
    let totalTests = aTestUtils.successCount + aTestUtils.failureCount;
    let permille = Math.floor((aTestUtils.successCount * 1000) / totalTests);

    let body = this.mResultsTable.tBodies[0];
    let row = body.lastChild.cloneNode(true);

    row.lastChild.removeAttribute("colspan");
    row.lastChild.textContent = aTestUtils.successCount;

    row.appendChild(document.createElementNS(XHTMLNS, "td"));
    row.lastChild.textContent = aTestUtils.failureCount;

    row.appendChild(document.createElementNS(XHTMLNS, "td"));
    row.lastChild.textContent =
      Math.floor(permille / 10) + "." + (permille % 10) + "%";

    body.replaceChild(row, body.lastChild);
  };
}
var exports = {};
