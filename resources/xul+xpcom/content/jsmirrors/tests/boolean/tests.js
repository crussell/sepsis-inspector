var Test = exports.Test = new (require('../testUtils'));

var $ = require('jsmirrors').fullLocal;
var x, m, p;

x = { p: "foo" };
m = $(x);
p = m.lookUp("p");

Test.ok( p.isWritable);
Test.ok(p.value.sameAs("foo"));

p.isWritable = $(false);
x.p = "bar";

Test.ok(!p.isWritable);
Test.ok(p.value.sameAs("foo"));

Test.throws(() => p.isWritable = {});
Test.throws(() => p.isWritable = null);
Test.throws(() => p.isWritable = new Boolean(false));
Test.throws(() => p.isWritable = $({}));
Test.throws(() => p.isWritable = $(null));
Test.throws(() => p.isWritable = $(new Boolean(false)));

Test.ok( Object.getOwnPropertyDescriptor(x, "p").enumerable);
Test.ok( p.isEnumerable);

p.isEnumerable = $(false);

Test.ok(!Object.getOwnPropertyDescriptor(x, "p").enumerable);
Test.ok(!p.isEnumerable);

Test.throws(() => p.isEnumerable = {});
Test.throws(() => p.isEnumerable = null);
Test.throws(() => p.isEnumerable = new Boolean(false));
Test.throws(() => p.isEnumerable = $({}));
Test.throws(() => p.isEnumerable = $(null));
Test.throws(() => p.isEnumerable = $(new Boolean(false)));

Test.ok( Object.getOwnPropertyDescriptor(x, "p").configurable);
Test.ok( p.isConfigurable);

p.isConfigurable = $(false);

Test.ok(!Object.getOwnPropertyDescriptor(x, "p").configurable);
Test.ok(!p.isConfigurable);

Test.throws(() => p.isConfigurable = true);

Test.throws(() => p.isEnumerable = {});
Test.throws(() => p.isEnumerable = null);
Test.throws(() => p.isEnumerable = new Boolean(false));
Test.throws(() => p.isEnumerable = $({}));
Test.throws(() => p.isEnumerable = $(null));
Test.throws(() => p.isEnumerable = $(new Boolean(false)));

p.delete();

Test.ok("p" in x);

p.delete($(false));

Test.ok("p" in x);

Test.throws(() => p.delete($(true)));
Test.throws(() => p.delete({}));
Test.throws(() => p.delete(null));
Test.throws(() => p.delete(new Boolean(false)));
Test.throws(() => p.delete($({})));
Test.throws(() => p.delete($(null)));
Test.throws(() => p.delete($(new Boolean(false))));

// XXX need to do wrapped versions of all the throwy stuff, too

x = { get p() { return 1; } };
p = $(x).lookUp("p");

p.becomeDataProperty($(x));

Test.is(x.p, x);
Test.ok($(x).lookUp("p").isWritable);

x.y = Object.create(null);

p.becomeDataProperty($(x.y), $(false));

Test.is(x.p, x.y);
Test.ok(!$(x).lookUp("p").isWritable);

x.y.z = Object.create(null);

x.p = x.y.z;

Test.is(x.p, x.y);
Test.ok(!$(x).lookUp("p").isWritable);

p.becomeDataProperty($(x.y.z), $(true));

Test.is(x.p, x.y.z);
Test.ok($(x).lookUp("p").isWritable);

Test.throws(() => p.becomeDataProperty($(x), {}));
Test.throws(() => p.becomeDataProperty($(x), null));
Test.throws(() => p.becomeDataProperty($(x), new Boolean(true)));
Test.throws(() => p.becomeDataProperty($(x), $({})));
Test.throws(() => p.becomeDataProperty($(x), $(null)));
Test.throws(() => p.becomeDataProperty($(x), $(new Boolean(true))));

$(x).put("p", $(x.y));

Test.is(x.p, x.y);

$(x).lookUp("p").isWritable = false;
$(x).put("p", $(x.y.z));

Test.is(x.p, x.y);

$(x).put("p", $(x.y.z));

Test.is(x.p, x.y);

Test.throws(() => $(x).put("p", $(x.y.z), $(true)));
Test.is(x.p, x.y);

p.becomeAccessorProperty();

Test.ok($(x).lookUp("p").isAccessor);

// Doesn't throw
$(x).put("p", $(x.y.z), $(false));

// Does throw
Test.throws(() => $(x).put("p", $(x.y.z), $(true)));

delete x.p;
Object.preventExtensions(x);

// Doesn't throw
$(x).put("p", $(x.y.z), $(false));
Test.ok(!("p" in x));

// Does throw
Test.throws(() => $(x).put("p", $(x.y.z), $(true)));
Test.ok(!("p" in x));
