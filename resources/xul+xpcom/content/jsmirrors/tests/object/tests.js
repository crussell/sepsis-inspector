var Test = exports.Test = new (require('../testUtils'));

var x, y, z, $;

$ = require('jsmirrors').introspect;
x = {};

Test.ok($(x).prototype.sameAs($(Object.prototype)));

Test.is($(Object.create(null)).prototype, null);

Test.is($({}).typeof, "object");
Test.is($([]).typeof, "object");
Test.is($(() => {}).typeof, "function");

Test.is($({}).internalClass, "Object");
Test.is($([]).internalClass, "Array");
Test.is($(() => {}).internalClass, "Function");
Test.is($(new Boolean()).internalClass, "Boolean");
Test.is($(new Date()).internalClass, "Date");
Test.is($(new Error()).internalClass, "Error");
Test.is($(new Number()).internalClass, "Number");
Test.is($(new String()).internalClass, "String");
Test.is($(new RegExp()).internalClass, "RegExp");

Test.is($(Math).internalClass, "Math");
Test.is($(JSON).internalClass, "JSON");

Test.is($((function() { return arguments; })()).internalClass, "Arguments");

x = Object.create(null);
y = Object.create(x);
z = Object.create(y);

Test.is($(x).prototype, null);
Test.ok($(y).prototype.sameAs($(x)));
Test.ok($(z).prototype.sameAs($(y)));

Test.ok(!$(x).hasOwn("foo"));

x.p = null;
y.q = null;
z.r = null;

Test.is($(z).propertyNames.length, 3);
Test.ok($(z).propertyNames.indexOf("p") >= 0);
Test.ok($(z).propertyNames.indexOf("q") >= 0);
Test.ok($(z).propertyNames.indexOf("r") >= 0);

Test.is($(x).hasOwn("p"), true);
Test.is($(y).hasOwn("p"), false);
Test.is($(z).hasOwn("p"), false);

Test.is($(x).hasOwn("q"), false);
Test.is($(y).hasOwn("q"), true);
Test.is($(z).hasOwn("q"), false);

Test.is($(x).hasOwn("r"), false);
Test.is($(y).hasOwn("r"), false);
Test.is($(z).hasOwn("r"), true);

Test.is($(x).has("p"), true);
Test.is($(y).has("p"), true);
Test.is($(z).has("p"), true);

Test.is($(x).has("q"), false);
Test.is($(y).has("q"), true);
Test.is($(z).has("q"), true);

Test.is($(x).has("r"), false);
Test.is($(y).has("r"), false);
Test.is($(z).has("r"), true);

Test.ok( $(x).lookUpOwn("p"));
Test.ok(!$(y).lookUpOwn("p"));
Test.ok(!$(z).lookUpOwn("p"));

Test.ok(!$(x).lookUpOwn("q"));
Test.ok( $(y).lookUpOwn("q"));
Test.ok(!$(z).lookUpOwn("q"));

Test.ok(!$(x).lookUpOwn("r"));
Test.ok(!$(y).lookUpOwn("r"));
Test.ok( $(z).lookUpOwn("r"));

Test.ok( $(x).lookUp("p"));
Test.ok( $(y).lookUp("p"));
Test.ok( $(z).lookUp("p"));

Test.ok(!$(x).lookUp("q"));
Test.ok( $(y).lookUp("q"));
Test.ok( $(z).lookUp("q"));

Test.ok(!$(x).lookUp("r"));
Test.ok(!$(y).lookUp("r"));
Test.ok( $(z).lookUp("r"));

Test.is(String($(x).keys), "p");
Test.is(String($(y).keys), "q");
Test.is(String($(z).keys), "r");

x.y = y;
x.z = z;

z.p = null;
z.q = null;

Test.is(String($(x).keys), "p,y,z");
Test.is(String($(y).keys), "q");
Test.is(String($(z).keys), "p,q,r");

Test.is($(z).propertyNames.length, 5);
Test.is(String($(z).propertyNames), "p,q,r,y,z");

y.z = z;

Test.is(String($(x).keys), "p,y,z");
Test.is(String($(y).keys), "q,z");
Test.is(String($(z).keys), "p,q,r");

Test.is($(z).propertyNames.length, 5);
Test.is(String($(z).propertyNames), "p,q,r,y,z");

Object.defineProperty(y, "foo", { value: "bar" });

Test.ok($(y).has("foo"));
Test.ok($(y).hasOwn("foo"));
Test.is($(y).keys.indexOf("foo"), -1);
Test.ok($(y).propertyNames.indexOf("foo") >= 0);

Test.ok($(x).isExtensible);
Test.ok($(y).isExtensible);
Test.ok($(z).isExtensible);

Object.preventExtensions(y);

Test.ok( $(x).isExtensible);
Test.ok(!$(y).isExtensible);
Test.ok( $(z).isExtensible);

x = {};

Test.ok( $(x).has());
Test.ok(!$(x).hasOwn());

x = Object.create(null);

Test.ok(!$(x).has());
Test.ok(!$(x).hasOwn());

y = Object.create(x);
z = Object.create(y);

Test.ok(!$(x).has());
Test.ok(!$(x).hasOwn());
Test.ok(!$(y).has());
Test.ok(!$(y).hasOwn());
Test.ok(!$(z).has());
Test.ok(!$(z).hasOwn());

y.p = null;

Test.ok(!$(x).has());
Test.ok(!$(x).hasOwn());
Test.ok( $(y).has());
Test.ok( $(y).hasOwn());
Test.ok( $(z).has());
Test.ok(!$(z).hasOwn());
