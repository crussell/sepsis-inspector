var Test = exports.Test = new (require('../testUtils'));

// Check that we've implemented privilege separation correctly

var $, x, p;

$ = require('jsmirrors').introspect;
// - No evaluation
// - No mutation

x = () => null;

Test.ok(!("define" in $(x)));
Test.ok(!("freeze" in $(x)));
Test.ok(!("seal" in $(x)));

Test.ok(!("execute" in $(x)));
Test.ok(!("invoke" in $(x)));

Test.ok(!("callMethod" in $(x)));
Test.ok(!("get" in $(x)));
Test.ok(!("put" in $(x)));

x = Object.create(null);

Test.ok(!("define" in $(x)));
Test.ok(!("freeze" in $(x)));
Test.ok(!("seal" in $(x)));

Test.ok(!("callMethod" in $(x)));
Test.ok(!("get" in $(x)));
Test.ok(!("put" in $(x)));

$ = require('jsmirrors').evaluation;
// - No introspection
// - No mutation

x = () => null;

Test.ok(!("name" in $(x)));
Test.ok(!("source" in $(x)));

Test.ok(!("has" in $(x)));
Test.ok(!("hasOwn" in $(x)));
Test.ok(!("internalClass" in $(x)));
Test.ok(!("isExtensible" in $(x)));
Test.ok(!("keys" in $(x)));
Test.ok(!("lookUp" in $(x)));
Test.ok(!("lookUpOwn" in $(x)));
Test.ok(!("propertyNames" in $(x)));
Test.ok(!("prototype" in $(x)));

Test.ok(!("define" in $(x)));
Test.ok(!("freeze" in $(x)));
Test.ok(!("seal" in $(x)));

x = Object.create(null);

Test.ok(!("has" in $(x)));
Test.ok(!("hasOwn" in $(x)));
Test.ok(!("internalClass" in $(x)));
Test.ok(!("isExtensible" in $(x)));
Test.ok(!("keys" in $(x)));
Test.ok(!("lookUp" in $(x)));
Test.ok(!("lookUpOwn" in $(x)));
Test.ok(!("propertyNames" in $(x)));
Test.ok(!("prototype" in $(x)));

Test.ok(!("define" in $(x)));
Test.ok(!("freeze" in $(x)));
Test.ok(!("seal" in $(x)));

// Properties
// - No mutation

$ = require('jsmirrors').introspect;
x = Object.create(null);
Object.defineProperty(x, "a", { get: () => null });
Object.defineProperty(x, "d", { value: null });

p = $(x).lookUp("a");

Test.ok( __lookupGetter__.call(p, "isConfigurable"));
Test.ok(!__lookupSetter__.call(p, "isConfigurable"));
Test.ok( __lookupGetter__.call(p, "isEnumerable"));
Test.ok(!__lookupSetter__.call(p, "isEnumerable"));
Test.ok( __lookupGetter__.call(p, "getter"));
Test.ok(!__lookupSetter__.call(p, "getter"));
Test.ok( __lookupGetter__.call(p, "setter"));
Test.ok(!__lookupSetter__.call(p, "setter"));

p = $(x).lookUp("d");

Test.ok( __lookupGetter__.call(p, "isConfigurable"));
Test.ok(!__lookupSetter__.call(p, "isConfigurable"));
Test.ok( __lookupGetter__.call(p, "isEnumerable"));
Test.ok(!__lookupSetter__.call(p, "isEnumerable"));
Test.ok( __lookupGetter__.call(p, "isWritable"));
Test.ok(!__lookupSetter__.call(p, "isWritable"));
Test.ok( __lookupGetter__.call(p, "value"));
Test.ok(!__lookupSetter__.call(p, "value"));
