var Test = exports.Test = new (require('../testUtils'));

var $ = require('jsmirrors').fullLocal;
var x, m, p;

x = Object.create({});
m = $(x);

Test.ok(m.prototype);

m.prototype = $(null);

Test.is(Object.getPrototypeOf(x), null);
Test.ok($(null).sameAs(m.prototype));
Test.is(m.prototype, null);

__defineGetter__.call(x, "p", () => null);
__defineSetter__.call(x, "p", () => null);

Test.ok(m.lookUp("p").getter);
Test.ok(m.lookUp("p").setter);

m.lookUp("p").getter = $(null);

Test.is(Object.getOwnPropertyDescriptor(x, "p").get, undefined);
Test.ok($(null).sameAs(m.lookUp("p").getter));
Test.is(m.lookUp("p").getter, null);

m.lookUp("p").setter = $(null);

Test.is(Object.getOwnPropertyDescriptor(x, "p").set, undefined);
Test.ok($(null).sameAs(m.lookUp("p").setter));
Test.is(m.lookUp("p").setter, null);

p = m.lookUp("p");

p.becomeAccessorProperty($(null), $(null));

Test.is(Object.getOwnPropertyDescriptor(x, "p").get, undefined);
Test.ok($(null).sameAs(m.lookUp("p").getter));
Test.is(m.lookUp("p").getter, null);

Test.is(Object.getOwnPropertyDescriptor(x, "p").set, undefined);
Test.ok($(null).sameAs(m.lookUp("p").setter));
Test.is(m.lookUp("p").setter, null);

p.becomeAccessorProperty($(() => null), $(() => null));

Test.ok(Object.getOwnPropertyDescriptor(x, "p").get);
Test.ok(!$(null).sameAs(m.lookUp("p").getter));
Test.ok(m.lookUp("p").getter !== null);

Test.ok(Object.getOwnPropertyDescriptor(x, "p").set);
Test.ok(!$(null).sameAs(m.lookUp("p").setter));
Test.ok(m.lookUp("p").setter);

p.becomeAccessorProperty($(null), $(null));

Test.is(Object.getOwnPropertyDescriptor(x, "p").get, undefined);
Test.ok($(null).sameAs(m.lookUp("p").getter));
Test.is(m.lookUp("p").getter, null);

Test.is(Object.getOwnPropertyDescriptor(x, "p").set, undefined);
Test.ok($(null).sameAs(m.lookUp("p").setter));
Test.is(m.lookUp("p").setter, null);

p = p.becomeDataProperty(0);

Test.ok(!p.isAccessor);

p = p.becomeAccessorProperty($(null), $(null));

Test.is(Object.getOwnPropertyDescriptor(x, "p").get, undefined);
Test.ok($(null).sameAs(m.lookUp("p").getter));
Test.is(m.lookUp("p").getter, null);

Test.is(Object.getOwnPropertyDescriptor(x, "p").set, undefined);
Test.ok($(null).sameAs(m.lookUp("p").setter));
Test.is(m.lookUp("p").setter, null);

p = p.becomeAccessorProperty($(null), $(() => null));

Test.is(p.getter, null);
Test.ok($(null).sameAs(m.lookUp("p").getter));
Test.is(m.lookUp("p").getter, null);

Test.ok(p.setter);
Test.ok(!$(null).sameAs(m.lookUp("p").setter));
Test.ok(m.lookUp("p").setter);

p = p.becomeAccessorProperty($(() => null), $(null));

Test.ok(p.getter);
Test.ok(!$(null).sameAs(m.lookUp("p").getter));
Test.ok(m.lookUp("p").getter);

Test.is(p.setter, null);
Test.ok($(null).sameAs(m.lookUp("p").setter));
Test.is(m.lookUp("p").setter, null);
