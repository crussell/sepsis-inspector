var Test = exports.Test = new (require('../testUtils'));

var Mirrors = require('jsmirrors');
var $ = Mirrors.fullLocal;

function equal(a, b) {
    return (
        Mirrors.introspect(a).sameAs(b) &&
        Mirrors.mutate(a).sameAs(b) &&
        Mirrors.evaluation(a).sameAs(b) &&
        Mirrors.introspectEval(a).sameAs(b) &&
        Mirrors.fullLocal(a).sameAs(b)
    );
}

Test.ok( equal(undefined, undefined));
Test.ok(!equal(undefined, null));
Test.ok(!equal(undefined, false));
Test.ok(!equal(undefined, ""));
Test.ok(!equal(undefined, 0));
Test.ok(!equal(undefined, {}));
        
Test.ok(!equal(null, undefined));
Test.ok( equal(null, null));
Test.ok(!equal(null, false));
Test.ok(!equal(null, ""));
Test.ok(!equal(null, 0));
Test.ok(!equal(null, {}));
        
Test.ok(!equal(false, undefined));
Test.ok(!equal(false, null));
Test.ok( equal(false, false));
Test.ok(!equal(false, ""));
Test.ok(!equal(false, 0));
Test.ok(!equal(false, {}));
        
Test.ok(!equal("", undefined));
Test.ok(!equal("", null));
Test.ok(!equal("", false));
Test.ok( equal("", ""));
Test.ok(!equal("", 0));
Test.ok(!equal("", {}));
        
Test.ok(!equal(0, undefined));
Test.ok(!equal(0, null));
Test.ok(!equal(0, false));
Test.ok(!equal(0, ""));
Test.ok( equal(0, 0));
Test.ok(!equal(0, {}));

Test.ok(!equal($({}), undefined));
Test.ok(!equal($({}), null));
Test.ok(!equal($({}), false));
Test.ok(!equal($({}), ""));
Test.ok(!equal($({}), 0));

Test.ok(!equal($({ p: 0 }).lookUp("p"), undefined));
Test.ok(!equal($({ p: 0 }).lookUp("p"), null));
Test.ok(!equal($({ p: 0 }).lookUp("p"), false));
Test.ok(!equal($({ p: 0 }).lookUp("p"), ""));
Test.ok(!equal($({ p: 0 }).lookUp("p"), 0));

Test.ok(!equal(false, new Boolean(false)));
Test.ok(!equal(true, new Boolean(true)));

Test.ok(!equal(0, new Number(0)));
Test.ok(!equal(1, new Number(1)));
Test.ok(!equal(-1, new Number(-1)));

Test.ok(!equal("", new String("")));
Test.ok(!equal("foo", new String("foo")));

var $p = ($$) => Object.getPrototypeOf($$);
var x;

x = $(null);

Test.throws(() => Object.defineProperty($p(x), "sameAs", {value: () => null}));
Test.throws(() => Object.defineProperty($p(x), "equals", {value: () => null}));
Test.throws(() => Object.defineProperty($p(x), "valueOf", {value: () => null}));
Test.throws(() => Object.defineProperty($p(x), "__val", {value: () => null}));

Test.throws(() => Object.defineProperty(x, "sameAs", {value: () => null}));
Test.throws(() => Object.defineProperty(x, "equals", {value: () => null}));
Test.throws(() => Object.defineProperty(x, "valueOf", {value: () => null}));
Test.throws(() => Object.defineProperty(x, "__val", {value: () => null}));

Test.throws(() => { "use strict"; delete x.__val; });

Test.ok(!Object.getPrototypeOf(x).isPrototypeOf(new x.constructor("evil")));

Test.is($(null).typeof, "object");
Test.is($(undefined).typeof, "undefined");
Test.is($("").typeof, "string");
Test.is($("non-empty").typeof, "string");
Test.is($(true).typeof, "boolean");
Test.is($(false).typeof, "boolean");
Test.is($(0).typeof, "number");
Test.is($(1).typeof, "number");
Test.is($(NaN).typeof, "number");

Test.is($(null).internalClass, "Null");
Test.is($(undefined).internalClass, "Undefined");
Test.is($("").internalClass, "String");
Test.is($("non-empty").internalClass, "String");
Test.is($(true).internalClass, "Boolean");
Test.is($(false).internalClass, "Boolean");
Test.is($(0).internalClass, "Number");
Test.is($(1).internalClass, "Number");
Test.is($(NaN).internalClass, "Number");
