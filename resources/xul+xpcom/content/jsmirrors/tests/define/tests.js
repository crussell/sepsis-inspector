var Test = exports.Test = new (require('../testUtils'));

var $ = require('jsmirrors').mutate;
var x, m, v, p;

x = Object.create(null); m = $(x);
v = {};
p = m.define("p", { value: $(v) });

Test.ok(p);

Test.is(x.p, v);
Test.ok(p.value.sameAs($(v)));

Test.is(p.name, "p");
Test.ok(p.owner.sameAs(m));

Test.ok(!p.isAccessor);
Test.ok(!p.isConfigurable);
Test.ok(!p.isEnumerable);
Test.ok(!p.isWritable);

x = Object.create(null); m = $(x);
v = {};
p = m.define("p", { value: $(v), isConfigurable: true });

Test.ok(!p.isAccessor);
Test.ok( p.isConfigurable);
Test.ok(!p.isEnumerable);
Test.ok(!p.isWritable);

x = Object.create(null); m = $(x);
v = {};
p = m.define("p", { value: $(v), isEnumerable: true });

Test.ok(!p.isAccessor);
Test.ok(!p.isConfigurable);
Test.ok( p.isEnumerable);
Test.ok(!p.isWritable);

x = Object.create(null); m = $(x);
v = {};
p = m.define("p", { value: $(v), isWritable: true });

Test.ok(!p.isAccessor);
Test.ok(!p.isConfigurable);
Test.ok(!p.isEnumerable);
Test.ok( p.isWritable);

x = Object.create(null); m = $(x);
v = () => ({});
p = m.define("p", { getter: $(v)});

Test.ok( p.isAccessor);

Test.ok(p.getter.sameAs($(v)))
Test.is(p.setter, null);

x = Object.create(null); m = $(x);
v = () => ({});
p = m.define("p", { setter: $(v)});

Test.ok( p.isAccessor);

Test.is(p.getter, null);
Test.ok(p.setter.sameAs($(v)))

x = Object.create(null); m = $(x);
v = {};
x.o = v;

Test.ok(!$(x).has("p"));

p = m.define("p", $(x).lookUpOwn("o"));

Test.ok( $(x).has("p"));
Test.ok( $(x).lookUp("p").value.sameAs($(v)));

x = Object.create(null); m = $(x);
x.p = 0;

Test.ok($(x).has("p"));
Test.throws(() => $(x).define("p", { value: 1 }));

Object.seal(x);

Test.throws(() => $(x).define("q", { value: 1 }));
