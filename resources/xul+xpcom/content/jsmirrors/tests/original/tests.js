var Test = exports.Test = new (require('../testUtils'));

var J = Object.create(
  {a: 1, b:2, get x() {return 'x getter'}},
  {c: {value: 3}, d: {value: 4, enumerable: true}}
);
Object.defineProperty(
  J, 'z', {value: J, configurable: true, enumerable: true, writable: false}
);
Object.preventExtensions(J);

var Mirrors = require('jsmirrors');

var introspector, j, error;

//Run mirror tests on normal objects using J as the root object
introspector = Mirrors.introspect;
j = introspector(J);

Test.ok(!!j);
Test.ok(!!j.prototype);

Test.is(j.isExtensible, false);

Test.is(String(j.keys), "d,z");

Test.ok(j.lookUpOwn("c"));
Test.is(j.lookUpOwn("x"), null);
Test.ok(j.lookUp("x"));

Test.ok(j.sameAs(introspector(J)));
Test.ok(!j.sameAs(Mirrors.introspect({ })));

Test.is(j.typeof, "object");
Test.is(j.internalClass, "Object");
Test.is(Mirrors.introspect([ ]).internalClass, "Array");

var f = introspector(function bar () {return "bar"});

Test.ok(f);

Test.is(f.name, "bar");
Test.is(
  f.source.replace(/\s/g, ""),
  'function bar () {return "bar"}'.replace(/\s/g, "")
);

Test.ok(!j.lookUpOwn("c").isAccessor);

Test.ok(!j.lookUpOwn("c").isConfigurable);
Test.ok(!j.lookUpOwn("c").isEnumerable);
Test.ok(!j.lookUpOwn("c").isWritable);

Test.ok(j.lookUpOwn("z").isConfigurable);
Test.ok(j.lookUpOwn("z").isEnumerable);
Test.ok(!j.lookUpOwn("z").isWritable);

Test.ok(j.lookUpOwn("c").value.sameAs(3));

// XXX is mirror
Test.ok(j.lookUpOwn("z").value);

Test.ok(j.lookUpOwn("z").value.sameAs(j));

Test.ok(j.lookUpOwn("d").owner.sameAs(j));

Test.ok(!j.lookUp("a").owner.sameAs(j));
Test.ok(j.lookUp("a").owner.sameAs(j.prototype));

Test.ok(j.lookUp("x").getter);
Test.is(j.lookUp("x").setter, null);

introspector = Mirrors.mutate;
j = introspector(J);

var n = {}
var mn = introspector(n);

Test.ok(mn);

mn.define(
  "p1", { value: "property p1", isWritable: true, isConfigurable: true}
);

mn.define(
  "p2", {
    getter: introspector(function() { return "property p2"; }),
    isConfigurable: true
  }
);


Test.is(n.p1 + " " + n.p2, "property p1 property p2");

mn.prototype = introspector({p3: "inherited p3"});

Test.is(mn.prototype.sameAs(introspector(Object.prototype)), false);

Test.is(n.p3, "inherited p3");

mn.isExtensible = false;

Test.is(mn.isExtensible, false);

try {
  error = mn.define("p4", { value: "property p4", isConfigurable: true });
}
catch (ex) {
  error = ex;
}

Test.ok(!!error);
error = null;

mn.isFrozen = true;
n.p1 = "modified value";

Test.is(n.p1, "property p1");

// Recreate n and m to do some mutable property tests

n = {};
mn = introspector(n);

mn.define(
  "p1", { value: "property p1", isWritable: true, isConfigurable: true }
);
mn.define(
  "p2", {
    get: introspector(function() {return "property p2"}),
    isConfigurable: true
  }
);

Test.ok(!!mn.lookUpOwn("p1"));

Test.is(n.p1, "property p1");

Test.ok(!mn.lookUpOwn("p1").isEnumerable);
Test.ok(mn.lookUpOwn("p1").isWritable);

mn.lookUpOwn("p1").isEnumerable = true;
mn.lookUpOwn("p1").isWritable = false;

Test.ok(mn.lookUpOwn("p1").isEnumerable);
Test.ok(!mn.lookUpOwn("p1").isWritable);

mn.lookUpOwn("p1").becomeAccessorProperty(
  introspector(function() { return "p1 is now an accessor"; })
);

Test.is(n.p1, "p1 is now an accessor");

mn.lookUpOwn("p1").becomeDataProperty("p1 is again a data property", true);

Test.is(n.p1, "p1 is again a data property");

J = Object.create(
  {a: 1, b:2, get x() {return 'x getter'}},
  {c: {value: 3}, d: {value: 4, enumerable: true}}
);
Object.defineProperty(
  J, 'z', {value: J, configurable: true, enumerable: true, writable: false}
);
Object.preventExtensions(J);

introspector = Mirrors.fullLocal;
j = introspector(J);

Test.ok(j.get("d").sameAs(4));
Test.ok(j.get("x").sameAs("x getter"));

j.put("f", introspector(function (a, b) { return a + b; }));

Test.ok(j.get("f").sameAs(undefined));

try {
  j.put("f", introspector(function (a, b) { return a + b; }), !!"strict");
}
catch (ex) {
  error = ex;
}

Test.ok(error);
error = null;

Test.ok(j.get("f").sameAs(undefined));

j.prototype.put("f", introspector(function (a, b) { return a + b; }));

Test.ok(!!j.get("f"));

Test.ok(
  j.callMethod("f", "called ", "inherited").sameAs("called inherited")
);

Test.ok(
  j.get("f").invoke(undefined, 3, introspector({ valueOf: () => 4 })).sameAs(7)
);
