var Test = exports.Test = new (require('../testUtils'));

var $ = require('jsmirrors').mutate;
var x, p, pp, atype = [], dtype = [];

x = Object.create(null);

x.q = null;

$(x).lookUpOwn("q").isEnumerable = false;

Test.ok(!Object.getOwnPropertyDescriptor(x, "q").enumerable);

Test.ok( Object.getOwnPropertyDescriptor(x, "q").configurable);

$(x).lookUpOwn("q").isConfigurable = false;

Test.ok(!Object.getOwnPropertyDescriptor(x, "q").configurable);

x.p = null;

Test.ok( ("p" in x));

$(x).lookUpOwn("p").delete();

Test.ok(!("p" in x));

x.p = null;
p = $(x).lookUpOwn("p");

p.value = $(x);

Test.ok($(x).sameAs($(x).lookUpOwn("p").value));

// NB: `p` will still be valid and remain a data property mirror.
pp = p.becomeAccessorProperty($(() => null));
atype.push(pp);

Test.ok(!pp.sameAs(p));
Test.ok(Object.getOwnPropertyDescriptor(x, "p").get);
Test.is(Object.getOwnPropertyDescriptor(x, "p").set, undefined);
Test.ok(pp.getter);
Test.is(pp.setter, null);

pp = p.becomeAccessorProperty($(() => null), null);
atype.push(pp);

Test.ok(!pp.sameAs(p));
Test.ok(Object.getOwnPropertyDescriptor(x, "p").get);
Test.is(Object.getOwnPropertyDescriptor(x, "p").set, undefined);
Test.ok(pp.getter);
Test.is(pp.setter, null);

pp = p.becomeAccessorProperty($(() => null), undefined);
atype.push(pp);

Test.ok(!pp.sameAs(p));
Test.ok(Object.getOwnPropertyDescriptor(x, "p").get);
Test.is(Object.getOwnPropertyDescriptor(x, "p").set, undefined);
Test.ok(pp.getter);
Test.is(pp.setter, null);

pp = p.becomeAccessorProperty(null, $(() => null));
atype.push(pp);

Test.ok(!pp.sameAs(p));
Test.is(Object.getOwnPropertyDescriptor(x, "p").get, undefined);
Test.ok(Object.getOwnPropertyDescriptor(x, "p").set);
Test.is(pp.getter, null);
Test.ok(pp.setter);

pp = p.becomeAccessorProperty(undefined, $(() => null));
atype.push(pp);

Test.ok(!pp.sameAs(p));
Test.is(Object.getOwnPropertyDescriptor(x, "p").get, undefined);
Test.ok(Object.getOwnPropertyDescriptor(x, "p").set);
Test.is(pp.getter, null);
Test.ok(pp.setter);

pp = p.becomeAccessorProperty();
atype.push(pp);

Test.ok(!pp.sameAs(p));
Test.is(Object.getOwnPropertyDescriptor(x, "p").get, undefined);
Test.is(Object.getOwnPropertyDescriptor(x, "p").set, undefined);
Test.is(pp.getter, null);
Test.is(pp.setter, null);

pp.getter = $(() => {});
pp.setter = $(() => {});

Test.ok(Object.getOwnPropertyDescriptor(x, "p").set);
Test.ok(Object.getOwnPropertyDescriptor(x, "p").get);
Test.ok(pp.getter);
Test.ok(pp.setter);

pp.getter = null;
pp.setter = null;

Test.is(Object.getOwnPropertyDescriptor(x, "p").set, undefined);
Test.is(Object.getOwnPropertyDescriptor(x, "p").get, undefined);
Test.is(pp.getter, null);
Test.is(pp.setter, null);

pp.getter = $(() => {});
pp.setter = $(() => {});

Test.ok(Object.getOwnPropertyDescriptor(x, "p").set);
Test.ok(Object.getOwnPropertyDescriptor(x, "p").get);
Test.ok(pp.getter);
Test.ok(pp.setter);

pp.getter = undefined;
pp.setter = undefined;

Test.is(Object.getOwnPropertyDescriptor(x, "p").set, undefined);
Test.is(Object.getOwnPropertyDescriptor(x, "p").get, undefined);
Test.is(pp.getter, null);
Test.is(pp.setter, null);

p = pp.becomeDataProperty(0, false);
dtype.push(p);

Test.is(x.p, 0);
Test.ok(p.value.sameAs(0));
Test.ok(!Object.getOwnPropertyDescriptor(x, "p").writable);

p = pp.becomeDataProperty(1, true);
dtype.push(p);

Test.is(x.p, 1);
Test.ok(p.value.sameAs(1));
Test.ok( Object.getOwnPropertyDescriptor(x, "p").writable);

p.isWritable = false;

Test.ok(!Object.getOwnPropertyDescriptor(x, "p").writable);

p = pp.becomeDataProperty(2);
dtype.push(p);

Test.is(x.p, 2);
Test.ok(p.value.sameAs(2));
Test.ok( Object.getOwnPropertyDescriptor(x, "p").writable);

Test.ok(atype.reduce((b, c, i) => !i || (b && c.sameAs(atype[i - 1])), true));
Test.ok(dtype.reduce((b, c, i) => !i || (b && c.sameAs(dtype[i - 1])), true));

x = Object.create(null);
x.p = 0;

p = $(x).lookUp("p");

delete x.p;
Test.ok(!("p" in x));

Test.throws(() => p.value);

p.becomeDataProperty(undefined, true);
p.value = 1;
Test.is(x.p, 1);
Test.ok(p.value.sameAs(1));

Test.is(Object.getOwnPropertyDescriptor(x, "p").configurable, true);
Test.is(p.isConfigurable, true);
Test.is(Object.getOwnPropertyDescriptor(x, "p").enumerable, true);
Test.is(p.isEnumerable, true);
Test.is(Object.getOwnPropertyDescriptor(x, "p").writable, true);
Test.is(p.isWritable, true);

x = Object.create(null);
__defineGetter__.call(x, "p", () => 0);

p = $(x).lookUp("p");

delete x.p;
Test.ok(!("p" in x));

Test.throws(() => p.getter);
Test.throws(() => p.setter);

p = p.becomeAccessorProperty($(() => 1));
Test.ok(__lookupGetter__.call(x, "p") !== undefined);
Test.ok(__lookupSetter__.call(x, "p") === undefined);

Test.is(Object.getOwnPropertyDescriptor(x, "p").configurable, true);
Test.is(p.isConfigurable, true);
Test.is(Object.getOwnPropertyDescriptor(x, "p").enumerable, true);
Test.is(p.isEnumerable, true);

x = Object.create(null);
__defineGetter__.call(x, "p", () => 0);

p = $(x).lookUp("p");

delete x.p;
Test.ok(!("p" in x));

Test.throws(() => p.getter);
Test.throws(() => p.setter);

p = p.becomeAccessorProperty(null, $(() => void 0));
Test.ok(__lookupGetter__.call(x, "p") === undefined);
Test.ok(__lookupSetter__.call(x, "p") !== undefined);

Test.is(Object.getOwnPropertyDescriptor(x, "p").configurable, true);
Test.is(p.isConfigurable, true);
Test.is(Object.getOwnPropertyDescriptor(x, "p").enumerable, true);
Test.is(p.isEnumerable, true);

x = Object.create(null);
x.p = 0;

p = $(x).lookUp("p").becomeAccessorProperty();

Test.ok( p.isAccessor);
Test.is(p.getter, null);
Test.is(p.setter, null);

x = Object.create(null);
x.p = 0;

p = $(x).lookUp("p").becomeAccessorProperty(null);

Test.ok( p.isAccessor);
Test.is(p.getter, null);
Test.is(p.setter, null);

x = Object.create(null);
x.p = 0;

p = $(x).lookUp("p").becomeAccessorProperty(null, null);

Test.ok( p.isAccessor);
Test.is(p.getter, null);
Test.is(p.setter, null);

x = Object.create(null);
x.p = 0;

p = $(x).lookUp("p").becomeAccessorProperty(undefined);

Test.ok( p.isAccessor);
Test.is(p.getter, null);
Test.is(p.setter, null);

x = Object.create(null);
x.p = 0;

p = $(x).lookUp("p").becomeAccessorProperty(undefined, undefined);

Test.ok( p.isAccessor);
Test.is(p.getter, null);
Test.is(p.setter, null);

x = Object.create(null);
Object.defineProperty(x, "p", { get: undefined, configurable: true });

p = $(x).lookUp("p").becomeDataProperty();

Test.ok(!p.isAccessor);
Test.ok(p.value.sameAs(undefined));

x = Object.create(null);
Object.defineProperty(x, "p", { get: undefined, configurable: true });

p = $(x).lookUp("p").becomeDataProperty(null);

Test.ok(!p.isAccessor);
Test.ok(p.value.sameAs(null));

x = Object.create(null);
Object.defineProperty(x, "p", { get: undefined, configurable: true });

p = $(x).lookUp("p").becomeDataProperty(undefined);

Test.ok(!p.isAccessor);
Test.ok(p.value.sameAs(undefined));
