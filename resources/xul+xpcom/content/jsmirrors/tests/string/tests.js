var Test = exports.Test = new (require('../testUtils'));

var $ = require('jsmirrors').fullLocal;
var x;

x = Object.create({ p: 0 }, { q: { value: 1 } });

Test.ok( $(x).has("p"));
Test.ok(!$(x).has("r"));

Test.ok( $(x).has($("p")));
Test.ok(!$(x).has($("r")));

Test.ok( $(x).hasOwn("q"));
Test.ok(!$(x).hasOwn("p"));

Test.ok( $(x).hasOwn($("q")));
Test.ok(!$(x).hasOwn($("p")));

Test.is($(x).lookUp("p").name, "p");
Test.is($(x).lookUp($("p")).name, "p");

Test.is($(x).lookUpOwn("q").name, "q");
Test.is($(x).lookUpOwn($("q")).name, "q");

$(x).define("r", { value: -1 });

Test.is(x.r, -1);
Test.ok($(x).get("r").sameAs(-1));
Test.ok($(x).get($("r").sameAs(-1)));

x = Object.create(null);

$(x).define($("r"), { value: 0 });

Test.is(x.r, 0);
Test.ok($(x).get("r").sameAs(0));
Test.ok($(x).get($("r").sameAs(0)));

$(x).put("p", 1);

Test.is(x.p, 1);

$(x).put($("p"), -1);

Test.is(x.p, -1);

x.p = function $$() { return x; };

Test.ok($(x).callMethod("p").sameAs($(x)));

x.p = function $$() { return $$; };

Test.ok($(x).callMethod($("p")).sameAs($(x.p)));
