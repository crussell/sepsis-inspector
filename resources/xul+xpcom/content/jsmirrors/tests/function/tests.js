var Test = exports.Test = new (require('../testUtils'));

var $ = require('jsmirrors').introspect;
var f;

f = () => {};
Test.ok($(f).name === "" || $(f).name === "f");
Test.is($(f).source, "() => {}");
Test.ok($(f).lookUp("length").value.sameAs(0));

f = f.bind(null);
Test.is($(f).name, f.name);
Test.ok($(f).name === "" || $(f).name === "bound " || $(f).name === "bound f");
Test.is($(f).source, null);

f = function foo() {};
Test.is($(f).name, "foo");

f = f.bind(null);
Test.is($(f).name, f.name);
Test.ok($(f).name === "foo" || $(f).name === "bound foo");

f = new Function("");
Test.is($(f).name, "anonymous");

f = f.bind(null);
Test.is($(f).name, f.name);
Test.ok($(f).name === "anonymous" || $(f).name === "bound anonymous");

f = () => console.log("foo");
Test.ok($(f).source.indexOf('console.log("foo")') >= 0);

f = () => console.log("this contains the string ') { [native code] }'");
Test.ok($(f).source !== null);

f = () => null;
f.toString = () => { throw new Error("evil attempt"); };
Test.is($(f).source, "() => null");

f.toString = () => { return { evil: true, match: () => null } };
Test.is($(f).source, "() => null");

f = function foo() {};

f.name = function () { throw Error("evil function"); }
Test.ok($(f).name === null || $(f).name === "foo");

f.name = function () { toString: () => { throw Error("evil masquerading"); } };
Test.ok($(f).name === null || $(f).name === "foo");

delete f.name;
Test.ok($(f).name === null || $(f).name === "foo");

if (!Object.prototype.hasOwnProperty.call(f, "name"))
Object.defineProperty(f, "name", { get:() => { throw Error("evil getter"); } });
Test.ok($(f).name === null || $(f).name === "foo");
