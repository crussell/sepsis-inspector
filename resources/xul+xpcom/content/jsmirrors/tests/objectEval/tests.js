var Test = exports.Test = new (require('../testUtils'));

var $ = require('jsmirrors').evaluation;
var x, v;

x = {};
v = {};
x.__defineGetter__("foo", function() { this.bar = true; return v; });

Test.ok(!("bar" in x));

Test.ok($(x).get("foo").sameAs($(v)));
Test.ok(x.bar);

delete x.bar;
x.__defineSetter__("foo", function() { this.bar = true; });

$(x).put("foo", $({}));

Test.ok(x.bar);

delete x.bar;
x.baz = x.__lookupGetter__("foo");

Test.ok($(x).callMethod("baz").sameAs($(v)));
Test.ok(x.bar);

x.baz = (y) => y.p;

Test.ok($(x).callMethod("baz", $({ p: v })).sameAs($(v)));

x = {};

Test.ok($(Object).hasInstance($(x)));
Test.throws(() => $(x).hasInstance($(Object)));
Test.throws(() => $(Object.create(null)).hasInstance($(x)));

v = function() { this.p = null; };
x = new v();

Test.ok($(v).hasInstance($(x)));
Test.throws(() => $(x).hasInstance($(v)));

Test.ok( $(Function).hasInstance($(v)));
Test.ok(!$(v).hasInstance($(Function)));
