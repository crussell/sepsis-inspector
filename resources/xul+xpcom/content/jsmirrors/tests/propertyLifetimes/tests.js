var Test = exports.Test = new (require('../testUtils'));

var $ = require('jsmirrors').mutate;
var x, a, d;

x = Object.create(null);
x.p = 0;

Test.is(x.p, 0);

d = $(x).lookUp("p");
Test.ok(!d.isAccessor);

delete x.p;

Test.throws(() => (d.isAccessor));

x.p = 1;

Test.ok(!d.isAccessor);

Object.__defineGetter__.call(x, "p", () => 2);

Test.throws(() => (d.isAccessor));

a = $(x).lookUp("p");
Test.ok(a.isAccessor);

delete x.p;

Test.throws(() => (a.isAccessor));
Test.throws(() => (d.isAccessor));

Test.ok(!a.sameAs(d));

var aa, dd;

Object.__defineGetter__.call(x, "p", () => 3);

aa = $(x).lookUp("p");

Test.ok( a.sameAs(aa));
Test.ok(!d.sameAs(aa));

delete x.p;
x.p = 4;

dd = $(x).lookUp("p");

Test.ok(!a.sameAs(dd));
Test.ok( d.sameAs(dd));

Test.ok( a.sameAs(aa));
Test.ok(!d.sameAs(aa));

delete x.p;

Test.ok( a.sameAs(aa));
Test.ok(!a.sameAs(dd));
Test.ok(!d.sameAs(aa));
Test.ok( d.sameAs(dd));

// Use of these fields is still permitted:

Test.ok(a.name !== null);
Test.ok(a.owner !== null);
Test.ok(a.base !== null);

Test.ok(d.name !== null);
Test.ok(d.owner !== null);
Test.ok(d.base !== null);

// ... whereas `isAccessor` (see checks above), along with these, is not:

Test.throws(() => a.isEnumerable);
Test.throws(() => a.isConfigurable);
Test.throws(() => a.getter);
Test.throws(() => a.setter);

Test.throws(() => d.isEnumerable);
Test.throws(() => d.isConfigurable);
Test.throws(() => d.isWritable);
Test.throws(() => d.value);

// We want coverage for the setters, too.

// When underlying property has been deleted:

Test.throws(() => a.isEnumerable = true);
Test.throws(() => a.isConfigurable = true);
Test.throws(() => a.getter = (() => null));
Test.throws(() => a.setter = (() => null));

Test.throws(() => d.isEnumerable = true);
Test.throws(() => d.isConfigurable = true);
Test.throws(() => d.isWritable = true);
Test.throws(() => d.value = null);

// When mirror and underlying property are adverse types, part 1 (data):

x = Object.create(null);
d = $(x).define("p", { isConfigurable: true });

a = $(x).lookUp("p").becomeAccessorProperty();

Test.throws(() => d.isEnumerable = true);
Test.throws(() => d.isConfigurable = true);
Test.throws(() => d.isWritable = true);
Test.throws(() => d.value = null);

// When mirror and underlying property are adverse types, part 2 (accessor):

d = d.becomeDataProperty();

Test.throws(() => a.isEnumerable = true);
Test.throws(() => a.isConfigurable = true);
Test.throws(() => a.getter = (() => null));
Test.throws(() => a.setter = (() => null));
