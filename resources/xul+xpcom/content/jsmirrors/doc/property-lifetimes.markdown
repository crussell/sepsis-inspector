Property mirror lifetimes
=========================

*author: Colby Russell*

This reflects some incomplete notes written during the design of the way
property mirrors behave in the face of mutations, whether through the mirror
interfaces or by changes to the underlying model by scripts in the reflection
domain.  This more or less represents how property mirrors actually ended up
being implemented.  In the jsmirrors "newspec" implementation, the fixed
4-tuple in the form name/base/owner/is-accessor? that defines the property
mirror is referred to as the property origin.

* * * * *

Questions: What should happen...

- ... if some property mirror P is created through `lookUp`, and the
  underlying property is deleted?
- ... if the reflectee has changed, so that a new `lookUp` would return a
  mirror with a different owner?  (Should one mirror be `sameAs` the other?)
- ... if the underlying property has changed from an accessor-type to a
  data-type property, or vice versa?

I think the model we should go with is one where `name`, `base`, `owner`, and
`isAccessor` are fixed at creation, and everything else is dynamic.  So the
answer to the second question above is that P1 could continue manipulating the
original underlying owner.

And in the third case I'd expect that an accessor-type property mirror A would
allow a `becomeDataProperty` to give a data-type property mirror D, although
meanwhile A should remain valid (XXX for some sense of "valid", but what does
that mean, exactly?).  To re-`define` the property as accessor-type would
allow A to return to "fully valid" state, even such that **A.sameAs(A')**,
where A' is the return value from `define` or a new lookup, should return
true.  And that also goes for **A.sameAs(A'')**, where A'' is the return value
from **D.becomeAccessorProperty**.

In fact, given the way we define the character of P as being in terms of the
name/base/owner/is-accessor? quad, it's natural that for any other property
mirror Q with the same quad, the two (P and Q) will be considered to have
sameness.

Continuing with the third case, I'd expect that manipulating any of the
attributes of A and D that are specific to accessor-type and data-type
properties respectively should leave the descriptor in a state that is
coherent with the last one used.  I.e., if P is an a-type property but we have
a previous d-type D for the property of the same name, then manipulating D's
`value` should implicitly convert the property with that name to d-type.
Conversely, if P is d-type and we manipulate `getter` or `setter` on some
previous A, then the property should be implicitly converted to a-type.  This
is consistent with how ES's `defineProperty` works.

(The only other approach that I can think would seem natural is e.g. for some
A to become completely invalid at the moment that the property ceases to exist
as a-type.  This requires some sort of synchronous mutation observer, which we
don't have.)

As for the first question, I guess we should take a similar approach.
Assigning any value, getter, or setter for a deleted property mirror should
create a property of that name.

XXX No, no, no.  `isAccessor` should remain live and not be fixed at the time
of creation.  If you have some previous a-type A and (fresh, valid) d-type D,
then to use any of the a-specific fields of A, it should require a call to
`becomeAccessorProperty`, and vice versa for d-type and `becomeDataProperty`,
respectively.  This caveat aside, though, everything else sounds good.  There
are some specifics we need to lay out:

* Calling `sameAs` shall remain valid even for stale mirrors, with behavior
  - A and A' -> true
  - D and D' -> true
  - A and D -> false
* Accessing fields correspending to any component of the
  name/base/owner/is-accessor? quad shall remain valid even for stale mirrors
* Accessing any of the a-type or d-type specific fields of a stale mirror
  should throw
* Accessing the `isAccessor` field of a stale mirror should throw
* Accessing the `isConfigurable` or `isEnumerable` fields of a stale mirror
  should throw?
* Expose some `isZombie` property on a-type and d-type mirrors to check
  whether a mirror is stale?  (We need a utility method in the mirrors impl in
  any case) XXX Just try to access `isAccessor`.  If it throws, then it's
  stale.  Consumers can wrap it in a try/catch.

... where 'stale' is defined as any a-type mirror for a property that has since been made d-type or deleted, or any d-type mirror that has since been made a-type or deleted.

Since everything outside the name/base/owner/is-accessor? 4-tuple is live (and
even `isAccessor` itself is somewhat live), then we may want a way to "embalm"
the property so that *everything* is fixed.  One use case is doing a property
lookup and wanting the value to remain stable, regardless of anyone who has
come along and deleted or changed it since the mirror was first created.  This
should arguably be the default, even?
