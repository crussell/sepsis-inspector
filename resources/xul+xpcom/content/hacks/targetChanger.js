/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Legacy helper for dispatching viewer-level "targetChanged" notifications.
 *
 * Prior to sepsis-inspector 0.4, viewer objects were expected to participate
 * in the `NotificationManager`-style observe/notify system to notify the
 * Inspector of changes to the viewer's target.  For version 0.4 and up,
 * panels have a `changeTarget` method that dispatches a panel-level
 * "targetChanged" notification.  This removes the requirement that viewers be
 * observable.
 *
 * Before this, viewers could acquire a `TargetChanger` that would take care
 * of dispatching the notification on their behalf.  In version 0.4 up until
 * it's removed, `TargetChanger` provides backwards compatibility for viewers
 * that were previously using it.  It works by mapping the target changer's
 * `changeTarget` calls to calls to the appropriate panel's `changeTarget`.
 *
 * Update path: When it's time to drop support for pre-0.4 viewers, then
 * remove use of `ReceivePanelHack`, delete this file along with
 * receivePanelHack.js and targetChanger.jsm, and make sure to delete the
 * override mapping in chrome.manifest that provides backwards compatibility
 * with utils/targetChanger.jsm.
 *
 * @deprecated
 * @see hacks/targetChanger.jsm
 */

module.singleton = true;

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.TargetChanger = TargetChanger;

const viewerToChangerMap = new WeakMap();

//////////////////////////////////////////////////////////////////////////////

/**
 * NB: `TargetChanger` is deprecated.
 *
 * @param aViewer
 *        The viewer on whose behalf we will be acting.
 * @deprecated
 * @see PanelManager.prototype._onViewerLoad
 */
function TargetChanger(aViewer)
{
  viewerToChangerMap.set(aViewer, this);

  this.viewer = aViewer;
}

{
  let proto = TargetChanger.prototype;
  proto.panel = null;
  proto.viewer = null;

  proto.getTargetChangerForViewer = TargetChanger.getTargetChangerForViewer =
    function TC_GetTargetChangerForViewer(aViewer)
  {
    return viewerToChangerMap.get(aViewer) || null;
  };

  /**
   * @param a_x_Target
   *        NB: `a_x_Target` is untrusted.
   */
  proto.changeTarget = function TC_ChangeTarget(a_x_Target)
  {
    // assert(this.panel != null);
    this.panel.changeTarget(a_x_Target);
  };
}
