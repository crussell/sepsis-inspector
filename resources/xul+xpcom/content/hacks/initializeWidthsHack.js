/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.InitializeWidthsHack = InitializeWidthsHack;

//////////////////////////////////////////////////////////////////////////////

function InitializeWidthsHack(aRootPanel, aPanelMaker)
{
  this.mRootPanel = aRootPanel;
  this.mPanelMaker = aPanelMaker;

  let win = this.mRootPanel.ownerDocument.defaultView;
  win.addEventListener("MozAfterPaint", this, true);
}

{
  let proto = InitializeWidthsHack.prototype;
  proto.mPanelMaker = null;
  proto.mRootPanel = null;

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIDOMEventListener

  proto.handleEvent = function IA_HandleEvent(aEvent)
  {
    switch (aEvent.type) {
      case "MozAfterPaint":
        this._onMozAfterPaint(aEvent);
      break;
      default:
        throw new Error("wat.  Event: " + aEvent.type);
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  proto._onMozAfterPaint = function IA_OnMozAfterPaint(aEvent)
  {
    try {
      this.mPanelMaker.setWidths(this.mRootPanel);
    }
    finally {
      let win = this.mRootPanel.ownerDocument.defaultView;
      win.removeEventListener("MozAfterPaint", this, true);
    }
  };
}
