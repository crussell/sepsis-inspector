/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Used to style, e.g., a tree's tooltip with a fixed width font.
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.FixedFontTooltipHack = FixedFontTooltipHack;

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aEventTarget
 *        The event target to attach the listeners to.  It should, of course,
 *        be on the tooltip's propagation path for this to have any effect at
 *        all.  Since this is used to style a XUL document's default tooltip,
 *        getting at the actual tooltip node is difficult.  The document's
 *        root element (`document.documentElement`) is a good choice here.
 * @param aTooltipNode
 *        The node whose tooltip should be styled with a fixed width font.
 */
function FixedFontTooltipHack(aEventTarget, aTooltipNode)
{
  this.mTooltipNode = aTooltipNode;

  aEventTarget.addEventListener("popupshowing", this, false);
  aEventTarget.addEventListener("popuphiding", this, false);
}

{
  let proto = FixedFontTooltipHack.prototype;
  proto.mTooltipNode = null;

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIDOMEventListener

  proto.handleEvent = function FFTH_HandleEvent(aEvent)
  {
    switch (aEvent.type) {
      case "popupshowing":
        this._onPopupShowing(aEvent);
      break;
      case "popuphiding":
        this._onPopupHiding(aEvent);
      break;
      default:
        throw new Error("wat.  Event: " + aEvent.type);
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  proto._onPopupShowing = function FFTH_OnPopupShowing(aEvent)
  {
    if (!this._isRelevantEvent(aEvent)) {
      return;
    }

    aEvent.originalTarget.style.setProperty("font-family", "-moz-fixed", "");
  };

  proto._onPopupHiding = function FFTH_OnPopupHiding(aEvent)
  {
    if (!this._isRelevantEvent(aEvent)) {
      return;
    }

    aEvent.originalTarget.style.removeProperty("font-family");
  };

  proto._isRelevantEvent = function FFTH_IsRelevantEvent(aEvent)
  {
    return aEvent.originalTarget.nodeName == "tooltip" &&
           this.mTooltipNode.ownerDocument.tooltipNode == this.mTooltipNode;
  };
}
