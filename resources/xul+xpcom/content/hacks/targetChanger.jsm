/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Legacy helper for dispatching viewer-level "targetChanged" notifications.
 *
 * NB: chrome.manifest uses an `override` directive to map this file to a
 * different URI a runtime.
 *
 * Update path: New viewers should not use `TargetChanger`, and old viewers
 * should switch to using the panel's `changeTarget` method.  `TargetChanger`
 * will be removed in future releases.
 *
 * @see hacks/targetChanger.js
 * @deprecated
 */

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

const EXPORTED_SYMBOLS = [ "TargetChanger" ];

Components.utils.import("chrome://sepsis-inspector/content/require.js");

var { TargetChanger } = require("hacks/targetChanger");

//////////////////////////////////////////////////////////////////////////////
