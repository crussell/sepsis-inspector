/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Compatibility shim for external viewers that used ViewerManager.
 *
 * NB: chrome.manifest uses an `override` directive to map this file to a
 * different URI a runtime.
 *
 * Update path: Remove this file when we drop support for pre-0.4 viewers.
 */

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

Components.utils.import("chrome://sepsis-inspector/content/require.js");

var { TheViewerRegistry } = require("registration/theViewerRegistry");

var { SynthBloc } = require("synthetic/synthBloc");

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

const EXPORTED_SYMBOLS = [ "ViewerManager" ];

__defineGetter__("ViewerManager", function VM_ViewerManager_Get()
{
  Components.utils.reportError(
    "ViewerManager is deprecated.  Use TheViewerRegistry instead."
  );

  // The interface changed in sepsis-inspector 0.6 to require a second
  // `SynthBloc` param to `isInspectable`.  So we emulate its old behavior,
  // too.
  //
  // NB: This hack will only work for pre-0.4 viewers, which means viewers
  // written for sepsis-inspector 0.5 will still be broken.  I'm pretty sure,
  // however, that I'm the only person who's ever even installed an external
  // viewer (the sepsis JS console), much less written one.  So viewers
  // targeting 0.5 will remain broken, I guess, because I'm pretty sure those
  // viewers are only hypothetical. --crussell
  return Object.create(TheViewerRegistry, {
    isInspectable: {
      // NB: `a_x_Val` is untrusted.
      value: function VMH_IsInspectable(a_x_Val) {
        // XXX This is not an officially supported way to call `SynthBloc`.
        let bloc = new SynthBloc();
        return TheViewerRegistry.isInspectable(bloc.mirror(a_x_Val), bloc);
      }
    }
  });
});
