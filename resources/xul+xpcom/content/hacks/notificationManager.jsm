/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Legacy file to import agent impl of inspector's observe/notify system.
 *
 * NB: chrome.manifest uses an `override` directive to map this file to a
 * different URI a runtime.
 *
 * Update path: Make sure all internal sepsis-inspector code uses
 *
 *   require("utils/notificationManager")
 *
 * ... and delete this file when dropping support for pre-0.6 viewers.
 *
 * @deprecated
 * @see utils/notificationManager.js
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

const EXPORTED_SYMBOLS = [ "NotificationManager" ];

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

Components.utils.import("chrome://sepsis-inspector/content/require.js");

var { NotificationManager } = require("utils/notificationManager");

//////////////////////////////////////////////////////////////////////////////
