/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Adapt old-style viewer info file format for use with `ViewerInfo`.
 *
 * The legacy info files mixed what is now known as the info metadata with the
 * info script; essentially, the metadata was embedded in the info script.
 *
 * For now, we continue to support the old-style viewer info.  What we do is
 * first try to create a `ViewerInfo` based on what should be the "JSON's
 * URI", and if it that fails, it's not JSON metadata at all, but probably the
 * old-style viewer info, instead.  If the URI ends in ".js", we try to
 * execute (XXX parse first?) it and check for `viewerURI` and `filter`
 * symbols.  If that goes okay, we'll assume it's a legacy info file, and fix
 * up what we find in there.
 *
 * MODERNIZATION PATH:
 *   To drop support for legacy info files and get rid of this code, replace
 *   all references to `LegacyInfoFallback.createViewerInfo` with direct uses
 *   of the `ViewerInfo` constructor.  Remove everything dealing with
 *   `fixedUpFromLegacyInfo`.
 */

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { Module } = require("loader/module");
var { ViewerInfo } = require("registration/viewerInfo");

var { SubScriptLoader } = require("xpcom/subScriptLoader");

//////////////////////////////////////////////////////////////////////////////

exports.LegacyInfoFallback = new (function LegacyInfoFallback() { })();

{
  let $proto = exports.LegacyInfoFallback.constructor.prototype;

  $proto.createViewerInfo = function LIF_CreateViewerInfo(aUID, aURI)
  {
    // The new format is mostly the same as the "config" data in the legacy info
    // files.  The only things we'll really have to do is:
    //
    //   - Make sure the viewer document URI is set as the `viewerURI` property;
    //     in the legacy files `viewerURI` was a global, separate from the
    //     (optional) `config` JSON object.
    //   - Go ahead and treat the legacy file's URI as the URI to what's now
    //     known in the new terminology as the "info script", by assigning it to
    //     the metadata's `script`.
    //
    // So what we'll do is, if we can't successfuly construct a `ViewerInfo`
    // object to completion, we'll pick things up where it left off, fix those
    // things up, and return the info with no one having to worry about anything
    // else.
    let partialInfo, parseError = null;
    try {
      return new ViewerInfo(aUID, aURI);
    }
    catch (ex if ex instanceof ViewerInfo.MetadataParseError) {
      if (aURI.substr(-3) == ".js") {
        parseError = ex;
        partialInfo = ex.info;
      }
      else {
        throw parseError;
      }
    }

    let scope = Object.create(null);
    try {
      SubScriptLoader.loadSubScript(aURI, scope);
    }
    catch (ex) {
      // Welp, we tried.  Just let the original exception escape.
      throw parseError;
    }

    if (!("viewerURI" in scope && "filter" in scope)) {
      // It's not the legacy format after all.
      throw parseError;
    }

    let fixedUpMetadata = Object.create(null);
    if ("config" in scope) {
      try {
        fixedUpMetadata = JSON.parse(JSON.stringify(scope.config));
      }
      catch (ex) {
        // Bah.  If it's not even valid JSON, we won't even bother.
        throw parseError;
      }
    }

    fixedUpMetadata.fixedUpFromLegacyInfo = true;
    fixedUpMetadata.viewerURI = scope.viewerURI;

    partialInfo.metadata = partialInfo.normalizeInfo(fixedUpMetadata);
    partialInfo.scriptModule = new Module(aURI);
    partialInfo.scriptModule.exports = scope;

    return partialInfo;
  }
}
