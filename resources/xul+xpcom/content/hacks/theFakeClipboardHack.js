/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Because you can't copy non-text objects to the XPCOM clipboard.
 *
 * Apparently.
 *
 * Warning: this is all very derpy, although this being hacks/
 * that should go without saying.
 *
 * TODO:
 * - We can notify about clipboard changes when we have ownership and it gets
 *   transferred to somebody else, or when we have it and change the clipboard
 *   contents, or when somebody else has it at first and then we change the
 *   clipboard contents,  But we can't do anything if somebody who isn't us
 *   makes changes while we're not the owner.
 *
 * @see hacks/theFakeClipboardHack.js
 * @singleton
 */

module.singleton = true;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");

var { NotificationManager } = require("utils/notificationManager");

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

const Cc = Components.classes;
const Ci = Components.interfaces;

exports.TheFakeClipboardHack = new FakeClipboardHack();

//////////////////////////////////////////////////////////////////////////////

function FakeClipboardHack()
{
  this.mNotificationManager = new NotificationManager(this);
  this.mContents = new Map();
  this.mClipboard =
    Cc["@mozilla.org/widget/clipboard;1"].getService(Ci.nsIClipboard);
}

{
  let proto = FakeClipboardHack.prototype;

  proto.QueryInterface = XPCOMUtils.generateQI(
    [ "nsITimerCallback", "nsIFlavorDataProvider" ]
  );

  proto.STRING_FLAVOR = FakeClipboardHack.STRING_FLAVOR = "text/unicode";

  proto.mDoSynchronousNotify = null;
  proto.mNotificationManager = null;
  proto.mContents = null;
  proto.mClipboard = null;
  proto.mHaveOwnership = null;

  /**
   * @param aContent
   * @param aFlavor [optional]
   *        Quasi-MIME type.  Required if the given content is a non-string.
   */
  proto.copyContents = function FC_CopyContents(aContent, aFlavor)
  {
    let flavor = aFlavor;
    if (!flavor) {
      if (typeof(aContent) != "string") {
        throw new Error(
          "Flavor must be specified for non-string clipboard contents"
        );
      }

      flavor = this.STRING_FLAVOR;
    }

    this.mDoSynchronousNotify = true;
    this.clearContents();
    // We've prevented the async notification from being set up.  Now reset
    // the flag for next time, otherwise `LosingOwnership` won't fire an async
    // one if someone else becomes responsible for the clipboard contents.
    //
    // XXX If someone else *does* take control of the clipboard, we'll receive
    // notification and be able to pass it on to the Inspector to update the
    // clipboard commands as appropriate, but *not* if/when there are any
    // subsequent third-party changes.  I hate the clipboard APIs. --crussell
    this.mDoSynchronousNotify = false;

    let transferable;
    if (flavor == this.STRING_FLAVOR) {
      let supportsString = Cc["@mozilla.org/supports-string;1"].
        createInstance(Ci.nsISupportsString);
      supportsString.data = aContent;
      transferable = this._createTransferable(this.STRING_FLAVOR);
      transferable.setTransferData(
        flavor, supportsString, aContent.length * 2
      );
    }
    else {
      this.mContents.set(aFlavor, aContent);

      // We act as a data provider.  See `getFlavorData`.
      transferable = this._createTransferable(aFlavor);
      transferable.setTransferData(aFlavor, this, 0);
    }

    this.mClipboard.setData(
      transferable, this, Ci.nsIClipboard.kGlobalClipboard
    );

    this.mNotificationManager.notify("clipboardChange");
  };

  proto.hasFlavor = function FC_HasFlavor(aFlavor)
  {
    return this._nativeClipboardHasFlavor(aFlavor);
  };

  proto.getContents = function FC_GetContents(aFlavor)
  {
    // Shortcut so the caller doesn't have to do QI on non-string content.
    if (this.mContents.has(aFlavor)) {
      return this.mContents.get(aFlavor);
    }

    let transferable = this._createTransferable(aFlavor);
    this.mClipboard.getData(transferable, Ci.nsIClipboard.kGlobalClipboard);

    let out = {};
    transferable.getTransferData(aFlavor, out, {});

    // If it's a string, go ahead and do the conversion.
    if (aFlavor == this.STRING_FLAVOR) {
      return out.value.QueryInterface(Ci.nsISupportsString).data;
    }

    // Must be QI'd by caller.
    return out.value;
  };

  proto.clearContents = function FC_ClearContents()
  {
    this.mClipboard.emptyClipboard(Ci.nsIClipboard.kGlobalClipboard);
    this.mContents = new Map();
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIFlavorDataProvider

  proto.getFlavorData =
    function FC_GetFlavorData(aTransferable, aFlavor, aData, aLength)
  {
    // XXX The `nsTransferable::GetTransferData` implementation won't accept a
    // length of 0.
    aLength.value = 1;
    aData.value = this.mContents.get(aFlavor);
    return;
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIClipboardOwner

  proto.LosingOwnership = function FC_LosingOwnership()
  {
    // XXX Stupid hack.  We don't want to dispatch the "clipboardChange"
    // notification right now, because we want the new clipboard data, if any,
    // to be available to observers handling the notification.  Something like
    // this is necessary, because we actually get called before the new data
    // makes it onto the clipboard.
    //
    // If we're being called because we're adding new content to the
    // clipboard ourselves, we just dispatch it synchronously above.
    if (!this.mDoSynchronousNotify) {
      this.mPendingChangeTimer = Cc["@mozilla.org/timer;1"].
        createInstance(Ci.nsITimer);
      this.mPendingChangeTimer.initWithCallback(
        this, 50, Ci.nsITimer.TYPE_ONE_SHOT
      );
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsITimerCallback

  /**
   * Called as a result of the timer we set up in `LosingOwnership`.
   *
   * @param aTimer
   *        The timer we're being notified about.
   * @see LosingOwnership
   */
  proto.notify = function FC_Notify(aTimer)
  {
    if (this.mPendingChangeTimer == aTimer) {
      this.mNotificationManager.notify("clipboardChange");
      this.mPendingChangeTimer = null;
    }
  };

  ////////////////////////////////////////////////////////////////////////////

  proto._nativeClipboardHasFlavor =
    function FC_NativeClipboardHasFlavor(aFlavor)
  {
    let gcb = Ci.nsIClipboard.kGlobalClipboard;
    return this.mClipboard.hasDataMatchingFlavors([ aFlavor ], 1, gcb);
  };

  /**
   * @param aFlavor [optional]
   *        A string naming the flavor of the data to be used with the
   *        transferable.
   */
  proto._createTransferable = function APH_CreateTransferable(aFlavor)
  {
    let transferable = Cc["@mozilla.org/widget/transferable;1"].
      createInstance(Ci.nsITransferable);
    transferable.init(null);

    if (aFlavor) {
      transferable.addDataFlavor(aFlavor);
    }

    return transferable;
  };
}
