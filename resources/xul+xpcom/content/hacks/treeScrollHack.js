/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Hack to enable resizing cols to big widths, for horizontal scrolling.
 *
 * Since there are both a treecol and a treecols element and both an
 * nsITreeColumn and nsITreeColumns interface, to reduce confusion, here's
 * some conventions for the terminology used throughout:
 *  treecols
 *    A XUL treecols element
 *  col or cols
 *    one or multiple XUL treecol elements
 *  column or columns
 *    one or multiple XUL nsITreeColumn implementations.  NB: "columns" is the
 *    plural; it *does not* refer to an nsITreeColumns implementation.
 *  user-specified width
 *    The width of the col, as indicated by where the user releases the
 *    splitter after starting a drag on it.  We treat this as the minimum
 *    possible width that the col should be allowed to shrink to before we
 *    start forcing overflow to allow for the tree to scroll horizontally.
 *
 * A word on widths:
 * The width attributes on cols are treated as the *preferred* width.
 * Ordinarily, if the sum of the widths of (flex) cols is different from the
 * actual tree "viewport" width, the actual width will differ from the value
 * of a col's width attribute.
 *
 * The value of the minwidth attribute, however, is a hard limit.  If the sum
 * of all visible cols' minwidths is greater than the width of the tree, the
 * vanilla binding automatically allows horizontal scrolling.  So we exploit
 * that, and that's how we achieve what we're going for here.
 *
 * How it works:
 * minwidth will be kept to the width specified by the user.  This can be done
 * by checking the value of the width attribute during resize.  At any given
 * time outside of a resize, the width and the minwidth will be the same,
 * except in some cases with the last column: if the sum of the user-specified
 * widths is less than the size of the tree itself, the width value of the
 * last col is padded with the difference, in order to completely fill the
 * tree.
 *
 * Set splitters to resizeafter="grow".
 *
 * Initialize col widths:
 *   Store and remove flex and set width/minwidth.
 *
 * Resizing:
 *   Two cases: the col being resized is the last one, or it's not.
 *   In both cases, need to remove minwidth so it can be sized smaller than
 *   its current width and store flex.
 *
 *   So store and remove flex and remove minwidth on all columns.
 *
 *   If non-last col:
 *     Set last col to flex, to fill dead space.  The vanilla tree binding
 *     will take care of the rest.
 *   If last col:
 *     Calculate the threshold and add the width listener.
 *     Width listener will make sure the visual width never falls below the
 *     threshold by setting flex if the width attribute value parses to less.
 *   
 * Finishing up (i.e., user moused up from a drag):
 *   Set the minwidth to the user-specified width.
 *     If drag state is still active, wait for it to end before continuing.
 *   Restore all cols' flex attributes so resizing the window distributes
 *   space correctly.
 *
 * This would probably be a lot easier to follow with a state diagram.  I
 * don't have one, and even if I did, it couldn't be embedded here.  But if
 * you're having trouble following, I recommend sketching one out.  In fact,
 * this whole thing is probably better written as states an transitions to
 * begin with... XXX --crussell
 *
 * Assumptions:
 *   - all cols are already flex
 *   - when filling in the tree to take up extra space, the last col is the
 *     one it should get distributed to
 *   - all "tree-splitter"-class splitters are, in fact, tree splitters
 *   - col ordinals may change (due to rearranging cols), but splitter ordinals
 *     won't
 *   - on that note, cols and splitters are set up to begin with in an
 *     interleaved way, i.e., splitters separate treecol nodes when traversed
 *     in DOM order
 *
 * TODO
 *   - Double-clicking the treecolpicker should automatically size the cols.
 *   - Smart expansion.  Basically, if there is no visible col to the right of
 *     the primary, and there's no user-specified width on it, the col should
 *     automatically grow to accommodate the new rows if their contents would
 *     ordinarily be elided.  We should maybe even do this without the "no col
 *     to the right" constraint.
 * XXX RTL?
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.TreeScrollHack = TreeScrollHack;

var { FILTER_ACCEPT, FILTER_REJECT } = Components.interfaces.nsIDOMNodeFilter;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { DeepIterator } = require("utils/deepIterator");

//////////////////////////////////////////////////////////////////////////////

function TreeScrollHack(aTree)
{
  this.mTree = aTree;
  this.mTreecols = aTree.querySelector("treecols");
  // assert(this.mTreecols);

  // Get the tree's colpicker.  We'll need its width in order to correctly
  // compute the space available to cols.
  //
  // It would be nice if `querySelector` traversed the shadow tree...  We'll
  // use an iterator instead.
  this.mColpicker = (new DeepIterator(this.mTreecols, {
    acceptNode: function TSH_constructor_filter_AcceptNode(aNode) {
      return (aNode.localName == "treecolpicker") ?
        FILTER_ACCEPT :
        FILTER_REJECT;
    }
  })).nextNode();

  let MutationObserver = aTree.ownerDocument.defaultView.MutationObserver;
  this.mColWidthObserver =
    new MutationObserver(this._onWidthChanges.bind(this));

  this.mSplitterMap = new WeakMap();

  this._prepSplitters();

  // NB: We make it capturing so we can set the necessary stuff in motion
  // before things get processed by the native splitter frame code.
  this.mTreecols.addEventListener("mousedown", this, true);
  this.mTree.addEventListener("DOMMouseScroll", this, false);
  this.mTree.addEventListener("keypress", this, false);
}

{
  let proto = TreeScrollHack.prototype;
  proto.mColpicker = null;
  proto.mColWidthObserver = null;
  proto.mLastCol = null;
  proto.mResizingCol = null;
  proto.mSplitterMap = null;
  proto.mThreshold = null;
  proto.mTree = null;
  proto.mTreecols = null;

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIDOMEventListener

  proto.handleEvent = function TSH_HandleEvent(aEvent)
  {
    switch (aEvent.type) {
      case "mousedown":
        this._onMouseDown(aEvent.target);
      break;
      case "mouseup":
        this._onMouseUp(aEvent.target);
      break;
      case "command":
        this._onSplitterCommand(aEvent.target);
      break;
      case "DOMMouseScroll":
        this._onScroll(aEvent);
      break;
      case "keypress":
        this._onKeyPress(aEvent);
      break;
      default:
        throw new Error("wat.  event: " + aEvent.type);
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  /**
   * Make sure the tree splitters are in a workable state.  This means there
   * must be one that follows the last splitter, and each one is
   * `resizeafter="grow"`.
   */
  proto._prepSplitters = function TSH_PrepSplitters()
  {
    let splitters =
      this.mTreecols.querySelectorAll("splitter[class='tree-splitter']");

    for (let i = 0, n = splitters.length; i < n; ++i) {
      splitters[i].setAttribute("resizeafter", "grow");
      // The map's values aren't important.  I'm just wary of using `Set`,
      // since its future doesn't seem as stable as `WeakMap`'s. --crussell
      this.mSplitterMap.set(splitters[i], true);
    }

    // Add a final splitter if there's not one there.
    let cols = this.mTreecols.querySelectorAll("treecol");
    // NB: Don't use `_getLastNonHiddenCol`.  It respects XUL ordinal
    // attributes.  We just want the last col in DOM order, and it's not
    // unusual to not put a splitter following it.
    let finalCol = cols[cols.length - 1];

    if (!this._isSplitter(finalCol.nextSibling)) {
      // Offset by a unit once to account for the col that should follow the
      // last splitter that we found, and again to get the next unused ordinal
      // in the series.  That makes two.
      let ordinal =
        parseInt(splitters[splitters.length - 1].getAttribute("ordinal")) + 2;
      // assert(
      //  this.mTreecols.querySelector("[ordinal='" + ordinal + "']") == null
      // );

      let splitter = finalCol.ownerDocument.createElementNS(
        finalCol.namespaceURI, "splitter"
      );
      splitter.setAttribute("ordinal", ordinal);
      splitter.setAttribute("resizeafter", "grow");
      splitter.classList.add("tree-splitter");
      // assert(finalCol.parentNode == this.mTreecols);
      // assert(finalCol.nextSibling === null);
      this.mTreecols.appendChild(splitter);

      this.mSplitterMap.set(splitter, true);
    }
  };

  /**
   * Determine whether the given node is a tree splitter.  The splitter must
   * have been present during the `_prepSplitter` call at hack initialization.
   *
   * @param aNode
   *        The DOM node to test.
   * @return
   *        True iff the node is a managed splitter.
   */
  proto._isSplitter = function TSH_IsSplitter(aNode)
  {
    return !!aNode && this.mSplitterMap.has(aNode);
  };
  
  /**
   * Make sure each column has a width and minwidth attribute reflecting the
   * actual (displayed) column size, and return the total widths of all cols,
   * except for the one passed as the first argument.
   *
   * @param aCol
   *        During width calculation, omit this col from the total.
   * @return
   *        Total width of all visible columns except for the provided one.
   */
  proto._lockDownWidths = function TSH_LockDownWidths(aCol)
  {
    let infos = [];
    let otherWidths = 0;

    let current = this.mTree.columns.getFirstColumn();
    // We have to loop twice, once for gathering measurements and again for
    // setting values.  Otherwise, setting the col width in the same loop will
    // affect the value measured in the next iteration.
    while (current) {
      infos.push({ col: current.element, width: current.width });
      if (current.element != aCol) {
        otherWidths += current.width;
      }

      current = current.getNext();
    }

    for (let i = 0, n = infos.length; i < n; ++i) {
      infos[i].col.setAttribute("width", infos[i].width);
      infos[i].col.setAttribute("minwidth", infos[i].width);
    }

    return otherWidths;
  };

  /**
   * Event listener for the treecols's "mousedown" event.  Only significant
   * after a mousedown on a splitter.
   *
   * @param aTarget
   *        The event target.  Nothing happens when called with a non-splitter
   *        target.
   */
  proto._onMouseDown = function TSH_OnMouseDown(aTarget)
  {
    if (!this._isSplitter(aTarget)) {
      return;
    }

    this.mTreecols.addEventListener("mouseup", this, false);

    // We need to make sure any dead space created during resizing gets
    // completely distributed to the col of our choosing while dragging.
    // (That means the last visible one.)
    this._stashFlex();

    // Need to remove minwidth on this col so it can be sized smaller than its
    // current width.  It gets reinstated with the new width when the drag is
    // over.
    this.mResizingCol = this._getResizingCol(aTarget);
    let otherWidths = this._lockDownWidths(this.mResizingCol);
    this.mResizingCol.removeAttribute("minwidth");

    this.mLastCol = this._getLastNonHiddenCol();
    if (this.mResizingCol == this.mLastCol) {
      // Calculate the threshold.  The col's visible width should never fall
      // below this, or negative space will show in the treecols element.
      this.mThreshold = this.mTreecols.boxObject.width -
                        this.mColpicker.boxObject.width - otherWidths;
      this.mColWidthObserver.observe(this.mLastCol,
                                     { attributes: true,
                                       attributeFilter: [ "width" ] });
    }
    else {
      // Getting the dead space distributed to the last col when it's not the
      // one being resized is a lot easier.  Just make sure it's the only
      // flexible col, and layout's flex box for XUL takes care of it for us.
      // Recall that other cols' "flex" attributes have already been removed
      // in the earlier `_stashFlex` call.
      this.mLastCol.setAttribute("flex", "1");
    }
  };

  /**
   * Mutation observer callback for splitter "width" attribute changes.  Only
   * active between a mousedown and a mouseup on a splitter.
   *
   * @param aRecords
   *        Array of mutation observer records.
   */
  proto._onWidthChanges = function TSH_OnWidthChanges(aRecords)
  {
    // assert(aRecords[aRecords.length - 1].attributeName == "width");
    let record = aRecords[aRecords.length - 1];
    let newWidth = parseInt(this.mLastCol.width);
    if (newWidth < this.mThreshold) {
      this.mLastCol.setAttribute("flex", "1");
    }
    else {
      this.mLastCol.removeAttribute("flex");
    }
  };

  /**
   * Event handler for the treecols's "mouseup" event.  Only active after a
   * mousedown on a splitter.
   *
   * @param aTarget
   *        The event target.  This should be the splitter.
   */
  proto._onMouseUp = function TSH_OnMouseUp(aTarget)
  {
    if (!this._isSplitter(aTarget)) {
      throw new Error("wtf.  target: " + aTarget.nodeName);
    }

    this.mTreecols.removeEventListener("mouseup", this, false);
    this.mColWidthObserver.disconnect();

    // If the splitter's state is still dragging, we can't set the final
    // widths now, because the splitter frame code hasn't completely processed
    // the mouseup and will override anything we do here with its own
    // width-setting.  In that case, we defer the width finalization until we
    // get the command event that it fires when it's done.
    if (aTarget.getAttribute("state") == "dragging") {
      aTarget.addEventListener("command", this, false);
    }
    else {
      this._finalizeColWidths();
    }
  };

  /**
   * Event handler for a splitter's "command" event.  Used to defer the
   * `_finalizeColWidths` call until the splitter frame code has made its last
   * change to the col width.
   *
   * @param aSplitter
   *        The tree splitter that fired the "command" event.
   * @see _onMouseUp
   */
  proto._onSplitterCommand = function TSH_OnSplitterCommand(aSplitter)
  {
    // assert(this._isSplitter(aSplitter));
    // assert(aSplitter.getAttribute("state") == "");
    aSplitter.removeEventListener("command", this, false);
    this._finalizeColWidths();
  };

  /**
   * Set a hard lower limit on the resized col's width, where that limit
   * reflects where the user dragged the splitter to.  Also make sure the last
   * col takes up any extra space, in the event that the sum of all col widths
   * falls below the threshold.  Restore the stashed flex values so resizing
   * the window to a larger size distributes width correctly.
   */
  proto._finalizeColWidths = function TSH_FinalizeColWidths()
  {
    // Set the minwidth to the user-specified width.
    this.mResizingCol.setAttribute("minwidth", this.mResizingCol.width);

    if (this.mLastCol.width < this.mThreshold) {
      this.mLastCol.width = this.mThreshold;
    }

    this._restoreFlex();
  };

  /**
   * @param aSplitter
   *        A tree splitter.
   * @return
   *        The col element `aSplitter` resizes.
   */
  proto._getResizingCol = function TSH_GetResizingCol(aSplitter)
  {
    let ordinal = parseInt(aSplitter.getAttribute("ordinal")) - 1;
    return this.mTreecols.querySelector("treecol[ordinal='" + ordinal + "']");
  };

  /**
   * Get the last, visible column, while respecting XUL "ordinal" order.
   *
   * NB: "Visible" in this context just means "isn't hidden".  Cols which
   * aren't hidden but are scrolled out of view are still "visible".
   *
   * @return
   *        A col which is guaranteed not to be hidden where all its siblings
   *        that follow in XUL "ordinal" order, if present, are guaranteed to
   *        be hidden.
   */
  proto._getLastNonHiddenCol = function TSH_GetLastNonHiddenCol()
  {
    let current = this.mTree.columns.getLastColumn();
    while (current) {
      if (!current.element.hidden) {
        return current.element;
      }
      current = current.getPrevious();
    }

    return null;
  };

  /**
   * Store the value of each col's "flex" attribute as "oldflex" and remove
   * its "flex" attribute.
   *
   * @see _restoreFlex
   */
  proto._stashFlex = function TSH_StashFlex()
  {
    let current = this.mTree.columns.getFirstColumn();
    while (current) {
      let col = current.element;
      col.setAttribute("oldflex", col.getAttribute("flex"));
      col.removeAttribute("flex");
      current = current.getNext();
    }
  };

  /**
   * Restore each col's "flex" attribute, reading its "oldflex" attribute.
   *
   * @see _stashFlex
   */
  proto._restoreFlex = function TSH_RestoreFlex()
  {
    let current = this.mTree.columns.getFirstColumn();
    while (current) {
      let col = current.element;
      col.setAttribute("flex", col.getAttribute("oldflex"));
      col.removeAttribute("oldflex");
      current = current.getNext();
    }
  };

  proto._onScroll = function TSH_OnScroll(aEvent)
  {
    if (this.mTree.editingColumn || aEvent.axis != aEvent.HORIZONTAL_AXIS) {
      return;
    }

    this._horizontalScrollBy(aEvent.detail);

    aEvent.preventDefault();
  };

  proto._horizontalScrollBy = function TSH_HorizontalScrollBy(aRows)
  {
    let newPosition = this.mTree.treeBoxObject.horizontalPosition +
                      aRows * this.mTree.treeBoxObject.rowHeight;
    if (newPosition < 0) {
      newPosition = 0;
    }
    this.mTree.treeBoxObject.scrollToHorizontalPosition(newPosition);
  };

  proto._horizontalScrollByPages =
    function TSH_HorizontalScrollByPages(aPages)
  {
    this._horizontalScrollBy(
      this.mTree.scrollWidth / this.mTree.treeBoxObject.rowHeight * aPages
    );
  };

  proto._onKeyPress = function TSH_OnKeyPress(aEvent)
  {
    if (this.mTree.editingColumn) {
      return;
    }

    let units = 1;
    let pages = 1;
    switch (aEvent.keyCode) {
      case aEvent.DOM_VK_PAGE_UP:
        pages = -1;
      // Fall through.
      case aEvent.DOM_VK_PAGE_DOWN:
        if (!aEvent.altKey) {
          return;
        }
        this._horizontalScrollByPages(pages);
      break;
      case aEvent.DOM_VK_LEFT:
        units = -1;
      // Fall through.
      case aEvent.DOM_VK_RIGHT:
        if (!aEvent.ctrlKey) {
          return;
        }
        this._horizontalScrollBy(units);
      break;
      default:
        return;
      break;
    }

    aEvent.preventDefault();
  };
}
