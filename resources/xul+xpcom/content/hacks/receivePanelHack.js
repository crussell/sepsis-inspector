/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Hack for compatibility with `TargetChanger` for sepsis-inspector < 0.4.
 *
 * `TargetChanger` is deprecated after sepsis-inspector 0.3.  In versions 0.4
 * and up, the panel's own `changeTarget` method should be used instead.  At
 * this point, we still maintain a compatibility shim for the `TargetChanger`
 * class that works by mapping calls to its `changeTarget` method to the
 * method of the same name on the appropriate panel.
 *
 * However, since the `TargetChanger` instance only has access to the viewer
 * object and not the panel that it needs to forward its `changeTarget` calls
 * to, this hack needs to inform each `TargetChanger` instance of which panel
 * the call should be routed to.
 *
 * @see hacks/targetChanger.js
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.ReceivePanelHack = ReceivePanelHack;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { Logger } = require("utils/logger");
var { NotificationManager } = require("utils/notificationManager");

var { TargetChanger } = require("hacks/targetChanger");

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aPanelManager
 *        The `PanelManager` whose panels this hack will handle.
 */
function ReceivePanelHack(aPanelManager)
{
  this.mNotificationManager = new NotificationManager(this);
  this.mPanelManager = aPanelManager;

  aPanelManager.addObserver("viewerLoaded", this.mNotificationManager);
}

{
  let proto = ReceivePanelHack.prototype;
  proto.mNotificationManager = null;
  proto.mPanelManager = null;

  ////////////////////////////////////////////////////////////////////////////
  //// Notifications

  proto.handleNotification = function RPH_HandleNotification(aName, aData)
  {
    switch (aName) {
      case "viewerLoaded":
        this._onViewerLoaded(aData.origin);
      break;
      default:
        throw new Error("wat.  Notification: " + aName);
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  proto._onViewerLoaded = function RPH_OnViewerLoaded(aPanel)
  {
    let targetChanger =
      TargetChanger.getTargetChangerForViewer(aPanel.viewer);

    if (!targetChanger) {
      return;
    }

    // assert(aPanel.viewer == targetChanger.viewer);
    targetChanger.panel = aPanel;

    Logger.print(
      "Use panel's changeTarget method instead of deprecated " +
      "TargetChanger for viewer with UID " + aPanel.viewerUID
    );
  };
}
