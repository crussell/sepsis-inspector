/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * For middle-level operations on the panels in a window.
 *
 * `InspectorActivity` and the panel binding delegate to `PanelManager` for
 * many non-trivial operations on panels.
 *
 * The Inspector's viewers are loaded as a framed document in a panel.  This
 * means every viewer is self-contained and exists in its own scope.  Viewers
 * can be loaded into a panel by creating a `ViewerLoad` object, which will
 * cause the viewer document to be loaded into a browser contained within the
 * panel.  See viewers/sample/info.js and viewers/sample/sampleViewer.js for
 * more information about the viewer interfaces.
 *
 * Like `InspectorActivity`, there is one `PanelManager` instance per window.
 * That panel manager is responsible for all the panels in the
 * `InspectorActivity`'s window, and it oversees the initialization and
 * destruction sequences of its panels' viewers.
 *
 * Panels can be added and removed using `addPanel` and `removePanel`.  The
 * creation and manipulation of the panel nodes (including DOM insertion and
 * removal) are actually handled by a `PanelMaker` instance.
 *
 * Panels exist in a parent/child relationship.  The panel manager was
 * supposed to be agnostic to what the relationship involves; the idea is that
 * we just send out notifications here, and allow any constraints on the
 * panels to be decided and enforced at a higher level.  This separation isn't
 * working out so well, and in fact, the panel manager is much more aware of
 * the exact nature of the relationship, with `getRelationForPanel`.
 *
 * (Practically speaking, the relationship is such that target changes
 * in the parent panel will result in that panel's linked child panels getting
 * updated.  The `InspectorActivity` handles this part.  When a subordinate
 * panel's subject gets updated due to a target change in the parent, it may
 * be the parent's target itself that gets used as the child's new subject, or
 * it may pass the target through an adapter first, translating it into a
 * different object that will be used as the child's new subject.)
 *
 * The panel manager keeps track of these relationships and has methods for
 * safely operating on its panels in order to help maintain those
 * relationships in a sane state.  (Namely, when a panel is removed with
 * `removePanel`, any children that belong to the same panel manager will get
 * removed as well, so it won't leave any panels from that window orphaned.)
 *
 * There exists in the panel manager the notion of unmanaged children and
 * unmanaged parents.  A panel may have one or more children that don't
 * "belong" to the same panel manager (and conversely, a panel's parent may
 * not belong to that panel's panel manager).  This can happen, for example,
 * when using "Inspect Target in New Window".  The panel whose target it is
 * (that the new window will open to inspect) may or may not already have a
 * child panel in the same window that it's in, managed by the same panel
 * manager.  When the new window opens, the panel from the first window will
 * have a new child that doesn't share the same panel manager: the new
 * window's root panel.
 *
 * Only a panel manager's root panel may have a parent managed by some other
 * panel manager. (In fact, if it's the root panel and it does have a parent,
 * that parent *must* be managed by some other panel manager--it can't have a
 * parent in the same window, managed by the same panel manager--that's what
 * makes it the root panel.) No panel may have more than one parent, whether
 * the parent is managed by the same panel manager or not.
 *
 * Finally, there's the notion of whether a panel is linked to its parent or
 * not.  Linkedness determines whether the Inspector will update the panel's
 * subject when the parent panel's target changes.  Non-root panels are
 * required to be linked to their parent.  Since linkedness determines the
 * constraint between updates to that panel's parent's target and updates to
 * that panel's subject itself, a panel with no parent (read: some windows'
 * root panels) cannot be linked.
 *
 * So to review:
 *
 * - A root panel may have a parent, so long as it's in some other window,
 *   managed by some other panel manager.  Alternatively, it may have no
 *   parent.
 *
 * - Any non-root panel will have a parent, and that parent will be from the
 *   same window and managed by the same panel manager.
 *
 * - The number of child panels that a panel has that are managed by other
 *   panel managers is unlimited.
 *
 * - Only a root panel may be in the unlinked state, and if it is, the
 *   Inspector will not update its subject when the parent panel's target
 *   changes.
 *
 * TODO:
 * - An aborted viewer load should reload the previous viewer (if any), and
 *   disable that viewer (at least for that subject).
 * - Clean up command mediator's listeners as some part of viewer destruction
 *   sequence.
 *
 * @see inspectorApp.js
 * @see utils/panelMaker.js
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.PanelManager = PanelManager;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { TheAdapterRegistry } = require("registration/theAdapterRegistry");
var { TheViewerRegistry } = require("registration/theViewerRegistry");
var { ThePrefManager } = require("utils/thePrefManager");

var { CommandMediator } = require("utils/commandMediator");
var { Logger } = require("utils/logger");
var { NotificationManager } = require("utils/notificationManager");
var { PanelMapEntry } = require("utils/panelMapEntry");
var { ViewerLoad } = require("utils/viewerLoad");

var { ReceivePanelHack } = require("hacks/receivePanelHack");

//////////////////////////////////////////////////////////////////////////////

/**
 * Responsible for the parent-to-child relationship for panels and safely
 * loading, initializing, and unloading a panel's viewers.  See above.
 *
 * A root panel will automatically be created during construction.
 *
 * @param aInspector
 *        The `InspectorActivity` instance driving the window we're in.
 * @param aPanelMaker
 *        The `PanelMaker` that will create, insert, and remove the panel
 *        nodes.
 * @param aSynthBlock
 *        The `SynthBlock` panels' subjects and targets may be originating
 *        from.
 * @param aCommandMediator
 *        The `CommandMediator` in charge of the undo/redo stack.
 * @constructor
 */
function PanelManager(aInspector, aPanelMaker, aSynthBloc, aCommandMediator)
{
  this.mNotificationManager = new NotificationManager(this);

  // XXX Remove this when we drop support for pre-0.4 viewers.
  this.mReceivePanelHack = new ReceivePanelHack(this);

  aPanelMaker.panelManager = this;
  this.mInspector = aInspector;
  this.mPanelMaker = aPanelMaker;
  this.mSynthBloc = aSynthBloc;
  this.mCommandMediator = aCommandMediator;
  this.mPanelMap = new WeakMap();
  this.mPendingLoads = new WeakMap();
  this.mViewerPanelMap = new WeakMap();

  this.addPanel(null);
}

{
  let proto = PanelManager.prototype;
  proto.mInspector = null;
  proto.mNotificationManager = null;
  proto.mPanelMaker = null;
  proto.mPanelMap = null;
  proto.mPendingLoads = null;
  proto.mReceivePanelHack = null;
  proto.mRootPanel = null;
  proto.mSynthBloc = null;
  proto.mViewerPanelMap = null;

  proto.commandMediator = null;
  proto.inspectee = null;

  proto.__defineGetter__("rootPanel", function PM_RootPanel_Get()
  {
    return this.mRootPanel;
  });

  proto.loadStarting = function PM_LoadStarting(aViewerLoad)
  {
    // XXX Before destroying the viewer, we should probably wait to see if the
    // load is actually successful.  If we have to abort the load, we can
    // carry on with the old viewer, assuming that the switch wasn't due to a
    // target change that requires a different viewer.  If it was from such a
    // target change, we can pick some other viewer to load.
    this._destroyViewer(aViewerLoad.panel);
    // NB: We do this second because we want the `_destroyViewer` code path
    // to be able to access the panel's `viewer` property; if the panel is a
    // key in `this.mPendingLoads` when we try to access its `viewer`
    // property, it will throw.
    this.mPendingLoads.set(aViewerLoad.panel, aViewerLoad);
  };

  /**
   * Called by a `ViewerLoad` instance after the "load" DOM event for a new
   * viewer document has occurred.  The `ViewerLoad` instance will have
   * already called `initializeViewer`, and the viewer will be available as
   * `aViewerLoad.viewer`.
   *
   * @param aViewerLoad
   *        The `ViewerLoad` object that has just finished.
   */
  proto.loadFinished = function PM_LoadFinished(aViewerLoad)
  {
    let panel = aViewerLoad.panel;
    this.mPendingLoads.delete(panel);

    this._rememberViewer(aViewerLoad);

    // For viewer author convenience, we define a global `viewer` in its
    // scope, if they didn't already define it themselves.
    if (!("viewer" in panel.viewerContext)) {
      // Since it's only for viewer author convenience, if there's a problem
      // it's recoverable by just ignoring it.
      try {
        panel.viewerContext.viewer = aViewerLoad.viewer;
      }
      catch (ex) {
        // Still report it, though...
        Logger.print(ex);
      }
    }

    panel.notificationManager.notify("viewerLoaded");

    this.changePanelSubject(panel, aViewerLoad.subject);
  };

  /**
   * Recognize an unmanaged panel (i.e., a panel belonging to some other panel
   * manager) as our root panel's parent, and reparent the synthetic bloc to
   * the bloc associated with the given panel, inheriting its synthetic
   * privileges.
   *
   * NB: If the synthetic bloc already has a parent, the bloc will lose the
   * synthetic privileges inherited from it, unless `aPanel` is null.  If
   * `aPanel` is null, then then root panel will become an orphan, and the
   * bloc will, too, unless `aRetain` is true; by default, the bloc will not
   * retain its parent or the inherited synthetic privileges.  If `aPanel` is
   * null, the root panel will also become necessarily unlinked, since orphan
   * panels cannot be linked.
   *
   * @param aPanel
   *        The panel to be used as the root panel's parent.  May be null.
   * @param aRetain [optional]
   *        True iff a null `aPanel` shouldn't cause the synthetic bloc to
   *        drop its synthetic privileges.  Defaults to false.
   * @throws
   *        Error if `aPanel` belongs to this panel manager.
   * @see isManagingPanel
   * @see SynthBloc
   * @see canHaveLinkedState
   */
  proto.setParentPanel = function PM_SetParentPanel(aPanel, aRetain)
  {
    if (aPanel || !aRetain) {
      this.mSynthBloc.parent = aPanel && aPanel.synthBloc;
    }

    if (aPanel) {
      aPanel.panelManager.rememberUnmanagedChild(this.rootPanel, aPanel);
      this._changeObservingState(aPanel.panelManager, true)
    }
    else {
      this.rootPanel.linked = false;
    }

    let parent = this.getParentPanel(this.rootPanel, true);
    // If we're being called because the parent is gone, `parent` won't have
    // any useful `panelManager` anymore, and whatever panel manager it had
    // belonged to wouldn't recognize the panel anymore, anyway.  That's all
    // right; in that case we will have already removed ourselves as an
    // observer when we handled the "panelRemoved" notification.  (If we got a
    // "panelManagerDestroyed" notification, we don't worry at all.  It won't
    // be sending anymore notifications and should get GCed shortly.)
    if (parent && "panelManager" in parent) {
      parent.panelManager.forgetUnmanagedChild(this.rootPanel, parent);
      this._changeObservingState(parent.panelManager, false)
    }

    this.mPanelMap.get(this.rootPanel).parent = aPanel;
  };

  proto.rememberUnmanagedChild =
    function PM_RememberUnmanagedChild(aChild, aParent)
  {
    if (this.isManagingPanel(aChild)) {
      throw new Error("Child panel is managed here.");
    }

    if (!this.isManagingPanel(aParent)) {
      throw new Error("Parent panel not managed here.");
    }

    this._rememberChildPanel(aChild, aParent);
  };

  proto.forgetUnmanagedChild =
    function PM_ForgetUnmanagedChild(aChild, aParent)
  {
    if (this.isManagingPanel(aChild)) {
      throw new Error("Child panel is managed here.");
    }

    if (!this.isManagingPanel(aParent)) {
      throw new Error("Parent panel not managed here.");
    }

    this._forgetChildPanel(aChild, aParent);
  };

  /**
   * @param aUID [optional]
   *        A string naming the UID of the viewer to use for "Inspect
   *        Application" mode.
   */
  proto.inspectApplication = function PM_InspectApplication(aUID)
  {
    let appSynthetic = this.mSynthBloc.appSynthetic;

    let uid;
    if (uid === undefined) {
      let currentUID = this.getUIDForPanel(this.rootPanel);
      if (this._viewerAccepts(currentUID, appSynthetic)) {
        uid = currentUID;
      }
      else {
        // Pick something else.
        uid = TheViewerRegistry.getCompatibleViewers(
          appSynthetic, this.mSynthBloc
        )[0];
      }
    }
    else {
      uid = aUID;
    }

    new ViewerLoad(this.rootPanel, appSynthetic, uid);
    this.mIsInspectingApplication = true;
  };

  proto.isInspectingApplication = function PM_IsInspectingApplication()
  {
    return this.mIsInspectingApplication;
  };

  proto.destroy = function PM_Destroy()
  {
    let viewers = [];
    let panels = [ this.rootPanel ];
    while (panels.length) {
      let panel = panels.pop();
      let kids = this.getChildPanels(panel);
      
      panels = panels.concat(kids);

      viewers.unshift(panel.viewer);
    }

    for (let i = 0, n = viewers.length; i < n; ++i) {
      if (viewers[i]) {
        this._destroyViewer(this.getViewerPanel(viewers[i]));
      }
    }

    let parent = this.getParentPanel(this.rootPanel, true);
    // NB: We check on both `parent` and its `panelManager` property, because
    // if the parent panel's window was closed, the panel will lose its XBL
    // binding, and `parent.panelManager` will be undefined.
    if (parent && "panelManager" in parent) {
      parent.panelManager.forgetUnmanagedChild(this.rootPanel, parent);
    }

    this.mNotificationManager.notify("panelManagerDestroyed");
  };

  /**
   * @param aPanel
   *        The "child".
   * @param aUnmanaged [optional]
   *        True iff this panel manager should return the parent if the parent
   *        isn't managed by a different panel manager (i.e., the parent is a
   *        panel from another window).  This can occur under certain
   *        circumstances when the given panel in question is the root panel.
   * @return
   *        The given panel's parent panel, or null if the panel has no
   *        parent.  (A null return value is only possible if the given panel
   *        is the root panel.)
   */
  proto.getParentPanel = function PM_GetParentPanel(aPanel, aUnmanaged)
  {
    let parent = this.mPanelMap.get(aPanel).parent;
    if (this.isManagingPanel(parent) || aUnmanaged) {
      return parent;
    }

    return null;
  };

  /**
   * @param aPanel
   *        The "parent".
   * @param aUnmanaged [optional]
   *        True iff the panels to return should include panels not managed by
   *        this panel manager (i.e., panels from other windows).  Defaults to
   *        false.
   * @return
   *        An array of the given panel's child panel nodes.
   */
  proto.getChildPanels = function PM_GetChildPanels(aPanel, aUnmanaged)
  {
    let kids = this.mPanelMap.get(aPanel).children.slice();
    if (aUnmanaged) {
      return kids;
    }

    let result = [];
    for (let i = 0, n = kids.length; i < n; ++i) {
      if (this.isManagingPanel(kids[i])) {
        result.push(kids[i]);
      }
    }

    return result;
  };

  /**
   * Create a new panel as the child of the given panel and insert it in the
   * well.
   *
   * @param aParentPanel
   *        The parent panel for the created panel.  The given panel *must* be
   *        managed by this panel manager instance.
   * @return
   *        The created panel element.
   * @see isManagingPanel
   * @see getParentPanel
   * @see getChildPanels
   */
  proto.addPanel = function PM_AddPanel(aParentPanel)
  {
    if (aParentPanel) {
      if (!this.isManagingPanel(aParentPanel)) {
        throw new Error("Parent panel not managed here");
      }
      if (this.getChildPanels(aParentPanel).length) {
        throw new Error(
          "Multiple child panels from same window aren't supported right now"
        );
      }
    }
    else if (this.mRootPanel) {
      throw new Error("Can't have more than one root panel");
    }

    let panel = this.mPanelMaker.addPanel(aParentPanel);

    // NB: We must establish the panel<->parent link before setting
    // `panel.panelManager`, or `isManagingPanel` will cause the setter to
    // throw, since it works by checking for an entry in `mPanelMap`.
    let entry = new PanelMapEntry(panel, aParentPanel);
    this.mPanelMap.set(panel, entry);
    if (aParentPanel) {
      this._rememberChildPanel(panel, aParentPanel);
    }
    else {
      this.mRootPanel = panel;
    }

    panel.panelManager = this;
    
    // As the panel manager, we always dispatch these on the panel's behalf,
    // but we need to make sure we're observing these notifications ourself so
    // they'll get passed through to our own observers.
    panel.addObserver("subjectChanged", this.mNotificationManager);
    panel.addObserver("targetChanged", this.mNotificationManager);
    panel.addObserver("viewerDestroyed", this.mNotificationManager);
    panel.addObserver("viewerLoaded", this.mNotificationManager);

    this.mNotificationManager.notify("panelAdded", { panel: panel });

    return panel;
  };

  /**
   * @param aPanel
   * @return
   *        True iff this `PanelManager` instance is responsible for the given
   *        panel.
   */
  proto.isManagingPanel = function PM_IsManagingPanel(aPanel)
  {
    return !!aPanel && this.mPanelMap.has(aPanel);
  };

  /**
   * Destroy the given panel's viewer and remove the panel from the Inspector
   * UI.
   *
   * @param aPanel
   */
  proto.removePanel = function PM_RemovePanel(aPanel)
  {
    if (!this.isManagingPanel(aPanel)) {
      throw new Error("Given panel not managed here");
    }

    let kids = this.getChildPanels(aPanel);
    for (let i = 0, n = kids.length; i < n; ++i) {
      this.removePanel(kids[i]);
    }

    this._destroyViewer(aPanel);

    let parentPanel = this.getParentPanel(aPanel);
    this._forgetChildPanel(aPanel, parentPanel);
    this.mPanelMap.delete(aPanel);

    this.mPanelMaker.removePanel(aPanel);

    this.mNotificationManager.notify("panelRemoved",
                                     { panel: aPanel,
                                       parentPanel: parentPanel });
  };

  /**
   * Root panels may only be linked if they have a parent.  Non-root panels
   * cannot be unlinked from their parent.
   *
   * @param aPanel
   *        The panel.
   * @param aState
   *        Boolean indicating linked (true) or not (false).
   * @return
   *        True iff the state is a valid one for the given panel's
   *        linkedness.
   * @see @fileoverview
   */
  proto.canHaveLinkedState = function PM_canHaveLinkedState(aPanel, aState)
  {
    if (!this.isManagingPanel(aPanel)) {
      throw new Error("Panel not managed here.");
    }

    return (aPanel == this.rootPanel) ?
      !aState || !!this.getParentPanel(aPanel, true) :
      !!aState;
  };

  /**
   * Invoked by the panel binding when its `viewer` getter is called.
   *
   * @param aPanel
   *        The panel whose `viewer` property is being accessed.
   * @throws
   *        Error if the `viewer` property isn't allowed to be accessed.  This
   *        is the case when a viewer load is still pending and the viewer
   *        doesn't set the panel's `viewer` property itself during
   *        initialization.
   */
  proto.getViewerForPanel = function PM_GetViewerForPanel(aPanel)
  {
    let loadData = this.mPendingLoads.get(aPanel) || null;
    if (loadData) {
      if (loadData.viewer) {
        return loadData.viewer;
      }

      throw new Error(
        "While viewer load pending, access to panel \"viewer\" property " +
        "only allowed if set by viewer during initializeViewer."
      );
    }

    return this.mPanelMap.get(aPanel).viewer;
  };

  /**
   * Invoked by the panel binding when its `viewer` setter is called.
   *
   * @param aPanel
   *        The panel whose `viewer` property is being set.
   * @param aViewer
   *        The value the viewer is being set to.
   * @throws
   *        Error if the property is being set at any time other than during
   *        viewer initialization. (I.e., the time after the viewer info's
   *        `initializeViewer` is called, but before it returns.)
   */
  proto.setViewerForPanel = function PM_SetViewerForPanel(aPanel, aViewer)
  {
    let loadData = this.mPendingLoads.get(aPanel) || null;
    if (!loadData || !loadData.initializing) {
      let error = new Error(
        "Setting panel viewer only allowed during viewer initialization."
      );

      // Try to give an error message with a bit more detail.  `!!loadData`
      // implies `!loadData.initializing`, but testing for the former
      // condition won't cause an error if `loadData` is null.
      if (loadData) {
        error.message +=
          " Initialization already finished for viewer with UID " +
          loadData.uid;
      }

      throw error;
    }

    if (!aViewer) {
      throw new Error("Viewer not valid.");
    }

    loadData.viewer = aViewer;
    this._rememberViewer(loadData);
  }

  /**
   * Callers are responsible for ensuring that the given panel is managed by
   * this panel manager, and that there is viewer loaded in the panel capable
   * of inspecting the given subject.
   *
   * NB: Instead of this method, you almost definitely want to use
   * `ViewerLoad` or `ViewerLoad.inspectParentTarget`.
   *
   * @param aPanel
   *        The panel managed by this instance and whose viewer's subject
   *        should be set to the given object.
   * @param aSubject
   *        The app synthetic iff the panel's viewer should be put in "Inspect
   *        Application" mode.  Otherwise, this should be a mirror for the
   *        object that gets passed to the viewer's `inspectObject` method.
   *        The app synthetic is only valid for use in the root panel; passing
   *        it in for a non-root `aPanel` will cause failure.
   * @throw
   *       Error if called while a load is pending for the given panel.
   *
   *       Error if the given subject null.
   *
   *       Error if the given subject is the app-subject synthetic, and the
   *       given panel is not the root panel.
   *
   *       Error if there isn't a viewer loaded in the given panel capable of
   *       inspecting the subject.
   */
  proto.changePanelSubject =
    function PM_ChangePanelSubject(aPanel, aSubject)
  {
    let isSynth = !this.validateSubject(aPanel, aSubject);

    if (this.isLoadPending(aPanel)) throw new Error(
      "Can't change panel subject while load is pending."
    );

    if (!aPanel.viewer) throw new Error(
      "No viewer loaded in panel."
    );

    let uid = aPanel.viewerUID;
    if (!this._viewerAccepts(uid, aSubject)) throw new Error(
      "Can't inspect subject using viewer with UID " + uid
    );

    // NB: This has to happen here instead of below (alongside where we set
    // `subject`); it needs to happen before we actually call into the
    // viewer with the new subject, because the viewer can call `changeTarget`
    // when that happens, and we'd be screwing that up.
    let entry = this.mPanelMap.get(aPanel);
    delete entry.target;

    // The `isSynth` check is included here so we can fail fast.
    if (isSynth && this.mSynthBloc.isAppSynthetic(aSubject)) {
      aPanel.viewer.inspectApplication();
    }
    else {
      // XXX Remove this when we drop support for pre-0.4 viewers.
      if (!("inspectObject" in aPanel.viewer) &&
          "subject" in aPanel.viewer) {
        Logger.print(
          "Viewer subject is deprecated.  Implement inspectObject.  UID: " +
          aPanel.viewerUID
        );
        // Pre-0.4 viewers are guaranteed not to expect mirrors.
        aPanel.viewer.subject = this.mSynthBloc.strip(aSubject);
      }
      else if (!TheViewerRegistry.viewerUsesMirrors(uid) && !isSynth) {
        // XXX Remove this branch when we start requiring that all viewers
        // understand mirrors.
        aPanel.viewer.inspectObject(this.mSynthBloc.strip(aSubject));
      }
      else {
        aPanel.viewer.inspectObject(aSubject);
      }
    }

    entry.subject = aSubject;
    aPanel.notificationManager.notify("subjectChanged");
  };

  /**
   * @param aPanel
   * @param aSubject
   * @return
   *        True iff the given subject is a mirror; false if it's synthetic.
   * @throws
   *        Error if the subject is neither a mirror nor a synthetic.
   *
   *        Error if the subject is the app subject synthetic and `aPanel` is
   *        not a root panel.
   *
   *        Error if the subject is null.
   */
  proto.validateSubject = function PM_ValidateSubject(aPanel, aSubject)
  {
    if (aSubject === null) throw new Error(
      "Panel subject must be non-null"
    );

    if (this.mSynthBloc.isAppSynthetic(aSubject)) {
      if (aPanel != this.rootPanel) throw new Error(
        "Can't use app subject synthetic in non-root panel."
      );
      return false;
    }

    let isMirror = !!this.mSynthBloc.getSynthBlocForMirror(aSubject);
    if (!isMirror && !this.mSynthBloc.isSynthetic(aSubject)) throw new Error(
      "Subject is not a trusted value"
    );

    return isMirror;
  };

  /**
   * @param aPanel
   * @param a_x_Target
   *        The panel's new target.  Targets that are null or undefined will
   *        be interpreted as "panel has no target".  Viewers that want to be
   *        able to indicate that the viewer has a target and the target is
   *        the primitive value null (or undefined) should use mirrors.
   *        NB: `a_x_Target` is untrusted.
   * @see registration/viewerInfo.js:"useMirrors"
   */
  proto.changePanelTarget =
    function PM_ChangePanelTarget(aPanel, a_x_Target)
  {
    if (this.isLoadPending(this)) {
      let error = new Error("Can't change panel target while load pending.");

      // Try to give a more specific error message.
      let loadData = this.mPendingLoads.get(aPanel);
      if (loadData.initializing) error.message +=
        "  Viewers must not call panel's changeTarget method during " +
        "initialization.  Instead, call changeTarget in inspectObject " +
        "or from inspectApplication for viewer with UID " + loadData.uid;

      throw error;
    }

    let entry = this.mPanelMap.get(aPanel);
    let notificationData = { oldTarget: entry.target };

    if (a_x_Target === null) {
      entry.target = null;
    }
    else if (this.mSynthBloc.getSynthBlocForMirror(a_x_Target) ||
             this.mSynthBloc.isSynthetic(a_x_Target)) {
      entry.target = a_x_Target;
    }
    else if (!TheViewerRegistry.viewerUsesMirrors(entry.uid)) {
      // XXX Remove this when we require that all viewers understand mirrors.
      entry.target = this.mSynthBloc.mirror(a_x_Target);
    }
    else {
      // We don't fix it up, because we want to enforce a strict policy for
      // viewers that have opted in to `useMirrors`.
      entry.target = null;
      Logger.print(
        "Ignoring untrusted target for viewer with UID " + entry.uid
      );
    }

    try {
      // NB: We use `a_x_Target` here, not the fixed up `entry.target`,
      // because we don't want to bother viewers that haven't opted in to
      // `useMirrors`; whatever a viewer calls `changeTarget` with is what we
      // should try to make their `target` property reflect.
      aPanel.viewer.target = a_x_Target;
    }
    catch (ex) {
      // Sorry I annoyed you with my convenience.
      //
      // Swallow the exception, because we were only doing this for the
      // viewer's assistance, anyway.
    }

    aPanel.notificationManager.notify("targetChanged", notificationData);
  };

  /**
   * @param aPanel
   * @return
   *        True iff called between the time the panel manager has begun to
   *        load the viewer document and just before the "viewerLoaded"
   *        notification.
   */
  proto.isLoadPending = function PM_IsLoadPending(aPanel)
  {
    return this.mPendingLoads.has(aPanel);
  };

  /**
   * Get the panel that the viewer is currently loaded in.
   *
   * @param aViewer
   *        The viewer object.
   * @return
   *        The panel element containing the given viewer.
   * @throws
   *        Error if `aViewer` is not the viewer object of one of the panels
   *        managed by this panel manager.
   * @see isManagingPanel
   */
  proto.getViewerPanel = function PM_GetViewerPanel(aViewer)
  {
    return this.mViewerPanelMap.get(aViewer);
  };

  /**
   * Get the UID that the viewer was initialized from.  (I.e., the UID that
   * this viewer's info file was registered with.)
   */
  proto.getUIDForPanel = function PM_GetUIDForPanel(aPanel)
  {
    return this.mPanelMap.get(aPanel).uid;
  };

  /**
   * Wrapper around `TheAdapterRegistry.prototype.getAdapterUIDRelation` that
   * will allow us to re-use a cached relation instead of generating a new one.
   *
   * This is necessary in order to keep the viewer switchers working in the
   * event that the given panel has become an orphan since the last time we
   * generated a relation for it.
   *
   * NB: Whereas `getAdapterUIDRelation` takes a panel parameter specifying
   * the panel whose target should get translated, the panel that this method
   * takes is the one that the viewer would actually get loaded in.
   *
   * @param aPanel
   *        The panel the relation will apply to.  (I.e., the UIDs will be
   *        eligible for loading in this panel, to inspect the target that
   *        `aPanel`'s parent is exporting [or whatever it has been translated
   *        into]).
   * @param aTarget [optional]
   *        A mirror or synthetic that if specified will be used rather than
   *        the target of the given panel's parent.  The `AdapterUIDRelation`
   *        will not be cached in this case, because specifying an alternate
   *        target like this is like asking, "Hypothetically, what kind of
   *        viewers and adapters could I use if the target looked like this?"
   *        I.e., it doesn't necessarily have any bearing on any past or
   *        future use.
   * @return
   *        An `AdapterUIDRelation`.
   */
  proto.getRelationForPanel = function PM_GetRelationForPanel(aPanel, aTarget)
  {
    let entry = this.mPanelMap.get(aPanel);
    let parentPanel = this.getParentPanel(aPanel, true);

    let relation = entry.cachedRelation;
    if (!relation || parentPanel || aTarget) {
      relation = this.getTargetRelation(parentPanel, aTarget);
      if (!aTarget) entry.cachedRelation = relation;
    }

    return relation;
  };

  /**
   * @param aPanel
   *        The panel whose target should be used to generate the relation
   *        listing viewers and adapters suitable for use in the children of
   *        the given panel, or null if generating a relation for viewers and
   *        adapters that can be used in the root panel.
   * @param aAltTarget [optional]
   *        A mirror or synthetic that should be used rather than the target
   *        from the given panel.  Note that if an alt-target is specified,
   *        the given panel isn't completely ignored, because adapters still
   *        expect a panel in `translateTarget`.
   * @return
   *        An `AdapterUIDRelation`.
   */
  proto.getTargetRelation = function PM_GetTargetRelation(aPanel, aAltTarget)
  {
    let target = aAltTarget;
    if (!target) target = aPanel && aPanel.target;
    if (!target) target = this.inspectee;

    let noTranslations = !ThePrefManager.getPref(".adapters.enabled");
    return TheAdapterRegistry.getAdapterUIDRelation(
      target, aPanel, this.mSynthBloc, noTranslations
    );
  };

  proto.clearRelationForPanel = function PM_ClearRelationForPanel(aPanel)
  {
    this.mPanelMap.get(aPanel).cachedRelation = null;
  };

  /**
   * Get the `CommandMediator` instance that acts as the given panel's
   * viewer's agent for getting transactions onto the Inspector's Undo/Redo
   * stack and generally keeps commands' enabled states fresh.
   *
   * There are two properties to note about this implementation:
   * - If some viewer A is loaded in the panel and then is displaced by some
   *   viewer B, then A's command mediator will get destroyed when A is
   *   unloaded and B will see an entirely different instance.
   * - We act as a lazy getter, so that if the currently loaded viewer never
   *   accesses the panel's `commandMediator` property, we don't waste any
   *   cycles or memory creating an instance.
   *
   * The second property plays a part in doing proper cleanup in the event
   * that the viewer calls `addTransactionListener` but never makes the
   * corresponding call to `removeTransactionListener` when destroyed.
   *
   * For the second property to be of any significance, it's important that
   * the panel's `commandMediator` property be reserved for use by viewer
   * code.  The Inspector framework should never access it directly.  If it
   * needs access to any command mediator that may exist for the panel, it
   * should call this method directly and explicitly specify not to create an
   * instance if one doesn't already exist.
   *
   * @param aPanel
   * @param aNoCreate [optional]
   *        True if a `CommandMediator` instance shouldn't be created for
   *        the panel when no such mediator already exists.
   * @return
   *        `CommandMediator` instance for the panel.
   *
   * @see utils/commandMediator.jsm
   */
  proto.getCommandMediatorForPanel =
    function PM_GetCommandMediatorForPanel(aPanel, aNoCreate)
  {
    let loadData = this.mPendingLoads.get(aPanel) || null;
    if (loadData && !loadData.initializing) {
      throw new PanelManager.CommandMediatorAccessError();
    }

    let entry = this.mPanelMap.get(aPanel);
    if (!entry.commandMediator && !aNoCreate) {
      entry.commandMediator = new CommandMediator(
        this.mCommandMediator, aPanel.viewerContext.document
      );
    }

    return entry.commandMediator;
  };

  proto.getSynthBlocForPanel = function PM_GetSynthBlocForPanel(aPanel)
  {
    return this.mSynthBloc;
  };

  /**
   * @param aPanel
   * @return
   *        Either a mirror for the panel's subject, or the app-subject
   *        synthetic.
   */
  proto.getSubjectForPanel = function PM_GetSubjectForPanel(aPanel)
  {
    return this.mPanelMap.get(aPanel).subject;
  };

  /**
   * @param aPanel
   * @return
   *        Either a mirror for the target that the panel's
   *        `changePanelTarget` was last called with, or null.
   */
  proto.getTargetForPanel = function PM_GetTargetForPanel(aPanel)
  {
    return this.mPanelMap.get(aPanel).target;
  };

  ////////////////////////////////////////////////////////////////////////////
  //// Notifications

  /**
   * @param aName
   *        A string naming the type of the notification.
   * @param aData
   *        The notification data object, containing an `origin` and other
   *        notification-specific properties.
   * @see notificationManager.js
   */
  proto.handleNotification = function PM_HandleNotification(aName, aData)
  {
    switch (aName) {
      case "panelRemoved":
        this._onPanelRemoved(aData.panel, aData.origin);
      break;
      case "panelManagerDestroyed":
        this._onPanelManagerDestroyed(aData.origin);
      break;
      case "subjectChanged":
      case "targetChanged":
      case "viewerDestroyed":
      case "viewerLoaded":
        // No-op.  Pass it on for observers to process.
      break;
      default:
        throw new Error("wat.  Notification: " + aName);
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  proto._rememberViewer = function PM_RememberViewer(aViewerLoad)
  {
    let entry = this.mPanelMap.get(aViewerLoad.panel);
    entry.viewer = aViewerLoad.viewer;
    entry.uid = aViewerLoad.uid;

    this.mViewerPanelMap.set(aViewerLoad.viewer, aViewerLoad.panel);
  };

  proto._rememberChildPanel = function PM_RememberChildPanel(aChild, aParent)
  {
    let entry = this.mPanelMap.get(aParent);
    if (entry.children.indexOf(aChild) >= 0) {
      throw new Error("Already tracking child.");
    }

    entry.children.push(aChild);
  };

  proto._forgetChildPanel = function PM_ForgetChildPanel(aChild, aParent)
  {
    let kids = this.mPanelMap.get(aParent).children;
    let idx = kids.indexOf(aChild);
    if (idx < 0) {
      throw new Error("Wasn't tracking child.");
    }

    kids.splice(idx, 1);
  };

  /**
   * There are three cases where this needs to happen:
   *  - a viewer switch
   *  - the the panel is closing
   *  - the top-level Inspector window is closing
   *
   * @param aPanel
   */
  proto._destroyViewer = function PM_DestroyViewer(aPanel)
  {
    if (!aPanel.viewer) return;
    
    if ("destroy" in aPanel.viewer) {
      try {
        aPanel.viewer.destroy();
      }
      catch (ex) {
        Logger.print(
          "Encountered error trying to destroy viewer: " + ex
        );
      }
    }

    aPanel.notificationManager.notify("viewerDestroyed");

    aPanel.findbar.removeControllers(aPanel.viewer);
    this.mViewerPanelMap.delete(aPanel.viewer);

    let oldEntry = this.mPanelMap.get(aPanel);
    let newEntry = new PanelMapEntry(aPanel, oldEntry.parent);

    newEntry.children = oldEntry.children;
    if (oldEntry.commandMediator) {
      oldEntry.commandMediator.destroy();
    }

    this.mPanelMap.set(aPanel, newEntry);
  };

  /**
   * @param aUID
   * @param aSubject
   *        Either a mirror or the app subject synthetic.
   * @return
   *        Boolean indicating whether the filter for the given viewer UID
   *        accepts the given subject.
   */
  proto._viewerAccepts = function PM_ViewerAccepts(aUID, aSubject)
  {
    return TheViewerRegistry.viewerSupportsSubject(
      aUID, aSubject, this.mSynthBloc
    );
  };

  /**
   * Used to add or remove ourselves as an observer for (some of) another
   * panel manager's notifications.
   *
   * @param aPanelManager
   *        The panel manager that will be sending out notifications.
   * @param aObserver
   *        Boolean indicating whether we should be observing the
   *        notifications or not; true iff we should add ourselves as an
   *        observer, and false if we should remove ourselves.
   * @see handleNotification
   */
  proto._changeObservingState =
    function PM_ChangeObservingState(aPanelManager, aObserve)
  {
    let $ = (aObserve) ?
      aPanelManager.addObserver :
      aPanelManager.removeObserver;

    $.call(aPanelManager, "panelManagerDestroyed", this.mNotificationManager);
    $.call(aPanelManager, "panelRemoved", this.mNotificationManager);
    $.call(aPanelManager, "targetChanged", this.mNotificationManager);
  };

  /**
   * Handle the "panelRemoved" notification.
   *
   * NB: This won't be a notification for one of the panels that belong to
   * this panel manager.  We'll get this notification from another panel
   * manager if our root panel has an unmanaged parent from another window.
   * That panel manager may or may not be `aPanelManager` itself.
   *
   * @param aPanel
   *        The removed panel.
   * @param aPanelManager
   *        The `PanelManager` instance that was managing the removed panel
   *        and dispatched the notification.
   */
  proto._onPanelRemoved = function PM_OnPanelRemoved(aPanel, aPanelManager)
  {
    if (aPanel == this.getParentPanel(this.rootPanel, true)) {
      this._changeObservingState(aPanelManager, false);
      this.setParentPanel(null, true);
    }
  };

  /**
   * Handle the "panelManagerDestroyed" notification.
   *
   * NB: This won't be a notification for one of the panels that belong to
   * this panel manager.  We'll get this notification from another panel
   * manager if our root panel has an unmanaged parent from another window.
   * That panel manager may or may not be `aPanelManager` itself.
   *
   * @param aPanelManager
   *        The `PanelManager` instance that was destroyed.
   */
  proto._onPanelManagerDestroyed =
    function PM_OnPanelManagerDestroyed(aPanelManager)
  {
    // XXX Is it possible that `parent` is dead by this time?  We are firing
    // synchronously from a window unload event...
    let parent = this.getParentPanel(this.rootPanel, true);
    if (aPanelManager.isManagingPanel(parent)) {
      this.setParentPanel(null, true);
    }
  };
}

PanelManager.CommandMediatorAccessError =
  function PM_CommandMediatorAccessError()
{
  this.stack = (new Error()).stack;
  this.message =
    "While viewer load pending, access to panel \"commandMediator\" " +
    "property only allowed after viewer initialization sequence started."
};

{
  let eproto = Object.create(Error.prototype);
  eproto.constructor = PanelManager.CommandMediatorAccessError;
  PanelManager.CommandMediatorAccessError.prototype = eproto;
}
