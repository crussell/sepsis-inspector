/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Relation between adapters and the UIDs of viewers compatible with the
 * translated targets.
 *
 * @see AdapterRegistry
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.AdapterUIDRelation = AdapterUIDRelation;

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aNullAdapterUIDs
 * @param aTarget
 */
function AdapterUIDRelation(aNullAdapterUIDs, aTarget)
{
  this.mNullAdapterUIDs = aNullAdapterUIDs.slice(0);
  this.originalTarget = aTarget;

  this.mAdapters = [];
  this.mAdapterToUIDsMap = new WeakMap();
  this.mUIDToAdaptersMap = new Map();

  this.mAdapterToTargetMap = new WeakMap();

  for (let i = 0, n = aNullAdapterUIDs.length; i < n; ++i) {
    this.mUIDToAdaptersMap.set(aNullAdapterUIDs[i], [ null ]);
  }
}

{
  let proto = AdapterUIDRelation.prototype;
  proto.mAdapters = null;
  proto.mAdapterToTargetMap = null;
  proto.mAdapterToUIDsMap = null;
  proto.mNullAdapterUIDs = null;
  proto.mUIDToAdaptersMap = null;

  proto.originalTarget = null;

  proto.hasAdapter = function AL_HasAdapter(aThing)
  {
    return (aThing) ?
      this.mAdapterToUIDsMap.has(aThing) :
      !!this.mNullAdapterUIDs.length;
  };

  proto.hasUID = function AL_HasUID(aThing)
  {
    return this.mUIDToAdaptersMap.has(aThing);
  };

  /**
   * @param aAdapter
   * @param aUID
   * @param aTranslation
   */
  proto.set = function AL_Set(aAdapter, aUID, aTranslation)
  {
    if (!aAdapter) {
      throw new Error("Can't add more UIDs for the null adapter");
    }

    if (this.mUIDToAdaptersMap.has(aUID) &&
        this.mUIDToAdaptersMap.get(aUID).indexOf(aAdapter) >= 0) {
      throw new Error("(Adapter, UID) already present");
    }

    let uids = (this.mAdapterToUIDsMap.has(aAdapter)) ?
      this.mAdapterToUIDsMap.get(aAdapter) :
      [];

    if (!uids.length) {
      this.mAdapters.push(aAdapter);
      this.mAdapterToUIDsMap.set(aAdapter, uids);
    }
    uids.push(aUID);

    this.mAdapterToTargetMap.set(aAdapter, aTranslation);

    let adapters = this.mUIDToAdaptersMap.has(aUID) ?
      this.mUIDToAdaptersMap.get(aUID) :
      [];
    if (!adapters.length) {
      this.mUIDToAdaptersMap.set(aUID, adapters);
    }
    adapters.push(aAdapter);
  };

  proto.getUIDs = function AL_GetUIDs()
  {
    // XXX Use `Array.from` when minimum supported Gecko version is >= 32.
    let result = [];
    for (let [uid] of this.mUIDToAdaptersMap) {
      result.push(uid);
    }
    
    return result;
  };

  proto.getAdapters = function AL_GetAdapters()
  {
    let adapters = this.mAdapters.slice(0);
    if (this.hasAdapter(null)) {
      adapters.unshift(null);
    }

    return adapters;
  };

  proto.getTargetForAdapter = function AL_GetTargetForAdapter(aAdapter)
  {
    if (!this.hasAdapter(aAdapter)) {
      throw new Error("No such adapter.");
    }

    return (aAdapter) ?
      this.mAdapterToTargetMap.get(aAdapter) :
      this.originalTarget;
  };

  proto.getUIDsForAdapter = function AL_GetUIDsForAdapter(aAdapter)
  {
    if (!this.hasAdapter(aAdapter)) {
      throw new Error("No such adapter.");
    }

    if (aAdapter) {
      return (this.mAdapterToUIDsMap.has(aAdapter)) ?
        this.mAdapterToUIDsMap.get(aAdapter).slice(0) :
        [];
    }

    return this.mNullAdapterUIDs.slice(0);
  };

  proto.getAdaptersForUID = function AL_GetAdaptersForUID(aUID)
  {
    return this.mUIDToAdaptersMap.has(aUID) ?
      this.mUIDToAdaptersMap.get(aUID).slice(0) :
      [];
  };
}
