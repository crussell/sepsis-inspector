/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Watches for an editable tree exiting edit mode.
 *
 * The XUL tree binding allows editing select data in cooperation with the
 * view implementation.  You can manually control edit mode through the use of
 * the tree's `startEditing` and `stopEditing` methods.  Since the tree offers
 * no way to detect when editing mode is exited, we have to implement a
 * watcher ourselves.
 *
 * The use cases for needing to have this ability include:
 * - Being able to cycle between editable cells (e.g., as with Tab in a
 *   spreadsheet).
 * - Moving to an adjacent editable cell after some value has been entered
 *   (e.g., as with Enter in a spreadsheet).
 *
 * This is accomplished by watching for changes to the "editing" attribute on
 * the tree element.
 *
 * We use the `NotificationManager` convention here.  Interested observers can
 * receive "editEnded" notifications by calling the watcher's `addObserver`
 * method.
 *
 * @see utils/notificationManager.js
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.TreeEditWatcher = TreeEditWatcher;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { NotificationManager } = require("utils/notificationManager");

//////////////////////////////////////////////////////////////////////////////

function TreeEditWatcher(aTree)
{
  this.mNotificationManager = new NotificationManager(this);

  this.mTree = aTree;

  let MutationObserver = this.mTree.ownerDocument.defaultView.MutationObserver;
  this.mMutationObserver = new MutationObserver(this.onMutation.bind(this));
}

{
  let proto = TreeEditWatcher.prototype;
  proto.mMutationObserver = null;
  proto.mNotificationManager = null;
  proto.mState = null;
  proto.mTree = null;

  proto.state = null;

  proto.__defineGetter__("state", function TEW_State_Get()
  {
    return this.mState;
  });

  proto.__defineSetter__("state", function TEW_State_Set(aValue)
  {
    this.mState = aValue;

    if (aValue) {
      this.mMutationObserver.observe(
        this.mTree, { attributes: true, attributeFilter: [ "editing" ] }
      );
    }
    else {
      this.mMutationObserver.disconnect();
    }
  });

  proto.destroy = function TEW_Destroy()
  {
    this.state = null;
    delete this.mMutationObserver;
  };

  proto.onMutation = function TEW_OnMutation(aRecords)
  {
    // Reading the current value of `editingRow` shouldn't pose a problem.  I
    // think. --crussell
    if (this.mTree.editingRow < 0) {
      this.mNotificationManager.notify("editEnded", { state: this.state });
    }
  };
}
