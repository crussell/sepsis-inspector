/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

//////////////////////////////////////////////////////////////////////////////
//// Global Symbols

exports.CommonTreeView = CommonTreeView;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { StubTreeView } = require("utils/stubTreeView");
var { AtomService } = require("xpcom/atomService");

//////////////////////////////////////////////////////////////////////////////

/**
 * A basic, incomplete multi-level tree view.
 *
 * Row level data and open states may be stored in `mLevels` and `mOpenStates`
 * respectively.  If consumers choose to do so, each must be initialized
 * during construction to an appropriate array and call `addManagedRowData` on
 * them and any other arrays which hold row data.
 *
 * For consumers choosing not to use `mLevels` and `mOpenStates`, they should
 * override `getLevel` and `isContainer` and friends as appropriate, as well as
 * `setRowOpenState`.  The latter will be called during `toggleOpenState`.
 *
 * Implementing `computeProperties` is optional.
 *
 * In any case, consumers must implement `expandRow`.
 */
function CommonTreeView()
{
  this.mManagedRowData = [];
}

CommonTreeView.arrayCopy =
  function CTV_ArrayCopy(aSource, aDestination, aIndex)
{
  Array.prototype.splice.apply(aDestination, ([aIndex, 0]).concat(aSource));
};

  /**
   * Determine which from a list of indexes is nearest to the given index.
   * @param aIndex
   *        The index to search for.
   * @param aIndexList
   *        A sorted list of indexes to be searched.
   * @return The index in the list closest to aIndex.  This will be aIndex
   *         itself if it appears in the list, or -1 if the list is empty.
   */
CommonTreeView.getNearestIndex =
  function CTV_GetNearestIndex(aIndex, aIndexList)
{
  // Four easy cases:
  //  - empty list
  //  - single element list
  //  - given index comes before the first element
  //  - given index comes after the last element
  if (aIndexList.length == 0) {
    return -1;
  }
  var first = aIndexList[0];
  if (aIndexList.length == 1 || aIndex <= first) {
    return first;
  }
  var high = aIndexList.length - 1;
  var last = aIndexList[high];
  if (aIndex >= last) {
    return last;
  }

  var mid, low = 0;
  while (low <= high) {
    mid = low + Math.floor((high - low) / 2);
    let current = aIndexList[mid];
    if (aIndex > current) {
      low = mid + 1;
    }
    else if (aIndex < current) {
      high = mid - 1;
    }
    else {
      return aIndex;
    }
  }

  // By handling the four easy cases above, we eliminated the possibility
  // that low or high will be out of bounds at this point.  If aIndex had
  // been present, it would have been sandwiched between these two values:
  var previous = aIndexList[high];
  var next = aIndexList[low];

  if ((aIndex - previous) < (next - aIndex)) {
    return previous;
  }
  // Even if previous and next are equidistant to aIndex's position, we'll
  // go with the one that's greater.
  return next;
};

CommonTreeView.getSelectedRowObject = function CTV_GetSelectedRowObject(aView)
{
  if (aView && aView.selection.count == 1) {
    let out = {};
    aView.selection.getRangeAt(0, out, {});
    return aView.getRowObject(out.value);
  }

  return null;
};

{
  let proto = Object.create(StubTreeView.prototype);
  proto.constructor = CommonTreeView;
  CommonTreeView.prototype = proto;

  proto.mLevels = null;
  proto.mManagedRowData = null;
  proto.mOpenStates = null;
  proto.mTreeBoxObject = null;

  proto.addManagedRowData = function CTV_AddManagedRowData(aDataArray)
  {
    this.mManagedRowData.push(aDataArray);
  };

  proto.updateManagedRowData =
    function CTV_UpdateManagedRowData(aOldArray, aNewArray)
  {
    let idx = this.mManagedRowData.indexOf(aOldArray);
    if (idx < 0) {
      throw new Error("Not a reference to any row data managed here.");
    }

    this.mManagedRowData.splice(idx, 1, aNewArray);
  };

  proto.getSelectedIndexes = function CTV_GetSelectedIndexes()
  {
    let indexes = [];
    for (let i = 0, n = this.selection.getRangeCount(); i < n; ++i) {
      let min = {};
      let max = {};
      this.selection.getRangeAt(i, min, max);
      for (let j = min.value; j <= max.value; ++j) {
        indexes.push(j);
      }
    }
    return indexes;
  };

  proto.setRowOpenState = function CTV_SetRowOpenState(aRow, aOpenState)
  {
    this.mOpenStates[aRow] = aOpenState;
  };

  proto.exciseRowData = function CTV_ExciseRowData(aRowStart, aHowMany)
  {
    for (let i = 0, n = this.mManagedRowData.length; i < n; ++i) {
      this.mManagedRowData[i].splice(aRowStart, aHowMany);
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsITreeView

  proto.collapseRow = function CTV_CollapseRow(aRow)
  {
    // Figure out how many descendant rows there are.
    let level = this.getLevel(aRow);
    let index = aRow + 1;
    while (index < this.rowCount && this.getLevel(index) > level) {
      ++index;
    }

    let changeCount = index - aRow - 1;
    if (changeCount) {
      this.exciseRowData(aRow + 1, changeCount);
    }

    return changeCount;
  };

  proto.getLevel = function CTV_GetLevel(aRow)
  {
    return this.mLevels[aRow];
  };

  proto.getParentIndex = function CTV_GetParentIndex(aRow)
  {
    let level = this.getLevel(aRow) - 1;
    for (let i = aRow - 1; i >= 0; --i) {
      if (this.getLevel(i) == level) {
        return i;
      }
    }

    return -1;
  };

  proto.hasNextSibling = function CTV_HasNextSibling(aRow, aAfterIndex)
  {
    return this.getNextSiblingIndex(aRow, aAfterIndex) >= 0;
  };

  /**
   * @param aRow
   * @param aAfterIndex
   * @param aHypothetical [optional]
   *        True if the index returned should indicate where the sibling
   *        *would* be, if it existed.
   */
  proto.getNextSiblingIndex =
    function CTV_GetNextSiblingIndex(aRow, aAfterIndex, aHypothetical)
  {
    let level = this.getLevel(aRow);

    // Idiosyncratic iteration style because we need the last index checked
    // inside the loop to persist if the loop terminates without having
    // returned a value.  This is so `aHypothetical` works as required.
    let currentIndex = aAfterIndex + 1;
    for (let n = this.rowCount; currentIndex < n; ++currentIndex) {
      if (this.getLevel(currentIndex) == level) {
        return currentIndex;
      }
      if (this.getLevel(currentIndex) < level) {
        break;
      }
    }

    if (aHypothetical) {
      // This is where it would be if it were here.
      return currentIndex;
    }

    return -1;
  };

  proto.isContainerOpen = function CTV_IsContainerOpen(aRow)
  {
    return this.mOpenStates[aRow];
  };

  /**
   * The interface changed in bug 407956.  `aProperties` may or may not be
   * defined.  A convenience implementation is provided in order to manage the
   * correct behavior.  It will invoke the `computeCellProperties` method, if
   * defined, passing in the given row and column.  Consumers who choose to
   * implement that method should return an array of strings naming the
   * properties.
   *
   * @see bug 407956
   */
  proto.getCellProperties =
    function CTV_GetCellProperties(aRow, aColumn, aProperties)
  {
    let properties = [];
    if ("computeCellProperties" in this) {
      properties = this.computeCellProperties(aRow, aColumn);
    }

    if (aProperties === undefined) {
      return properties.join(" ");
    }

    for (let i = 0, n = properties.length; i < n; ++i) {
      aProperties.AppendElement(
        AtomService.getAtom(properties[i])
      );
    }

    // Suppress doesn't-always-return-value warning.
    return undefined;
  };

  proto.setTree = function CTV_SetTree(aTreeBoxObject)
  {
    this.mTreeBoxObject = aTreeBoxObject;
  };

  proto.toggleOpenState = function CTV_ToggleOpenState(aRow)
  {
    if (!this.isContainer(aRow) || this.isContainerEmpty(aRow)) {
      return;
    }

    if (this.mTreeBoxObject) {
      this.mTreeBoxObject.beginUpdateBatch();
    }

    let isOpen = this.isContainerOpen(aRow);
    let changeCount = (isOpen) ?
      -this.collapseRow(aRow) :
      this.expandRow(aRow);

    this.setRowOpenState(aRow, !isOpen);

    this.rowCount += changeCount;

    if (this.mTreeBoxObject) {
      this.mTreeBoxObject.rowCountChanged(aRow + 1, changeCount);
      this.mTreeBoxObject.endUpdateBatch();
    }
  };
}
