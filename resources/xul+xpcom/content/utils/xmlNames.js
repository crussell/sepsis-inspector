/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Translates between common XML prefixes and namespace URIs.
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.XMLNames = XMLNames;

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aEmpty
 *        True iff the relation should be returned empty rather than being
 *        automatically populated with some common prefixes and URIs.
 *        Defaults to false.
 */
function XMLNames(aEmpty)
{
  this.mURIToPrefixMap = new Map();
  this.mPrefixToURIMap = new Map();

  if (!aEmpty) {
    this.addPair(
      "html", "http://www.w3.org/1999/xhtml"
    );
    this.addPair(
      "xhtml", "http://www.w3.org/1999/xhtml"
    );
    this.addPair(
      "xul", "http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul"
    );
    this.addPair(
      "xbl", "http://www.mozilla.org/xbl"
    );
    this.addPair(
      "svg", "http://www.w3.org/2000/svg"
    );
    this.addPair(
      "rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    );
    this.addPair(
      "dc", "http://purl.org/dc/elements/1.1/"
    );
    this.addPair(
      "cc", "http://creativecommons.org/ns#"
    );
    this.addPair(
      "xlink", "http://www.w3.org/1999/xlink"
    );
    this.addPair(
      "sodipodi", "http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
    );
    this.addPair(
      "inkscape", "http://www.inkscape.org/namespaces/inkscape"
    );
  }
}

{
  let proto = XMLNames.prototype;
  proto.mPrefixToURIMap = null;
  proto.mURIToPrefixMap = null;

  proto.addPair = function XN_AddPair(aPrefix, aNamespaceURI)
  {
    this.mPrefixToURIMap.set(aPrefix, aNamespaceURI);
    this.mURIToPrefixMap.set(aNamespaceURI, aPrefix);
  };

  proto.lookupNamespaceURI = function XN_LookupNamespaceURI(aPrefix)
  {
    return (this.mPrefixToURIMap.has(aPrefix)) ?
      this.mPrefixToURIMap.get(aPrefix) :
      null;
  };

  proto.lookupPrefix = function XN_LookupPrefix(aNamespaceURI)
  {
    return (this.mURIToPrefixMap.has(aNamespaceURI)) ?
      this.mURIToPrefixMap.get(aNamespaceURI) :
      null;
  };
}
