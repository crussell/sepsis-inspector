/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Service to access application preferences.
 *
 * `ThePrefManager` has convenience methods for accessing the inspector's pref
 * branch.  By default, preferences are read from the "sepsis.inspector" pref
 * branch for prefs that begin with a leading dot (".").  See `getPref`.
 *
 * @singleton
 */

module.singleton = true;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { PrefUtils } = require("utils/prefUtils");

//////////////////////////////////////////////////////////////////////////////
//// Global variables

exports.ThePrefManager = new PrefManager();

const Cu = Components.utils;

//////////////////////////////////////////////////////////////////////////////

function PrefManager()
{
  this.mPrefUtils = new PrefUtils("");
}

{
  let $proto = exports.ThePrefManager.constructor.prototype;

  $proto.mPrefUtils = null;

  /**
   * Get the value of the given pref.
   *
   * The pref name is normalized; if the given pref name begins with a leading
   * dot ("."), it will be accessed from the "sepsis.inspector" branch.  E.g.,
   *
   *   ThePrefManager.getPref(".defaultViewer")
   *
   * will access the "sepsis.inspector.defaultViewer" pref.  Others will be
   * accessed from the "root" pref branch, as normal.
   *
   * @param aName
   *        The name of the pref to access.  The name will be normalized (see
   *        description above).
   * @return 
   *        An XPCOM boolean, long, or AString, depending on the type of the
   *        named pref.
   * @throws
   *        TypeError if the pref with the normalized name isn't known.
   */
  $proto.getPref = function PM_GetPref(aName)
  {
    return this.mPrefUtils.getPref(this._normalizeName(aName));
  }

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  $proto._normalizeName = function PM_NormalizeName(aName)
  {
    return (aName.charAt(0) == ".") ?
      "sepsis.inspector" + aName :
      aName;
  }
};
