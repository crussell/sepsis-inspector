/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/*
 * @fileoverview
 * This file originates from
 *   https://hg.mozilla.org/users/sevenspade_gmail.com/prefUtils/
 *
 * Consider changing the upstream source instead of local copies.
 */

//////////////////////////////////////////////////////////////////////////////
//// Global variables

exports.PrefUtils = PrefUtils;

const Cc = Components.classes;
const Ci = Components.interfaces;

//////////////////////////////////////////////////////////////////////////////
//// PrefUtils

/**
 * Class to wrap away the XPCOM ugliness of nsIPrefService and nsIPrefBranch.
 *
 * @param aBranch
 *        The fully-qualified name of the branch this instance will be
 *        wrapping.
 */
function PrefUtils(aBranch)
{
  var prefService =
    Cc["@mozilla.org/preferences-service;1"].getService(Ci.nsIPrefService);

  this.mBranch = prefService.getBranch(aBranch || "");
}

{
  let proto = PrefUtils.prototype;
  proto.mBranch = null;

  proto.addObserver = function PU_AddObserver(aDomain, aObserver, aHoldWeak)
  {
    this.mBranch.addObserver(aDomain, aObserver, !!aHoldWeak);
  };

  proto.removeObserver = function PU_RemoveObserver(aDomain, aObserver)
  {
    this.mBranch.removeObserver(aDomain, aObserver);
  };

  /**
   * Given the name of an existing pref, change its value to the one given.
   *
   * This method will not work for prefs which don't already exist.  (Don't do
   * that anyway; use defaults/ appropriately.)  Additionally, the new value
   * must be the same type as the old one.  (Don't try to do otherwise here,
   * either.)
   *
   * @param aName
   *        The name of the pref to change, relative to the branch given an
   *        instantiation.
   * @param aValue
   *        The new value.
   * @throws
   */
  proto.setPref = function PU_SetPref(aName, aValue)
  {
    var type = this.mBranch.getPrefType(aName);
    switch (type) {
      case Ci.nsIPrefBranch.PREF_BOOL:
        this.mBranch.setBoolPref(aName, aValue);
      break;
      case Ci.nsIPrefBranch.PREF_INT:
        this.mBranch.setIntPref(aName, aValue);
      break;
      case Ci.nsIPrefBranch.PREF_STRING:
        let str = Cc["@mozilla.org/supports-string;1"].createInstance(
          Ci.nsISupportsString
        );

        str.data = aValue;
        this.mBranch.setComplexValue(aName, Ci.nsISupportsString, str);
      break;
      default:
        throw new TypeError("Unknown pref type for " + aName + ": " + type);
      break;
    }
  };

  /**
   * Get the value of the given pref.
   *
   * @param aName
   *        The name of the pref whose value should be returned, relative to
   *        the branch given an instantiation.
   * @throws
   */
  proto.getPref = function PU_GetPref(aName)
  {
    var rv;
    var type = this.mBranch.getPrefType(aName);
    switch (type) {
      case Ci.nsIPrefBranch.PREF_BOOL:
        rv = this.mBranch.getBoolPref(aName);
      break;
      case Ci.nsIPrefBranch.PREF_INT:
        rv = this.mBranch.getIntPref(aName);
      break;
      case Ci.nsIPrefBranch.PREF_STRING:
        rv = this.mBranch.getComplexValue(aName, Ci.nsISupportsString).data;
      break;
      default:
        throw new TypeError("Unknown pref type for " + aName + ": " + type);
      break;
    };

    return rv;
  };
}
