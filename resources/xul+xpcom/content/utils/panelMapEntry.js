/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 *
 * The panel manager keeps track of information about every viewer loaded in
 * its panels, and all of its panels' children and parents.  It does this with
 * a map, keyed off the panel.  `PanelMapEntry` are the values in that map.
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.PanelMapEntry = PanelMapEntry;

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aPanel
 * @param aParentPanel [optional]
 */
function PanelMapEntry(aPanel, aParentPanel)
{
  if (aParentPanel) this.parent = aParentPanel;

  this.panel = aPanel;
  this.children = [];
}

{
  let proto = PanelMapEntry.prototype;
  proto.cachedRelation = null;
  proto.children = null;
  proto.commandMediator = null;
  proto.panel = null;
  proto.parent = null;
  proto.uid = null;
  proto.viewer = null;
  proto.subject = null;
  proto.target = null;
}
