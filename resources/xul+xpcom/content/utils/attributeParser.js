/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Parses a string into an (optional prefix and an) attribute name/value pair.
 *
 * TODO
 * - support single quotes
 * - support common entities
 * - support shortform attributes (`finish`?)
 * - support an options parameter to configure stuff like XML vs HTML parsing
 *   mode and deferred input parsing.
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.AttributeParser = AttributeParser;

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aInput [optional]
 *        The string that should be parsed.
 */
function AttributeParser(aInput)
{
  this.mInput = (aInput !== undefined) ?
    aInput :
    "";

  this.state = this.EMPTY;
  this.mInputPointer = 0;

  if (this.mInput) {
    this.consumeInput();
  }
}

{
  let proto = AttributeParser.prototype;
  proto.mCurrentSequence = null;
  proto.mInput = null;
  proto.mInputPointer = null;

  proto.EMPTY = AttributeParser.EMPTY = 1;
  proto.NAMESTARTED = AttributeParser.NAMESTARTED = 2;
  proto.AWAITINGSECONDNAME = AttributeParser.AWAITINGSECONDNAME = 3;
  proto.SECONDNAMESTARTED = AttributeParser.SECONDNAMENSTARTED = 4;
  proto.NAMEFINISHED = AttributeParser.NAMEFINISHED = 5;
  proto.VALUESTARTED = AttributeParser.VALUESTARTED = 6;
  proto.FINISHED = AttributeParser.FINISHED = 7;

  proto.localName = null;
  proto.prefix = null;
  proto.quoteType = null;
  proto.state = null;
  proto.value = null;

  AttributeParser.quoteValue = function AP_QuoteValue(aValue)
  {
    let hasDouble = /"/.test(aValue);
    let hasSingle = /'/.test(aValue);
    if (hasDouble && hasSingle) {
      // XXX We should really support entities during parsing itself...
      aValue = aValue.replace("\"", "&quot;");
    }
    return (hasDouble && !hasSingle) ?
      "'" + aValue + "'" :
      "\"" + aValue + "\"";
  };

  proto.__defineGetter__("input", function AP_Input_Get()
  {
    return this.mInput;
  });

  /**
   * Parse all existing input.
   *
   * @return
   *        The state of the parser after consuming the remaining input.
   * @see this.input
   */
  proto.consumeInput = function AP_ConsumeInput()
  {
    let state;
    while (this.mInputPointer < this.mInput.length) {
      state = this.feed();
    }

    return state;
  };

  /**
   * Continue parsing where we last left off, optionally supplying additional
   * input.
   *
   * @param aMoreInput [optional]
   *        A string containing more input to parse.  Defaults to parsing one
   *        additional character from the existing input if not provided.
   *        Otherwise, parse the entire provided string.
   * @return
   *        The current state of the parser.
   */
  proto.feed = function AP_Feed(aMoreInput)
  {
    if (aMoreInput || aMoreInput == "") {
      this.mInput += aMoreInput;
      return this.consumeInput();
    }

    let c = this._getChar();
    switch (this.state) {
      case this.EMPTY:
        this.state = this._fromEmpty(c);
      break;
      case this.NAMESTARTED:
        this.state = this._fromNameStarted(c);
      break;
      case this.AWAITINGSECONDNAME:
        this.state = this._fromAwaitingSecondName(c);
      break;
      case this.SECONDNAMESTARTED:
        this.state = this._fromSecondNameStarted(c);
      break;
      case this.NAMEFINISHED:
        this.state = this._fromNameFinished(c);
      break;
      case this.VALUESTARTED:
        this.state = this._fromValueStarted(c);
      break;
      case this.FINISHED:
        throw new Error("Invalid sequence");
      break;
      default:
        throw new Error("wat.  state: " + this.state);
      break;
    }

    return this.state;
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  proto._getChar = function AP_GetChar()
  {
    if (this.mInputPointer < this.mInput.length) {
      let c = this.mInput[this.mInputPointer];
      ++this.mInputPointer;
      return c;
    }

    throw new Error("No input available");
  };

  proto._fromEmpty = function AP_FromEmpty(aChar)
  {
    if (this._isNameStartChar(aChar)) {
      this.mCurrentSequence = aChar;
      return this.NAMESTARTED;
    }

    throw new Error("Invalid sequence");
  };

  proto._isNameStartChar = function AP_IsNameStartChar(aChar)
  {
    // See <http://www.w3.org/TR/xml11/#NT-NameStartChar>
    // XXX If you actually check the link, you'll note that we should accept
    // ":" here, too, but that'll have to wait until we support a different
    // parsing modes, including one where namespaces are disabled (e.g., for
    // HTML).
    let cc = aChar.charCodeAt(0);
    return aChar == "_" ||
           (cc >= "A".charCodeAt(0) && cc <= "Z".charCodeAt(0)) ||
           (cc >= "a".charCodeAt(0) && cc <= "z".charCodeAt(0)) ||
           (cc >= "\u00C0".charCodeAt(0) && cc <= "\u00D6".charCodeAt(0)) ||
           (cc >= "\u00D8".charCodeAt(0) && cc <= "\u00F6".charCodeAt(0)) ||
           (cc >= "\u00F8".charCodeAt(0) && cc <= "\u02FF".charCodeAt(0)) ||
           (cc >= "\u0370".charCodeAt(0) && cc <= "\u037D".charCodeAt(0)) ||
           (cc >= "\u037F".charCodeAt(0) && cc <= "\u1FFF".charCodeAt(0)) ||
           (cc >= "\u200C".charCodeAt(0) && cc <= "\u200D".charCodeAt(0)) ||
           (cc >= "\u2070".charCodeAt(0) && cc <= "\u218F".charCodeAt(0)) ||
           (cc >= "\u2C00".charCodeAt(0) && cc <= "\u2FEF".charCodeAt(0)) ||
           (cc >= "\u3001".charCodeAt(0) && cc <= "\uD7FF".charCodeAt(0)) ||
           (cc >= "\uF900".charCodeAt(0) && cc <= "\uFDCF".charCodeAt(0)) ||
           (cc >= "\uFDF0".charCodeAt(0) && cc <= "\uFFFD".charCodeAt(0));
  };

  proto._fromNameStarted = function AP_FromNameStarted(aChar)
  {
    if (this._isNameChar(aChar)) {
      this.mCurrentSequence += aChar;
      return this.NAMESTARTED;
    }
    
    if (aChar == ":") {
      this.prefix = this.mCurrentSequence;
      delete this.mCurrentSequence;
      return this.AWAITINGSECONDNAME;
    }
    
    if (aChar == "=") {
      this.localName = this.mCurrentSequence;
      delete this.mCurrentSequence;
      return this.NAMEFINISHED;
    }

    throw new Error("Invalid sequence");
  };

  proto._isNameChar = function AP_IsNameChar(aChar)
  {
    // See <http://www.w3.org/TR/xml11/#NT-NameChar>
    let cc = aChar.charCodeAt(0);
    return this._isNameStartChar(aChar) ||
           aChar == "-" || aChar == "." || aChar == "\u00B7" ||
           (cc >= "0".charCodeAt(0) && cc <= "9".charCodeAt(0)) ||
           (cc >= "\u0300".charCodeAt(0) && cc <= "\u0300".charCodeAt(0)) ||
           (cc >= "\u203F".charCodeAt(0) && cc <= "\u2040".charCodeAt(0));
  };

  proto._fromAwaitingSecondName = function AP_FromAwaitingSecondName(aChar)
  {
    if (this._isNameStartChar(aChar)) {
      this.mCurrentSequence = aChar;
      return this.SECONDNAMESTARTED;
    }

    throw new Error("Invalid sequence");
  };

  proto._fromSecondNameStarted = function AP_FromSecondNameStarted(aChar)
  {
    if (this._isNameChar(aChar)) {
      this.mCurrentSequence += aChar;
      return this.SECONDNAMESTARTED;
    }

    if (aChar == "=") {
      this.localName = this.mCurrentSequence;
      delete this.mCurrentSequence;
      return this.NAMEFINISHED;
    }

    throw new Error("Invalid sequence");
  };

  proto._fromNameFinished = function AP_FromNameFinished(aChar)
  {
    if (aChar == "\"" || aChar == "'") {
      this.quoteType = aChar;
      // Initialize the sequence to the value for the empty string.  Otherwise
      // it'll be null when we try to append the first character to it.
      this.mCurrentSequence = "";
      return this.VALUESTARTED;
    }

    throw new Error("Invalid sequence");
  };

  proto._fromValueStarted = function AP_FromValueStarted(aChar)
  {
    if (aChar == this.quoteType) {
      this.value = this.mCurrentSequence;
      delete this.mCurrentSequence;
      return this.FINISHED;
    }

    if (aChar != "<") { // XXX Check this.
      this.mCurrentSequence += aChar;
      return this.VALUESTARTED;
    }

    throw new Error("Invalid sequence");
  };
}
