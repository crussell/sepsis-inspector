/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.FunctionInfo = FunctionInfo;

////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { Reflect } = require("xpcom/reflect");

////////////////////////////////////////////////////////////////////////////

/**
 * @param aFunctionMirror
 */
function FunctionInfo(aFunctionMirror)
{
  this.name = aFunctionMirror.name;


  let source = aFunctionMirror.source;
  this.isNative = source === null;

  if (!this.isNative) {
    let statement = Reflect.parse(source).body[0];

    // Case breakdown:
    //   A: function p(x) { x*x; }
    //   B: (function (x) { return x*x; })
    //   C: (function (x) x*x)
    //   D: x => { return x*x; }
    //   E: x => x*x
    //   F: (x) => x*x
    //
    // Example A is a FunctionDeclaration and its body is accessible as
    // `body[0].body`, while the other examples when evaled are each an
    // ExpressionStatement, and the body is accessible as `body[0].body.
    // expression.body`.
    //
    // NB: Although `Function.prototype.toString` is supposed to return a
    // string in the form of a FunctionDeclaration according to ES5, arrow
    // functions aren't a part of ES5.  `Function.prototype.toSource` seems to
    // be able to distinguish between functions that were created with a
    // FunctionDeclaration and those created in a FunctionExpression, and will
    // wrap the source of FunctionExpressions in parens (even if they weren't
    // used in the original source), if they are ES5-style expressions (i.e.,
    // not arrow functions).  This has the effect of keeping them in
    // FunctionExpression form if you parse the `toSource` output for them.
    //
    // We go ahead and use `toSource` above instead of `toString`, even though
    // using `toString` would allow us to disregard parsing to a
    // FunctionExpression for B and C and treat them the same as A, because:
    //
    //   1) We still have to handle the source of arrow functions coming out
    //      in the form of a FunctionExpression (even when `toString` is used,
    //      because arrow functions cannot be made to match the form of a
    //      FunctionDeclaration), so we don't gain much by trying to
    //      consolidate handling of cases B and C with A.
    //   2) With the introduction of arrow functions, the definition of
    //      `toString` could change in ES6+ to allow it to return a string in
    //      the form of a FunctionExpression, and if so, it may not limit that
    //      behavior for arrow functions only; it may allow non-arrow
    //      functions to do the same.
    switch (statement.type) {
      case "FunctionDeclaration":
        this.ast = statement;
      break;
      case "ExpressionStatement":
        this.ast = statement.expression;
      break;
      default:
        throw new Error(
          "Expected a FunctionDeclaration or ExpressionStatement" +
          (this.name && " for " + this.name)
        );
      break;
    }
  }

  this.isArrow = !!this.ast && this.ast.type == "ArrowExpression";

  this.params = this._extractParamList(this.ast);

  let sourceLines = source.split(/\r\n|\r|\n/);
  this.bodyText = this._extractBody(sourceLines, this.ast);
}

{
  let proto = FunctionInfo.prototype;
  proto.ast = null;
  proto.bodyText = null;
  proto.isArrow = null;
  proto.isNative = null;
  proto.name = null;
  proto.params = null;

  /**
   * @param aSourceLines
   *        Array of strings representing the function's source.
   * @param aAST
   *        Parser API node tree.  May be null.
   */
  proto._extractBody = function FI_ExtractBody(aSourceLines, aAST)
  {
    if (!aAST) {
      return null;
    }

    // The parser API in Gecko 26 doesn't seem to give sane results for the
    // location data.  (A BlockStatement's start is the line and col for the
    // opening brace, but the end is the line and col of the block's final
    // statement and not the closing brace?  A FunctionDeclaration statement
    // starts at the beginning of the identifier, and not at the `function`
    // keyword?)  The craziness may actually be written into the parser API
    // "spec" and not a bug in the implementation, I'm not sure, but it's
    // craziness either way. --crussell
    //
    // The location start data, on the other hand, for the function bodies of
    // the previous examples (see above) looks like this:
    //
    //   A: function p(x) { x*x; }
    //                    ^
    //   B: (function (x) { return x*x; })
    //                    ^
    //   C: (function (x) x*x)
    //                    ^
    //   D: x => { return x*x; }
    //           ^
    //   E: x => x*x
    //           ^
    //   F: (x) => x*x
    //             ^
    //
    // This works, so we'll go ahead and use it, we'll just make sure to
    // pretend that the terrible location end data doesn't exist and do some
    // checks of our own devising.
    let start = aAST.body.loc.start;
    // NB: Parser API nodes' line numbers are not zero-based, they start at 1.
    let bodyLines = aSourceLines.slice(start.line - 1);

    // NB: Nodes' column numbers start at 1, too.
    bodyLines[0] = bodyLines[0].substr(start.column - 1);

    // If the function is wrapped in parens (as in B and C), get rid of them.
    if (aAST.type == "FunctionExpression") {
      // assert(aSourceLines[0] == "(" &&
      //        bodyLines[bodyLines.length - 1].substr(-1) == ")");
      bodyLines[0] = bodyLines[0].substr(1);
      let lastLine = bodyLines.pop();
      bodyLines.push(lastLine.substr(0, lastLine.length - 1));
    }

    // Unindent.  Once again, the various syntactic extensions to ES3 style
    // function expressions make this really hard.  Examples:
    //
    //   foo = {
    //     sq: function (x)
    //     {
    //       return x * x;
    //     }
    //   };
    //  
    //   foo = {
    //     sq: function (x)
    //     (
    //       x * x
    //     )
    //   };
    //     
    //   foo = {
    //     sq: x =>
    //     (
    //       x * x
    //     )
    //   };
    //     
    // (Note the parens in the latter two.)
    //
    // Assuming that the "f" in "foo" occupies the first column, in each case,
    // we'll want to remove the two leading spaces from the beginning of each
    // line.  We might be tempted to look at the whitespace on the first line
    // and remove a similar pattern from the others.  But now consider this
    // case:
    //
    //   foo = {
    //     sq = function (x) {
    //       return x * x;
    //     }
    //   };
    //
    // In fact, even for the examples above, the body's open brace won't be
    // indented, because of our use of `start.column` above.
    //
    // Instead, we take a similar approach, but we look at the last line's
    // indentation.
    let lastLine = bodyLines[bodyLines.length - 1];
    let indentation = lastLine.substr(0, lastLine.search(/[^ \t]/));
    if (indentation) {
      // We need to check that it's present on every line.  Remember that the
      // opening brace (if any) won't be indented, which is why our condition
      // isn't `i >= 0`.
      //
      // In case it's not present on every line, we'll just ditch the whole
      // unindenting effort, but we don't want to leave it partially
      // uncompleted, with some lines unindented and some lines not.  In order
      // to prevent having to loop twice so that we can fully check before
      // messing with any of the lines, we'll take this approach:
      let backup = bodyLines.slice(0);
      for (let i = bodyLines.length - 1; i > 0; --i) {
        if (bodyLines[i].indexOf(indentation) != 0) {
          // Throw away our changes.
          bodyLines = backup;
          break;
        }

        bodyLines[i] = bodyLines[i].substr(indentation.length);
      }
    }

    return bodyLines.join("\n");
  };

  /**
   * @param aAST
   *        Parser API node tree.  May be null.
   */
  proto._extractParamList = function FI_ExtractParamList(aAST)
  {
    if (!aAST) {
      return [];
    }

    // The param list can include defaults and/or span across several lines,
    // e.g.:
    //   function (aFoo, // some comment describing `aFoo`
    //             aBar) // some comment describing `aBar`
    //   { /* ... */ }
    //
    // There's not a simple way to make sure this gets formatted nicely
    // (especially if the viewer is loaded in a narrow width panel), so we
    // punt for now and rely on the Parser API and only show the names.
    let params = [];
    for (let i = 0, n = aAST.params.length; i < n; ++i) {
      params.push(aAST.params[i].name);
    }
    if (aAST.rest) {
      params.push("..." + aAST.rest.name);
    }

    return params;
  };
}
