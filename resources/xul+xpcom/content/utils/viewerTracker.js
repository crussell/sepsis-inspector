/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * For tracking the lifetime of some panel's viewer
 *
 * Implemented as a convenient abstraction over "viewerLoaded"/
 * "viewerDestroyed" notifications.
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.ViewerTracker = ViewerTracker;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { NotificationManager } = require("utils/notificationManager");

//////////////////////////////////////////////////////////////////////////////

function ViewerTracker()
{
  this.mNotificationManager = new NotificationManager(this);
  this.mPanelLoadMap = new WeakMap();
}

{
  let proto = ViewerTracker.prototype;
  proto.mNotificationManager = null;
  proto.mPanelLoadMap = null;

  /**
   * @param aPanel
   *        The panel who's viewer we should track.
   */
  proto.trackViewerForPanel = function VT_TrackViewerForPanel(aPanel)
  {
    // We'll want to continue tracking this panel until it has been removed or
    // it has loaded another viewer.
    aPanel.panelManager.addObserver("viewerDestroyed", this);
    aPanel.panelManager.addObserver("viewerLoaded", this);

    // We have to be able to handle the case where the supplier's constructor
    // is called during viewer initialization.  If that happens, we'll get a
    // "viewerLoaded" notification for the panel shortly after we begin
    // tracking it.  So as we begin tracking it, we check to see if there's a
    // pending load for it.  If so, we'll want to skip that first
    // "viewerLoaded" notificaton we get for it, for the reason above.
    //
    // NB: We also need to be able to handle the case where we've begun
    // tracking the panel, then another viewer is selected that also calls the
    // supplier constructor during initialization.  So far, so good;
    // `addObserver` is idempotent in that case, and this should do the right
    // thing, too:
    this.mPanelLoadMap.set(
      aPanel, aPanel.panelManager.isLoadPending(aPanel)
    );
  };

  proto.isTrackingViewerForPanel =
    function VT_IsTrackingViewerForPanel(aPanel)
  {
    // We need to make sure that a) we're aware of the panel in question, and
    // b) if we began trying to track it while a load was still pending, that
    // we aren't still waiting for it to load.
    return this.mPanelLoadMap.has(aPanel) && !this.mPanelLoadMap.get(aPanel);
  };

  ////////////////////////////////////////////////////////////////////////////
  //// Notifications

  /**
   * @see utils/notificationManager.js
   */
  proto.handleNotification = function VT_HandleNotification(aName, aData)
  {
    switch (aName) {
      case "viewerLoaded":
        this._onViewerLoaded(aData.origin);
      break;
      case "viewerDestroyed":
        this._dropPanel(aData.origin);
      break;
      default:
        throw new Error("wat.  Notification: " + aName);
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  /**
   * Handle the "viewerLoaded" notification.
   *
   * We need to make sure we only keep track of panels for the duration of the
   * lifetime of its current viewer.  (Or the duration of the lifetime of the
   * upcoming viewer, if a load was still pending at the time we began
   * tracking it.)
   *
   * @param aPanel
   *        The panel the viewer has just loaded in.
   */
  proto._onViewerLoaded = function VT_OnViewerLoaded(aPanel)
  {
    if (!this.mPanelLoadMap.has(aPanel)) {
      // We're not tracking this panel.
      return;
    }

    // assert(this.mPanelLoadMap.get(aPanel));
    // assert(!aPanel.panelManager.isLoadPending(aPanel));

    // Mark this panel as not-anticipating-load and continue tracking it.
    this.mPanelLoadMap.set(aPanel, false);
  };

  /*
   * @param aPanel
   *        The inspectorpanel element to stop tracking.
   * @see trackViewerForPanel
   */
  proto._dropPanel = function VT_DropPanel(aPanel)
  {
    if (!this.mPanelLoadMap.has(aPanel)) {
      // We're not tracking this panel's viewer.
      return;
    }

    this.mPanelLoadMap.delete(aPanel);
  };
}
