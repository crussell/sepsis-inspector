/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * It sucks having to stub out nsITransactionListener methods, when you're
 * only interested in a couple.  (This is one of the reasons that you should
 * re-use the existing nsIObserver interface instead of rolling your own new
 * one with a bunch of single-purpose callback methods.)
 *
 * NB: You probably don't need this.  Viewers should use `CommandMediator`.
 * This exists essentially as an implementation detail of `CommandMediator`.
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.ForwardingTransactionListener = ForwardingTransactionListener;

//////////////////////////////////////////////////////////////////////////////

/**
 * Allows the owner to selectively implement only the nsITransactionListener
 * methods it's interested in.
 *
 * See the note above about using `CommandMediator` instead of
 * `ForwardingTransactionListener`, if possible.
 *
 * @param aOwner
 *        The object that implements zero or more nsITransactionListener
 *        methods.
 * @constructor
 */
function ForwardingTransactionListener(aOwner)
{
  this.mOwner = aOwner;
}

{
  let proto = ForwardingTransactionListener.prototype;
  proto.mOwner = null;

  /**
   * Forward to the given listener the transaction listener method invocation
   * with the given name, and the arguments that the transaction manager
   * passed in.
   *
   * @param aListener
   *        The object that implements the nsITransactionListener method of
   *        the given name.
   * @param aName
   *        The name of the nsITransactionListener method that the transaction
   *        manager has called.
   * @param aArgs
   *        The arguments that the transaction manager has passed to the
   *        nsITransactionListener method.
   */
  proto.forward = function FTL_Forward(aListener, aName, aArgs)
  {
    if (aName in aListener) {
      aListener[aName].apply(aListener, aArgs);
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsITransactionListener

  proto.willDo = function FTL_WillDo()
  {
    this.forward(this.mOwner, "willDo", arguments);
  };

  proto.didDo = function FTL_DidDo()
  {
    this.forward(this.mOwner, "didDo", arguments);
  };

  proto.willUndo = function FTL_WillUndo()
  {
    this.forward(this.mOwner, "willUndo", arguments);
  };

  proto.didUndo = function FTL_DidUndo()
  {
    this.forward(this.mOwner, "didUndo", arguments);
  };

  proto.willRedo = function FTL_WillRedo()
  {
    this.forward(this.mOwner, "willRedo", arguments);
  };

  proto.didRedo = function FTL_DidRedo()
  {
    this.forward(this.mOwner, "didRedo", arguments);
  };

  proto.willBeginBatch = function FTL_WillBeginBatch()
  {
    this.forward(this.mOwner, "willBeginBatch", arguments);
  };

  proto.didBeginBatch = function FTL_DidBeginBatch()
  {
    this.forward(this.mOwner, "didBeginBatch", arguments);
  };

  proto.willEndBatch = function FTL_WillEndBatch()
  {
    this.forward(this.mOwner, "willEndBatch", arguments);
  };

  proto.didEndBatch = function FTL_DidEndBatch()
  {
    this.forward(this.mOwner, "didEndBatch", arguments);
  };

  proto.willMerge = function FTL_WillMerge()
  {
    this.forward(this.mOwner, "willMerge", arguments);
  };

  proto.didMerge = function FTL_DidMerge()
  {
    this.forward(this.mOwner, "didMerge", arguments);
  };
}
