/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * For accessing statically defined configuration parameters
 *
 * TODO:
 * - Categories should be optional
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.TheStaticStore = new StaticStore();

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { CategoryManager } = require("xpcom/categoryManager");

//////////////////////////////////////////////////////////////////////////////

function StaticStore() { }

{
  let $proto = StaticStore.prototype;

  $proto.readValue = function SS_ReadValue(aName, aCategory)
  {
    try {
      return CategoryManager.getCategoryEntry(aCategory, aName);
    }
    catch (ex) {
      let error = new Error(
        "Error reading " + aName + " from category " + aCategory
      );
      error.innerException = ex;
      error.message += "\nCaught exception:\n" + String(ex);
      throw error;
    }
  };
}
