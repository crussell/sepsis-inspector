/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.FindByClick = FindByClick;

const Cu = Components.utils;
const Ci = Components.interfaces;

//////////////////////////////////////////////////////////////////////////////

function FindByClick(aRoot, aSynthBloc, aObserver)
{
  if (!aSynthBloc.mirror(Ci.nsIDOMDocument).hasInstance(aRoot)) {
    throw new TypeError("Not a document");
  }

  this.mDocument = aSynthBloc.mirror(
    XPCNativeWrapper(aSynthBloc.strip(aRoot))
  );
  this.mSynthBloc = aSynthBloc;
  this.mObserver = aObserver;
  this.active = true;
}

{
  let proto = FindByClick.prototype;
  proto.mActive = null;
  proto.mDocument = null;
  proto.mObserver = null;

  proto.event = null;

  proto.__defineGetter__("active", function FBC_Active_Get()
  {
    return this.mActive;
  });

  proto.__defineSetter__("active", function FBC_Active_Set(aVal)
  {
    if (aVal == this.mActive) {
      return;
    }

    if (aVal) {
      this._start();
    }
    else {
      this._stop();
    }

    this.mActive = aVal;
  });

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIDOMEventListener

  /**
   * @param a_x_Event
   *        NB: `a_x_Event` is untrusted.
   */
  proto.handleEvent = function FBC_HandleEvent(a_x_Event)
  {
    let event = this.mSynthBloc.mirror(a_x_Event);
    switch (event.get("type").valueOf()) {
      case "click":
        this._onClick(event);
      break;
      case "mousedown":
      case "mouseup":
        this._stopEvent(event);
      break;
      default:
        throw new Error("wat. Event: " + event.get("type"));
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  proto._start = function FBC_Start()
  {
    let self = this.mSynthBloc.mirror(this);
    this.mDocument.callMethod("addEventListener", "mousedown", self, true);
    this.mDocument.callMethod("addEventListener", "mouseup", self, true);
    this.mDocument.callMethod("addEventListener", "click", self, true);
  };

  proto._stop = function FBC_Stop()
  {
    this._removeListeners();
  };

  proto._removeListeners = function FBC_RemoveListeners()
  {
    let self = this.mSynthBloc.mirror(this);
    this.mDocument.callMethod("removeEventListener", "mousedown", self, true);
    this.mDocument.callMethod("removeEventListener", "mouseup", self, true);
    this.mDocument.callMethod("removeEventListener", "click", self, true);
  };

  proto._onClick = function FBC_OnClick(aEventMirror)
  {
    this._stopEvent(aEventMirror);
    this.event = aEventMirror;
    this.mObserver.observe(this, "find-by-click-finished", "");
    this._removeListeners();
  };

  proto._stopEvent = function FBC_StopEvent(aEventMirror)
  {
    aEventMirror.callMethod("stopPropagation");
    aEventMirror.callMethod("preventDefault");
  };
}
