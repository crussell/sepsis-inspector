/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Legacy wrapper over iterables.
 *
 * Note: `Enumerator` was originally written as a wrapper for the non-standard
 * `Iterator`, because the design was awful.  Thankfully, the proposal for
 * `Iterator` was killed once and for all in ES6.  It will go away soon in
 * SpiderMonkey.  Consequently, this class doesn't really make sense anymore
 * and consumers should be updated.  I'm not going to do that right now,
 * though, so I've just changed the implementation here not to rely on
 * `Iterator`.  The original documentation for `Enumerator` follows.
 *
 * `Enumerator` instances allow you to:
 *   a) Check if there is an item that follows the current item in the
 *      sequence, or if the current item is instead the last one.
 *   b) Recall the value for the item at the current position.
 *
 * When an `Enumerator` instance is first created, its `key` will be null and
 * trying to call `getValue` will throw an exception.  Calling `next` will
 * position it over the first element (if it has one).  You can call
 * `getValue`, which will be the same value that was returned by `next`.  `key`
 * will also be updated.  At any time you can call `hasNext` to check if you've
 * reached the end of the sequence.  Attempting to call `next` after reaching
 * the end will throw an error and leave the enumerator positioned at the last
 * item, so that `key` and `getValue` continue to reflect the same things as
 * they did after the last successful call to `next`.
 *
 * This design follows a movement model similar to DOM NodeIterators, in that
 * they start out pointing to a sort of dead space at creation.  However,
 * there is otherwise no notion of being "between" items, and these things can
 * only move forward; there is no way to move backwards to revisit the item
 * that precedes the one at the current position.
 *
 * There is another way to use an `Enumerator` instance that involves starting
 * at the first element (instead of before it; i.e., so that `key` and
 * `getValue` will return those for the first element, and the first `next`
 * call actually moves to the second element, if any), and dying when the end
 * is reached.  See `InitializedEnumerator`.
 *
 * @see Document Object Model Traversal, Section 1.1.1.1
 *      http://w3.org/TR/DOM-Level-2-Traversal-Range/traversal.html
 */

////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.Enumerator = Enumerator;

////////////////////////////////////////////////////////////////////////////

function Enumerator(aArray)
{
  // Make sure [[Call]] acts like [[Construct]]
  if (!Enumerator.prototype.isPrototypeOf(this)) {
    return new Enumerator(aArray);
  }

  this.mKeys = [];
  this.mValues = [];
  this.mPosition = -1;

  if (aArray !== undefined) {
    for (let i = 0, n = aArray.length; i < n; ++i) {
      this.mKeys.push(i);
      this.mValues.push(aArray[i]);
    }
  }

  // XXX Suppress "does not always return a value" warnings.
  return this;
}

{
  let proto = Enumerator.prototype;
  proto.mKeys = null;
  proto.mPosition = null;
  proto.mValues = null;

  proto.key = null;

  proto.__defineGetter__("key", function E_Key_Get()
  {
    return (!this.isDead && this.mPosition >= 0) ?
      this.mKeys[this.mPosition] :
      null;
  });

  proto.__defineGetter__("isDead", function E_IsDead_Get()
  {
    return this.mPosition === null;
  });

  proto.__defineSetter__("isDead", function E_IsDead_Set(aVal)
  {
    if (aVal) {
      this.destroy();
    }
  });

  /**
   * @return
   *        The iterated value returned by the last `next()` call.
   * @throws
   *        Error if called before the first call to `next`.
   */
  proto.getValue = function E_GetValue()
  {
    this._checkValid();
    if (this.mPosition < 0) {
      throw new Error(
        "Can't call enumerator's getValue() before the first call to next()"
      );
    }

    return this.mValues[this.mPosition];
  };

  proto.hasNext = function E_HasNext()
  {
    this._checkValid();
    return this.mPosition + 1 < this.mValues.length;
  };

  proto.next = function E_Next()
  {
    if (!this.hasNext()) {
      throw new Error("Reached enumerator end.");
    }

    // assert(this.mIsPrimed);
    ++this.mPosition;

    return this.getValue();
  };

  proto.destroy = function E_Destroy()
  {
    delete this.mKeys;
    delete this.mValues;
    delete this.mPosition;
  };

  proto._checkValid = function E_CheckValid()
  {
    if (this.isDead) {
      throw new Error("Enumerator is dead.");
    }
  };
}
