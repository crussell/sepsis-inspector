/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 *
 * With `InitializedEnumerator`, you can use an enumerator that is immediately
 * positioned over the first element, if possible, and dies when moving "past"
 * the last element.  Otherwise, the semantics of `Enumerator` are such that a
 * newly created enumerator is positioned "before" the first element, and
 * remains positioned at the last element when it is reached.
 *
 * Use it like this:
 *
 *   let enumerator = new InitializedEnumerator(iterable);
 *   while (!enumerator.isDead) {
 *     // ...
 *     // Do something with `enumerator.getValue()` or `enumerator.key`
 *     // ...
 *
 *     // Go on.
 *     enumerator.move();
 *   }
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.InitializedEnumerator = InitializedEnumerator;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { Enumerator } = require("utils/enumerator");

//////////////////////////////////////////////////////////////////////////////

/**
 * NB: The enumerator will immediately be marked dead if there are no elements
 * to iterate over.  Always check `isDead` before accessing `key` or calling
 * `getValue()`.
 *
 * @param aIterableThing
 *       Anything that `Iterator` operate on.
 */
function InitializedEnumerator(aIterableThing)
{
  Enumerator.call(this, aIterableThing);
  this.move();
}

{
  let proto = Object.create(Enumerator.prototype);
  InitializedEnumerator.prototype = proto;
  proto.constructor = InitializedEnumerator;

  /**
   * @return
   *        True if the enumerator was able to move forward and remains alive,
   *        or false if it was positioned at the last element and this call
   *        has caused the enumerator to self-destruct.
   * @throws
   *        Error if it was already dead.
   * @see Enumerator.prototype.destroy
   */
  proto.move = function E_Move()
  {
    if (this.hasNext()) {
      this.next();
      return true;
    }

    this.destroy();
    return false;
  };
}
