/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Command controller for the domNode viewer's attribute tree.
 *
 * The tree is the one shown when an element is being inspected in the viewer.
 *
 * NB: There's a problem.  We want to be able to paste attributes in the forms
 *
 *   foo:bar="baz"
 *   omg="hax"
 *
 * ... but given a string that matches the first case, there's no way for us
 * to know what namespace the "foo" prefix is actually supposed to refer to. 
 * There exists the `lookupNamespaceURI` method that we can call on the
 * inspected element, but it's not failsafe.
 *
 * We could mitigate this by copying the attribute object itself to the
 * clipboard (or a clone, since we wouldn't want a live reference), rather
 * than a pasteable plain text serialization that looks like XML/HTML.  The
 * only problem there is that I still have no idea about how to actually get
 * such a thing onto the clipboard, despite hours of reading the docs and
 * poring through ancient Gecko internals trying to understand the
 * implementation.
 *
 * So what we do is accept the consequences.  We use a text serialization and
 * recognize a set of common prefixes, and if the source node was bound to an
 * unrecognized namespace, then we paste it in using the anonymous namespace,
 * and that's just the way it's going to be.  Fortunately, namespaced
 * attributes are *fairly* exotic for the use cases we expect to see, even for
 * XML documents.
 *
 * TODO
 * - If the clipboard doesn't parse into a proper attribute pair/triplet,
 *   set up a new attribute with that name in edit mode (only if it doesn't
 *   match an existing attribute?).
 * - Multiple selection/paste.
 * - When copying and pasting, if the copied attribute's `prefix` is null but
 *   its `namespaceURI` isn't, we probably want to use some "x-unknown" prefix
 *   instead of the empty string...  Also, pasting something with some
 *   uncommon prefix (including "x-unknown") should probably engage the user
 *   in a dialog about what's going on, allowing them to manually specify the
 *   namespace themselves.
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.AttributesPasteHelper = AttributesPasteHelper;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { EditAttributeTransaction } = require("transactions/editAttributeTransaction");
var { AttributeParser } = require("utils/attributeParser");
var { XMLNames } = require("utils/xmlNames");

var { TheFakeClipboardHack } = require("hacks/theFakeClipboardHack");

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aCommandMediator
 *        The `CommandMediator` to handle the paste transaction.
 * @param aXMLNames [optional]
 *        `XMLNames` instance
 */
function AttributesPasteHelper(aCommandMediator, aXMLNames)
{
  this.mCommandMediator = aCommandMediator;
  this.mXMLNames = (aXMLNames !== undefined) ?
    aXMLNames :
    new XMLNames();
}

{
  let proto = AttributesPasteHelper.prototype;
  proto.mCommandMediator = null;
  proto.mXMLNames = null;

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  /**
   * @param aNode [optional]
   *        The DOM node the attribute would be added to.  If no node is
   *        provided, this method can be used to find out if the clipboard
   *        contains something that parses to an attribute.
   * @param aParser [optional]
   *        Newly created `AttributeParser` instance.  Caller should create
   *        and provide the parser if it wants access it after the validation
   *        attempt.  The caller should NOT attempt to re-use an existing
   *        parser.  The parser must be fresh; its state must be
   *        `AttributeParser.EMPTY`.  If not provided, a parser will be
   *        created and discarded for GC after determining the validation
   *        result.
   * @return
   *        Boolean indicating whether the clipboard contains an attribute
   *        that can be pasted.  Will return false if a node is given and it
   *        not an element node.
   */
  proto.isClipboardPasteable =
    function APH_IsClipboardPasteable(aNode, aParser)
  {
    if (aNode && aNode.nodeType != aNode.ELEMENT_NODE) {
      return false;
    }

    if (!TheFakeClipboardHack.hasFlavor(TheFakeClipboardHack.STRING_FLAVOR)) {
      return false;
    }

    let parser = (aParser === undefined) ?
      new AttributeParser() :
      aParser;

    if (parser.state != AttributeParser.EMPTY) {
      // assert(aParser !== undefined);
      throw new Error("Attribute parser must be fresh.");
    }

    let clipboardContents =
      TheFakeClipboardHack.getContents(TheFakeClipboardHack.STRING_FLAVOR);
    try {
      parser.feed(clipboardContents);
    }
    catch (ex) {
      return false;
    }

    return parser.state == AttributeParser.FINISHED;
  };

  /**
   * @param aNode
   *        The DOM node the attribute should be added to.
   * @return
   *        True iff we were able to successfully complete the transaction.
   *        (Will be false if the clipboard doesn't contain a valid attribute
   *        serialization or the given node is a non-element node.)
   */
  proto.paste = function APH_Paste(aNode)
  {
    let parser = new AttributeParser();
    if (this.isClipboardPasteable(aNode, parser)) {
      let namespaceURI = this.mXMLNames.lookupNamespaceURI(parser.prefix);
      let transaction = new EditAttributeTransaction(
        aNode, namespaceURI, parser.localName, parser.value
      );
      this.mCommandMediator.doTransaction(transaction);
      return true;
    }

    return false;
  };
}
