/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 *
 * Useful for sorting a row of data, based on some criteria.
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.SortSack = SortSack;

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aValue
 *        The primitive value that should be returned when the instance is
 *        used in contexts where a primitive is expected.
 */
function SortSack(aValue, aPayload)
{
  this.value = aValue;
  if (aPayload) {
    this.payload = aPayload;
  }
};

{
  let proto = SortSack.prototype;
  proto.value = null;
  proto.payload = null;

  proto.toString =
  proto.valueOf = function SS_ValueOf()
  {
    return this.value;
  };
}
