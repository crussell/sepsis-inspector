/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * We want to enable viewers to participate in what goes on the Undo/Redo
 * stack, but we want to discourage direct access to the Inspector's
 * transaction manager.  So we've got this `CommandMediator` thing to take
 * care of some stuff.
 *
 * `CommandMediator` instances can also decorate the owner with utility
 * methods for updating and dispatching commands.
 *
 * The idea is that the `InspectorActivity` instance--which is responsible for
 * driving most (non-viewer parts) of the UI for a single Inspector
 * window--will have a `CommandMediator` instance with a transaction manager
 * (aka, the undo/redo stack).  Viewers which have commands that support undo/
 * redo (aka, transactions) can also have created for them a `CommandMediator`
 * instance, which acts as their agent for making sure their commands make it
 * onto the stack.
 *
 * The interface exposed to viewers for dealing with this is very simple: the
 * command mediator has a `doTransaction` method that takes a transaction.
 * `CommandMediator` owners can optionally implement a subset of the
 * nsITransactionListener interface, and add themselves as listeners to be
 * called before and after a transaction has been added to the stack or an
 * undo or redo occurs.  (See `addTransactionListener`.)
 *
 * For a more detailed explanation of how this all works, read on.
 *
 * For superordinate command mediators, a transaction manager is created to
 * handle all the transactions from the Inspector window that the `Inspector`
 * instance is responsible for (including transactions that orginate from its
 * panels' sub-documents, i.e., from the viewers loaded in the Inspector
 * window).
 *
 * There can exist subordinate command mediators, created to serve a specific
 * viewer.  These will not have an independent undo/redo stack.  Subordinate
 * mediators "chain up" to the parent mediator, so that `doTransaction`,
 * `addTransactionListener`, et cetera, will effect changes to the parent's
 * undo/redo stack.
 *
 * Between this and `ForwardingTransactionListener`, there's some heavy
 * dynamic dispatch going on for transaction listener methods.  Sorry about
 * that.  To save you some hassle and possibly frustration, the code path for
 * listener method invocation looks like this:
 *
 *   The transaction manager starts things off.
 *     It makes its call to notify listeners of stack activity (i.e., before
 *     or after an Undo, a Redo, or a new transaction is added).
 *   ForwardingTransactionListener#<method>
 *     For the FTL owned by the `CommandMediator` that was used to add the
 *     listener.
 *   ForwardingTransactionListener#forward(<mediator>, <method>, <args>)
 *     -
 *   CommandMediator#<method>
 *     On the same `CommandMediator` instance as above.
 *   CommandMediator#_forwardListenerCall(<listener>, <method>, <args>)
 *     -
 *   ForwardingTransactionListener#forward(<method>, <args>)
 *     Used by the `CommandMediator` like a static utility method in order to
 *     forward to the listener below.
 *   <listener>#<method>
 *     where <listener> is one of the listeners in the command mediator's
 *     internal transaction listeners array, registered with
 *     `addTransactionListener`.
 *
 * TODO:
 * - Some resolution scheme to get the top-level instance's subordinates.
 *   (For issue #125.)
 *
 * @see utils/forwardingTransactionListener
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.CommandMediator = CommandMediator;

const Cc = Components.classes;
const Ci = Components.interfaces;
const Cu = Components.utils;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { ForwardingTransactionListener } = require("utils/forwardingTransactionListener");

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aOwner
 *        If we are a subordinate command mediator that should use the
 *        Inspector window's top-level undo/redo stack instead of creating a
 *        new transaction manager for the owner, this should be the
 *        superordinate `CommandMediator` instance.  If we are the
 *        superordinate instance itself, this should be the
 *        `InspectorActivity` instance we belong to.
 * @param aDocument
 *        The owner's DOM document.  The `updateCommand` method, for example,
 *        will seek XUL command elements that exist in this document.
 * @constructor
 */
function CommandMediator(aOwner, aDocument)
{
  this.mOwner = aOwner;
  this.mDocument = aDocument;
  if (!this._getParent()) {
    // Create the undo/redo stack.
    this.mTransactionManager = Cc["@mozilla.org/transactionmanager;1"].
      createInstance(Ci.nsITransactionManager);
  }
}

{
  let proto = CommandMediator.prototype;
  proto.mDocument = null;
  proto.mForwardingTransactionListener = null;
  proto.mOwner = null;
  proto.mTransactionListeners = null;
  proto.mTransactionManager = null;

  proto.destroy = function CM_Destroy()
  {
    if (this.mForwardingTransactionListener) {
      this._removeForwardingListener();
    }
  };

  proto.decorate = function CM_Decorate(aObject)
  {
    aObject.dispatchCommand = this.dispatchCommand.bind(this);
    aObject.updateCommand = this.updateCommand.bind(this);
    aObject.updateCommandSet = this.updateCommandSet.bind(this);
  };

  proto.getTransactionManager = function CM_GetTransactionManager()
  {
    if (this.mTransactionManager) {
      return this.mTransactionManager;
    }

    throw new Error(
      "Raw access to transaction manager not allowed for panel command " +
      "mediators."
    );
  };

  /**
   * Execute the given transaction and add it to the Undo/Redo stack.
   *
   * NB: The transaction should come from a JSM.  Transactions from a viewer
   * context will leak the viewer's nsIDOMWindow global and so will not be
   * allowed through.  (The way the leak manifest is when a viewer creates its
   * own transaction and passes it to `doTransaction`, then sometime later the
   * viewer is destroyed, either because another viewer has taken its place in
   * the containing panel or the panel was removed.  Meanwhile, the
   * transaction from the original viewer remains in the Undo/Redo stack.)
   *
   * @param aTransaction
   *        The nsITransaction object.
   * @throws
   *        Error if the transaction's global is an nsIDOMWindow.
   */
  proto.doTransaction = function CM_DoTransaction(aTransaction)
  {
    let global = Cu.getGlobalForObject(aTransaction);
    if (global instanceof Ci.nsIDOMWindow) {
      throw new Error(
        "Transactions should not come from a window.  Move the transaction " +
        "constructor to a JSM, and import the JSM into the viewer context."
      );
    }
    
    this._getTransactionManagerInternal().doTransaction(aTransaction);
  };

  /**
   * Since viewers don't ordinarily have access to the transaction manager
   * without jumping through some hoops (see `getTransactionManager`), they
   * should use this method to listen to transaction operations.
   *
   * The goals of this approach are
   *   a) encourage (read: enforce) good practices for transaction listeners,
   *      such as not allowing transactions through that will leak the the
   *      viewer context (compared to transactions from a JSM), and
   *   b) to ease requirements on consumers who might otherwise succumb to
   *      method stubbing fatigue, making every nsITransactionListener method
   *      optional to implement, and
   *   c) to discourage use of nsITransactionManager's more "exotic" features
   *      (e.g., batching, merging), and
   *   d) to discourage direct access to the transaction manager in general,
   *      as above.
   *
   * The only methods that the listener will receive calls to are the will-/
   * did- Do/Undo/Redo methods.
   *
   * @see _forwardListenerCall
   */
  proto.addTransactionListener = function CM_AddTransactionListener(aListener)
  {
    if (!this.mForwardingTransactionListener) {
      // assert(!this.mTransactionListeners);
      this.mTransactionListeners = [];

      this.mForwardingTransactionListener =
        new ForwardingTransactionListener(this);

      this._getTransactionManagerInternal().AddListener(
        this.mForwardingTransactionListener
      );
    }

    if (this.mTransactionListeners.indexOf(aListener) < 0) {
      this.mTransactionListeners.push(aListener);
    }
  };

  proto.removeTransactionListener =
    function CM_RemoveTransactionListener(aListener)
  {
    let idx = this.mTransactionListeners.indexOf(aListener)
    if (idx >= 0) {
      this.mTransactionListeners.splice(idx, 1);
      if (!this.mTransactionListeners.length) {
        // assert(this.mForwardingTransactionListener);
        this._removeForwardingListener();
      }
    }
    else {
      throw new Error("No such transaction listener");
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// partial nsITransactionListener (see ForwardingTransactionListener)

  proto.willDo = function CM_WillDo(aManager, aTransaction)
  {
    this._forwardListenerCall("willDo", arguments);
  };

  proto.didDo = function CM_DidDo(aManager, aTransaction, aResult)
  {
    this._forwardListenerCall("didDo", arguments);
  };

  proto.willUndo = function CM_WillUndo(aManager, aTransaction)
  {
    this._forwardListenerCall("willUndo", arguments);
  };

  proto.didUndo = function CM_DidUndo(aManager, aTransaction, aResult)
  {
    this._forwardListenerCall("didUndo", arguments);
  };

  proto.willRedo = function CM_WillRedo(aManager, aTransaction)
  {
    this._forwardListenerCall("willRedo", arguments);
  };

  proto.didRedo = function CM_DidRedo(aManager, aTransaction, aResult)
  {
    this._forwardListenerCall("didRedo", arguments);
  };

  ////////////////////////////////////////////////////////////////////////////
  //// Decoration methods

  proto.dispatchCommand = function CM_DispatchCommand(aName)
  {
    let controller =
      this.mDocument.commandDispatcher.getControllerForCommand(aName);

    if (controller && controller.isCommandEnabled(aName)) {
      controller.doCommand(aName);
    }
  };

  /**
   * Update a command's disabled state.
   *
   * If a XUL command element exists in both the subordinate scope and the
   * superordinate scope, we want both elements to be updated, e.g., so that
   * any Paste menu item in a viewer document and the one one in the
   * Inspector's menu bar reflect the same thing.
   *
   * What we do is, regardless of whether a direct call was made to the
   * superordinate or the subordinate, start by trying to update via the
   * subordinate mediator.  If we get an answer about the command's enabled
   * state (i.e., found the controller and called `isCommandEnabled`), we pass
   * it along to the superordinate so any identically named commands can be
   * updated in the Inspector document.  (We won't try to look for the
   * controller again or ask about the command's enabled state; we just work
   * with whatever we found before in order to prevent unnecessary
   * computation.)
   *
   * If we aren't able to find the controller with the subordinate mediator,
   * we will try to seek an answer in the superordinate mediator.  This
   * handles the case where there are some command elements in the Inspector
   * document but no matching ones in the viewer document.
   *
   * This method used to be so simple.
   *
   * @param aName
   *        The name of the command.  This should be the element ID of a
   *        XUL command element.
   * @param aEnabled [optional]
   *        For internal use only.  Used for quasi-"recursive" calls to update
   *        commands in a parent scope based on what we found in the child.
   *        Bad things may happen if this is used by outside callers.
   * @return
   *        True iff the command was found needing to be enabled.  Falsy values
   *        indicate the command was disabled.  Furthermore, if the falsy
   *        value is explicitly null, that indicates that no controller was
   *        even found for the command.
   */
  proto.updateCommand = function CM_UpdateCommand(aName, aEnabled)
  {
    // NB: This method relies on subtle differences between false, null, and
    // undefined.
    //
    // Case A:
    //   We are the subordinate mediator.  We need to set the command state in
    //   our own document (if any), and then propagate whatever we find to the
    //   superordinate.
    // Case B1:
    //   We are the superordinate mediator and our caller is not the
    //   subordinate propagating its findings on to us.  This means we are on
    //   the "downstroke" and need to pass the request to the subordinate (see
    //   above).  Then we'll unwind.
    // Case B2:
    //   We are the superordinate and our caller is the subordinate,
    //   propagating its findings to us on the "upstroke".  If it found
    //   anything, use it.  If not, try to get an answer ourselves.
    //
    // `aEnabled` will be used to help orient ourselves so we can determine
    // which of the above cases applies to us.  On the downstroke, `aEnabled`
    // will be undefined.  When we bottom out in case A and transition to the
    // upstroke, all subordinate-to-superordinate calls will have `aEnabled`
    // as a boolean indicating the enabled state the subordinate found, or
    // null to indicate it found no answer and we should seek it ourselves.

    let enabled = aEnabled;
    if (enabled === undefined) {
      // Currently on the downstroke.  Are we case A or B1?
      if (!this._getParent()) {
        // It's B1.
        let subordinate = this.mOwner.getSubordinateCommandMediator();
        if (subordinate) {
          return subordinate.updateCommand(aName);
        }
      }

      // We've now bottomed out.  Time to transition to the upstroke.
      enabled = null;
    }

    // We can be either A or B2 here, but they're both treated the same-ish.
    if (enabled == null) {
      // ... i.e., we need to try to get an answer ourselves if the above
      // condition holds...
      let controller =
        this.mDocument.commandDispatcher.getControllerForCommand(aName);
      if (controller) {
        enabled = controller.isCommandEnabled(aName);
      }
    }

    // ... and update our own command element with the findings, whether they
    // be our own from above, or whether they're being propagated to us, as in
    // the case of subordinate-to-superordinate calls.
    let command = this.mDocument.getElementById(aName);
    if (command) {
      // NB: At this point `enabled` may still be null.  We treat it the same
      // as false.  (See comments above about this method's return value.)
      if (!!enabled) {
        command.removeAttribute("disabled");
      }
      else {
        command.setAttribute("disabled", "true");
      }
    }

    // Now we actually discriminate between A and B2.
    let superordinate = this._getParent();
    if (superordinate) {
      // It's A.  Make the subordinate-to-superordinate call so the
      // superordinate can go through the same steps we just did.
      return superordinate.updateCommand(aName, enabled);
    }

    // This is for case B2.
    return enabled;
  };

  /**
   * Recursively update all the commands in a commandset element.
   *
   * @param aName
   *        The ID of the XUL commandset element.
   */
  proto.updateCommandSet = function CM_UpdateCommandSet(aName)
  {
    let kids = this.mDocument.getElementById(aName).childNodes;
    for (let i = 0, n = kids.length; i < n; ++i) {
      if (kids[i].localName == "commandset") {
        this.updateCommandSet(kids[i].id);
      }
      else if (kids[i].localName == "command") {
        this.updateCommand(kids[i].id);
      }
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  proto._getParent = function CM_GetParent()
  {
    return (this.mOwner instanceof CommandMediator) ?
      this.mOwner :
      null;
  };

  proto._getTransactionManagerInternal =
    function CM_GetTransactionManagerInternal()
  {
    return this.mTransactionManager ||
           this._getParent().getTransactionManager();
  };

  /**
   * Removes the forwarding listener if we have no more subordinate listeners
   * interested in hearing about transaction manager activity.
   *
   * @see addTransactionListener
   * @see fileoverview
   */
  proto._removeForwardingListener = function CM_RemoveForwardingListener()
  {
    this._getTransactionManagerInternal().RemoveListener(
      this.mForwardingTransactionListener
    );

    delete this.mForwardingTransactionListener;
  };

  /**
   * Forward an nsITransactionListener method invocation to the subordinate
   * listeners.
   *
   * If you're here because you're trying to follow the code paths for
   * nsITransactionListener method calls, apologies for the dynamic dispatch
   * (two layers, even!).  See above.
   *
   * @see @fileoverview
   * @see addTransactionListener
   * @see utils/forwardingTransactionListener.jsm
   */
  proto._forwardListenerCall = function CM_ForwardListenerCall(aName, aArgs)
  {
    for (let i = 0, n = this.mTransactionListeners.length; i < n; ++i) {
      this.mForwardingTransactionListener.forward(
        this.mTransactionListeners[i], aName, aArgs
      );
    }
  };
}
