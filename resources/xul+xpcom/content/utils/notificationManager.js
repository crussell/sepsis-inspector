/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * We use an observe/notify system between viewers, the panel manager, and the
 * inspector.  The semantics are kind of a hybrid between nsIObserverService
 * and DOM events.
 *
 * Notifications bubble up.  Unlike DOM events, the bubbling behavior is not
 * determined by the position of nodes in the DOM.  (In fact, none of the
 * major participants are even DOM nodes).  Instead, notifications propagate
 * throughout a directed graph, from the observed component that dispatches
 * the notification to its observers and then *their* observers, in a reverse
 * depth-first order.  This means every observer must itself be observable. 
 *
 * Participants must implement three methods:
 *
 * handleNotification(aName, aData)
 *   `aName` is a string describing the name of the notification.  `aData` is
 *   an object containing at least one property `origin`, which is a reference
 *   to the participant that issued the notification.  Other fields may be
 *   present, depending on the type of notification.
 * addObserver(aName, aObserver)
 *   Register `aObserver` as observing the notification `aName`.  When the
 *   participant receives a notification matching `aName` or when it issues
 *   its own, it must notify the interested observers by calling their
 *   `handleNotification` methods.  Multiple calls to add an observer for a
 *   notification it's already registered to receive must have the same effect
 *   as if that observer were only registered once.
 * removeObserver(aName, aObserver)
 *   Unregister `aObserver` as being interested in notifications of the name
 *   `aName`.
 *
 * `NotificationManager` is provided below for convenience so that
 * participants need not worry about properly implementing the bubbling
 * behavior, and instead focus on responding to notifications and issuing
 * their own.
 *
 * Participants must be careful not to form a cycle in the observer chain, to
 * prevent an infinite loop.  If the participants form structures in the graph
 * that resemble the classic multiple inheritance problem, they must also be
 * careful to avoid responding to the same notification multiple times.
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.NotificationManager = NotificationManager;

//////////////////////////////////////////////////////////////////////////////

/**
 * Owners who make use of NotificationManager must only worry about
 * implementing `handleNotification` and responding to notifications received
 * there.  Everything else, including bubbling, will be handled here.
 *
 * When owners wish to observe notifications, they should pass their
 * NotificationManager instance as the observer, rather than themselves.
 *
 * @param aOwner
 *        The instantiating object which is delegating observer juggling and
 *        notification management to this instance.
 * @constructor
 */
function NotificationManager(aOwner)
{
  this.mObservers = new Map();

  this.mOwner = aOwner;
  this.mOwner.addObserver = this.boundAddObserver =
    this.addObserver.bind(this);
  this.mOwner.removeObserver = this.boundRemoveObserver =
    this.removeObserver.bind(this);
};

{
  let proto = NotificationManager.prototype;
  proto.mObservers = null;
  proto.mOwner = null;

  proto.boundAddObserver = null;
  proto.boundRemoveObserver = null;

  /**
   * See above.
   */
  proto.addObserver = function NM_AddObserver(aName, aObserver)
  {
    if (!this.mObservers.has(aName)) {
      this.mObservers.set(aName, []);
    }

    if (this.mObservers.get(aName).indexOf(aObserver) < 0) {
      this.mObservers.get(aName).push(aObserver);
    }
  };

  /**
   * See above.
   */
  proto.removeObserver = function NM_RemoveObserver(aName, aObserver)
  {
    if (!this.mObservers.has(aName)) {
      return;
    }

    let idx = this.mObservers.get(aName).indexOf(aObserver);
    if (idx >= 0) {
      this.mObservers.get(aName).splice(idx, 1);
    }
  };

  /**
   * See above.
   */
  proto.handleNotification = function NM_HandleNotification(aName, aData)
  {
    if ("handleNotification" in this.mOwner) {
      this.mOwner.handleNotification(aName, aData);
    }
    this.notify(aName, aData, true);
  };

  /**
   * Owners should use this method to issue their own notifications to
   * interested observers.
   *
   * @param aName
   *        The name of the notification being issued.
   * @param aData [optional]
   *        Additional notification-specific data.  Owners need not worry
   *        about correctly specifying `origin`, so long as they don't use the
   *        `aIsntOrigin` third argument below.
   * @param aIsntOrigin [optional]
   *        Boolean indicating whether the notification is being passed
   *        through, rather than originating at the owner.  Default is false.
   *        This is really just for internal use; owners should never need to
   *        use this.
   */
  proto.notify = function NM_Notify(aName, aData, aIsntOrigin)
  {
    if (!this.mObservers.has(aName)) {
      return;
    }

    let data = aData;
    if (!aIsntOrigin) {
      if (!data) {
        data = {};
      }
      data.origin = this.mOwner;
    }
    
    let observers = this.mObservers.get(aName);
    for (let i = 0, n = observers.length; i < n; ++i) {
      observers[i].handleNotification(aName, data);
    }
  };
}
