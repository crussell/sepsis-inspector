/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * inIDeepTreeWalker's NodeIterator cousin.
 *
 * TODO
 *   - Mutations.  See:
 *     <http://www.w3.org/TR/2012/WD-dom-20120105/#iterator-collection>
 *   - whatToShow
 *   - If filter is a callback, fix it up.
 *
 * @see inIDeepTreeWalker
 *      <http://hg.mozilla.org/mozilla-central/file/tip/layout/inspector/public/inIDeepTreeWalker.idl>
 * @see NodeIterator
 *      <http://www.w3.org/TR/2012/WD-dom-20120105/#traversal>
 *      <http://www.w3.org/TR/DOM-Level-2-Traversal-Range/traversal.html#Iterator-overview>
 */

//////////////////////////////////////////////////////////////////////////////
//// Global variables

exports.DeepIterator = DeepIterator;

const Ci = Components.interfaces;
const Cr = Components.results;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { Logger } = require("utils/logger");

var { DOMUtils } = require("xpcom/domUtils");

//////////////////////////////////////////////////////////////////////////////

/**
 * @param a_x_Root
 *        The root of the subtree that the iterator should traverse.
 *        NB: `a_x_Root` is untrusted.
 * @param aFilter [optional]
 *        NodeFilter.  Defaults to null.
 * @param aShowAnons [optional]
 *        Whether the iterator should respect the structure of the shadow
 *        tree.  Defaults to true.
 * @param aShowSubDocs [optional]
 *        Whether the iterator should traverse subdocuments for elements like
 *        iframes, et cetera.  Defaults to true.
 * @param a_x_StartAfter [optional]
 *        Start off with the iterator's position after this node, rather than
 *        before `a_x_Root`.
 *        NB: `a_x_StartAfter` is untrusted.
 */
function DeepIterator(a_x_Root, aFilter, aShowAnons, aShowSubDocs,
                      a_x_StartAfter)
{
  this.root = new XPCNativeWrapper(a_x_Root);

  this.filter = (aFilter === undefined) ?
    null :
    aFilter;

  this.mShowAnonymousNodes = (aShowAnons === undefined) ?
    true :
    aShowAnons;

  this.mShowSubDocuments = (aShowSubDocs === undefined) ?
    true :
    aShowSubDocs;

  if (a_x_StartAfter === undefined) {
    this.reset();
  }
  else {
    this.referenceNode = new XPCNativeWrapper(a_x_StartAfter);
    this.pointerBeforeReferenceNode = false;
  }
}

{
  let proto = DeepIterator.prototype;
  proto.filter = null;
  proto.pointerBeforeReferenceNode = null;
  proto.referenceNode = null;
  proto.root = null;

  proto.mAcceptNodeActive = null;
  proto.mDetached = null;
  proto.mShowAnonymousNodes = null;
  proto.mShowSubDocuments = null;

  /**
   * Reset the iterator, even if it's been detached.  
   *
   * @param aAtEnd [optional]
   *        Whether to reset the position to the end of `root`'s subtree,
   *        rather than at the root itself.
   */
  proto.reset = function DI_Reset(aAtEnd)
  {
    delete this.mDetached;

    if (aAtEnd) {
      this.referenceNode = this._getEndNode(this.root);
    }
    else {
      this.referenceNode = this.root;
    }
    this.pointerBeforeReferenceNode = !aAtEnd;
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIDOMNodeIterator

  proto.__defineGetter__("whatToShow", function DI_WhatToShow_Get()
  {
    throw Cr.NS_ERROR_NOT_IMPLEMENTED;
  });

  proto.detach = function DI_Detach()
  {
    this.mDetached = true;
    this.referenceNode = null;
  };

  proto.nextNode = function DI_NextNode()
  {
    return this._traverse();
  };

  proto.previousNode = function DI_PreviousNode()
  {
    return this._traverse(true);
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  proto._traverse = function DI_Traverse(aBackward)
  {
    this._checkFlag(this.mDetached);

    let filterResult, node = this.referenceNode;
    do {
      if (aBackward && this.pointerBeforeReferenceNode) {
        node = this._moveBackward(node);
      }
      else if (!aBackward && !this.pointerBeforeReferenceNode) {
        node = this._moveForward(node);
      }

      if (!node) {
        return null;
      }

      this.pointerBeforeReferenceNode = !!aBackward;

      this._checkFlag(this.mAcceptNodeActive);
      // XXX This is where we'd check `whatToShow`.

      this.mAcceptNodeActive = true;

      filterResult = (this.filter === null) ?
        Ci.nsIDOMNodeFilter.FILTER_ACCEPT :
        this.filter.acceptNode(node);

      delete this.mAcceptNodeActive;

      this._checkFlag(this.mDetached);
    } while (filterResult != Ci.nsIDOMNodeFilter.FILTER_ACCEPT);

    this.referenceNode = node;
    return node;
  };

  proto._checkFlag = function DI_CheckFlag(aFlag)
  {
    if (aFlag) {
      // XXX Not the right way, but I'm not too concerned atm. --crussell
      throw Ci.nsIDOMDOMException.INVALID_STATE_ERR;
    }
  };

  proto._moveForward = function DI_MoveForward(aNode)
  {
    let nodeGroup = this._getDescendants(aNode);
    if (nodeGroup.length) {
      return nodeGroup[0];
    }

    let current = aNode;
    do {
      let ancestor = null;
      if (current != this.root) {
        ancestor =
          DOMUtils.getParentForNode(current, this.mShowAnonymousNodes);
        nodeGroup = this._getDescendants(ancestor);
        let idx = nodeGroup.indexOf(current);
        if (idx + 1 < nodeGroup.length) {
          return nodeGroup[idx + 1];
        }
      }

      current = ancestor;
    } while (current);

    // assert(aNode == this._getEndNode(this.root));
    return null;
  };

  proto._moveBackward = function DI_MoveBackward(aNode)
  {
    let ancestor = null;
    if (aNode != this.root) {
      ancestor = DOMUtils.getParentForNode(aNode, this.mShowAnonymousNodes);
      let nodeGroup = this._getDescendants(ancestor);
      let idx = nodeGroup.indexOf(aNode);
      if (idx > 0) {
        return this._getEndNode(nodeGroup[idx - 1]);
      }
      // assert(idx == 0)
    }

    return ancestor;
  };

  /**
   * If we visualize the DOM as a traditional, downward branching,
   * left-to-right tree, return the rightmost leaf of the subtree rooted at
   * the given node.  (This means we return the given node if it's a leaf
   * itself.)
   *
   * @param aNode
   *        The node where to begin searching its subtree.
   */
  proto._getEndNode = function DI_GetEndNode(aNode)
  {
    let node = aNode;
    let nodeGroup = this._getDescendants(node);
    while (nodeGroup.length) {
      node = nodeGroup[nodeGroup.length - 1];
      nodeGroup = this._getDescendants(node);
    }
    return node;
  };

  proto._getDescendants = function DI_GetDescendants(aNode)
  {
    return DeepIterator.getDescendants(aNode, this.mShowAnonymousNodes,
                                       this.mShowSubDocuments);
  };
}

/**
 * Utility method similar to `inIDOMUtils.getChildrenForNode`, but capable of
 * seeing subdocuments, too.
 *
 * @param aNode
 *        The node whose descendants should be returned.
 * @param aShowAnons
 *        Whether 
 * @param aShowSubDocs
 *        Whether subdocuments should show up in the result array.
 * @return
 *        Array (not a NodeList) of the descendant nodes.
 *        NB: `getDescendants`'s return value's elements are untrusted.
 */
DeepIterator.getDescendants =
  function DI_GetDescendants(aNode, aShowAnons, aShowSubDocs)
{
  // NB: `kids_x_`'s elements are untrusted.
  let kids_x_ = Array.prototype.slice.call(
    DOMUtils.getChildrenForNode(aNode, aShowAnons), 0
  );

  if (aShowSubDocs) {
    try {
      if ("contentDocument" in aNode &&
          aNode.contentDocument instanceof Ci.nsIDOMDocument) {
        // NB: `aNode.contentDocument` is untrusted.
        let _x_doc = aNode.contentDocument;
        let parentNode = DOMUtils.getParentForNode(_x_doc, aShowAnons);
        if (parentNode && aNode == parentNode) {
          kids_x_.push(_x_doc);
        }
      }
    }
    catch (ex) {
      Logger.print("Encountered error trying to get subdocument:\n" + ex);
    }
  }

  return kids_x_;
};
