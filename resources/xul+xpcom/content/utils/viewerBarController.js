/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 *
 * Like `PanelManager`'s relationship to the panel binding, this class exists
 * to keep the viewer bar binding lightweight, because, you know, XBL.
 *
 * Another similarity to the panel manager is that there is one viewer bar
 * controller per window.  It is responsible for each panel's viewer bar, for
 * all panels in that window.
 *
 * TODO
 * - Absorb most of the business logic from the findbar?
 * - Hitting enter on a bad input value should give some sort of visual
 *   feedback besides just not loading anything.
 * - If a viewer bar is in editing mode, Ctrl+Tab and Shift+Ctrl+Tab should
 *   jump to its panel's sibling viewer bars, and put them in edit mode.
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.ViewerBarController = ViewerBarController;

const STYLE_SHEET_URI = "chrome://sepsis-inspector/skin/viewerbar.css";

// NB: These must stay in sync with the rules' actual positions in the style
// sheet.
const LABEL_TRANSFORM_RULE_INDEX = 0;
const DROP_MARKER_TRANSFORM_RULE_INDEX = 2;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { TheViewerRegistry } = require("registration/theViewerRegistry");

var { NotificationManager } = require("utils/notificationManager");
var { ViewerLoad } = require("utils/viewerLoad");

var { DOMUtils } = require("xpcom/domUtils");

//////////////////////////////////////////////////////////////////////////////

function ViewerBarController(aPanelWell, aPanelManager)
{
  this.mNotificationManager = new NotificationManager(this);

  this.mPanelWell = aPanelWell;
  this.mPanelManager = aPanelManager;

  // We need to be aware of any existing panels.
  let panels = [ aPanelManager.rootPanel ];
  while (panels.length) {
    let panel = panels.pop();

    let kids = aPanelManager.getChildPanels(panel);
    panels = panels.concat(kids);

    this._controlViewerBar(panel.viewerBar);
  }

  aPanelManager.addObserver("panelAdded", this.mNotificationManager);
}

{
  let proto = ViewerBarController.prototype;
  proto.mNotificationManager = null;
  proto.mShowPopupWhenOpened = null;
  proto.mPanelManager = null;
  proto.mPanelWell = null;
  proto.mStyleSheet = null;

  ////////////////////////////////////////////////////////////////////////////

  /**
   * We want Ctrl+Enter in the viewer bar to trigger "quick select": to pick
   * the best match based on the input.  Otherwise, simply hitting Enter will
   * only load a viewer if the input is an exact match.
   *
   * Instantiating `QuickSelectListener` will do that, by picking the first
   * item in the auto-complete popup.
   *
   * @param aViewerBar
   *        The viewer bar whose popup's first match should be quick selected.
   */
  proto.QuickSelectListener = ViewerBarController.QuickSelectListener =
    function VBC_QuickSelectListener(aViewerBar)
  {
    this.mViewerBar = aViewerBar;

    aViewerBar.input.popup.addEventListener("select", this, false);
    aViewerBar.input.popup.selectedIndex = 0;
  };

  {
    let qslproto = ViewerBarController.QuickSelectListener.prototype;
    qslproto.mViewerBar = null;

    qslproto.handleEvent = function VBC_QSL_HandleEvent(aEvent)
    {
      this.mViewerBar.input.popup.removeEventListener("select", this, false);
      this.mViewerBar.input.controller.handleEnter(true);
    };
  }

  ////////////////////////////////////////////////////////////////////////////

  proto.startEditing = function VBC_StartEditing(aViewerBar)
  {
    this._setUpStyle(aViewerBar);
    aViewerBar.addEventListener("transitionend", this, false);
    aViewerBar.editing = true;
  };

  proto.stopEditing = function VBC_StopEditing(aViewerBar)
  {
    aViewerBar.editing = false;
    aViewerBar.layer = aViewerBar.label;
  };

  ////////////////////////////////////////////////////////////////////////////
  //// Notifications

  proto.handleNotification = function VBC_HandleNotification(aName, aData)
  {
    switch (aName) {
      case "panelAdded":
        this._onPanelAdded(aData.panel);
      break;
      default:
        throw new Error("wat.  Notification: " + aName);
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// interface nsIDOMEventListener

  proto.handleEvent = function VBC_HandleEvent(aEvent)
  {
    switch (aEvent.type) {
      case "click":
        this._onClick(aEvent);
      break;
      case "command":
        this._onCommand(aEvent);
      break;
      case "keypress":
        this._onKeyPress(aEvent);
      break;
      case "blur":
        this._onBlur(aEvent);
      break;
      case "transitionend":
        this._onTransitionEnd(aEvent);
      break;
      default:
        throw new Error("wat.  Event: " + aEvent.type);
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  proto._onPanelAdded = function VBC_OnPanelAdded(aPanel)
  {
    // Make sure we're only messing with our own panel manager's panels, and
    // not its ancestors'.
    if (!this.mPanelManager.isManagingPanel(aPanel)) {
      return;
    }

    this._controlViewerBar(aPanel.viewerBar);
  };

  proto._controlViewerBar = function VBC_ControlViewerBar(aViewerBar)
  {
    aViewerBar.controller = this;
    aViewerBar.addEventListener("click", this, false);
    aViewerBar.addEventListener("command", this, false);
    aViewerBar.addEventListener("keypress", this, false);
    aViewerBar.addEventListener("blur", this, false);
  };

  /**
   * @param aEvent
   *        The "click" DOM event on the viewer bar.
   */
  proto._onClick = function VBC_OnClick(aEvent)
  {
    let viewerBar = aEvent.currentTarget;
    if (!viewerBar.editing) {
      // Ordinarily, we just put the viewer bar in editing mode, just like the
      // Firefox location bar.  Similarly, we have a dropmarker, and we want
      // to show the popup if the dropmarker is clicked.  What we're actually
      // going to do is show it if one of two things has happened:
      // - he or she clicked directly on the label layer's dropmarker, or
      // - he or she clicked where the input layer's dropmarker *would* be, if
      //   the input layer were visible.
      let rect = viewerBar.autoCompleteDropMarker.getBoundingClientRect();
      if (aEvent.originalTarget == viewerBar.labelDropMarker ||
          (rect.left < aEvent.clientX && aEvent.clientX < rect.right &&
           rect.top < aEvent.clientY && aEvent.clientY < rect.bottom)) {
        viewerBar.showPopupOnEditStart = true;
      }

      this.startEditing(viewerBar);
    }
  };

  proto._onKeyPress = function VBC_OnKeyPress(aEvent)
  {
    let viewerBar = aEvent.currentTarget;
    if (aEvent.keyCode == aEvent.DOM_VK_ESCAPE) {
      this.stopEditing(viewerBar);
    }
  };

  /**
   * We manually fire the "command" event in viewer bar binding in a couple
   * cases:
   * - When pressing Enter in the viewer input.
   * - When picking an auto-complete result, either by clicking it or
   *   selecting it with the keyboard and hitting Enter.  (From the
   *   ontextentered attribute on the input in the viewer bar binding.)
   *
   * @param aEvent
   *        The "command" DOM event.
   */
  proto._onCommand = function VBC_OnCommand(aEvent)
  {
    let viewerBar = aEvent.currentTarget;
    if (aEvent.originalTarget != viewerBar.input) {
      return;
    }

    let registeredUIDs = TheViewerRegistry.getUIDs();
    for (let i = 0, n = registeredUIDs.length; i < n; ++i) {
      let uid = registeredUIDs[i];
      let query = viewerBar.input.value.toLocaleLowerCase();
      if (query == TheViewerRegistry.getViewerName(uid).toLocaleLowerCase()) {
        ViewerLoad.inspectParentTarget(viewerBar.panel, uid);
        this.stopEditing(viewerBar);
        return;
      }
    }
  };

  proto._onBlur = function VBC_OnBlur(aEvent)
  {
    // The event will fire on the viewer textbox's anonymous html:input
    // element, provided by the xul:textbox binding.
    let doc = aEvent.originalTarget.ownerDocument;
    let input = doc.getBindingParent(aEvent.originalTarget);
    let viewerBar = doc.getBindingParent(input);
    this.stopEditing(viewerBar);
  };

  proto._setUpStyle = function VBC_SetUpStyle(aViewerBar)
  {
    let styleSheet = this._getTransformStyleSheet(aViewerBar);

    let labelTransformRule = styleSheet.cssRules[LABEL_TRANSFORM_RULE_INDEX];
    let labelRect = aViewerBar.label.getBoundingClientRect();
    let inputContentRect =
      aViewerBar.input.editor.rootElement.getBoundingClientRect();

    // XXX RTL.
    labelTransformRule.style.transform =
      "translateX(-" + (labelRect.left - inputContentRect.left) + "px)";

    let dropMarkerTransformRule =
      styleSheet.cssRules[DROP_MARKER_TRANSFORM_RULE_INDEX];
    let labelMarkerRect =
      aViewerBar.labelDropMarker.getBoundingClientRect();
    let autoCompleteMarkerRect =
      aViewerBar.autoCompleteDropMarker.getBoundingClientRect();

    dropMarkerTransformRule.style.transform =
      "translateX(" +
      (autoCompleteMarkerRect.left - labelMarkerRect.left +
       Math.abs(autoCompleteMarkerRect.width - labelMarkerRect.width) / 2) +
      "px)";

    aViewerBar.ownerDocument.mozSetImageElement(
      "viewer-bar-input", aViewerBar.input
    );
  };

  proto._getTransformStyleSheet = function
  VBC_GetTransformStyleSheet(aViewerBar)
  {
    if (this.mStyleSheet) {
      return this.mStyleSheet;
    }

    // We need to get a handle on the rule where the label's transform is
    // defined; we want to tweak its translation value so that the label
    // lines up with the input's text at the end of the translation.
    //
    // The element we pass to `getCSSStyleRules` here could be anything,
    // really, so long as it's styled in the viewer bar style sheet.
    {
      let rules = DOMUtils.getCSSStyleRules(aViewerBar.label);
      // NB: `getCSSStyleRules` returns `nsISupportsArray`, which is not
      // array-like.
      for (let i = 0, n = rules.Count(); i < n; ++i) {
        let rule = rules.GetElementAt(i);
        if (rule.parentStyleSheet.href == STYLE_SHEET_URI) {
          // Found it.
          this.mStyleSheet = rule.parentStyleSheet;
          return this.mStyleSheet;
        }
      }
    }

    // We should have found it by now.
    throw new Error("Couldn't find viewer bar style sheet");
  };

  proto._onTransitionEnd = function VBC_OnTransitionEnd(aEvent)
  {
    let viewerBar = aEvent.target;
    viewerBar.removeEventListener("transitionend", this, false);
    viewerBar.layer = viewerBar.input;
    viewerBar.input.focus();
    viewerBar.input.select();

    if (viewerBar.showPopupOnEditStart) {
      viewerBar.input.showHistoryPopup();
      viewerBar.showPopupOnEditStart = false;
    }
  };
}
