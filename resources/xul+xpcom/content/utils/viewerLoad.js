/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 *
 * Represents an attempt to load a viewer in a panel, and various static
 * methods for picking a suitable viewer.
 *
 * `ViewerLoad` is responsible for the constraint that the subjects of a
 * panel's linked children are derived from that parent panel's current
 * target (by way of `ViewerLoad.inspectParentTarget`).
 *
 * We need to be able to handle a few use cases:
 * A. When the parent's target changes, we want to be able to say "use the
 *    parent's target".
 * B. When the Inspector first opens, we also want to say "... with this UID,
 *    if you can".
 * C. When the viewer is selected, we want to be able to say "find an adapter
 *    where we can use this UID, and load that viewer, inspecting its
 *    translated target".
 *
 * The way we'll do this is we'll have `ViewerLoad` instances and a static
 * `inspectParentTarget` method that acts as a nice way to create them.  Given
 * this `ViewerLoad` can afford to be fairly unforgiving.
 *
 * `ViewerLoad` will *always* cause the panel's subject to end up as the one
 * given (albeit, possibly after an asynchronous load), or throw.  And if a
 * UID is specified, it *must* be able to handle the given subject; otherwise,
 * again, we throw.
 *
 * `ViewerLoad.inspectParentTarget` will look at the parent panel's target to
 * derive a subject for the given panel itself and will contain the viewer
 * selection logic.  When treating the given UID as a hint, if that viewer is
 * unable to inspect one of the adapter targets (translated from the parent
 * panel's target), it will pick some other UID that can.  If we're not
 * treating it as a hint, we will throw.
 *
 * TODO:
 *  - The `loadStarting` and `loadFinished` methods should go away, and we
 *    should use the observe/notify system for this.  We'd send out a
 *    "loadStarting" notification and the "viewerLoaded" notification that the
 *    panel manager is responsible for sending out at the moment, and the
 *    panel manager itself would observe them and act accordingly.  The only
 *    reason we aren't already doing this is because we don't want to call
 *    `changePanelSubject` from within the panel manager's response to the
 *    "viewerLoaded" notification; we want "subjectChanged" to go out *after*
 *    the "viewerLoaded" notification has reached all its observers.
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.ViewerLoad = ViewerLoad;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { Logger } = require("utils/logger");

var { TheViewerRegistry } = require("registration/theViewerRegistry");

//////////////////////////////////////////////////////////////////////////////

/**
 * Inspect the given object in the given panel, or put the panel in "Inspect
 * Application" mode.
 *
 * You almost definitely don't want to use this directly.  Instead, use
 * `ViewerLoad.inspectParentTarget`, which you should find to be much nicer to
 * work with and which will return a `ViewerLoad` object.
 *
 * When a UID is specified that differs from the UID of the panel's current
 * viewer, it will cause a new viewer to load.  NB: The loads are
 * asynchronous.  Callers should not rely on the ability to poke at the viewer
 * or any of its properties immediately after creating a `ViewerLoad` instance
 * without first verifying that that the load is not pending.  Interested
 * parties can check for this by passing the panel in question to the panel
 * manager's `isLoadPending` method.  If there's a pending load, the panel's
 * `viewer` property will not be immediately available.  Anyone can be alerted
 * to the state where this is no longer the case by participing in the
 * observe/notify system and watching for the "viewerLoaded" notification.
 *
 * @param aPanel
 * @param aSubject
 *        Either a mirror for the object that should be inspected in the
 *        panel, or an app-subject synthetic.
 * @param aUID [optional]
 *        The UID of the viewer to inspect the subject with.  If not provided,
 *        the panel's current viewer is to be used.  This is not optional if
 *        there is no viewer currently loaded in the panel (e.g., when the
 *        Inspector is first opened.)
 * @throws
 *        Error if the subject cannot be inspected in the panel.  (E.g., the
 *        app subject synthetic for a non-root panel; or null; or some subject
 *        for which there are no compatible viewers registered.)
 *
 *        Error if a UID is specified and the given subject does not pass the
 *        filter, or if the viewer does not have `canInspectApplication` set
 *        to true in its info file when the app subject synthetic is used.
 *        NB: This includes calls that do not specify a UID and where the
 *        given subject cannot be inspected with the panel's currrent viewer.
 * @see utils/notificationManager.js
 * @see ViewerRegistry.prototype.viewerSupportsSubject
 * @constructor
 */
function ViewerLoad(aPanel, aSubject, aUID)
{
  aPanel.panelManager.validateSubject(aPanel, aSubject);

  this.panel = aPanel;
  this.subject = aSubject;

  this.uid = (aUID === undefined) ?
    aPanel.viewerUID :
    aUID;

  this.noLoad = aPanel.viewer && this.uid == aPanel.viewerUID
  if (this.noLoad) {
    aPanel.subject = aSubject;
  }
  else {
    this._loadViewerDocument(aPanel);
  }
}

{
  let proto = ViewerLoad.prototype;
  proto.mInitializing = null;

  proto.noLoad = null;
  proto.panel = null;
  proto.subject = null;
  proto.uid = null
  proto.viewer = null;

  proto.__defineGetter__("initializing", function VL_Initializing_Get()
  {
    return this.mInitializing;
  });

  /**
   * NB: If this is hard to understand, it's because we try to do the thing
   * that is most useful to the user, rather than using the laziest heuristic.
   *
   * Case breakdown and preferred order for viewer selection:
   *   No UID specified
   *     A. Use the UID of the currently loaded viewer as a hint, and
   *        proceed to D through F to pick an appropriate adapter, or failing
   *        that, G through I to pick both an adapter and a UID.
   *
   *     So B and C will be used in the event that the UID wasn't specified
   *     and there's no viewer currently loaded in the panel.
   *
   *     B. Use the passthrough adapter, with any UID, if possible, or
   *     C. Use some other adapter, with any UID.
   *
   *   UID is specified
   *     D. Use the current adapter if it yields the specified UID, or
   *     E. Use the passthrough if it yields the specified UID, if possible
   *     F. Arbitrary adapter that yields the specified UID, if possible
   *
   *     If we reach this point, we'll need to check if the UID is meant to
   *     be only a hint, and throw if not (see above), instead of proceeding
   *     to G through I.
   *
   *     G. Current adapter, yielding any other UID, if possible, or
   *     H. Passthrough adapter, yielding any other UID, if possible, or
   *     I. Arbitrary adapter, yielding any other UID
   *
   *     And for all of the above except for D, E, and F, try to use a
   *     different UID from the parent's viewer, if possible.  Whenever
   *     picking an arbitrary UID (such as with G, H, and I), we favor the
   *     ones that are not marked as low priority.
   *
   * @param aPanel
   * @param aUID [optional]
   * @param aIsHint [optional]
   *        NB: This only has an effect if a UID is specified.
   * @return
   *        A `ViewerLoad` object.
   * @throws
   *        Error if a UID is specified and `aIsHint` is not true and the
   *        viewer cannot be used to inspect a subject derived from the parent
   *        panel's target.
   */
  ViewerLoad.inspectParentTarget =
    function VL_InspectParentTarget(aPanel, aUID, aIsHint)
  {
    let uid, adapter;
    let relation = aPanel.getRelation();
    if (aUID !== undefined) {
      // For D, E, or F (or A in a recursive call; see below).
      // NB: This may return undefined.
      adapter = this._selectAdapter(aPanel, relation, aUID);
      if (adapter === undefined) {
        if (!aIsHint) {
          throw new Error("Can't use UID for panel target: " + aUID);
        }
      }
      else {
        uid = aUID;
      }
    }
    else if (aPanel.viewer) {
      // For A, do a recursive call.  Recursion terminates at max depth: 2.
      return ViewerLoad.inspectParentTarget(aPanel, aPanel.viewerUID, true);
    }
    // Else, B, C, G, H or I will be tried below together.

    // NB: We check explicitly for undefined, because `!adapter` will do the
    // wrong thing if `adapter` is the passthrough (null).
    // NB: We're checking `adapter` instead of sticking this in an else clause
    // hanging off the the above checks because the `_selectAdapter` call
    // above could have returned undefined.  In that case, if the UID was only
    // meant to be a hint, we want to give it the same treatment as if no UID
    // had been provided.
    if (adapter === undefined) {
      // Need to pick an adapter and UID for B, C, G, H, and I.
      adapter = this._selectAdapter(aPanel, relation);
      // NB: Not `!adapter`, for the same reason as above.
      if (adapter === undefined) {
        throw new Error(
          "No viewers or adapters support the parent's target."
        );
      }

      // assert(relation.getUIDsForAdapter(adapter).length > 0);
      let candidateUIDs = relation.getUIDsForAdapter(adapter);

      let narrowerChoices = candidateUIDs.filter(function priorityFilter(aUID)
      {
        return TheViewerRegistry.getViewerPriority(aUID) >= 0;
      });

      if (narrowerChoices.length) {
        candidateUIDs = narrowerChoices;
      }

      // Try to use a UID different from the viewer in the parent panel.
      let parentPanel = aPanel.panelManager.getParentPanel(aPanel, true);
      let parentUID = parentPanel && parentPanel.viewerUID;
      uid = (candidateUIDs[0] == parentUID && candidateUIDs.length > 1) ?
        candidateUIDs[1] :
        candidateUIDs[0];
    }

    aPanel.adapter = adapter;
    return new ViewerLoad(aPanel, relation.getTargetForAdapter(adapter), uid);
  };

  ////////////////////////////////////////////////////////////////////////////
  //// Interface nsIDOMEventListener

  proto.handleEvent = function VL_HandleEvent(aEvent)
  {
    switch (aEvent.type) {
      case "load":
        this._onLoad(this.panel, this.uid);
      break;
      default:
        throw new Error("wat.  Event: " + aEvent.type);
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// Internal methods

  proto._loadViewerDocument = function VL_LoadViewerDocument(aPanel)
  {
    let uri = TheViewerRegistry.getViewerURI(this.uid);

    aPanel.panelManager.loadStarting(this);
    aPanel.browser.addEventListener("load", this, true);
    aPanel.browser.setAttribute("src", uri);
  };

  /**
   * Respond to the "load" DOM event produced by a viewer document being
   * loaded in the panel.
   *
   * @param aPanel
   *        The panel where the viewer document has just loaded.
   * @param aUID
   *        The UID of the viewer whose viewer document has just loaded.
   */
  proto._onLoad = function VL_OnLoad(aPanel, aUID)
  {
    aPanel.browser.removeEventListener("load", this, true);

    this.mInitializing = true;

    // During initialization, the viewer can manually set the panel's `viewer`
    // property itself.  In that case, we treat that as the viewer, rather
    // than the `initializeViewer` return value.  If that happens, the panel
    // will call the panel manager's `setViewerForPanel` method, which
    // captures the viewer-set `viewer` there.
    //
    // Note that setting the panel's `viewer` is only allowed during the
    // narrow window delimited by our assignments to `mInitializing` above and
    // below.  See `PanelManager.prototype.setViewerForPanel`.
    let initResult;
    try {
      initResult = TheViewerRegistry.initializeViewer(aUID, aPanel);
    }
    catch (ex) {
      Logger.print(ex);
    }

    this.mInitializing = false;

    if (!this.viewer) {
      this.viewer = initResult;
    }

    aPanel.panelManager.loadFinished(this);
  };

  /**
   * @param aPanel
   * @param aRelation
   * @param aUID [optional]
   *        If specified, only the adapters that will allow the viewer
   *        corresponding to the given UID will be considered.
   * @return
   *        An adapter for use in translating the an object into an
   *        appropriate subject for the given panel.
   */
  ViewerLoad._selectAdapter =
    function VL_SelectAdapter(aPanel, aRelation, aUID)
  {
    let candidateAdapters = (aUID === undefined) ?
      aRelation.getAdapters() :
      aRelation.getAdaptersForUID(aUID);

    if (aPanel.viewer && candidateAdapters.indexOf(aPanel.adapter) >= 0) {
      return aPanel.adapter;
    }

    // NB: This prefers the passthrough adapter over the others, because if
    // the passthrough adapter is available, it will be at the beginning of
    // the list.
    return (candidateAdapters.length) ?
      candidateAdapters[0] :
      undefined;
  };
}
