/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.StubTreeView = StubTreeView;

/////////////////////////////////////////////////////////////////////////////
//// Imported symbols

Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");

/////////////////////////////////////////////////////////////////////////////

/**
 * A stub implementation of nsITreeView.
 */
function StubTreeView() { }

{
  let proto = StubTreeView.prototype;

  let stub = function STV_Stub() { };
  let falseStub = function STV_FalseStub() { return false; };
  let emptyStringStub = function STV_EmptyStringStub() { return ""; };

  proto.QueryInterface = XPCOMUtils.generateQI([ Components.interfaces.nsITreeView ]);

  proto.rowCount = 0;
  proto.selection = null;

  proto.getParentIndex = function STV_GetParentIndex()
  {
    return -1;
  };

  proto.getLevel = function STV_GetLevel()
  {
    return 0;
  };

  proto.cycleHeader =
  proto.cycleCell =
  proto.drop =
  proto.getCellValue =
  proto.performAction =
  proto.performActionOnRow =
  proto.performActionOnCell =
  proto.selectionChanged =
  proto.setCellValue =
  proto.setCellText =
  proto.setTree =
  proto.toggleOpenState = stub;

  proto.canDrop =
  proto.hasNextSibling =
  proto.isContainer =
  proto.isContainerEmpty =
  proto.isContainerOpen =
  proto.isEditable =
  proto.isSelectable =
  proto.isSeparator =
  proto.isSorted = falseStub;

  proto.getCellProperties =
  proto.getCellText =
  proto.getColumnProperties =
  proto.getImageSrc =
  proto.getRowProperties = emptyStringStub;
}
