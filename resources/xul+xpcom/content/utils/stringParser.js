/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * An online parser for ES5 string literals.
 *
 * (NB: This file originates from
 *   https://hg.mozilla.org/users/sevenspade_gmail.com/stringparser/
 *
 * Consider changing the upstream source instead of local copies.)
 *
 * Where the SpiderMonkey implementation differs from the spec, we emulate the
 * former.  Specifically, the SpiderMonkey parser is known to deviate from
 * section B.1.2 in two ways.
 *
 * ES5 permits octal escape sequences in string literals, matching the
 * productions:
 *
 *   OctalEscapeSequence :: OctalDigit [lookahead ∉ DecimalDigit]
 *                          ZeroToThree OctalDigit [lookahead ∉ DecimalDigit]
 *                          FourToSeven OctalDigit
 *                          ZeroToThree OctalDigit OctalDigit
 *
 * The SpiderMonkey parser recognizes octal escape sequences in a way similar
 * to the first two rules, but allows for the lookahead to be a DecimalDigit,
 * so long as it's not an OctalDigit.  (I.e., the lookahead is allowed to be 8
 * or 9.  This is actually a sane approach, and it seems like an error in the
 * spec.)  This disparity only manifests itself outside of strict mode.
 *
 * Relatedly, the spec says that the syntax and semantics extensions for octal
 * support in string literals are not allowed in strict mode.  But in fact,
 * SpiderMonkey uses the same parser for both, emitting a SyntaxError in
 * strict mode when the OctalEscapeSequence production is matched.
 * Confusingly, it uses a spec-compliant no-DecimalDigit lookahead constraint
 * (rather than no-OctalDigit, as above).
 *
 * I say that this is a deviation because, given the spec as written, a
 * conforming implementation would be expected to use the grammar from
 * section 7.8.4, sans octal extensions.  This means it would appear to
 * simply ignore escape sequences which match the productions above, treating
 * the first character of the escape sequence the same way any other
 * NonEscapeCharacter is treated, except for sequences which start with 0,
 * which would be expected to yield a NUL character at that position.
 *
 * Prior to Gecko 7, the first character of unterminated hex and Unicode
 * escape sequences were treated like NonEscapeCharacters.  Now they throw.
 * The spec is not clear about which is correct behavior.  (We follow current
 * Gecko and throw.)
 *
 * Fun fact: You don't need a CFG to describe string literals; they're
 * regular.  If you're a hacker without an academic background, or you just
 * don't know what this means, learn about it!  I suggest Introduction to
 * Automata Theory, Languages, and Computation, 3rd ed. by Hopcroft, Motwani,
 * and Ullman.  It's useful for following along below.  It's also ~broadly
 * applicable, plus this stuff is just neat.
 *
 * TODO:
 *   Support < Gecko 7 and detect what we're dealing with at instantiation?
 *
 *   Clarity.  StringParser.uneval exists.  Shouldn't we think of StringParser
 *   as "eval for string literals", and even describe it as such?  Are there
 *   any cases where this isn't accurate?  If so, we should fix it.
 *
 * XXX This implementation actually consumes more LOC than the one in
 * TokenStream.cpp.  WTF.  --crussell
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.StringParser = StringParser;

//////////////////////////////////////////////////////////////////////////////

function StringParser(aStringLiteral)
{
  this.mOffset = -1;
  this.mCompleteSequences = [];
  this.state = StringParser.START_STATE;

  if (aStringLiteral) {
    // This will throw if aStringLiteral is not a string and neither toString
    // nor valueOf return a primitive value.  Don't suppress the exception.
    let literal = String(aStringLiteral);
    for (let i = 0, n = literal.length; i < n; ++i) {
      this.feed(literal[i]);
    }
  }
}

StringParser.START_STATE = 0;
StringParser.READY_STATE = 1;
StringParser.DEAD_STATE = 2;
StringParser.ACCEPTING_STATE = 3;

StringParser.ESCAPE_START_STATE = 4;

StringParser.HEX_START_STATE = 5;
StringParser.HEX_AFTER_FIRST_STATE = 6;

StringParser.UNICODE_START_STATE = 7;
StringParser.UNICODE_AFTER_FIRST_STATE = 8;
StringParser.UNICODE_AFTER_SECOND_STATE = 9;
StringParser.UNICODE_AFTER_THIRD_STATE = 10;

// On a small piece of paper, I have a concise explanation of the subset
// construction I use, but I can't adequately reproduce it here, because in
// 2012 we're still almost exclusively using line editors for writing (and
// commenting) code.
//
// But basically, it goes like this: all octal escape sequences will take the
// form of one of:
//
//   A: [01234567]
//   B: [0123][01234567]
//   C: [0123][01234567][01234567]
//   D: [4567][01234567]
//
// ... prepended by a backslash, and where the matched part consists of the
// entire escape sequence.  (I.e., We can see that B is a prefix of C, but if
// the escape sequence is, for example, `\00`, and we know that this is the
// entire[1] escape sequence, it just matches B, not B and C.)
//
// OCTAL_A_D_STATE is the state where the corresponding NFA could be in either
// state A or state D, and so on.
//
// 1. Since we need a lookahead to discriminate between cases, we can't
//    *really* know that this is the entire escape sequence until we either
//    read the next character in the string or read the close quote.
//
//    You'll notice that the `value` getter prefers the "shortest" of the
//    cases when it's ambiguous.  (I.e., if we're in state A|B|C, `this.value`
//    will parse the sequence as if it's known that it's actually going to be
//    A.  Likewise, if we're in state B|C, it parses the sequence as if it's
//    known that it will be B, and so on.)  This decision is tentative, and
//    I'm up for changing it or allowing a configuration parameter/property if
//    any other consumers turn up, so don't rely upon it just yet.
//
//                                                                  --crussell
StringParser.OCTAL_A_D_STATE = 11;
StringParser.OCTAL_A_B_C_STATE = 12;
StringParser.OCTAL_B_C_STATE = 13;

StringParser.lineTerminators = [
  "\u000A", // Line feed
  "\u000D", // Carriage return
  "\u2028", // Line separator
  "\u2029"  // Paragraph separator
];

{
  let v = Object.create(null);
  // Hey, Ken.
  v["\\"] = "\\";
  v["\'"] = "\'";
  v["\""] = "\"";
  v["b"] = "\b";
  v["f"] = "\f";
  v["n"] = "\n";
  v["r"] = "\r";
  v["t"] = "\t";
  v["v"] = "\v";

  StringParser.specialCharacters = v;
}

{
  let proto = StringParser.prototype;
  proto.mCompleteSequences = null;
  proto.mOffset = null;
  proto.mQuoteType = null;
  proto.mValue = null;

  proto.unterminatedSequence = null;
  proto.state = null;

  proto.__defineGetter__("value", function SP_Value_Get()
  {
    if (this.state == StringParser.DEAD_STATE) {
      throw new Error("StringParser.DEAD_STATE");
    }

    // Check escapeSequence and work with what we've got.
    var escapedValue = "";
    var c = this.unterminatedSequence && this.unterminatedSequence.charAt(0);
    if (this._isDigit(c, 8)) {
      escapedValue =
        String.fromCharCode(parseInt(this.unterminatedSequence, 8));
    }
    else if ((c == "x" && this.unterminatedSequence.length == 3) ||
             (c == "u" && this.unterminatedSequence.length == 5)) {
      let hexString = this.unterminatedSequence.substring(1);
      escapedValue = String.fromCharCode(parseInt(hexString, 16));
    }
    else if (c) {
      let type = "";
      if (c == "x") {
        type = "hexadecimal";
      }
      else if (c == "u") {
        type = "Unicode";
      }
      if (type) {
        throw new SyntaxError("malformed " + type +
                              " character escape sequence");
      }
    }

    return this.mValue + escapedValue;
  });

  proto.__defineGetter__("escapeSequences", function SP_EscapeSequences_Get()
  {
    // Explicitly check for non-null instead of `!this.unterminatedSequence`,
    // because it may be a zero-length string, and those are falsy.
    if (this.unterminatedSequence != null) {
      return this.mCompleteSequences.concat({
        index: this.mOffset,
        escapeSequence: this.unterminatedSequence
      });
    }
    return this.mCompleteSequences.slice(0);
  });

  proto.getKnownValue = function SP_GetKnownValue()
  {
    return this.mValue;
  };

  proto.feed = function SP_Feed(aCharacter)
  {
    // String can throw TypeError.  Don't suppress it.
    var c = String(aCharacter);
    if (c.length != 1) {
      this.state = StringParser.DEAD_STATE;
      throw new TypeError("Expected a character, got a string of length " +
                          c.length + ": " + c);
    }

    switch (this.state) {
      case StringParser.START_STATE:
        if (c != "'" && c != "\"") {
          this.state = StringParser.DEAD_STATE;
          throw new SyntaxError("String literal must start with quote");
        }
        this.mQuoteType = c;
        this.mValue = "";
        this.state = StringParser.READY_STATE;
      break;

      case StringParser.READY_STATE:
        ++this.mOffset;

        if (c == this.mQuoteType) {
          this.state = StringParser.ACCEPTING_STATE;
        }
        else if (c == "\\") {
          this.unterminatedSequence = "";
          this.state = StringParser.ESCAPE_START_STATE;
        }
        else {
          this.mValue += c;
        }
      break;

      case StringParser.ESCAPE_START_STATE:
        if (StringParser.lineTerminators.indexOf(c) >= 0) {
          // "The SV of LineContinuation :: \ LineTerminatorSequence is the
          // empty character sequence", from ECMA-262, ed. 5.1.
          this._storeSequence(c);
          this.state = StringParser.READY_STATE;
        }
        else if (c in StringParser.specialCharacters) {
          this.mValue += StringParser.specialCharacters[c];
          this._storeSequence(c);
          this.state = StringParser.READY_STATE;
        }
        else if (this._isDigit(c, 8)) {
          if (c < "4") {
            this._consumeDigit(c, 8, StringParser.OCTAL_A_B_C_STATE);
          }
          else {
            this._consumeDigit(c, 8, StringParser.OCTAL_A_D_STATE);
          }
        }
        else if (c == "x") {
          this.unterminatedSequence += c;
          this.state = StringParser.HEX_START_STATE;
        }
        else if (c == "u") {
          this.unterminatedSequence += c;
          this.state = StringParser.UNICODE_START_STATE;
        }
        else {
          // "The CV of NonEscapeCharacter :: SourceCharacter but not one of
          // EscapeCharacter or LineTerminator is the SourceCharacter
          // character itself", from ECMA-262, ed. 5.1.
          this.mValue += c;
          this._storeSequence(c);
          this.state = StringParser.READY_STATE;
        }
      break;

      case StringParser.HEX_START_STATE:
        this._consumeDigit(c, 16, StringParser.HEX_AFTER_FIRST_STATE);
      break;

      case StringParser.UNICODE_START_STATE:
        this._consumeDigit(c, 16, StringParser.UNICODE_AFTER_FIRST_STATE);
      break;

      case StringParser.UNICODE_AFTER_FIRST_STATE:
        this._consumeDigit(c, 16, StringParser.UNICODE_AFTER_SECOND_STATE);
      break;

      case StringParser.UNICODE_AFTER_SECOND_STATE:
        this._consumeDigit(c, 16, StringParser.UNICODE_AFTER_THIRD_STATE);
      break;

      case StringParser.HEX_AFTER_FIRST_STATE:
      case StringParser.UNICODE_AFTER_THIRD_STATE:
        this._consumeDigit(c, 16, StringParser.READY_STATE);
      break;

      case StringParser.OCTAL_A_B_C_STATE:
        this._consumeDigit(c, 8, StringParser.OCTAL_B_C_STATE);
      break;

      case StringParser.OCTAL_A_D_STATE:
      case StringParser.OCTAL_B_C_STATE:
        this._consumeDigit(c, 8, StringParser.READY_STATE);
      break;

      case StringParser.ACCEPTING_STATE:
      case StringParser.DEAD_STATE:
        let state = (this.state == StringParser.DEAD_STATE) ?
          "DEAD" :
          "ACCEPTING";
        throw new Error("Cannot feed characters in StringParser." + state +
                        "_STATE");
      break;

      default:
        throw new Error("Unexpected state: " + this.state);
      break;
    }
  };

  proto._isDigit = function SP_IsDigit(aCharacter, aRadix)
  {
    return !isNaN(parseInt(aCharacter, aRadix));
  };

  proto._consumeDigit = function SP_ConsumeDigit(aCharacter, aRadix, aState)
  {
    if (this._isDigit(aCharacter, aRadix)) {
      this.unterminatedSequence += aCharacter;

      if (aState == StringParser.READY_STATE) {
        this.mValue = this.value;
        this._storeSequence();
      }

      this.state = aState;
    }
    else {
      // If the escape sequence we've been fed so far is an octal, we can
      // work with it.
      try {
        this.mValue = this.value;
      }
      catch (ex) {
        // It must have been a partial hex or Unicode escape sequence.  Mark
        // ourselves as dead.
        this.state = StringParser.DEAD_STATE;
        throw ex;
      }

      // It was an octal.  Recursively handle aCharacter.  (Recursion from
      // this terminates at a maximum depth of 2; i.e., our `feed` call here
      // will not itself perform another recursive call.)
      this._storeSequence();
      this.state = StringParser.READY_STATE;
      this.feed(aCharacter);
    }
  };

  /**
   * Finalize the current escape sequence by adding it to the list of complete
   * sequences and eliminating its presence in `this.unterminatedSequence`.
   *
   * @param aSequence [optional]
   *        If provided, `aSequence` will be the sequence stored, rather than
   *        trying to pull it from `this.unterminatedSequence`.  (This is
   *        simply for callers' convenience of storing single-character
   *        sequences.)
   */
  proto._storeSequence = function SP_StoreSequence(aSequence)
  {
    // `aSequence` XOR `this.unterminatedSequence`
    // assert((aSequence === undefined) !=
    //        (this.unterminatedSequence === null))
    this.mCompleteSequences.push({
      index: this.mOffset,
      escapeSequence: aSequence || this.unterminatedSequence
    });
    this.unterminatedSequence = null;
  };

  proto.reset = function SP_Reset()
  {
    delete this.mQuoteType;
    delete this.mValue;
    StringParser.call(this);
  };
}

/**
 * Get a string which when parsed as a string literal has a value equal to
 * that of the given string.
 *
 * JSON.stringify and the old String.prototype.quote are similar, but they
 * don't allow you to specify the quote type or substitute your own escape
 * sequences.
 *
 * @param aString
 *        The string to be quoted.
 * @param aQuoteType [optional]
 *        A single or a double quote.  Double quote is default.
 * @param aEscapeSequences [optional]
 *        An array where each element has a number-valued property called
 *        `index` and a string-valued property called `escapeSequence`.
 *        During the serialization process, when an index is reached that
 *        matches an element's `index` property, the corresponding
 *        `escapeSequence` is substituted, rather than what the default
 *        serialization would have output.  `escapeSequence` may be specified
 *        with or without the leading backslash.
 * @return A properly escaped string wrapped in quotes.
 */
StringParser.uneval =
  function SP_Uneval(aString, aQuoteType, aEscapeSequences)
{
  if (aQuoteType != "'" && aQuoteType != "\"" && aQuoteType !== undefined) {
    throw new Error(aQuoteType + " is not a valid string literal quote");
  }

  var comparator = function SP_Uneval_Sort_Comparator(a, b) {
    if (isNaN(a.index) || isNaN(b.index)) {
      throw new TypeError("index is not a number");
    }

    if (Number(a.index) < Number(b.index)) {
      return a;
    }
    if (Number(a.index) > Number(b.index)) {
      return b;
    }

    throw new Error("escape sequence elements must have unique indexes");
  };

  var sequences =
    aEscapeSequences && aEscapeSequences.slice(0).sort(comparator);
  var sequencesPosition = 0;
  var result = aQuoteType || "\"";

  for (let i = 0, n = aString.length; i < n; ++i) {
    let c = aString[i];

    if (sequences && sequencesPosition < sequences.length &&
        sequences[sequencesPosition].index == i) {
      let sequenceString = sequences[sequencesPosition].escapeSequence;

      // We're going to parse sequenceString in order to verify that it parses
      // to the character it's supposed to be (`c`).

      // Escaping intricacies.  Work it out.
      if (sequenceString.charAt(0) == "\\") {
        sequenceString = sequenceString.substring(1);
      }
      let quoted = JSON.stringify(sequenceString);
      quoted = "\"\\" + quoted.substring(1);

      let parser;
      try {
        parser = new StringParser(quoted);
      }
      catch (ex) {
        throw new SyntaxError("invalid escape sequence " + quoted);
      }

      if (parser.value != c) {
        throw new Error("escape sequence " + quoted +
                        " doesn't match character " + JSON.stringify(c) +
                        " at position " + i);
      }

      result += "\\" + sequenceString;

      ++sequencesPosition;
    }
    else {
      // Relevant parts of ECMA-262, ed 5.1, section 7.8.4:
      // StringLiteral ::
      //   " DoubleStringCharacters[opt] "
      //   ' SingleStringCharacters[opt] '
      // [...]
      // DoubleStringCharacter ::
      //   SourceCharacter but not double-quote " or backslash \ or
      //     LineTerminator
      //   [...]
      // SingleStringCharacter ::
      //   SourceCharacter but not single-quote ' or backslash \ or
      //     LineTerminator
      //   [...]
      switch (c) {
        // Backslash
        case "\\":
          result += "\\\\";
        break;

        // <LineTerminator>
        case "\n":
          result += "\\n";
        break;
        case "\r":
          result += "\\r";
        break;
        case "\u2028":
          result += "\\u2028";
        break;
        case "\u2029":
          result += "\\u2029";
        break;

        // Single/double quote
        case "\"":
        case "'":
          // If `c` is the same quote type as the open quote, it needs to be
          // escaped.
          if (c == result[0]) {
            result += "\\";
          }
          // Fall through.

        // Other <SourceCharacter>s
        default:
          result += c;
        break;
      }
    }
  }

  // Append the close quote.
  result += result[0];

  return result;
};
