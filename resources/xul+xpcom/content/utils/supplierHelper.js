/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * For properly implementing a synthetic supplier
 *
 * To use `SupplierHelper`, implement your supplier like this:
 *
 *   FooSupplier.create = function (aPanel)
 *   {
 *     const yourSID = ;// synthetic SID, e.g., "org.example.synthetic.foo"
 *     let helper = new SupplierHelper(yourSID);
 *     return helper.createOrGetSupplier(aPanel);
 *   };
 *
 * The supplier can then be used from within your viewer, e.g.:
 *
 *   var { FooSupplier } = require("./FooSupplier.js");
 *
 *   function FooViewer(aPanel)
 *   {
 *     // ... viewer implementation code ...
 *     this.supplier = FooSupplier.create(aPanel);
 *     // ... more viewer implementation code ...
 *   }
 *
 * If you want to be able to hang on the the `SupplierHelper` instance, pass
 * another argument `true` to `createOrGetSupplier` after the SID, and make
 * sure your supplier instances implement an `initializeSupplier` method:
 *
 *   initializeWithHelper(aHelper)
 *     The `SupplierHelper` instance passed as the first argument will be the
 *     one that was custom made for this supplier instance.  You can also do
 *     any intializating that you would normally do in the supplier
 *     constructor.
 *
 * It's important to remember that when you're using a supplier from a viewer,
 * the supplier outlasts the lifetime of the viewer and may be shared across
 * multiple viewers.  For example, if you have the Inspector open in a two-
 * panel layout and both viewers from each panel use `FooSupplier`, then they
 * will get the same supplier instance.  For this reason, you should not
 * implement a supplier as if it's tied to a particular viewer, and you should
 * avoid storing viewer objects in a supplier.
 *
 * NB: Suppliers that specialize in multiple synthetics are allowed by the
 * Inspector framework, but they are not supported by `SupplierHelper`.  If
 * you want a supplier that deals with multiple synthetics, you will have to
 * handle the supplier implementation without the aid of `SupplierHelper`.)
 *
 * @see registry/theSupplierRegistry
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.SupplierHelper = SupplierHelper;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { TheSupplierRegistry } = require("registration/theSupplierRegistry");

var { ViewerTracker } = require("utils/viewerTracker");

//////////////////////////////////////////////////////////////////////////////

function SupplierHelper(aSID)
{
  this.mSID = aSID;
}

{
  let $proto = SupplierHelper.prototype;

  $proto.mSID = null;
  $proto.mViewerTracker = null;

  $proto.createOrGetSupplier =
    function SH_CreateOrGetSupplier(aPanel, aNeedsInit)
  {
    this.mViewerTracker = new ViewerTracker();
    this.mViewerTracker.trackViewerForPanel(aPanel);

    let supplier =
      TheSupplierRegistry.createSupplier(this.mSID, aPanel.synthBloc);

    if (aNeedsInit) supplier.initializeWithHelper(this);

    return supplier;
  };

  /**
   * If a viewer/supplier combo wants to use `SyntheticAdapter`, the supplier
   * can implement a `getTargetSynthetic` method that delegates to this
   * utility method, and the viewer can implement a method of the same name
   * that takes a string as an argument that names the SID of the kind of
   * synthetic we're interested in.
   *
   * If the viewer's current target can be translated into such a synthetic,
   * it can return one, or null if not.
   *
   * @param aPanel
   *        The panel where we should look for a synthetic, or null.
   * @return
   *        A synthetic from the given panel, or null if there isn't one
   *        associated with the panel's current target or if the panel isn't a
   *        tracked panel.  The kind of synthetic returned is the kind
   *        associated with the supplier's SID.
   *
   * @see adapters/syntheticAdapter
   */
  $proto.getTargetSynthetic = function SH_GetTargetSynthetic(aPanel)
  {
    if (!this.mViewerTracker.isTrackingViewerForPanel(aPanel)) return null;
    if (!("getTargetSynthetic" in aPanel.viewer)) return null;
    return aPanel.viewer.getTargetSynthetic(this.mSID);
  };
}
