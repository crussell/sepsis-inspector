/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.NodeTypeHelper = NodeTypeHelper;

//////////////////////////////////////////////////////////////////////////////

function NodeTypeHelper()
{
}

{
  let $proto = exports.NodeTypeHelper.constructor.prototype;
  
  NodeTypeHelper.getName = $proto.getName = function NTH_GetName(aValue)
  {
    let type;
    // From <http://www.w3.org/TR/2012/WD-dom-20121206/#dom-node-nodetype>.
    // (Mostly.  See ATTRIBUTE_NODE below.)
    switch (aValue) {
      case 1:
        type = "ELEMENT_NODE";
      break;
      case 2:
        // XXX Remove when minimum supported version of Gecko no longer has
        // Attr inheriting from Node.  See bug 611787.
        type = "ATTRIBUTE_NODE";
      break;
      case 3:
        type = "TEXT_NODE";
      break;
      case 7:
        type = "PROCESSING_INSTRUCTION_NODE";
      break;
      case 8:
        type = "COMMENT_NODE";
      break;
      case 9:
        type = "DOCUMENT_NODE";
      break;
      case 10:
        type = "DOCUMENT_TYPE_NODE";
      break;
      case 11:
        type = "DOCUMENT_FRAGMENT_NODE";
      break; 
    }

    return type;
  }
};
