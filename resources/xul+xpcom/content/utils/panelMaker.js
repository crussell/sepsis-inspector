/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.PanelMaker = PanelMaker;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { Logger } = require("utils/logger");
var { NotificationManager } = require("utils/notificationManager");

//////////////////////////////////////////////////////////////////////////////

/**
 * DOM-level node creation and manipulation for a panel manager's panels are
 * handled by the panel maker.
 *
 * The `addPanel` and `removePanel` calls are not expected to be used by
 * anything other than the `PanelManager` methods of the same name.
 *
 * @param aPanelWell
 *        The DOM element to be used to hold the panels.
 * @constructor
 */
function PanelMaker(aPanelWell)
{
  this.mPanelWell = aPanelWell;

  this.mNotificationManager = new NotificationManager(this);
}

{
  let proto = PanelMaker.prototype;
  proto.mNotificationManager = null;
  proto.mPanelWell = null;
  proto.mPanelManager = null;

  proto.__defineGetter__("panelManager", function PM_PanelManager_Get()
  {
    return this.mPanelManager;
  });

  proto.__defineSetter__("panelManager",
    function PM_PanelManager_Set(aPanelManager)
  {
    if (this.mPanelManager) {
      Logger.print("Overwriting panelManager.  Why?");
      this.mPanelManager.removeObserver("panelAdded",
                                        this.mNotificationManager);
    }

    this.mPanelManager = aPanelManager;
    aPanelManager.addObserver("panelAdded", this.mNotificationManager);
  });

  proto.addPanel = function PM_AddPanel(aParentPanel)
  {
    let panel = this.mPanelWell.ownerDocument.createElementNS(
      this.mPanelWell.namespaceURI,
      "inspectorpanel"
    );
    panel.setAttribute("flex", "1");

    if (aParentPanel) {
      // XXX Should we add mutation observer that watches for changes to the
      // linked attribute and enforces the panel manager's
      // `canHaveLinkedState`?
      panel.setAttribute("linked", "true");

      if (this.panelManager.isManagingPanel(aParentPanel)) {
        this.setWidths(this.panelManager.rootPanel, true);

        let splitter = this.mPanelWell.ownerDocument.createElementNS(
          this.mPanelWell.namespaceURI, "splitter"
        );
        this.mPanelWell.appendChild(splitter);
      }
    }

    this.mPanelWell.appendChild(panel);

    return panel;
  };

  proto.removePanel = function PM_RemovePanel(aPanel)
  {
    let splitter = this.getPanelSplitter(aPanel);
    this.mPanelWell.removeChild(aPanel);

    if (splitter) {
      this.mPanelWell.removeChild(splitter);
    }
  };

  /**
   * @param aPanel
   *        The panel node.
   * @return
   *        The splitter that separates the given panel from its parent panel.
   */
  proto.getPanelSplitter = function PM_GetPanelSplitter(aPanel)
  {
    return aPanel.previousSibling;
  };

  /**
   * @param aSplitter
   *        The splitter separating a parent and child panel.
   * @return
   *        The child panel the given splitter is separating from its parent.
   */
  proto.getPanelForSplitter = function PM_GetPanelForSplitter(aSplitter)
  {
    return aSplitter.nextSibling;
  };

  /**
   * Equalize the widths of all panels managed by the panel manager.
   *
   * @param aRootPanel
   * @param aRemove [optional]
   *        True iff the panels' width attributes should be removed rather
   *        than set.
   */
  proto.setWidths = function PM_SetWidths(aRootPanel, aRemove)
  {
    // We have to walk through twice to gather the panels and figure out out
    // many there are.
    // XXX Assumes each of the panel manager's panels has a maximum of one
    // child that the panel manager is managing (i.e., from the same window).
    let panels = [];
    let panel = aRootPanel;
    while (panel) {
      panels.push(panel);

      // If we're called on a panel we just created, we won't be able to use
      // `getChildPanels` on it.  That's okay though, because in that case we
      // already know how many children it has: none.
      let kids = (panel.panelManager) ?
        panel.panelManager.getChildPanels(panel) :
        [];

      panel = kids.length && kids[0];
    }

    let width = Math.floor(this.mPanelWell.boxObject.width / panels.length);
    for (let i = 0, n = panels.length; i < n; ++i) {
      if (aRemove) {
        panels[i].removeAttribute("width");
      }
      else {
        panels[i].width = width;
      }
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// Notifications

  /**
   * @see utils/notificationManager.js
   */
  proto.handleNotification = function PM_HandleNotification(aName, aData)
  {
    switch (aName) {
      case "panelAdded":
        this._onPanelAdded(aData.panel);
      break;
      default:
        throw new Error("wat.  Notification: " + aName);
      break;
    }
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  /**
   * Handle the "panelAdded" notification.
   */
  proto._onPanelAdded = function PM_OnPanelAdded(aPanel)
  {
    this.setWidths(this.mPanelManager.rootPanel);
  };
}
