/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Default initializer assigned to the info object for a registered viewer.
 *
 * If the metadata of a registered viewer specifies its own initializer,
 * `MagicInitializer` will NOT be automatically assigned to handle that
 * viewer.  This is a convenience class to make it easier to implement viewers
 * and reduce the amount of boilerplate necessary.
 *
 * There are two primary ways that `MagicInitializer` tries to initialize a
 * viewer:
 *
 *   - It looks for an `initializerViewer` function in the info script scope.
 *   - Alternatively, it calls `initializeViewer` from the viewer document's
 *     scope.
 *
 * `initializeViewer` from the info script scope is the preferred way; it will
 * be tried first.  If there is no such function defined in the info script,
 * we'll fall back to trying `initializeViewer` in the viewer document's
 * scope.
 *
 * (There's a third, deprecated way, which is the `initialize` function in the
 * info script scope.  It's deprecated in favor of the more descriptive
 * `initializeViewer`, and for consistency with the function from the viewer
 * document scope.  The info script's scope's `initialize` will be tried last,
 * if `initializeViewer` is undefined in both scopes, and a warning will be
 * printed to the error console.  Support for this exists only for backward
 * compatibility and will be removed in the future.)
 *
 * @see viewers/sample/info.js
 * @see registration/viewerInfo
 *      For an overview of the types of bootstrap objects (i.e., "WTF is an
 *      initializer?")
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.MagicInitializer = MagicInitializer;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { Logger } = require("utils/logger");

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aInfo
 *        The `ViewerInfo` instance corresponding the viewer that we will be
 *        initializing.
 * @constructor
 */
function MagicInitializer(aInfo)
{
  this.info = aInfo;
}

{
  let proto = MagicInitializer.prototype;
  proto.info = null;

  /**
   * @param
   *       A XUL inspectorpanel element.  This is the panel the viewer is
   *       being loaded in.
   * @return
   *        The viewer object to be loaded in the panel.
   */
  proto.initializeViewer = function MI_InitializeViewer(aPanel)
  {
    let viewer;
    if ("initializeViewer" in this.info.scriptModule.exports) {
      viewer = this.info.scriptModule.exports.initializeViewer(aPanel);
    }
    else if ("initializeViewer" in aPanel.viewerContext) {
      viewer = aPanel.viewerContext.initializeViewer(aPanel);
    }
    else {
      let message =
        "No initializeViewer defined for viewer with UID " + this.info.uid;

      // XXX Remove this for sepsis-inspector 0.6.
      if ("initialize" in this.info.scriptModule.exports) {
        viewer = this.info.scriptModule.exports.initialize(aPanel);
        message +=
          "  (The info script's initialize function is deprecated.  Use " +
          "initializeViewer instead, or define a custom initializer)";
      }

      Logger.print(message);
    }

    return viewer;
  };
}
