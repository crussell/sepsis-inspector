/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Register suppliers to coordinate the creation and lifetime of synthetics
 *
 * A supplier must be registered in with an SID in the chrome.manifest before
 * the `createSupplier` method can be successfully invoked.  The SID should
 * match the type of synthetics the supplier handles.
 *
 * Note that when registering a supplier, it is a factory that is registered;
 * the supplier factory must implement the `createSupplier` method:
 *
 *   createSupplier(aSynthBloc)
 *     Return an instance of the associated supplier to be used in the given
 *     synthetic bloc's privilege domain.
 *
 * Suppliers themselves are a pattern that you are free to use (or not); a
 * synthetic bloc is completely agnostic to their existence.  However, it's
 * a good pattern to use, and doing so will even simplify the creation of an
 * adapter to translate a panels target into the synthetic of interest.
 *
 * @see adapters/syntheticAdapter
 */

module.singleton = true;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { Module } = require("loader/module");
var { Resolver } = require("loader/resolver");
var { Loader } = require("loader/loader");

var { TheStaticStore } = require("utils/theStaticStore");

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.TheSupplierRegistry = new SupplierRegistry();

//////////////////////////////////////////////////////////////////////////////

function SupplierRegistry()
{
  // This will be indexed by `SynthBloc` instances, and the values will be a
  // map from URI strings to supplier instances that have been created for use
  // with the synthetic bloc.
  this.mSuppliers = new WeakMap();

  let resolver = new Resolver("chrome://sepsis-inspector/content/");
  this.mLoader = new Loader(
    resolver, { id: "sepsis-inspector supplier loader" }
  );
}

{
  let $proto = SupplierRegistry.prototype;

  $proto.mSuppliers = null;
  $proto.mLoader = null;

  $proto.APPSYNTHETIC_SID = SupplierRegistry.APPSYNTHETIC_SID =
    "org.mozilla.sepsis.inspector.synthetic.appSynthetic";

  /**
   * Used to create a supplier with the given SID.
   *
   * NB: Despite the name, this method will *not* create another supplier if
   * there is already a supplier associated with the given SID.
   *
   * TODO: If the same factory is registered with multiple SIDs and this
   * method is called with one of them, we should cache the supplier instance
   * for those alternate SIDs, too.
   *
   * @param aSID
   *        A string naming the SID for the supplier that should be created.
   *        If the supplier with the SID already has a supplier associated
   *        with the given synthetic bloc (i.e., was created with a previous
   *        call to `createSupplier`), then that supplier is returned.
   *        Otherwise, the newly created supplier is returned.
   * @param aSynthBloc
   *        The `SynthBloc` instance that the supplier will have privileges
   *        for.  May be null, but only if the supplier being created is
   *        itself a `SynthBloc`.
   * @throws
   *        Error if the factory's `createSupplier` returns a bad value (e.g.,
   *        null), or if no such supplier has been registered.
   * @see TheSupplierRegistry.registerFactory
   */
  $proto.createSupplier = function SR_CreateSupplier(aSID, aSynthBloc)
  {
    let bloc = aSynthBloc;
    if (!bloc && aSID !== this.APPSYNTHETIC_SID) throw new Error(
      "SynthBloc required for creation of non-SynthBloc suppliers"
    );

    // A map from URI to supplier is effectively the same as a map from module
    // to supplier.  In the end, we get away with only needing to make a call
    // to each module's `createSupplier` once per synthetic bloc.
    let uri = TheStaticStore.readValue(aSID, "sepsis-inspector-supplier");
    let module = this.mLoader.getCachedModule(uri);
    if (!module) {
      module = new Module(uri);
      let exports = this.mLoader.load(module);
      if (Object.keys(exports).length != 1) {
        this.mLoader.unload(module);
        throw new Error("Supplier must export exactly one symbol " + uri);
      }
    }

    // NB: The reason `uriToSupplierMap` isn't `sidToSupplierMap` is so that
    // we can allow a supplier to be registered to specialize in multiple
    // synthetics.  (Note that this isn't recommended, since
    // `SyntheticAdapter` has no support for it, but we don't want to make it
    // impossible for implementations that are aware of the pitfalls.)
    let uriToSupplierMap = (bloc && this.mSuppliers.get(bloc)) || new Map();

    let supplier = uriToSupplierMap.get(uri);
    if (!supplier) {
      let [ name ] = Object.keys(module.exports);
      supplier = new (module.exports[name])(bloc);
      if (!supplier) throw new Error("Invalid supplier returned: " + aSID);
      uriToSupplierMap.set(uri, supplier);

      if (!bloc) bloc = supplier;
    }

    if (!this.mSuppliers.has(bloc)) {
      this.mSuppliers.set(bloc, uriToSupplierMap);
    }

    return supplier;
  };

  /**
   * Get an instance of the supplier that has been registered with the given
   * SID.
   *
   * @param aSynthBloc
   *        The `SynthBloc` instance the supplier has privileges for.
   * @param aSID
   *        A string naming the SID the supplier was registered with.
   * @param aParent [optional]
   *        True iff we should recursively probe the synthetic bloc's
   *        ancestors in the event that there is no such supplier associated
   *        with the given instance.  Defaults to false.
   * @return
   *        The supplier.
   */
  $proto.getSupplier = function SR_GetSupplier(aSynthBloc, aSID, aParent)
  {
    let bloc = aSynthBloc;
    while (bloc) {
      let uri = TheStaticStore.readValue(aSID, "sepsis-inspector-supplier");
      let uriToSupplierMap = this.mSuppliers.get(bloc);
      if (uriToSupplierMap && uriToSupplierMap.has(uri)) {
        return uriToSupplierMap.get(uri);
      }

      if (!aParent) break;

      bloc = bloc.parent;
    }

    return null;
  };
}
