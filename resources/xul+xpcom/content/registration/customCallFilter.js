/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Filter for viewers that specify the "filterFunction" metadata property.
 *
 * For viewers which don't specify a filter implementation, they are given an
 * instance of `MagicFilter` by default, which looks for a `filter` function
 * in the viewer info script, which will be called when the magic filter is
 * asked to do its thing.
 *
 * `CustomCallFilter` works similarly, except that it allows viewer authors to
 * specify some other name besides `filter` to look for instead.  Whatever
 * function from the viewer info script that matches the "filterFunction"
 * value will be called.
 *
 * @see registration/theViewerRegistry
 * @see registration/viewerInfo
 *      For an overview of the types of bootstrap objects (i.e., "WTF is a
 *      filter and info script?").
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.CustomCallFilter = CustomCallFilter;

//////////////////////////////////////////////////////////////////////////////

function CustomCallFilter(aInfo)
{
  this.info = aInfo;
}

{
  let proto = CustomCallFilter.prototype;
  CustomCallFilter.propertyName =
  proto.propertyName = "filterFunction";

  proto.info = null;

  /**
   * @param a_x_Object
   *        NB: `a_x_Object` is untrusted.
   * @param aSynthBloc [optional]
   *        The `SynthBloc` instance for this inspection context.  Will only
   *        be provided if the viewer metadata indicating it accepts
   *        synthetics.
   * @return
   *        A boolean indicating whether the viewer (associated with
   *        `this.info` supports inspecting the given object
   */
  proto.test = function CCF_Test(a_x_Object, aSynthBloc)
  {
    let functionName = this.info.metadata[this.propertyName];
    // read as: is-defined(synthBloc) implies is-true(wantsSynthetic)
    // assert(!aSynthBloc || this.info.metadata.wantsSynthetic);
    if (functionName in this.info.scriptModule.exports) {
      return this.info.scriptModule.exports[functionName](
        a_x_Object, aSynthBloc
      );
    }

    // XXX throw?
    return false;
  };
}
