/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Default localizer assigned to a registered viewer.
 *
 * Of the viewer "bootstrap objects" (see `ViewerInfo`), the localizer is
 * responsible for returning a human-readable name for the viewer, suitable
 * for use in the Inspector UI and localized to the intended locale.
 *
 * Viewers which do not specify a custom localizer can make sure that the
 * Inspector is informed of the viewer's name by defining a `getLocalizedName`
 * function in the info script for that viewer; a `MagicLocalizer` instance
 * will be assigned by default to viewers that do not explicitly specify their
 * own localizer, and the default localizer will call `getLocalizedName` in
 * the viewer info script's scope to get the viewer's name.
 *
 * @see registration/viewerInfo
 *      For an overview of the types of bootstrap objects (i.e., "WTF is a
 *      localizer?")
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.MagicLocalizer = MagicLocalizer;

//////////////////////////////////////////////////////////////////////////////

function MagicLocalizer(aInfo)
{
  this.info = aInfo;
}

{
  let proto = MagicLocalizer.prototype;
  proto.info = null;

  /**
   * @param aLocale
   *        A string naming the locale for which the viewer name should be
   *        localized to.
   * @return
   *        A string that returns the human-readable name of the viewer
   *        associated with the `ViewerInfo` instance.
   */
  proto.getName = function ML_GetName(aLocale)
  {
    return this.info.scriptModule.exports.getLocalizedName(aLocale);
  };
}
