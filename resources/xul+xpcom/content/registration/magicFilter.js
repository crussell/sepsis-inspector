/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Default filter assigned to a registered viewer.
 *
 * Of the viewer "bootstrap objects" (see `ViewerInfo`), the filter is
 * responsible for indicating to the Inspector whether the viewer supports
 * inspecting a given object.
 *
 * A viewer that does not otherwise specify a custom filter or declaratively
 * list which kinds of objects the viewer can inspect can define a `filter`
 * function in the info script registered with the viewer.  By default, a
 * `MagicFilter` instance will be implicitly created and assigned to a viewer
 * that does not explicitly declare its filter in its registration metadata.
 * This "default" filter will "magically" resolve the filter's duties by
 * looking for `filter` function in scope of the info script.  When the
 * Inspector needs to determine whether the viewer is a valid object to
 * inspect some object, whatever boolean that function returns will be the
 * answer given to the Inspector.
 *
 * @see registration/viewerInfo
 *      For an overview of the types of bootstrap objects (i.e., "WTF is a
 *      filter?")
 * @see registration/theViewerRegistry
 *      For questions like "WTF is an info script?"
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.MagicFilter = MagicFilter;

//////////////////////////////////////////////////////////////////////////////

function MagicFilter(aInfo)
{
  this.info = aInfo;
}

{
  let proto = MagicFilter.prototype;
  proto.info = null;

  /**
   * @param a_x_Object
   *        NB: `a_x_Object` is untrusted.
   * @param aSynthBloc [optional]
   *        The `SynthBloc` instance for this inspection context.  Will only
   *        be provided if the viewer metadata indicating it accepts
   *        synthetics.
   * @return
   *        A boolean indicating whether the viewer (associated with
   *        `this.info` supports inspecting the given object
   */
  proto.test = function MF_Test(a_x_Object, aSynthBloc)
  {
    // read as: is-defined(synthBloc) implies is-true(wantsSynthetic)
    // assert(!aSynthBloc || this.info.metadata.wantsSynthetic);
    if ("filter" in this.info.scriptModule.exports) {
      return this.info.scriptModule.exports.filter(a_x_Object, aSynthBloc);
    }

    return false;
  };
}
