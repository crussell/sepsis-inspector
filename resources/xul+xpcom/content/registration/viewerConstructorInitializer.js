/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Initializer for viewers with a "viewerClass" metadata property.
 *
 * The "viewerClass" metadata property can specify a constructor for the
 * viewer.  The Inspector will assign a `ViewerConstructorInitializer`
 * instance as the viewer's intializer, which will in turn create an instance
 * of the class with a name that matches the one provided whenever the viewer
 * is being loaded.
 *
 * NB: Unlike the "initializerFunction" metadata property, the function named
 * by the "viewerClass" property must exist in the viewer document's scope; we
 * don't check for it in the info script's scope.
 *
 * @see registration/theViewerRegistry
 * @see registration/viewerInfo
 *      For an overview of the types of bootstrap objects (i.e., "WTF is an
 *      initializer and an info script?")
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.ViewerConstructorInitializer = ViewerConstructorInitializer;

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aInfo
 *        The `ViewerInfo` instance corresponding the viewer that we will be
 *        initializing.
 * @constructor
 */
function ViewerConstructorInitializer(aInfo)
{
  this.info = aInfo;
}

{
  let proto = ViewerConstructorInitializer.prototype;
  ViewerConstructorInitializer.propertyName =
  proto.propertyName = "viewerClass";

  proto.info = null;

  /**
   * @param aPanel
   *        A XUL inspectorpanel element.  This is the panel the viewer is
   *        being loaded in.
   * @return
   *        The viewer object to be loaded in the panel.
   */
  proto.initializeViewer = function VCI_InitializeViewer(aPanel)
  {
    let functionName = this.info.metadata[this.propertyName];
    return new aPanel.viewerContext[functionName](aPanel);
  };
}
