/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Localizer for viewers that specify the "stringBundle" metadata property.
 *
 * Viewer authors may specify a XUL property file resource that contains the
 * viewer name, rather than having to implement a localizer.  When a viewer is
 * registered with a "stringBundle" property in its JSON metadata, a
 * `StringBundleLocalizer` instance will be generated and assigned to the
 * viewer by the Inspector and which will look in the string bundle for the
 * viewer's name.
 *
 * The "stringBundle" property should contain the URI of some XUL string
 * property file, and there should be some property in the file matching the
 * viewer's UID followed by ".magicViewerName".  For example, if the viewer's
 * UID is "org.example.someKindOfViewer", then there should be an "org.
 * example.someKindOfViewer.magicViewerName" property containing the name of
 * the viewer.
 *
 * The viewer should, of course, use a URI that points to a file that exists
 * in some chrome locale directory.
 *
 * @see registration/theViewerRegistry
 * @see registration/viewerInfo
 *      For an overview of the types of bootstrap objects (i.e., "WTF is a
 *      localizer and info script?").
 * @see https://developer.mozilla.org/XUL/Tutorial/Property_Files
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.StringBundleLocalizer = StringBundleLocalizer;

const Cc = Components.classes;
const Ci = Components.interfaces;
const Cu = Components.utils;

//////////////////////////////////////////////////////////////////////////////

function StringBundleLocalizer(aInfo)
{
  this.info = aInfo;
}

{
  let proto = StringBundleLocalizer.prototype;
  StringBundleLocalizer.propertyName =
  proto.propertyName = "stringBundle";

  proto.info = null;

  /**
   * @param aLocale
   *        A string naming the locale for which the viewer name should be
   *        localized to.
   * @return
   *        A string that returns the human-readable name of the viewer
   *        associated with the `ViewerInfo` instance.
   */
  proto.getName = function SBL_GetName(aLocale)
  {
    // XXX Should this get inited by the viewer registry instead?  Probably.
    if (!this.info.stringBundle) {
      let bundleService = Cc["@mozilla.org/intl/stringbundle;1"].
        getService(Ci.nsIStringBundleService);

      this.info.stringBundle =
        bundleService.createBundle(this.info.metadata[this.propertyName]);
    }

    // We choose a "magicViewerName" tag instead of something more generic
    // like "name" or "label" in order to combat the effects of a codebase
    // that's too adversarial to the grep test by being "too DRY".  See
    // <http://jamie-wong.com/2013/07/12/grep-test/>
    let bundlePropertyName = this.info.uid + ".magicViewerName";
    return this.info.stringBundle.GetStringFromName(bundlePropertyName);
  };
}
