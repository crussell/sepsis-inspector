/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 *
 * XXX
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.Adapter = Adapter;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { Logger } = require("utils/logger");

var { TheViewerRegistry } = require("registration/theViewerRegistry");

//////////////////////////////////////////////////////////////////////////////

function Adapter(aURI, aModuleExports)
{
  this.uri = aURI;
  this.mModuleExports = aModuleExports;
}

{
  let proto = Adapter.prototype;

  proto.mModuleExports = null;
  proto.uri = null;

  proto.translateTarget =
    function A_TranslateTarget(aTarget, aSynthBloc, aParentPanel)
  {
    let newTarget;
    try {
      newTarget = this.mModuleExports.translateTarget(
        aTarget, aSynthBloc, aParentPanel
      );
    }
    catch (ex) {
      Logger.print(
        "Encountered error during translation for adapter loaded from URI " +
        this.uri
      );

      return null;
    }

    if (newTarget === null) return null;

    let bloc = aSynthBloc.getSynthBlocForMirror(newTarget);
    if (!bloc && !aSynthBloc.isSynthetic(newTarget)) {
      Logger.print(
        "Ignoring untrusted translation returned by adapter from URI " +
        this.uri
      );

      return null;
    }

    if (!TheViewerRegistry.isInspectable(newTarget, aSynthBloc)) {
      Logger.print(
        "Ignoring non-inspectable translation returned by adapter from URI" +
        this.uri
      );

      return null
    }

    return newTarget;
  }
}
