/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * For registering adapters and getting adapter–UID relations.
 *
 * Adapters can translate an object into a related object.  Ordinarily, in any
 * given parent/child pair of panels, selecting a target in the parent panel
 * will set that object as the child panel's subject.  This is not always
 * desirable.
 *
 * For example, suppose that in some code you're debugging, you have a
 * listener set for an event, where the listener is supposed to modify some
 * attributes of the element the event is fired on.  So you'd like to be able
 * to watch the events in the "Events" viewer, and when you select one, you
 * want to see the event target in the "DOM Node" viewer in the child panel. 
 * The problem is that the "Events" viewer's target is an nsIDOMEvent object,
 * but the "DOM Node" viewer's filter only accepts nsIDOMNode objects.  Enter
 * adapters.
 *
 * With adapters, there can be an "Event Target" adapter that translates an
 * event object into the node the event was fired on (or any target into any
 * other object--including synthetics--for that matter).
 *
 * The way this works is the adapter exports a `translateTarget` function, and
 * the Inspector will allow the registered adapters to "sit between" a parent
 * panel and its child.  When the parent panel's target is updated, the
 * Inspector will find all the viewers whose filters accept that object.  Then
 * the Inspector will run the target through all the registered adapters, and
 * if an adapter returns a inspectable object, it will find all the viewers
 * whose filters accept *that* object, adding each to the list of viewers
 * available for use in the child panel.
 *
 * To create an adapter, implement a `translateTarget` function and load the
 * file using `TheAdapterRegistry.load`.  Alternatively, a viewer can ensure
 * that an adapter is registered by listing the URI where the adapter is
 * defined its `adapterURIs` array in the viewer info's JSON metadata.
 *
 * The `translateTarget` method should be as follows:
 *
 *   function translateTarget(aTarget, aParentPanel)
 *     `aTarget` is a mirror for the target of the parent panel that should be
 *     translated into some other target.  If the adapter is unable to handle
 *     the given target--i.e., to translate it into some other meaningful
 *     target-- then the adapter should return null.  (Otherwise it should
 *     return the translated target.  The translated target must be either a
 *     mirror or a synthetic.  `aParentPanel` is the panel whose target is
 *     being translated.  NB: `aParentPanel` may be null if the translation is
 *     intended to be inspected in the root panel, since the root panel is
 *     allowed to have no parent.
 *
 * If the new, translated target returned by an adapter isn't inspectable (see
 * `ViewerRegistry.isInspectable`), then a warning will be printed to the
 * console and the Inspector will treat the adapter as if it had returned
 * null.
 *
 * TODO
 * - Shut down automatically?  Unlike `ViewerRegistry` we don't have any
 *   usefully viewer-facing APIs that we need to make sure are available even
 *   when no Inspector window is open.  Re-opening the Inspector after all
 *   others have been closed would be slower.  (Would it be noticeably slower,
 *   though?)
 */

module.singleton = true;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

var { Module } = require("loader/module");
var { Resolver } = require("loader/resolver");
var { Loader } = require("loader/loader");
var { Adapter } = require("registration/adapter");
var { TheViewerRegistry } = require("registration/theViewerRegistry");
var { AdapterUIDRelation } = require("utils/adapterUIDRelation");
var { Logger } = require("utils/logger");

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.TheAdapterRegistry = new AdapterRegistry();

//////////////////////////////////////////////////////////////////////////////

function AdapterRegistry()
{
  this.mURIToAdapterMap = new Map();

  let globals = Object.create(null);
  globals.Cc = Components.classes;
  globals.Ci = Components.interfaces;
  globals.Cu = Components.utils;
  let loaderOptions = {
    id: "sepsis-inspector adapter registry loader",
    globals: globals
  };

  let resolver = new Resolver("chrome://sepsis-inspector/content/");
  this.mLoader = new Loader(resolver, loaderOptions);
}

{
  let proto = AdapterRegistry.prototype;
  proto.mURIToAdapterMap = null;
  proto.mLoader = null;

  AdapterRegistry.AdapterLoadError = proto.AdapterLoadError =
    function AR_AdapterLoadError(aInnerException)
  {
    let error = new Error(
      "Encountered error loading adapter:\n" + String(aInnerException)
    );
    error.innerException = aInnerException;
    error.stack = aInnerException.stack || (new Error()).stack;
    if ("fileName" in aInnerException) {
      error.message += "\n" +
        aInnerException.fileName + ":" + aInnerException.lineNumber;
    }

    return error;
  };

  /**
   * Load the script at the given URI, so that an adapter may initialize and
   * register itself.  The script will be loaded in its own scope where
   * `Cc`, `Ci`, and `Cu` will already be defined.
   *
   * Calling this method with the same URI multiple times has the same effect
   * as if this method were only called once with the URI, unless the script
   * at the given URI fails to register an adapter.
   *
   * @param aURI
   *        A string naming the chrome path to the file where an adapter is
   *        defined.
   * @throws
   *        Error if the given URI is not a chrome URI, or if the script at
   *        the URI throws an error during load, or if called with a URI that
   *        doesn't successfully register an adapter.
   */
  proto.load = function AR_Load(aURI)
  {
    if (this.mURIToAdapterMap.has(aURI)) return;

    if (!aURI.startsWith("chrome://")) {
      throw new Error("Adapter URI must be a chrome URI.");
    }

    let module = new Module(aURI);
    try {
      this.mLoader.load(module);
    }
    catch (ex) {
      throw new AdapterRegistry.AdapterLoadError(ex);
    }

    if (!("translateTarget" in module.exports)) {
      throw new Error(
        "No translateTarget for adapter loaded from URI " + aURI
      );
    }

    this.mURIToAdapterMap.set(aURI, new Adapter(aURI, module.exports));
  };

  ////////////////////////////////////////////////////////////////////////////

  /**
   * Get an adapter–UID relation for the adapters that are valid for the given
   * target.  That is, get an `AdapterUIDRelation` based on the adapters that
   * will translate the given panel's parent's target into some other
   * inspectable object.
   *
   * NB: It's important to notice that the target is in fact the given panel's
   * parent's target, not the panel's own target.
   *
   * @param aTarget
   *        The target the adapters should attempt to translate.  Must be
   *        either a mirror or a synthetic.
   * @param aParentPanel
   *        The panel whose target this is.  NB: This may be null if the
   *        prospective viewers are to be loaded in root panel, since the root
   *        panel is allowed to have no parent.
   * @param aSynthBloc
   *        The `SynthBloc` for the associated synthetic domain.
   * @param aNull [optional]
   *        True iff the relation should have only the null (passthrough)
   *        adapter and its associated UIDs, and not other adapters or the
   *        UIDs found with their translations.
   * @return
   *        An `AdapterUIDRelation` based on valid adapters for `aTarget`.  A
   *        null adapter in the relation signifies passthrough; the viewers
   *        with the UIDs associated with the null adapter can handle the
   *        target directly, without being translated.
   */
  proto.getAdapterUIDRelation = function AR_GetAdapterUIDRelation(
    aTarget, aParentPanel, aSynthBloc, aNull
  )
  {
    // First, seed the relation with the viewers that directly support
    // inspecting the target (i.e., it passes their filter).
    let uids = [];
    if (TheViewerRegistry.isInspectable(aTarget, aSynthBloc)) {
      uids = TheViewerRegistry.getCompatibleViewers(aTarget, aSynthBloc);
    }

    let result = new AdapterUIDRelation(uids, aTarget);

    // Now, get the viewers that could be used if the target is run through an
    // adapter and translated into something else.
    if (!aNull && !aSynthBloc.isSynthetic(aTarget)) {
      let entries = this._getTranslations(aTarget, aSynthBloc, aParentPanel);
      for (let i = 0, n = entries.length; i < n; ++i) {
        let { adapter, newTarget } = entries[i];

        let uids = TheViewerRegistry.getCompatibleViewers(
          newTarget, aSynthBloc
        );

        for (let j = 0, m = uids.length; j < m; ++j) {
          result.set(adapter, uids[j], newTarget);
        }
      }
    }

    return result;
  };

  ////////////////////////////////////////////////////////////////////////////
  //// internal methods

  /**
   * @param aTarget
   * @param aSynthBloc
   * @param aParentPanel
   * @return
   *        An array of objects with an `adapter` property and a `newTarget`
   *        property, that are for what they sound like.  The translations are
   *        guaranteed to be non-primitive, with the exception of null, which
   *        indicates the adapter couldn't come up with a good way to
   *        translate the given object into anything worthwhile.
   */
  proto._getTranslations =
    function AR_GetTranslations(aTarget, aSynthBloc, aParentPanel)
  {
    let results = [];
    for (let [uri, adapter] of this.mURIToAdapterMap) {
      let newTarget = adapter.translateTarget(
        aTarget, aSynthBloc, aParentPanel
      );

      if (newTarget === null) continue;

      results.push({
        adapter: adapter,
        newTarget: newTarget
      });
    }

    return results;
  };
}
