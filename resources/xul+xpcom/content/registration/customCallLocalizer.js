/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Localizer for viewers that specify the "localizerFunction" in the metadata.
 *
 * For viewers that don't specify a localizer in the registration metadata, a
 * `MagicLocalizer` instance is created and assigned to the viewer.  It will
 * try to call a `getLocalizedName` function in the viewer's info script when
 * the Inspector framework needs get a human-readable name for the viewer for
 * use in the Inspector UI.
 *
 * `CustomCallLocalizer` works similarly, except that rather than requiring
 * the function to be called `getLocalizedName`, it allows authors to specify
 * the name of the function to be used instead.  Whatever the value of the
 * "localizerFunction" property in the registration metadata, that's the
 * function that will be called to get the viewer's name.
 *
 * @see registration/theViewerRegistry
 * @see registration/viewerInfo
 *      For an overview of the types of bootstrap objects (i.e., "WTF is a
 *      localizer and info script?").
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.CustomCallLocalizer = CustomCallLocalizer;

//////////////////////////////////////////////////////////////////////////////

function CustomCallLocalizer(aInfo)
{
  this.info = aInfo;
}

{
  let proto = CustomCallLocalizer.prototype;
  CustomCallLocalizer.propertyName =
  proto.propertyName = "localizerFunction";

  proto.info = null;

  /**
   * @param aLocale
   *        A string naming the locale for which the viewer name should be
   *        localized to.
   * @return
   *        A string that returns the human-readable name of the viewer
   *        associated with the `ViewerInfo` instance.
   */
  proto.getName = function CCL_GetName(aLocale)
  {
    let functionName = this.info.metadata[this.propertyName];
    return this.info.scriptModule.exports[functionName](aLocale);
  };
}
