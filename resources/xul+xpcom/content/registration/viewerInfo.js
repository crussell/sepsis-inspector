/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Represents a registered viewer.
 *
 * The viewer registry deals with `ViewerInfo` objects.  When a viewer is
 * registered, an info object is created and associated with that viewer, to
 * encapsulate things like its UID and other metadata.  Additionally, the
 * `ViewerInfo` object will need an initializer to be used when the viewer
 * needs to be loaded in some panel to inspect an object; and before that, a
 * filter to determine whether the viewer is even eligible to inspect that
 * kind of object; and a localizer to retrieve localized, human-readable
 * strings (including the viewer's name) that will be used in the Inspector
 * UI.
 *
 * When a viewer is registered and a `ViewerInfo` object is created, the
 * Inspector framework will parse and/or execute the viewer's metadata file
 * and the info script it supplies.  After parsing the metadata, the registry
 * will set up the `ViewerInfo` instance's `filter`, `initializer`, and
 * `localizer`.  If the viewer metadata fails to specify one or more of these
 * essential "bootstrap" objects, they will be filled out with the default
 * implementations.
 *
 * initializer.initializeViewer(aPanel)
 *   The initializer must implement an `initializeViewer` method, which
 *   returns the actual viewer object.  The parameter is the panel the viewer
 *   will be loaded in.  A `MagicInitializer` instance will be used by default
 *   if no initializer is specifed.
 *
 * filter.test(a_x_Object, aSynthBloc)
 *   The filter must implement a `test` method to return a boolean indicating
 *   whether the viewer supports the given object.  If the metadata specifies
 *   no filter, a `MagicFilter` instance will be used by default.
 *
 * localizer.getName(aLocale)
 *   The localizer must implement a `getName` method to return a string
 *   containing the human-readable name of the viewer.  A `MagicLocalizer`
 *   instance will be used by default if no localizer is otherwise specified.
 *
 * As the names suggest, there is some level of "magic" involved with what the
 * default implementations do to fulfill their duties, and they all entail the
 * search for and use of specially named symbols in the viewer's supplied info
 * script.  See the documentation in viewers/sample/info.js or the respective
 * implementations of the bootstrap objects themselves for a fuller treatment
 * of what's going on.
 *
 * Viewers are not required to use the default initializer, filter, and
 * localizer implementations; a viewer can used custom bootstrap objects by
 * signalling to the viewer registry its intention to do so with the use of
 * various properties in the viewer metadata.  See the `ViewerRegistry`
 * documentation for more info.
 *
 * TODO
 * - Tests.  E.g., how do the generated bootstrap objects behave if there's no
 *   "scriptURI"?
 *
 * @see viewers/sample/info.js
 * @see registration/magicFilter.jsm
 * @see registration/magicInitializer.jsm
 * @see registration/magicLocalizer.jsm
 * @see registration/theViewerRegistry
 */

//////////////////////////////////////////////////////////////////////////////
//// Global Symbols

exports.ViewerInfo = ViewerInfo;

var { IOService } = require("xpcom/ioService");
var { XMLHTTPRequest } = require("xpcom/xmlHTTPRequest");

//////////////////////////////////////////////////////////////////////////////

function ViewerInfo(aUID, aURI)
{
  let error = this.getURIValidationError(aURI);
  if (error) {
    throw new ViewerInfo.BadURIError(
      this, aURI, ViewerInfo.BadURIError.INFO_METADATA, error
    );
  }

  this.uid = aUID;
  this.jsonURI = aURI;

  let request = new XMLHTTPRequest();
  // XXX synchronous for now
  request.open("GET", aURI, false);
  request.overrideMimeType("application/json");
  request.send(null);

  let json;
  try {
    json = JSON.parse(request.responseText);
  }
  catch (ex) {
    throw new ViewerInfo.MetadataParseError(this, request.responseText, ex);
  }

  this.metadata = this.normalizeInfo(json);
}

{
  let proto = ViewerInfo.prototype;
  proto.metadata = null;
  proto.scriptModule = null;
  proto.uid = null;
  proto.jsonURI = null;
  proto.stringBundle = null;

  proto.normalizeInfo = function VI_NormalizeInfo(aJSON)
  {
    let metadata = Object.create(null);
    this._setProperty("adapterURIs", [], metadata, aJSON);
    this._setProperty("canInspectApplication", false, metadata, aJSON);
    this._setProperty("scriptURI", null, metadata, aJSON);
    this._setProperty("useMirrors", false, metadata, aJSON);
    this._setProperty("viewerURI", null, metadata, aJSON);
    this._setProperty("wantsSynthetic", false, metadata, aJSON);
    this._setProperty("lowPriority", false, metadata, aJSON);

    // Copy the rest.
    for (let key in aJSON) {
      if (!(key in metadata)) {
        metadata[key] = aJSON[key];
      }
    }

    return metadata;
  };

  /**
   * @param aURI
   *        A string that should parse to a proper chrome URI.
   * @return
   *        The exception generated when validating the given URI, or null if
   *        there are no known problems.
   */
  proto.getURIValidationError = function VI_GetURIValidationError(aURI)
  {
    let xpcomURI;
    try {
      xpcomURI = IOService.newURI(aURI, null, null);
    }
    catch (ex) {
      return ex;
    }

    if (!xpcomURI.schemeIs("chrome")) {
      return new ViewerInfo.ChromeURIRequiredError(aURI);
    }

    return null;
  };

  proto._setProperty =
    function VI_SetProperty(aName, aValue, aObject, aOverrides)
  {
    if (aName in aOverrides) {
      aObject[aName] = aOverrides[aName];
    }
    else {
      aObject[aName] = aValue;
    }
  };
}

ViewerInfo.ChromeURIRequiredError = function VI_ChromeURIRequiredError(aURI)
{
  this.uri = aURI;
  this.stack = (new Error()).stack;
  this.message = "chrome URI required; given " + aURI;
};

{
  let eproto = Object.create(Error.prototype);
  eproto.constructor = ViewerInfo.ChromeURIRequiredError;
  ViewerInfo.ChromeURIRequiredError.prototype = eproto;

  eproto.uri = null;
}

ViewerInfo.MetadataParseError =
  function VI_MetadataParseError(aInfo, aText, aSyntaxError)
{
  this.info = aInfo;
  this.fileContents = aText;
  this.innerException = aSyntaxError;
};

// XXX Should inherit from `Error.prototype`?  Probably need an `ErrorUtils`
// to make sure stuff gets set up the right way.
{
  let eproto = ViewerInfo.MetadataParseError.prototype;
  eproto.fileContents = null;
  eproto.info = null;
  eproto.innerException = null;
}

ViewerInfo.BadURIError = function VI_BadURIError(aInfo, aURI, aType, aInner)
{
  this.info = aInfo;
  this.uri = aURI;
  this.type = aType;

  if (aInner !== undefined) this.innerException = aInner;

  this.stack = (new Error()).stack;
  this.message = "Validation error with " + this._typeToString() + " URI";
  if (this.innerException) {
    this.message += ":\n" + String(this.innerException);
  }
};

{
  let eproto = Object.create(Error.prototype);
  eproto.constructor = ViewerInfo.BadURIError;
  ViewerInfo.BadURIError.prototype = eproto;

  ViewerInfo.BadURIError.ADAPTER = eproto.ADAPTER = 1;
  ViewerInfo.BadURIError.INFO_METADATA = eproto.INFO_METADATA = 2;
  ViewerInfo.BadURIError.INFO_SCRIPT = eproto.INFO_SCRIPT = 3;
  ViewerInfo.BadURIError.VIEWER_DOCUMENT = eproto.VIEWER_DOCUMENT = 4;

  eproto.info = null;
  eproto.type = null;
  eproto.uri = null;
  eproto.innerException = null;

  eproto._typeToString = function VI_BUE_TypeToString()
  {
    switch (this.type) {
      case this.ADAPTER:
        return "Adapter";
      break;
      case this.INFO_METADATA:
        return "Info metadata";
      break;
      case this.INFO_SCRIPT:
        return "Info script";
      break;
      case this.VIEWER_DOCUMENT:
        return "Viewer document";
      break;
    }

    return "(Unknown URI type " + this.type + ")";
  };
}
