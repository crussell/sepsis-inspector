/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Initializer for viewers that specify "initializerFunction" in the metadata.
 *
 * A `MagicInitializer` instance will be assigned to viewers that specify no
 * initializer.  It will look for some `initializeViewer` function either in
 * the associated info script or in the viewer document's scope and call it
 * when the initializer is used.
 *
 * `CustomCallInitializer` is similar, except that it allows the viewer author
 * to specify some other name that the function goes by, besides
 * `initializeViewer`.  For viewers which specify an "initializerFunction" in
 * the metadata, whatever its value is, that's the function that will be
 * called to initialize the viewer.
 *
 * @see registration/theViewerRegistry
 * @see registration/viewerInfo
 *      For an overview of the types of bootstrap objects (i.e., "WTF is an
 *      initializer and info script?").
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.CustomCallInitializer = CustomCallInitializer;

//////////////////////////////////////////////////////////////////////////////

/**
 * @param aInfo
 *        The `ViewerInfo` instance corresponding the viewer that we will be
 *        initializing.
 * @constructor
 */
function CustomCallInitializer(aInfo)
{
  this.info = aInfo;
}

{
  let proto = CustomCallInitializer.prototype;
  CustomCallInitializer.propertyName =
  proto.propertyName = "initializerFunction";

  proto.info = null;

  /**
   * @param aPanel
   *        A XUL inspectorpanel element.  This is the panel the viewer is
   *        being loaded in.
   * @return
   *        The viewer object to be loaded in the panel.
   */
  proto.initializeViewer = function CCI_InitializeViewer(aPanel)
  {
    let functionName = this.info.metadata[this.propertyName];

    let viewer;
    if (functionName in this.info.scriptModule.exports) {
      viewer = this.info.scriptModule.exports[functionName](aPanel);
    }
    else if (functionName in aPanel.viewerContext) {
      viewer = aPanel.viewerContext[functionName](aPanel);
    }

    return viewer;
  };
}
