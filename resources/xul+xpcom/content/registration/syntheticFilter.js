/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 * Filter provided to viewers that have a "syntheticFilter" metadata property.
 *
 * For viewers that only support inspecting one or more types of synthetics,
 * the viewer author can use the "syntheticFilter" property in the viewer's
 * registration metadata to list the SIDs of the supported synthetics.  The
 * Inspector framework will instantiate a `SyntheticFilter` and assign it to
 * the viewer so that the author need not provide a filter implementation
 *
 * @see registration/theViewerRegistry
 * @see registration/viewerInfo
 *      For an overview of the types of bootstrap objects (i.e., "WTF is a
 *      filter?")
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

exports.SyntheticFilter = SyntheticFilter;

//////////////////////////////////////////////////////////////////////////////

function SyntheticFilter(aInfo)
{
  // XXX Warn if `wantsSynthetic` isn't true?
  this.info = aInfo;
}

{
  let proto = SyntheticFilter.prototype;
  SyntheticFilter.propertyName =
  proto.propertyName = "syntheticFilter";

  proto.info = null;

  /**
   * @param a_x_Object
   *        NB: `a_x_Object` is untrusted.
   * @param aSynthBloc [optional]
   *        The `SynthBloc` instance for this inspection context.  Will only
   *        be provided if the viewer metadata indicating it accepts
   *        synthetics.
   * @return
   *        A boolean indicating whether the viewer (associated with
   *        `this.info` supports inspecting the given object
   */
  proto.test = function IF_Test(a_x_Object, aSynthBloc)
  {
    let sids = this.info.metadata[this.propertyName];
    if (aSynthBloc) {
      for (let i = 0, n = sids.length; i < n; ++i) {
        if (aSynthBloc.isSynthetic(a_x_Object, sids[i])) return true;
      }
    }

    return false;
  };
}
