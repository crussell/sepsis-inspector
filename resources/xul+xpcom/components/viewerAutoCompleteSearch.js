/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @fileoverview
 *
 * TODO
 * - Allow matching on a full text search.  Viewers can have a name
 *   (required), a short description, and a long description.  Typing into the
 *   viewer bar should match from all three (weighted appropriately, of
 *   course, so that a match on the name ranks that viewer higher than a match
 *   on the short description, which itself ranks higher than a match on the
 *   long description.  The name and short description (or at least part of
 *   it) will always be shown for viewers that match.  The long description
 *   will only be shown if it contains a match.
 *
 *   I'm thinking that the match should show up to two lines of the short
 *   description, ellipsing as appropriate in order to make sure the matched
 *   position is visible à la Google.  If the long description contains a
 *   match, replace the second line of the short description with a line of
 *   the long description, also ellipsed as necessary.
 *
 * - Right now, the autocomplete favors matches where the search string's
 *   index is nearer to the beginning of the viewer name.  We should also
 *   weight matches that occur just after a word boundary as higher than those
 *   that match within the middle of a word.
 */

//////////////////////////////////////////////////////////////////////////////
//// Global symbols

const Cu = Components.utils;
const Ci = Components.interfaces;
const Cc = Components.classes;

//////////////////////////////////////////////////////////////////////////////
//// Imported symbols

Cu.import("chrome://sepsis-inspector/content/require.js");

Cu.import("resource://gre/modules/XPCOMUtils.jsm");

var { SortSack } = require("utils/sortSack");
var { TheViewerRegistry } = require("registration/theViewerRegistry");

//////////////////////////////////////////////////////////////////////////////

function ViewerAutoCompleteSearch() {
}

{
  let proto = ViewerAutoCompleteSearch.prototype;
  proto.NumericalSort = ViewerAutoCompleteSearch.NumericalSort =
    function (a, b)
  {
    if (a < b) {
      return -1;
    }
    else if (a > b) {
      return 1;
    }

    return 0;
  };

  proto.classID = Components.ID("{de60c0cd-fe02-4c02-93ec-724c8e14a402}");

  proto.QueryInterface = XPCOMUtils.generateQI([Ci.nsIAutoCompleteSearch]);

  proto.startSearch =
    function VACS_StartSearch(aString, aParam, aPreviousResult, aListener)
  {
    let controller = aListener.QueryInterface(Ci.nsIAutoCompleteController);
    let viewerBar =
      controller.input.ownerDocument.getBindingParent(controller.input);

    let result = Cc["@mozilla.org/autocomplete/simple-result;1"].
      createInstance(Ci.nsIAutoCompleteSimpleResult);
    result.setSearchString(aString);

    let query = aString.toLocaleLowerCase();

    // Two cases we need to handle:
    //  - The input is empty.  We want to display all viewers available to
    //    load in the panel, similar to the viewer list from the View menu.
    //  - The input is non-empty.  We want to match based on what's in the
    //    input.
    let uids = viewerBar.panel.getRelation().getUIDs();
    for (let i = 0, n = uids.length; i < n; ++i) {
      // assert(!!TheViewerRegistry.adapterRegistry);
      let name = TheViewerRegistry.getViewerName(uids[i]);
      // Empty or non-empty?
      if (!aString) {
        // Empty.
        result.appendMatch(name, "");
      }
      else {
        // Non-empty.  We want to allow matches from anywhere within the
        // viewer name.  (E.g., hitting Ctrl+L, typing "properties", and
        // hitting arrow down and Enter should load the "Object Properties and
        // Methods" viewer, if it's compatible and there are no other viewers
        // installed that are better matches.)
        let idx = name.toLocaleLowerCase().indexOf(query);
        if (idx >= 0) {
          if (!sacks) {
            var sacks = [];
          }

          sacks.push(new SortSack(idx, name))
        }
      }
    }

    if (sacks) {
      sacks = sacks.sort(ViewerAutoCompleteSearch.NumericalSort);

      for (let i = 0, n = sacks.length; i < n; ++i) {
        // `sacks[i].payload` is the `name` from above.
        result.appendMatch(sacks[i].payload, "");
      }
    }

    result.setSearchResult(Ci.nsIAutoCompleteResult.RESULT_SUCCESS);
    aListener.onSearchResult(this, result);
  };

  proto.stopSearch = function VACS_StopSearch()
  {
  };
};

this.NSGetFactory = XPCOMUtils.generateNSGetFactory(
  [ ViewerAutoCompleteSearch ]
);
