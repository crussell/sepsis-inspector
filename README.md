About sepsis-inspector
----------------------

The Inspector is an extension that aims to be compatible with Mozilla toolkit
applications (e.g., Firefox) for inspecting the Web and the host applications themselves.

See the [Owner's Manual](OWNERSMANUAL.md) for info about using the Inspector.

Canonical repo
--------------

The canonical repo for the sepsis-inspector project is
<https://hg.mozilla.org/users/sevenspade_gmail.com/sepsis-inspector>.  The
mirror on GitLab is just a mirror.  (However, I wouldn't reject a contribution
solely because it came in the form of a GitLab merge request. --crussell)

Bugs
----

The [issue tracker for sepsis-inspector on GitLab]
(https://gitlab.com/crussell/sepsis-inspector/issues)
is the canonical bug tracker.

There are details on the GitLab-hosted wiki about [bug triage]
(https://gitlab.com/crussell/sepsis-inspector/wikis/bug-triage).

sepsis-console
--------------

The "JavaScript Console" viewer is developed separately.  See the canonical
[sepsis-console repo]
(https://hg.mozilla.org/users/sevenspade_gmail.com/sepsis-console)
or the [sepsis-console mirror on GitLab]
(https://gitlab.com/crussell/sepsis-console).

Other information
-----------------

sepsis-inspector is on the Mozilla Wiki at <https://wiki.mozilla.org/Sepsis>.

I've tried to make sure the Inspector codebase is extremely well-commented and
approachable.  If you have any questions, suggestions, or complaints, I'm
Colby, and you should feel free to email me at colbyrussell.com.  (Note: that
isn't code for "Use the address me@colbyrussell.com".  Mail to that address
will go nowhere.)  AMA.

[Patches wanted](http://www.colbyrussell.com/2013/08/06/patches-wanted.html).
