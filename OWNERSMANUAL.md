    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

Sepsis Inspector Owner's Manual
===============================

Overview
--------

Sepsis Inspector is an interactive inspector utility to assist in debugging
Mozilla toolkit applications and web pages.

A note: The Inspector extension supports many different applications.  Below,
when the phrase "host app" occurs, it refers to the application that is
hosting the Inspector extension.  For example, if you installed the Inspector
as a Firefox extension, the host app would be Firefox.  The Inspector has
explicit support for Firefox, Thunderbird, and SeaMonkey, which means it
includes integration points to best work with those applications (like the
keyboard shortcuts that open the Inspector).  The Inspector implicitly
supports any other toolkit application, though.  For toolkit apps that aren't
explicitly supported, the Inspector extension or the host app itself may need
to be modified to do things like launch the Inspector or use the host app's
location bar as an input to the Inspector.

Using the Inspector
-------------------

The Inspector has multiple viewers for inspecting different aspects of an
object or the host application.  The Inspector is most commonly used for
inspecting documents.  To begin inspecting documents, you'll first need to
open an Inspector window.

### Opening the Inspector

By default the Inspector can be opened by pressing Ctrl+Shift+I or Cmd+Shift+I
from the host application's main window.

When the Inspector is installed, it will override any other commands in the
host app's main window that use the same keyboard shortcut so that the
Inspector will be launched.  To prevent the Inspector from trying to use its
default shortcut, you can toggle the .integration.overrideKeys setting in
about:config.  (See "Inspector Settings" below.)

By default, the Inspector will open to a two-pane layout.  The Inspector's
primary target (that is, the object being inspected) will be the page
currently open in the foreground tab of the host app (for browsers) or the XUL
document comprising the host app's primary window (for non-browsers).

For host apps that the Inspector doesn't explicitly support, the Inspector can
be opened using the |sepsis.openInspector| method in
chrome://sepsis-inspector/content/integration/hooks.js.

What the Inspector uses as its initial primary target is controlled by a
preference: .integration.<host-app-name>.context.  The default viewer can be
controlled by the .defaultViewer pref.  (See below.)

### Choosing What to Inspect

The target that the Inspector attaches to affects what shows up for inspection
in the root panel.

#### Attaching to Documents

You can attach to any content document for inspection using the "Attach to
Content Document" menu in the File menu.  Browser-like host apps will load web
pages with content-level privileges.  Use this menu to inspect the pages in
the host app's tabs.

Most of the stuff in the host app's own windows and the windows created by
extensions will exist in a context with chrome-level privileges.  Inspect the
host app's chrome using the "Attach to Chrome Document" menu from the File
menu.  Use this to inspect your extension's XUL by selecting the chrome
document containing it.

The tooltips in both "Attach..." menus will show the full page title (if any)
and the document URI.

#### "Inspect Appplication" mode

Some viewers support "Inspect Application" mode.  Enter "Inspect Application"
mode from the File menu.  Use this to inspect things that are application-wide
and not tied to any one specific document.  This includes background services,
et cetera.  For example, the Observer Service viewer allows you to inspect
every notification that passes through the host application's observer
service.

Just like viewers that have strict filters that determine what kinds of
objects they can inspect, not all viewers support "Inspect Application" mode.

When in "Inspect Application" mode, the root panel's viewer doesn't have a
subject, it's just said to be inspecting the application, but viewers that
support "Inspect Application" mode can still export targets for viewers in
descendant panels to inspect.

#### Auto-Attach

When Auto-Attach is on, navigating away from the inspected web page to another
one will cause the Inspector to automatically begin inspecting the new page.
Toggle Auto-Attach from the File menu.

Other Features
--------------

### Sepsis JavaScript Console

By default, the Inspector does not come with an interactive JavaScript
console.  You can get a console in the Inspector, though, if you install the
[Sepsis Console](https://addons.mozilla.org/en-US/addon/sepsis-console/)
extension from AMO.

### Adding and Removing Panels

If you want to open a new panel to inspect the end panel's target, you can
use the shortcut Ctrl+T or Cmd+T.  An "Inspect Target in New Panel" menu item
to do the same thing is located in the Edit menu.  Note that this only applies
to the end panel.  When panels other than the end panel are focused, this
command is disabled.

To remove any panel, use the Ctrl+W or Cmd+W shortcut.  You can also middle
click the splitter separating any panel from its parent panel to close that
panel (i.e., the panel that is the "child").  Whenever a panel is closed all
its descendant panels are also closed.

The root panel cannot be closed without closing the Inspector window.

### Find By Click

You can enter "Find By Click" mode in viewers that support it by pressing
Ctrl+K.  In the DOM Tree viewer, for example, clicking any element on the page
when using Find By Click will target that element in the DOM Tree viewer.

### Switching Between Panels

You can quickly switch between panels using the Alt key and the number of the
panel to switch to.  For example, to focus the first panel, press Alt+1.
Switching to the second, third, et cetera panels works similarly.  Focus the
last active panel closest to the end at any time by pressing Alt+0.

Understanding the Inspector (or, Panels and Viewers)
----------------------------------------------------

The Inspector isn't just one tool for inspecting documents: it's a whole set
of tools for inspecting all kinds of objects.  A _viewer_ can be installed to
handle specific kinds of objects, even though it might not know how to inspect
others.  Every viewer has a _filter_ that determines whether that viewer will
be able to inspect any given object.

For example, the DOM Tree viewer is the default viewer (i.e., the Inspector,
when first opened, will automatically try to load it in the first panel).  The
DOM Tree viewer's filter will not accept anything that isn't a document.

A viewer in one _panel_ can be swapped out for a viewer of a different type,
to be loaded in the same panel.  This can happen automatically by the
Inspector in some cases, or you can manually select a panel's viewer from a
list of installed viewers that can work with the panel's parent's current
target.

By default, the Inspector will open with a second panel.  It's linked to the
root panel, so that when the viewer in the root panel updates its target, the
child panel will also update to reflect that target.  Typically, the viewer
target is updated simply by selecting one of the objects shown in the viewer.

For example, if you're inspecting a document with the DOM Tree viewer in the
root panel, and the CSS Rules and Properties viewer is loaded in the second
panel, and then you select a different element in the DOM Tree viewer, the CSS
Rules and Properties viewer will update to inspect that element in the second
panel.

Other viewers work similarly; for any two panels, when the viewer's target in
the parent panel changes, the child panel's subject will be updated.

### Viewer Switching

#### Automatic Viewer Switching

When the parent panel's target changes, if the object doesn't pass the filter
of the viewer currently loaded in the child panel (and there's no adapter that
can translate the target into anything that will pass the filter, see
"Adapters" below), then the Inspector will automatically select a different
viewer that *is* capable of inspecting the parent's target and load it in the
child panel.

For example, if the CSS Rules and Properties viewer is loaded in the second
panel while inspecting some element from the DOM Tree viewer in the first
panel, then you select in the DOM Tree viewer a different, non-element node
(like the document node), the second panel will switch away from the CSS Rules
and Properties viewer (because the CSS Rules and Properties viewer's filter
doesn't accept document nodes, since CSS doesn't apply to document nodes).

Viewers may not always have an inspectable target.  When that happens, the
child panel will become inactive and any viewer loaded there will disappear.
For example, if the DOM Tree viewer is loaded in the root panel with the DOM
Node viewer in the second panel, and the node in the DOM Tree viewer is
deselected, then the second panel will become inactive.

#### Manual viewer switching

The viewer to be used to inspect an object can be chosen from the View >
Viewer menu.  Note that this menu only allows switching between the viewers
for the most recently focused panel.  For example, if the Inspector window
contains two panels, and the second panel is focused, there is no way at this
time to switch between the viewers of the first panel without first focusing
it.  This sucks and will change in the future.

The viewer list only reflects the viewers that are valid for a panel's current
subject.  For example, when a DOM document node is being inspected, the CSS
Rules and Properties viewer will not be available as a viewer choice, because
the CSS Style viewer's filter doesn't accept DOM documents, because documents
cannot be directly styled by CSS.

### Adapters

The object a child panel is inspecting is not *always* necessarily the same as
the parent's target.  The Inspector has a notion of _adapters_.  Adapters can
sometimes translate a panel's parent's target to a related object that other
viewers may accept when they wouldn't ordinarily be able to handle the
parent's actual target.

For example, if you attach to a document and load the Events viewer in the
root panel, it will list the DOM events that occur as they reach the document
node.  If an event with a target node is selected in the Events viewer in the
parent panel, using the Event Target adapter allows you to inspect the target
node in the DOM Node viewer.  Ordinarily, this would not be possible, because
the DOM Node viewer only knows how to inspect nodes, and the objects in the
Events viewer are indeed events.  Luckily, the Events viewer registers the
Event Target adapter, which translates an event to its target node.

Right now, you can implicitly use adapters when the Inspector automatically
finds the relevant ones and does the translation while building the Viewer
list in the View menu, but there is no other UI for controlling adapters.
This will change in the future, where the Inspector will indicate when an
adapter is or would be in use, provide a means to switch adapters, allow
creating custom adapters from the Inspector UI, and enabling or disabling any
of the adapters, including built-in ones.

Note that adapters are preffed off by default for now.

Built-In Viewers
----------------
               
#### CSS Rules and Properties

Inspect the style that applies to an element or the rules that a style sheet
provides.

#### DOM Tree

Inspect a document's structure.

#### DOM Node

Inspect any DOM node.  Show an element's attributes, a document's details, or
the text content of various other nodes.

#### Events

Watch the events that reach an event target (e.g., an element, a document, a
window).

#### Event Listeners

Inspect the listeners attached to an event target.

#### HTTP Transaction

Use in conjunction with the Network Activity viewer to inspect the details of
an HTTP Transaction, including request and response headers.

#### JavaScript Object

Inspect an object's properties and methods.

#### Category Manager

Inspect the categories exposed by the category manager background service.
Use this from "Inspect Application" mode, or when inspecting the
nsICategoryManager service.

#### Network Activity

Watch the network activity across the host app.  Use this from "Inspect
Application" mode, or when inspecting the nsIHttpActivityDistributor service.
Only supports HTTP transactions and doesn't support per-window activity at the
moment.

#### Observer Service

Watch the notifications that pass through the host app's background observer
service, and inspect the objects associated with the notification topic.  Use
this from "Inspect Application" mode, or when inspecting the
nsIObserverService.  Definitely causes observe effects, so use carefully.

Installing Other Viewers
------------------------

The Inspector comes with a default set of viewers that are expected to be
helpful for debugging, but additional viewers can be installed.

It will eventually be possible to create viewers from within the Inspector UI
itself, but for now, the files must be written by hand.  To create a new
viewer, you only need an info file and a viewer document file, written in XUL or
HTML (plus whatever scripts you include in your viewer document to implement
its UI and behavior).

The interface that viewers must satisfy is minimal, and there's not a huge
framework that you're required to work within and that dictates how you've got
to write your code.  There's a sample viewer, thoroughly documented, at:

    http://hg.mozilla.org/users/sevenspade_gmail.com/resources/content/viewers/sample/info.js

If you want to create an extension to add a new viewer to the Inspector, all
you have to do is make sure the viewer files are packaged in your extension
and register it in the "sepsis-viewer" category.  (See viewers/sample/
info.js.)  The viewer will be automatically picked up by the inspector and
available in the viewer list to inspect elements, windows, and other objects
that the viewer's filter accepts.

There is already at least one extension to add to add an external viewer to
the Inspector: the Sepsis Console integrates with the Inspector and exposes an
interactive JavaScript console as a viewer, with autocomplete.  See the
"Sepsis JavaScript Console" section above.

Inspector Settings
------------------

There is no settings UI for the Inspector yet.  There are a number of
preferences in about:config that control the behavior of the Inspector and its
viewers.

The Inspector's preferences are in the "sepsis.inspector" pref branch.

.adapters.enabled
  Controls whether or not adapters (see above) are enabled or not.  Defaults
  to false.

.defaultViewer

  The UID of the viewer to try to load when the Inspector is first opened.
  The viewer will only be successfully loaded if the root panel's subject
  passes the viewer's filter.

  If you want to change the default viewer, you can set this pref to the UID
  of the viewer you want to use.  There's no UI for getting a particular
  viewer's UID, but viewers are registered in the "sepsis-viewer" category in
  the host app's category manager.  You can use the Inspector itself to get a
  list of all registered UIDs by entering "Inspect Application" mode (from the
  File menu) and looking at the "sepsis-viewer" entries in the Category
  Manager viewer.

  The .useDefaultOnAttach preference also works in conjunction with the
  .defaultViewer pref to attempt to use the default viewer when attaching to
  new Inspector targets.  See below.

.integration.overrideKeys

  Whether the Ctrl+Shift+I/Cmd+Shift+I should cause the Inspector to be
  launched in the host app.  If set to false, the Inspector will not try to
  override the keyboard combo to launch itself.

.integration.firefox.context
.integration.seamonkey.context
.integration.thunderbird.context

  The default behavior when the Inspector is launched in Firefox, SeaMonkey,
  and Thunderbird, respectively.  Valid values are described in
  chrome://sepsis-inspector/content/integration/generic.js.

.useDefaultOnAttach

  Whether attaching to a new Inspector target (via the File menu items) should
  attempt to load the default viewer.  See the .defaultViewer pref above.

.viewers.domTree.showAnonymousNodes

  Whether the DOM Tree viewer should show only nodes explicitly present in the
  DOM or whether to also show nodes from the XBL shadow tree and native
  anonymous content.

.viewers.domTree.showSubDocuments

  Whether framed documents (e.g., from an iframe or a XUL browser element)
  should be represented in the tree view as a child of that element.

Project Motivation
------------------

The Inspector was started to replace the old DOM Inspector codebase.  It's
part of the Sepsis project.  The Sepsis project's priorities are:

- Not tied to any one app (e.g., Firefox-only).  Sepsis tools have broad
  support for toolkit applications in mind from the start.

- Highly configurable.  Sepsis utilities should handle most of the needs of
  almost all of its target users.  These tools are going to be used in
  contexts where the user is already working on a problem, and likely to
  frustrate easily (if not already so).  Users shouldn't be averse to ever
  using Sepsis utilities because, e.g., they don't like the approach to
  auto-complete, and when they do use them, they shouldn't have to do so
  begrudgingly.

- "Worse is better".  The effect of waiting on a fix for a platform bug can
  delay progress indefinitely.  Contrast the success and general utility of
  Firebug, which has essentially followed the "worse is better" approach, to
  DOM Inspector, which has generally held off on development until a "proper"
  fix has been made.  When Sepsis runs into one of these kinds of
  dependencies, it takes the "worse is better" path to make sure the approach
  most useful to users is available to them right now while waiting for things
  to be ironed out elsewhere.

Contributing
------------

See the [README](README.md) for info about contributing.
