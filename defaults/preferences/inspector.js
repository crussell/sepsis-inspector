/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

pref("sepsis.inspector.viewers.domTree.showAnonymousNodes", true);
pref("sepsis.inspector.viewers.domTree.showSubDocuments", true);
pref("sepsis.inspector.defaultViewer", "org.mozilla.sepsis.viewers.domTree");
pref("sepsis.inspector.integration.firefox.context", 4);
pref("sepsis.inspector.integration.seamonkey.context", 4);
pref("sepsis.inspector.integration.thunderbird.context", 5);
pref("sepsis.inspector.adapters.enabled", true);
pref("sepsis.inspector.useDefaultOnAttach", true);
pref("sepsis.inspector.tests.commandEnabled", false);
