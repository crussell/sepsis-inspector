XPI_NAME=sepsis-inspector.xpi

all:
	rm -f $(XPI_NAME)
	zip -r $(XPI_NAME) ./* --exclude Makefile $(XPI_NAME)
